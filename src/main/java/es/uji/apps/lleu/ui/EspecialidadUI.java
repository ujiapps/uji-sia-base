package es.uji.apps.lleu.ui;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.Especialidad;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class EspecialidadUI
{
    private Long pieId;
    private String nombre;
    private String superarStr;
    private String creditosOb;
    private String creditosOp;

    public static List<EspecialidadUI> toUI (List<Especialidad> le, String idioma) {
        return le.stream()
                .map(e -> new EspecialidadUI(e, idioma)).sorted(Comparator.comparing(EspecialidadUI::getNombre))
                .collect(Collectors.toList());
    }

    public static EspecialidadUI toUI (Especialidad e, String idioma) {
        return new EspecialidadUI(e, idioma);
    }

    private EspecialidadUI (Especialidad e, String idioma) {
        this.pieId = e.getId();
        switch (idioma.toLowerCase()) {
            case "es":
                this.nombre = e.getNombreES();
                break;
            case "en":
                this.nombre = e.getNombreEN();
                break;
            default:
                this.nombre = e.getNombreCA();
        }

        this.superarStr = LocalizedStrings.getSuperarStr(e.getCreditosOB(), e.getCreditosOP());

        this.creditosOb = new CreditoUI(e.getCreditosOB()).toString();
        this.creditosOp = new CreditoUI(e.getCreditosOP()).toString();
    }

    public Long getPieId()
    {
        return pieId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getSuperarStr()
    {
        return superarStr;
    }

    public String getCreditosOb()
    {
        return creditosOb;
    }

    public String getCreditosOp()
    {
        return creditosOp;
    }
}
