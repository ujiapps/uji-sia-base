package es.uji.apps.lleu.ui;

import java.util.ArrayList;
import java.util.List;

public class ListaAsignaturasMasterUI extends ArrayList<AsignaturaMasterUI>
{
    private boolean mostrarCurso;
    private ArrayList<AsignaturaMasterUI> lista;

    public ListaAsignaturasMasterUI (List<AsignaturaMasterUI> lista, boolean mostarCurso) {
        super(lista);
        this.mostrarCurso = mostarCurso;
    }
}
