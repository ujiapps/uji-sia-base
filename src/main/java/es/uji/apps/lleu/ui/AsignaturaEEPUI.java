package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaEEP;
import es.uji.apps.lleu.model.AsignaturaEEPExtra;

public class AsignaturaEEPUI
{

    private Integer cursoAca;
    private String asignaturaId;
    private Long estudioId;
    private String nombre;

    /**
     * Devuelve una instancia de AsignaturaEEPUI con información para la asignatura traducida según el idiom indicado
     * @param a la asignatura
     * @param idioma el idioma que queremos "ca", "es", "en"
     * @return
     */
    private AsignaturaEEPUI(AsignaturaEEP a, String idioma)
    {

        this.asignaturaId = a.getAsignaturaId();

        switch (idioma)
        {
            case "es":
                this.nombre= a.getNombreES();
                break;
            case "en":
                this.nombre = a.getNombreEN();
                break;
            case "ca":
            default:
                this.nombre=a.getNombreCA();
                break;
        }
    }
    private AsignaturaEEPUI(AsignaturaEEPExtra a, String idioma)
    {

        this.asignaturaId = a.getAsignaturaId();

        switch (idioma)
        {
            case "es":
                this.nombre= a.getNombreES();
                break;
            case "en":
                this.nombre = a.getNombreEN();
                break;
            case "ca":
            default:
                this.nombre=a.getNombreCA();
                break;
        }
    }

    public static List<AsignaturaEEPUI> toUI (List<AsignaturaEEP> asignaturas, String idioma) {
        return asignaturas.stream().map((e) -> new AsignaturaEEPUI(e, idioma)).collect(Collectors.toList());
    }

    public static List<AsignaturaEEPUI> toExtraUI (List<AsignaturaEEPExtra> asignaturas, String idioma) {
        return asignaturas.stream().map((e) -> new AsignaturaEEPUI(e, idioma)).collect(Collectors.toList());
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
}
