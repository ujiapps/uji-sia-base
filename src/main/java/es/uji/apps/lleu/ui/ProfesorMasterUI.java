package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.ProfesorMaster;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class ProfesorMasterUI
{
    private Long perId;
    private String nombre;
    private String categoria;
    private String nombreUbicacion;
    private String otraUniversidad;
    private String empresa;
    private String doctor;
    private String cargo;
    private String url;
    private String curriculum;
    private String email;

    public ProfesorMasterUI(ProfesorMaster prof, String idioma)
    {
        this.perId = prof.getPerId();
        this.nombre = prof.getNombre();
        switch (idioma)
        {
            case "es":
                this.categoria = prof.getCategoriaES();
                break;
            case "en":
                this.categoria = prof.getCategoriaEN();
                break;
            default:
                this.categoria = prof.getCategoriaCA();
        }
        this.otraUniversidad = prof.getOtraUniversidad();
        this.empresa = prof.getEmpresa();
        this.doctor = LocalizedStrings.getDoctor(prof.getDoctor());
        this.cargo = prof.getCargo();
        this.url = prof.getUrl();
        this.curriculum = prof.getCurruculum();
        this.email = prof.getEmail();
    }

    public static List<ProfesorMasterUI> toUI (List<ProfesorMaster> profesores, String idioma)
    {
        return profesores.stream()
                .map((e) -> new ProfesorMasterUI(e, idioma))
                .collect(Collectors.toList());
    }

    public CajaInfoUI getCajaInfoUI()
    {
        return new CajaInfoUI(this.nombre, this.categoria);
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCategoria()
    {
        return categoria;
    }

    public void setCategoria(String categoria)
    {
        this.categoria = categoria;
    }

    public String getNombreUbicacion()
    {
        return nombreUbicacion;
    }

    public void setNombreUbicacion(String nombreUbicacion)
    {
        this.nombreUbicacion = nombreUbicacion;
    }

    public String getOtraUniversidad()
    {
        return otraUniversidad;
    }

    public void setOtraUniversidad(String otraUniversidad)
    {
        this.otraUniversidad = otraUniversidad;
    }

    public String getEmpresa()
    {
        return empresa;
    }

    public void setEmpresa(String empresa)
    {
        this.empresa = empresa;
    }

    public String getDoctor()
    {
        return doctor;
    }

    public void setDoctor(String doctor)
    {
        this.doctor = doctor;
    }

    public String getCargo()
    {
        return cargo;
    }

    public void setCargo(String cargo)
    {
        this.cargo = cargo;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getCurriculum()
    {
        return curriculum;
    }

    public void setCurriculum(String curriculum)
    {
        this.curriculum = curriculum;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }
}
