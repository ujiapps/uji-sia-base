package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.TreeMap;

import es.uji.apps.lleu.model.VacantesGrupoGrado;

public class VacantesGrupoGradoUI
{

    /**
     * Vacantes por grupo. En el TreeMap la clave es el grupo y el valor es el número de vacantes. Si vacantes es
     * -1 es que el grupo está cerrado.
     */
    private TreeMap<String, Integer> vacantesGrupos;
    private TreeMap<String, Integer> vacantesNuevosGrupos;

    private VacantesGrupoGradoUI(List<VacantesGrupoGrado> lvg)
    {
        this.vacantesGrupos = new TreeMap<>();
        this.vacantesNuevosGrupos = new TreeMap<>();

        lvg.forEach((vg) ->
        {

            try
            {
                Integer parsedVacantes = Integer.parseInt(vg.getVacantes());
                this.vacantesGrupos.put(vg.getGrupo(), parsedVacantes);

            }
            catch (NumberFormatException e)
            {
                this.vacantesGrupos.put(vg.getGrupo(), -1);
            }
            try
            {
                Integer parsedVacantes = Integer.parseInt(vg.getVacantesNuevos());
                this.vacantesNuevosGrupos.put(vg.getGrupo(), parsedVacantes);

            }
            catch (NumberFormatException e)
            {
                this.vacantesNuevosGrupos.put(vg.getGrupo(), -1);
            }
        });


    }

    public static VacantesGrupoGradoUI toUI(List<VacantesGrupoGrado> lvg)
    {
        return new VacantesGrupoGradoUI(lvg);
    }

    public TreeMap<String, Integer> getVacantesGrupos()
    {
        return vacantesGrupos;
    }

    public TreeMap<String, Integer> getVacantesNuevosGrupos()
    {
        return vacantesNuevosGrupos;
    }
}
