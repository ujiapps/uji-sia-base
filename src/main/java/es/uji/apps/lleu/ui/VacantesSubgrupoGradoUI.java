package es.uji.apps.lleu.ui;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import es.uji.apps.lleu.model.VacantesSubgrupoGrado;

public class VacantesSubgrupoGradoUI
{
    /**
     * Vacantes por subgrupo. La clave es el grupo y el valor es un TreeMap donde String es el subgrupo y el valor
     * es el número de vacantes disponibles.
     */
    private HashMap<String, TreeMap<String, Integer>> vacantesSubgrupos;
    private HashMap<String, TreeMap<String, Integer>> vacantesNuevosSubgrupos;

    /**
     * Lo utilizamos para ordenar los subgrupos de la siguiente manera:
     *
     *    TE
     *    PR
     *    LA
     *    SE
     *    TU
     *    AV
     */
    private static class SubgrupoComparator implements Comparator<String>
    {

        private HashMap<String, Integer> orden;

        public SubgrupoComparator()
        {
            this.orden = new HashMap<>(6);
            this.orden.put("AV", 6);
            this.orden.put("TU", 5);
            this.orden.put("SE", 4);
            this.orden.put("LA", 3);
            this.orden.put("PR", 2);
            this.orden.put("TE", 1);
        }

        @Override
        public int compare(String o1, String o2)
        {
            int ret = 0;

            String tipo1 = o1.substring(0, 2);
            String tipo2 = o2.substring(0, 2);

            if (!this.orden.containsKey(tipo1) || !this.orden.containsKey(tipo2)) {
                return 0;
            }

            if (this.orden.get(tipo1) == this.orden.get(tipo2)) {
                Integer subgrupo1 = Integer.parseInt(o1.substring(2));
                Integer subgrupo2 = Integer.parseInt(o2.substring(2));
                if (subgrupo1 > subgrupo2) {
                    ret = 1;
                } else if (subgrupo1 < subgrupo2) {
                    ret = -1;
                }
            } else if (this.orden.get(tipo1) < this.orden.get(tipo2)) {
                ret = -1;
            } else {
                ret = 1;
            }

            return ret;
        }
    }

    public static VacantesSubgrupoGradoUI toUI (List<VacantesSubgrupoGrado> lvsg) {
        return new VacantesSubgrupoGradoUI(lvsg);
    }


    private VacantesSubgrupoGradoUI(List<VacantesSubgrupoGrado> subGrupos) {


        HashMap<String, TreeMap<String, Integer>> vacantesSubgrupos = new HashMap<>(10);
        HashMap<String, TreeMap<String, Integer>> vacantesNuevosSubgrupos = new HashMap<>(10);

        for (VacantesSubgrupoGrado asub : subGrupos)
        {

            if (!vacantesSubgrupos.containsKey(asub.getGrupo()))
            {
                vacantesSubgrupos.put(asub.getGrupo(), new TreeMap<>(new SubgrupoComparator()));
                vacantesNuevosSubgrupos.put(asub.getGrupo(), new TreeMap<>(new SubgrupoComparator()));
            }

            TreeMap<String, Integer> mapGrupo = vacantesSubgrupos.get(asub.getGrupo());
            TreeMap<String, Integer> mapGrupoNuevo = vacantesNuevosSubgrupos.get(asub.getGrupo());
            try
            {
                Integer parsedVacantes = Integer.parseInt(asub.getVacantes());
                mapGrupo.put(asub.getTipo() + asub.getSubGrupo(), parsedVacantes);
            }
            catch (NumberFormatException e)
            {
                mapGrupo.put(asub.getTipo() + asub.getSubGrupo(), -1);
            }
            try
            {
                Integer parsedVacantes = Integer.parseInt(asub.getVacantesNuevos());
                mapGrupoNuevo.put(asub.getTipo() + asub.getSubGrupo(), parsedVacantes);
            }
            catch (NumberFormatException e)
            {
                mapGrupoNuevo.put(asub.getTipo() + asub.getSubGrupo(), -1);
            }


            this.vacantesSubgrupos = vacantesSubgrupos;
            this.vacantesNuevosSubgrupos = vacantesNuevosSubgrupos;
        }
    }

    public HashMap<String, TreeMap<String, Integer>> getVacantesSubgrupos()
    {
        return vacantesSubgrupos;
    }

    public HashMap<String, TreeMap<String, Integer>> getVacantesNuevosSubgrupos()
    {
        return vacantesNuevosSubgrupos;
    }
}
