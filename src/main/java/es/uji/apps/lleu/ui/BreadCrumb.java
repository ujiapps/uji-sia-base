package es.uji.apps.lleu.ui;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa la entrada de un breadcrumb
 */
public class BreadCrumb
{
    private String url;
    private String text;
    private Boolean textIsLabel;

    public static List<BreadCrumb> getBreadcrumbList (BreadCrumb ... crumbs) {
        List<BreadCrumb> ret = new ArrayList<>(crumbs.length);
        for (BreadCrumb c : crumbs) {
            ret.add(c);
        }
        return ret;
    }

    public BreadCrumb(String url, String text, Boolean textIsLabel)
    {
        this.url = url;
        this.text = text;
        this.textIsLabel = textIsLabel;
    }

    public String getUrl()
    {
        return url;
    }

    public String getText()
    {
        return text;
    }

    public Boolean getTextIsLabel()
    {
        return textIsLabel;
    }
}
