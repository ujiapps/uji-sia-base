package es.uji.apps.lleu.ui;

import java.util.List;

public class CajaInfoReqMatriculaEEP extends CajaInfoUI
{
    private List<String []> listaSubtitulo;
    public CajaInfoReqMatriculaEEP(String titulo, List<String []> subtitulo)
    {
        super(titulo, "");
        this.listaSubtitulo = subtitulo;
    }

    public List<String[]> getListaSubtitulo()
    {
        return listaSubtitulo;
    }
}
