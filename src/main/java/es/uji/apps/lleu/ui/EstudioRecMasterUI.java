package es.uji.apps.lleu.ui;

import es.uji.apps.lleu.model.EstudioRecMaster;

public class EstudioRecMasterUI
{
    private Long estudioId;
    private boolean experiencia;
    private boolean superior;
    private boolean propio;

    public static EstudioRecMasterUI toUI (EstudioRecMaster erm) {
        return new EstudioRecMasterUI(erm);
    }

    private EstudioRecMasterUI(EstudioRecMaster erm) {
        this.estudioId = erm.getEstudioId();
        this.experiencia = erm.getExperiencia().equals("S");
        this.superior = erm.getSuperior().equals("S");
        this.propio = erm.getPropio().equals("S");
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public boolean getExperiencia()
    {
        return experiencia;
    }

    public boolean getSuperior()
    {
        return superior;
    }

    public boolean getPropio()
    {
        return propio;
    }
}
