package es.uji.apps.lleu.ui;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.utils.LocalizedStrings;

public class LeyendaSubgrupoUI
{
    private String subgrupo;

    private static HashMap<String, Integer> subgrupoOrder;
    static {
        subgrupoOrder = new HashMap<>();
        subgrupoOrder.put("TE", 7);
        subgrupoOrder.put("PR", 6);
        subgrupoOrder.put("LA", 3);
        subgrupoOrder.put("TP", 4);
        subgrupoOrder.put("TU", 3);
        subgrupoOrder.put("SE", 2);
        subgrupoOrder.put("AV", 1);
        subgrupoOrder.put("ERA", 0);
    }

    private static Integer getSubgrupoOrder(String s) {
        if (subgrupoOrder.containsKey(s.toUpperCase())) {
            return subgrupoOrder.get(s);
        }
        return -1;
    }

    public static List<LeyendaSubgrupoUI> toUI(List<String> tiposSubgrupo) {
        if(tiposSubgrupo!=null) {
            return tiposSubgrupo
                    .parallelStream()
                    .sorted((a, b) -> getSubgrupoOrder(b) - getSubgrupoOrder(a))
                    .map(LeyendaSubgrupoUI::new)
                    .collect(Collectors.toList());
        }
        return null;
    }

    private LeyendaSubgrupoUI(String tipoSubgrupo) {
        this.subgrupo = LocalizedStrings.getSubgrupoKey(tipoSubgrupo);
    }

    public String getSubgrupo()
    {
        return subgrupo;
    }
}
