package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaComentarios;

public class AsignaturaComentariosUI
{

    private Long estudioId;
    private String asignaturaId;
    private Integer cursoAca;
    private Long cursoId;
    private Integer orden;
    private String comentario;

    public static List<AsignaturaComentariosUI> toUI(List<AsignaturaComentarios> l, String idioma) {
        return l.stream()
                .map(a -> new AsignaturaComentariosUI(a, idioma))
                .collect(Collectors.toList());
    }



    public AsignaturaComentariosUI(AsignaturaComentarios ac, String idioma) {
        this.cursoAca = ac.getCursoAca();
        this.asignaturaId = ac.getAsignaturaId();
        this.estudioId = ac.getEstudioId();
        this.cursoAca = ac.getCursoAca();
        this.orden = ac.getOrden();
        switch (idioma) {
            case "es":
                this.comentario = ac.getComentarioES();
                break;
            case "en":
                this.comentario = ac.getComentarioEN();
                break;
            default:
                this.comentario = ac.getComentarioCA();
                break;
        }
    }


    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }
}
