package es.uji.apps.lleu.ui;

import java.util.HashMap;

public class ResumenCreditosCabeceraUI extends HashMap<String, CreditoUI>
{

    public static ResumenCreditosCabeceraUI toUI (HashMap<String, Float> m) {
        return new ResumenCreditosCabeceraUI(m);
    }

    private  ResumenCreditosCabeceraUI(HashMap<String, Float> resumenCreditos) {
        for (String s : resumenCreditos.keySet())
        {
            this.put(s, new CreditoUI(resumenCreditos.get(s)));
        }
    }
}
