package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.Actividad;

public class ActividadUI
{
    private Integer cursoAca;
    private String asignaturaId;
    private String nombre;
    private String horasPresenciales;
    private String horasNoPresenciales;

    /**
     * El parámetro es una List<Actividad> o List<ActividadGuidGrado>
     *
     * @param l
     * @return
     */
    public static List<ActividadUI> toUI(List<Actividad> l, String idioma) {
        return l.stream()
                .map(a -> new ActividadUI(a, idioma))
                .collect(Collectors.toList());
    }

    public CajaInfoUI getCajaInfoUI() {
        CajaInfoUI ret = new CajaInfoUI(this.nombre, "");
        ret.addInfo(this.horasPresenciales, "horasPresenciales");
        ret.addInfo(this.horasNoPresenciales, "horasNoPresenciales");

        return ret;
    }

    public ActividadUI(Actividad agg, String idioma) {
        this.cursoAca = agg.getCursoAca();
        this.asignaturaId = agg.getAsignaturaId();
        switch (idioma) {
            case "es":
                this.nombre = agg.getNombreES();
                break;
            case "en":
                this.nombre = agg.getNombreEN();
                break;
            default:
                this.nombre = agg.getNombreCA();
                break;
        }
        this.horasPresenciales = new HorasPresencialesUI(agg.getHorasPresenciales()).toString();
        this.horasNoPresenciales =  new HorasPresencialesUI(agg.getHorasNoPresenciales()).toString();
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getHorasPresenciales()
    {
        return horasPresenciales;
    }

    public void setHorasPresenciales(String horasPresenciales)
    {
        this.horasPresenciales = horasPresenciales;
    }

    public String getHorasNoPresenciales()
    {
        return horasNoPresenciales;
    }

    public void setHorasNoPresenciales(String horasNoPresenciales)
    {
        this.horasNoPresenciales = horasNoPresenciales;
    }
}
