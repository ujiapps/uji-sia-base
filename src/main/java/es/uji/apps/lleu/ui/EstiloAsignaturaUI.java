package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.EstiloAsignatura;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class EstiloAsignaturaUI {

    private Long estudioId;
    private Integer cursoAca;

    private String asignaturaId;
    private String ambito;
    private String nombre;
    private String creditos;
    private Integer curso;
    private String semestre;
    private String caracter;
    private String numeroSemestre;
    private String tipo;

    public static Map<String, List<EstiloAsignaturaUI>> toUI (List<EstiloAsignatura> lea, String idioma) {
        return lea
                .stream()
                .map(a -> new EstiloAsignaturaUI(a, idioma))
                .collect(Collectors.groupingBy(EstiloAsignaturaUI::getAmbito));
    }

    public static List<String> getAmbitos(List<EstiloAsignatura> lea) {
        return lea
                .stream()
                .map(a -> a.getAmbito().toLowerCase())
                .sorted((a, b) -> a.compareTo(b))
                .distinct()
                .collect(Collectors.toList());
    }

    public EstiloAsignaturaUI (EstiloAsignatura ea, String idioma) {

        this.cursoAca = ea.getCursoAca();
        this.estudioId = ea.getEstudioId();

        this.asignaturaId = ea.getAsignaturaId();

        this.ambito = ea.getAmbito().toLowerCase();

        switch (idioma) {
            case "es":
                this.nombre = ea.getNombreES();
                break;
            case "en":
                this.nombre = ea.getNombreEN();
                break;
            default:
                this.nombre = ea.getNombreCA();
        }

        this.creditos = new CreditoUI(ea.getCreditos()).toString();
        this.tipo = ea.getTipo();
        this.curso = ea.getCurso();
        if(this.tipo.equals("A")){
            this.semestre = LocalizedStrings.getSemestreKey(this.tipo);
            this.numeroSemestre = "a";

        }else{
            this.semestre = LocalizedStrings.getSemestreKey(ea.getSemestre());
            this.numeroSemestre = ea.getSemestre().toString();
        }
        this.caracter = LocalizedStrings.getCaracterKey(ea.getCaracter());


    }

    public CajaAsignaturaUI getCajaAsignaturaUI() {
        CajaAsignaturaUI ret = new CajaAsignaturaUI(
                this.estudioId,
                this.cursoAca,
                this.nombre,
                this.asignaturaId,
                this.curso.toString(),
                this.semestre,
                this.caracter,
                null,null
        );

        ret.addInfo(this.creditos, "asignaturas.contenido.creditos");

        return ret;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getAmbito()
    {
        return ambito;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getCreditos()
    {
        return creditos;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getTipo()
    {
        return tipo;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public String getNumeroSemestre()
    {
        return numeroSemestre;
    }
}
