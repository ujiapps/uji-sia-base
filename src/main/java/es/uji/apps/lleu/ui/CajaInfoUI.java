package es.uji.apps.lleu.ui;

import java.util.ArrayList;
import java.util.List;

public class CajaInfoUI
{
    private String titulo;
    private String subtitulo;

    private List<String []> datos;

    public CajaInfoUI (String titulo, String subtitulo, List<String []> datos) {
        this.titulo = titulo;
        this.subtitulo = subtitulo;
        this.datos = datos;
    }

    public CajaInfoUI(String titulo, String subtitulo) {
        this.titulo = titulo;
        this.subtitulo = subtitulo;
        this.datos = new ArrayList<>();
    }

    public void addInfo (String dato, String leyenda) {
        this.datos.add(new String [] {dato, leyenda});
    }

    public String getTitulo()
    {
        return titulo;
    }

    public String getSubtitulo()
    {
        return subtitulo;
    }

    public List<String[]> getDatos()
    {
        return datos;
    }
}
