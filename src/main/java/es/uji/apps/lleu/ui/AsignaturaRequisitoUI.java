package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaRequisito;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class AsignaturaRequisitoUI
{
    private String asignaturaId;
    private Integer cursoAca;
    private String asignaturaIdIncompatible;
    private Long estudioId;
    private String semestre;
    private Integer curso;
    private String caracter;
    private String creditos;
    private String nombre;

    public static List<AsignaturaRequisitoUI> toUI (List<AsignaturaRequisito> l, String idioma) {
        return l.stream()
                .map((ar) -> new AsignaturaRequisitoUI(ar, idioma))
                .collect(Collectors.toList());
    }

    public CajaAsignaturaUI getCajaAsignaturaUI () {
        CajaAsignaturaUI ret = new CajaAsignaturaUI(
                this.estudioId,
                this.cursoAca,
                this.nombre,
                this.asignaturaIdIncompatible,
                this.curso.toString(),
                this.semestre,
                this.caracter,
                null,null
        );
        ret.addInfo(this.creditos, "creditos");

        return ret;
    }

    private AsignaturaRequisitoUI(AsignaturaRequisito r, String idioma) {
        this.asignaturaId = r.getAsignaturaId();
        this.cursoAca = r.getCursoAca();
        this.asignaturaIdIncompatible = r.getAsignaturaIdIncompatible();
        this.estudioId = r.getEstudioId();
        this.semestre = LocalizedStrings.getSemestreKey(r.getSemestre());
        this.curso = r.getCurso();
        this.caracter = LocalizedStrings.getCaracterKey(r.getCaracter());
        this.creditos = new CreditoUI(r.getCreditos()).toString();
        switch (idioma) {
            case "es":
                this.nombre = r.getAsiNombreES();
                break;
            case "en":
                this.nombre = r.getAsiNombreEN();
                break;
            case "ca":
            default:
                this.nombre = r.getAsiNombreCA();
                break;
        }
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public String getAsignaturaIdIncompatible()
    {
        return asignaturaIdIncompatible;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public String getCaracter() { return this.caracter; };

    public String getCreditos()
    {
        return creditos;
    }

    public String getNombre()
    {
        return nombre;
    }
}
