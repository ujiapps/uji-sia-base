package es.uji.apps.lleu.ui;

import java.util.Arrays;

import es.uji.apps.lleu.model.PresentacionMaster;

public class PresentacionMasterUI
{
    private String web;
    private String creditos;
    private String imagen;
    private String info;

    private String noAdmiteNuevosEstudiantes;

    /**
     * Ids de estudio que no admiten nuevos estudiantes
     */
    private static Integer []estudiosNoAdmitenEstudiantes = {
            42105, 42147, 42134, 42115, 42110, 42112, 42130, 42136, 42129, 42116, 42143, 42144, 42106, 42120,
            42107, 42108, 42109, 42124, 42119, 42138, 42141, 42125, 42127, 42128, 42118, 42103, 42113, 42101,
            42132, 42115
    };

    public static PresentacionMasterUI toUI(PresentacionMaster pm, String idioma) {
        return new PresentacionMasterUI(pm, idioma);
    }

    private PresentacionMasterUI(PresentacionMaster pm, String idioma) {
        this.web = pm.getWeb();
        this.creditos = new CreditoUI(pm.getCreditos()).toString();
        this.imagen = pm.getImagen();

        if (Arrays.asList(estudiosNoAdmitenEstudiantes).contains(pm.getEstudioId())) {
            this.noAdmiteNuevosEstudiantes = "presentacion.noadmite";
        } else {
            this.noAdmiteNuevosEstudiantes = null;
        }

        switch (idioma.toLowerCase()) {
            case "es":
                this.info = pm.getInfoES();
                break;
            case "en":
                this.info = pm.getInfoEN();
                break;
            default:
                this.info = pm.getInfoCA();
        }

        if (pm.getWeb() == null) {
            this.web = "http://www.uji.es/" + idioma.toUpperCase() + "/infoest/estudis/postgrau/oficial/e@/22891/?pTitulacion=" + pm.getEstudioId();
        } else {
            this.web = pm.getWeb();
        }
    }

    public String getWeb()
    {
        return web;
    }

    public String getCreditos()
    {
        return creditos;
    }

    public String getImagen()
    {
        return imagen;
    }

    public String getNoAdmiteNuevosEstudiantes()
    {
        return noAdmiteNuevosEstudiantes;
    }

    public String getInfo()
    {
        return info;
    }
}
