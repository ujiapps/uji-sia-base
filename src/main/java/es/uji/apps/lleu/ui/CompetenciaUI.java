package es.uji.apps.lleu.ui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.Competencia;

public class CompetenciaUI
{
    private Long competenciaId;
    private Integer cursoAca;
    private String asignaturaId;
    private String nombre;

    private static Pattern codeRegex;
    static {
        codeRegex = Pattern.compile("^([A-Z0-9\\-]+)( - .*)");
    }

    public static List<CompetenciaUI> toUI (List<Competencia> l, String idioma) {
        if (l == null || l.size() == 0) {
            return new ArrayList<>();
        }
        return l
                .stream()
                .map(c -> new CompetenciaUI(c, idioma))
                .sorted(Comparator.comparing(CompetenciaUI::getNombre))
                .collect(Collectors.toList());
    }

    public CompetenciaUI(Competencia c, String idioma) {
        this.competenciaId = c.getCompetenciaId();
        this.cursoAca = c.getCursoACa();
        this.asignaturaId = c.getAsignaturaId();
        switch (idioma) {
            case "es":
                this.nombre = c.getNombreES();
                break;
            case "en":
                this.nombre = c.getNombreEN();
                break;
            case "ca":
            default:
                this.nombre = c.getNombreCA();
                break;
        }

        this.nombre = this.addCSSClassToCodeName(this.nombre);
    }


    private String addCSSClassToCodeName(String name) {
        String ret;
        /**
         * Los códigos de las competencias van dentro del nombre, por lo que les asignaré la clase CSS codigo con una
         * expresión regular
         */
        Matcher m = codeRegex.matcher(name);
        if (m.find()) {
            ret = "<span class=\"codigo-asignatura\">" + m.group(1) + "</span> " + m.group(2);
        } else {
            ret = name;
        }

        return ret;
    }

    public Long getCompetenciaId()
    {
        return competenciaId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public static Pattern getCodeRegex()
    {
        return codeRegex;
    }
}
