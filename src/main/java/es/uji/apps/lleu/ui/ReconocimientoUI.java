package es.uji.apps.lleu.ui;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.Reconocimiento;

public class ReconocimientoUI
{
    private TreeMap<String, String> cursadas;
    private Map<String, Boolean> cursadasEnLleu;
    private TreeMap<String, String> reconocidas;

    private String parcial;

    private String tipo;
    private Integer cursoAca;

    private Integer titulacionCursada;
    private Integer titulacionReconocida;

    private String tipoTitulacionCursada;
    private String tipoTitulacionReconocida;

    /**
     * Devolvemos un map indexado por tcoid y on una instancia de ReconocimientoUI que tiene correctamente inicializadas
     * las asignaturas cursadas y las reconocidas.
     *
     * @param lr
     * @param idioma
     * @return
     */
    public static Map<Integer, ReconocimientoUI> toUI(Collection<Reconocimiento> lr, String idioma)
    {
        Map<Integer, ReconocimientoUI> ret = new LinkedHashMap<>();

        // ordeno por asignatura cursada.
        lr = lr.parallelStream()
                .sorted(Comparator.comparing(Reconocimiento::getAsigCursada))
                .collect(Collectors.toList());

        for (Reconocimiento reconocimiento : lr)
        {
            ReconocimientoUI rui;
            if ( ret.containsKey(reconocimiento.getTcoId()) ) {
                rui = ret.get(reconocimiento.getTcoId());
            } else {
                rui = toUI(reconocimiento, idioma);
                ret.put(reconocimiento.getTcoId(), rui);
            }
            rui.cursadas.put(reconocimiento.getAsigCursada(), getNombreCursada(reconocimiento, idioma));
            rui.reconocidas.put(reconocimiento.getAsigReconocida(), getNombreReconocida(reconocimiento, idioma));
            rui.cursadasEnLleu.put(reconocimiento.getAsigCursada(), reconocimiento.getCursadaEnLleu() == 1);

        }

        // ordeno
        return ret;
    }

    public static ReconocimientoUI toUI(Reconocimiento r, String idioma) {
        return new ReconocimientoUI(r, idioma);
    }

    private ReconocimientoUI (Reconocimiento r, String idioma) {
        this.parcial = r.getParcial();

        switch (r.getTipo()) {
            // Los reconocimientos de master no tienen tipo
            case "12C":
            case "D":
            case "POP":
                this.tipo = null;
                break;

            default:
                this.tipo = r.getTipo();
        }

        this.cursoAca = r.getCursoAca();
        this.titulacionCursada = r.getTitulacionCursada();
        this.titulacionReconocida = r.getTitulacionReconocida();

        this.tipoTitulacionCursada = r.getEstudioCursado().getTipo();
        this.tipoTitulacionReconocida = r.getEstudioReconocido().getTipo();

        this.cursadasEnLleu = new HashMap<>();
        this.cursadas = new TreeMap<>();
        this.reconocidas = new TreeMap<>();

    }

    public static String getNombreCursada(Reconocimiento r, String idioma)
    {
        String ret;
        switch (idioma.toLowerCase()) {
            case "es":
                ret = r.getNombreCursadaES();
                break;
            case "en":
                ret = r.getNombreCursadaEN();
                break;
            default:
                ret = r.getNombreCursadaCA();
        }
        return ret;
    }

    public Map<String, String> getCursadas()
    {
        return cursadas;
    }

    public void setCursadas(TreeMap<String, String> cursadas)
    {
        this.cursadas = cursadas;
    }

    public Map<String, String> getReconocidas()
    {
        return reconocidas;
    }

    public void setReconocidas(TreeMap<String, String> reconocidas)
    {
        this.reconocidas = reconocidas;
    }

    public static String getNombreReconocida(Reconocimiento r, String idioma)
    {
        switch (idioma.toLowerCase()) {
            case "en":
                return r.getNombreReconocidaEN();
            case "es":
                return r.getNombreReconocidaES();
            default:
                return r.getNombreReconocidaCA();
        }
    }

    public String getParcial()
    {
        return parcial;
    }

    public void setParcial(String parcial)
    {
        this.parcial = parcial;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Integer getTitulacionCursada()
    {
        return titulacionCursada;
    }

    public void setTitulacionCursada(Integer titulacionCursada)
    {
        this.titulacionCursada = titulacionCursada;
    }

    public Integer getTitulacionReconocida()
    {
        return titulacionReconocida;
    }

    public void setTitulacionReconocida(Integer titulacionReconocida)
    {
        this.titulacionReconocida = titulacionReconocida;
    }

    public Map<String, Boolean> getCursadasEnLleu()
    {
        return cursadasEnLleu;
    }

    public String getTipoTitulacionCursada()
    {
        return tipoTitulacionCursada;
    }

    public void setTipoTitulacionCursada(String tipoTitulacionCursada)
    {
        this.tipoTitulacionCursada = tipoTitulacionCursada;
    }

    public String getTipoTitulacionReconocida()
    {
        return tipoTitulacionReconocida;
    }

    public void setTipoTitulacionReconocida(String tipoTitulacionReconocida)
    {
        this.tipoTitulacionReconocida = tipoTitulacionReconocida;
    }
}
