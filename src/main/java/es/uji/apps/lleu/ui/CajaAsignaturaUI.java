package es.uji.apps.lleu.ui;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaCircuito;
import es.uji.apps.lleu.utils.LocalizedStrings;

/**
 * Esta clase la utilizamos para representar los datos de un fragmento caja-asignatura.
 */
public class CajaAsignaturaUI
{
    private Long estudioId;
    private Integer cursoAca;

    private String nombre;

    private String asignaturaId;
    private String curso;
    private String semestre;
    private String caracter;
    private String soloConvalida;
    private String asignaturaCF;


    private List<String[]> info;

    public CajaAsignaturaUI(AsignaturaCircuito c, List<String[]> info, String idioma)
    {

        this.estudioId = c.getEstudioId();

        switch (idioma.toLowerCase())
        {
            case "es":
                this.nombre = c.getNombreAsignaturaES();
                break;
            case "en":
                this.nombre = c.getNombreAsignaturaEN();
                break;
            default:
                this.nombre = c.getNombreAsignaturaCA();

        }

        this.asignaturaId = c.getAsignaturaId();
        this.curso = c.getCurso().toString();
        this.cursoAca = c.getCursoAca();
        this.semestre = LocalizedStrings.getSemestreKey(c.getSemestre());
        this.caracter = LocalizedStrings.getCaracterKey(c.getCaracter());

        this.info = info;
    }

    public CajaAsignaturaUI(Long estudioId, Integer cursoAca, String nombre, String asignaturaId, String curso, String semestre, String caracter, String soloConvalida, String asignaturaCF)
    {
        this.estudioId = estudioId;
        this.cursoAca = cursoAca;
        this.nombre = nombre;
        this.asignaturaId = asignaturaId;
        this.curso = curso;
        this.semestre = semestre;
        this.caracter = caracter;
        this.soloConvalida = soloConvalida;
        this.asignaturaCF = asignaturaCF;

        this.info = new ArrayList<>();
    }

    /**
     * Dada una lista de AsignaturaCircuito devuelve la correspondiente lista preparada para ser representada
     * por una caja de asignaturas
     *
     * @param lac
     * @param idioma
     * @return
     */
    public static List<CajaAsignaturaUI> toUI(List<AsignaturaCircuito> lac, String idioma)
    {

        List<CajaAsignaturaUI> ret = new ArrayList<>();

        lac.stream()
                .collect(Collectors.groupingBy(AsignaturaCircuito::getAsignaturaId))

                .forEach((asiId, lac2) ->
                {
                    List<String[]> info = new ArrayList<>();

                    lac2.forEach((ac) ->
                    {
                        info.add(
                                new String[]{
                                        ac.getSubgrupoTipo() + ac.getSubgrupoId(),
                                        LocalizedStrings.getSubgrupoKey(ac.getSubgrupoTipo())
                                }
                        );
                    });

                    ret.add(new CajaAsignaturaUI(lac2.get(0), info, idioma));
                });

        return ret.stream().sorted(Comparator.comparing(CajaAsignaturaUI::getAsignaturaId)).collect(Collectors.toList());
    }

    public String listadoDetalle(String curso,String semestre,String caracter,String soloConvalida)
    {
        List<String> list = new ArrayList<>();
        list.add(curso);
        list.add(semestre);
        list.add(caracter);
        list.add(soloConvalida);

        return list.stream()
                .filter((it)-> it.length()>0)
                .collect(Collectors.joining(" - "));

    }

    public void addInfo(String dato, String leyenda)
    {
        this.info.add(new String[]{dato, leyenda});
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getCurso()
    {
        return curso;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public List<String[]> getInfo()
    {
        return info;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public String getSoloConvalida()
    {
        return soloConvalida;
    }

    public String getAsignaturaCF()
    {
        return asignaturaCF;
    }
}
