package es.uji.apps.lleu.ui;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.Convocatoria;


public class ConvocatoriaUI
{
    private Long id;
    private String nombre;
    private String tipo;
    private String extra;
    private Integer orden;
    private String nombreCert;


    public static ConvocatoriaUI toUI(Convocatoria c, String idioma) {
        return new ConvocatoriaUI(c, idioma);
    }

    public static List<ConvocatoriaUI> toUI (Collection<Convocatoria> convocatorias, String idioma) {
        return convocatorias.stream()
                .map((c) -> new ConvocatoriaUI(c, idioma))
                .sorted(Comparator.comparing(a -> a.getOrden()))
                .collect(Collectors.toList());
    }

    private ConvocatoriaUI(Convocatoria c, String idioma) {
        this.id = c.getId();

        switch (idioma) {
            case "es":
                this.nombre = c.getNombreES().replace("Grado","Junio-Julio");
                this.nombreCert = c.getNombreCertES();
                break;
            case "en":
                this.nombre = c.getNombreEN().replace("Grade","June-July");
                this.nombreCert = c.getNombreCertEN();
                break;
            default:
                this.nombre = c.getNombreCA().replace("Grau","Juny-Juliol");
                this.nombreCert = c.getNombreCertCA();
        }

        this.tipo = c.getTipo();
        this.extra = c.getExtra();
        this.orden = c.getOrden();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getExtra()
    {
        return extra;
    }

    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }

    public String getNombreCert()
    {
        return nombreCert;
    }

    public void setNombreCert(String nombreCert)
    {
        this.nombreCert = nombreCert;
    }
}
