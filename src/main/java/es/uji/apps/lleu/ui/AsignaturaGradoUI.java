package es.uji.apps.lleu.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturasGrado;
import es.uji.apps.lleu.model.VacantesGrupoGrado;
import es.uji.apps.lleu.utils.LocalizedStrings;

/**
 * Facade de Asignatura. La utilizamos para evitar tener que tratar con los idiomas en el campo nombre a nivel
 * de plantilla. Esta clase debe instanciarse haciendo uso de los métodos toUI().
 */
public class AsignaturaGradoUI
{

    private Long estudioId;
    private Long curId;
    private String asignaturaId;
    private String nombre;
    private CreditoUI creditos;
    private String caracter;
    private String eep;
    private String tipo;
    private String semestre;
    /**
     * Parece redundante con semestre, pero esta me devuelve solo 1, 2 o a.
     */
    private String numeroSemestre;
    private Integer cursoAca;
    private String idioma;
    private String harmonizada;
    private String cursoAdaptacion;

    private AsignaturaInformacionUI asignaturasInformacion;

    private String caracterStrKey;

    // Calculamos las vacantes y vacantesNuevos a partir de la información que hay en subgrupos
    private String vacantes;
    private String vacantesNuevos;

    /**
     * Vacantes por grupo. En el TreeMap la clave es el grupo y el valor es el número de vacantes. Si vacantes es
     * -1 es que el grupo está cerrado.
     */
    private TreeMap<String, Integer> vacantesGrupos;
    private TreeMap<String, Integer> vacantesNuevosGrupos;

    /**
     * Vacantes por subgrupo. La clave es el grupo y el valor es un TreeMap donde String es el subgrupo y el valor
     * es el número de vacantes disponibles.
     */
    private HashMap<String, TreeMap<String, Integer>> vacantesSubgrupos;
    private HashMap<String, TreeMap<String, Integer>> vacantesNuevosSubgrupos;

    /**
     * Lo utilizamos para ordenar los subgrupos de la siguiente manera:
     *
     *    TE
     *    PR
     *    LA
     *    SE
     *    TU
     *    AV
     */
    private static class SubgrupoComparator implements Comparator<String> {

        private HashMap<String, Integer> orden;

        public SubgrupoComparator()
        {
            this.orden = new HashMap<>(6);
            this.orden.put("AV", 6);
            this.orden.put("TU", 5);
            this.orden.put("SE", 4);
            this.orden.put("LA", 3);
            this.orden.put("PR", 2);
            this.orden.put("TE", 1);
        }

        @Override
        public int compare(String o1, String o2)
        {
            int ret = 0;

            String tipo1 = o1.substring(0, 2);
            String tipo2 = o2.substring(0, 2);

            if (!this.orden.containsKey(tipo1) || !this.orden.containsKey(tipo2)) {
                return 0;
            }

            if (this.orden.get(tipo1) == this.orden.get(tipo2)) {
                Integer subgrupo1 = Integer.parseInt(o1.substring(2));
                Integer subgrupo2 = Integer.parseInt(o2.substring(2));
                if (subgrupo1 > subgrupo2) {
                    ret = 1;
                } else if (subgrupo1 < subgrupo2) {
                    ret = -1;
                }
            } else if (this.orden.get(tipo1) < this.orden.get(tipo2)) {
                ret = -1;
            } else {
                ret = 1;
            }

            return ret;
        }
    }

    /**
     * Dada una colección de asignaturaGrado y un idioma, devuelve una lista de asignaturaGrado UI. Si el idioma no está
     * soportado, utiliza por defecto CA.
     *
     * @param asignaturasGrado normalmente un conjunto de asignaturasGrado a tratar
     * @param asigSubgrupoList
     *@param idioma           el idioma que necesitamos  @return una listado de AsignaturaGradoUI con nombre traducido al idioma especificado
     */
    public static List<AsignaturaGradoUI> toUI(Collection<AsignaturasGrado> asignaturasGrado, List<VacantesGrupoGrado> asigSubgrupoList, String idioma)
    {
        Map<String, List<VacantesGrupoGrado>> subGrupos = asigSubgrupoList.stream().collect(Collectors.groupingBy(VacantesGrupoGrado::getAsignaturaId));
        return asignaturasGrado.stream()
                .map((a) -> new AsignaturaGradoUI(a, subGrupos.get(a.getAsignaturaId()),idioma))
                .collect(Collectors.toList());
    }

    public static List<AsignaturaGradoUI> toUI(Collection<AsignaturasGrado> asignaturasGrado,  String idioma)
    {
        return asignaturasGrado.stream()
                .map((a) -> new AsignaturaGradoUI(a, new ArrayList<>(),idioma))
                .collect(Collectors.toList());
    }

    /**
     * Dada una asignatura y un idioma, devuelve una instancia de AsignaturaGradoUI. Si el idioma no está soportado,
     * utiliza por defecto, CA
     *
     * @param a      instancia de Asignatura
     * @param idioma el idioma ("CA", "EN", "ES")
     * @return instancia de AsignaturaGradoUI con nombre en el idioma especificado
     */
    public static AsignaturaGradoUI toUI(AsignaturasGrado a, List<VacantesGrupoGrado> subGrupos, String idioma)
    {
        return new AsignaturaGradoUI(a, subGrupos, idioma);
    }

    public static AsignaturaGradoUI toUI(AsignaturasGrado a, String idioma)
    {
        return new AsignaturaGradoUI(a, new ArrayList<>(), idioma);
    }

    public CajaAsignaturaUI getCajaAsignaturaUI(){
        CajaAsignaturaUI caja = new CajaAsignaturaUI(
                this.estudioId,
                this.cursoAca,
                this.nombre,
                this.asignaturaId,
                (this.curId != null) ? this.curId.toString() : null,
                this.semestre,
                this.caracterStrKey,
                null,
                null
        );
        return caja;
    }

    private AsignaturaGradoUI(AsignaturasGrado a, List<VacantesGrupoGrado> subGrupos, String idioma) {
        this.estudioId = a.getEstudioId();
        this.curId = a.getCurId();
        this.asignaturaId = a.getAsignaturaId();

        switch (idioma.toLowerCase()) {
            case "es":
                this.nombre = a.getAsiNombreES();
                this.idioma = a.getIdiomaES();
                break;
            case "en":
                this.nombre = a.getAsiNombreEN();
                this.idioma = a.getIdiomaEN();
                break;
            default:
                this.nombre = a.getAsiNombreCA();
                this.idioma = a.getIdiomaCA();
                break;
        }

        this.creditos = new CreditoUI(a.getCreditos());
        this.caracter = a.getCaracter();
        this.eep = a.getEep();

        if(this.eep.equals("S") && this.caracter.equals("LC")){
            this.caracterStrKey = LocalizedStrings.getCaracterKey("pr");
        }else
        {
            this.caracterStrKey = LocalizedStrings.getCaracterKey(this.caracter);
        }

        this.tipo = a.getTipo();
        if(this.tipo.equals("A")){
            this.semestre = LocalizedStrings.getSemestreKey(this.tipo);
            this.numeroSemestre = "a";

        }else{
            this.semestre = LocalizedStrings.getSemestreKey(a.getSemestre());
            this.numeroSemestre = a.getSemestre().toString();
        }
        this.cursoAca = a.getCursoAca();
        this.harmonizada = a.getHarmonizada();
        this.cursoAdaptacion = a.getCursoAdaptacion();

        this.asignaturasInformacion = AsignaturaInformacionUI.toUI(a.getAsignaturasInformacion(), idioma);

        // Calculamos vacantes para representar fácil en thymeleaf

        this.vacantes="0";
        this.vacantesNuevos="0";
        if(subGrupos!=null)
        {
            Integer vacantes = 0;
            Integer vacantesNuevos = 0;

            for (VacantesGrupoGrado vgg : subGrupos)
            {
                try
                {
                    vacantes += Integer.parseInt(vgg.getVacantes());
                } catch (NumberFormatException e)
                {
                }
                try {
                    vacantesNuevos += Integer.parseInt(vgg.getVacantesNuevos());
                } catch (NumberFormatException e) {
                }
            }

            this.vacantes = vacantes.toString();
            this.vacantesNuevos = vacantesNuevos.toString();

        }
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public Long getCurId()
    {
        return curId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public CreditoUI getCreditos()
    {
        return creditos;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getEep()
    {
        return eep;
    }

    public String getTipo()
    {
        return tipo;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public String getHarmonizada()
    {
        return harmonizada;
    }

    public String getCursoAdaptacion()
    {
        return cursoAdaptacion;
    }

    public AsignaturaInformacionUI getAsignaturasInformacion()
    {
        return asignaturasInformacion;
    }

    public String getCaracterStrKey()
    {
        return caracterStrKey;
    }

    public String getVacantes()
    {
        return vacantes;
    }

    public String getVacantesNuevos()
    {
        return vacantesNuevos;
    }

    public TreeMap<String, Integer> getVacantesGrupos()
    {
        return vacantesGrupos;
    }

    public TreeMap<String, Integer> getVacantesNuevosGrupos()
    {
        return vacantesNuevosGrupos;
    }

    public HashMap<String, TreeMap<String, Integer>> getVacantesSubgrupos()
    {
        return vacantesSubgrupos;
    }

    public HashMap<String, TreeMap<String, Integer>> getVacantesNuevosSubgrupos()
    {
        return vacantesNuevosSubgrupos;
    }

    public String getNumeroSemestre()
    {
        return numeroSemestre;
    }
}
