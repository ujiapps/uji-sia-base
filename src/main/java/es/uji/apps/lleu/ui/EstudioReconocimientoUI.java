package es.uji.apps.lleu.ui;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.Reconocimiento;

public class EstudioReconocimientoUI
{
    private Long estudioId;
    private String nombreEstudio;

    private Reconocimiento reconocimiento;

    // la key es el tco_id / id de convalidacion
    private Map<Integer, ReconocimientoUI> reconocimientosUI;

    /**
     * Dado un listado de reconocimientos y un idioma se devuelve un Map que agrupa cada reconocimiento con su estudio cursado.
     * En el Map la key es el id de estudio y el value un EstudioReconocimientoUI.
     * @param reconocimientos
     * @param idioma
     * @return Map<Long, EstudioReconocimientoUI>
     */
    public static Map<Long, EstudioReconocimientoUI> toUI(List<Reconocimiento> reconocimientos, String idioma)
    {
        Map<Long, EstudioReconocimientoUI> res = new LinkedHashMap<>();


        reconocimientos.stream()
                // ordeno por nombre de estudio cursado
                .sorted((a, b) -> {
                    String nombreA, nombreB;
                    switch (idioma) {
                        case "es":
                            nombreA = a.getEstudioCursado().getNombreES();
                            nombreB = b.getEstudioCursado().getNombreES();
                            break;
                        case "en":
                            nombreA = a.getEstudioCursado().getNombreEN() == null ? a.getEstudioCursado().getNombreCA(): a.getEstudioCursado().getNombreEN();
                            nombreB = b.getEstudioCursado().getNombreEN() == null ? b.getEstudioCursado().getNombreCA(): b.getEstudioCursado().getNombreEN();
                            break;
                        default:
                            nombreA = a.getEstudioCursado().getNombreCA();
                            nombreB = b.getEstudioCursado().getNombreCA();
                    }
                    return nombreA.compareTo(nombreB);
                })

                // agrupo por id de estudio
                .collect(Collectors.groupingBy(
                        r -> r.getEstudioCursado().getId(),
                        LinkedHashMap::new,
                        Collectors.toList()
                ))

                // relleno el map de vuelta con los reconocimiento
                .forEach((estudioId, recsByEstudio) -> {

                    // TODO: repensar la ordenación de las asignaturas / reconocimientos
                    // cojo un reconocimiento de ejemplo para sacar el nombe del estudio. Solo por eso
                    List<Reconocimiento> list = new ArrayList<>();
                    recsByEstudio.stream().forEach(reconocimiento ->
                            reconocimientos.stream().filter(rec -> rec.getTcoId().equals(reconocimiento.getTcoId()))
                                .forEach(list::add)
                    );

                    Reconocimiento aux = recsByEstudio.get(0);
                    res.put(
                            estudioId,
                            new EstudioReconocimientoUI(
                                    aux,
                                    ReconocimientoUI.toUI(list, idioma),
                                    idioma
                            )
                    );

                });

        return res;
    }


    private EstudioReconocimientoUI (Reconocimiento r, Map<Integer, ReconocimientoUI> reconocimientosUI, String idioma)  {

        this.estudioId = r.getEstudioCursado().getId();

        switch (idioma.toLowerCase()) {
            case "es":
                this.nombreEstudio = r.getEstudioCursado().getNombreES();
                break;
            case "en":
                this.nombreEstudio = r.getEstudioCursado().getNombreEN() == null ? r.getEstudioCursado().getNombreCA() : r.getEstudioCursado().getNombreEN();
                break;
            default:
                this.nombreEstudio = r.getEstudioCursado().getNombreCA();
        }

        this.reconocimiento = r;
        this.reconocimientosUI = reconocimientosUI;
    }

    private EstudioReconocimientoUI (Reconocimiento r, String idioma)  {

        this.estudioId = r.getEstudioCursado().getId();

        switch (idioma.toLowerCase()) {
            case "es":
                this.nombreEstudio = r.getEstudioCursado().getNombreES();
                break;
            case "en":
                this.nombreEstudio = r.getEstudioCursado().getNombreEN() == null ? r.getEstudioCursado().getNombreCA() : r.getEstudioCursado().getNombreEN();
                break;
            default:
                this.nombreEstudio = r.getEstudioCursado().getNombreCA();
        }

        this.reconocimientosUI = new LinkedHashMap<>();
    }


    public Map<Integer, ReconocimientoUI> getReconocimientosUI()
    {
        return reconocimientosUI;
    }

    public void setReconocimientosUI(Map<Integer, ReconocimientoUI> reconocimientosUI)
    {
        this.reconocimientosUI = reconocimientosUI;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getNombreEstudio()
    {
        return nombreEstudio;
    }

    public void setNombreEstudio(String nombreEstudio)
    {
        this.nombreEstudio = nombreEstudio;
    }

    public Reconocimiento getReconocimiento()
    {
        return reconocimiento;
    }
}
