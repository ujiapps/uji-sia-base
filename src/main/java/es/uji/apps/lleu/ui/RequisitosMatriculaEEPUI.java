package es.uji.apps.lleu.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.RequisitosMatriculaEEP;

public class RequisitosMatriculaEEPUI
{
    private String idioma;

    private Long estudioId;
    private Integer cursoAca;
    private String asignaturaId;
    private String nombreAsignatura;
    private String asignaturaPreviaId;
    private String nombreAsignaturaPrevia;
    private Long itinerarioId;
    private String itinerario;
    private String creditos;
    private String creditosPorcentaje;
    private String excluirEEPTFG;
    private String caracter;
    private String creditosObItinerario;
    private String excluirTFG;
    private String incluirEEP;
    private String texto;

    private RequisitosMatriculaEEPUI(RequisitosMatriculaEEP r, String idioma)
    {
        this.idioma = idioma;
        this.asignaturaId = r.getAsignaturaId();
        this.asignaturaPreviaId = r.getAsignaturaPreviaId();
        this.itinerarioId = r.getItinerarioId();
        this.cursoAca = r.getCursoAca();
        this.estudioId = r.getEstudioId();
        this.creditos = new CreditoUI(r.getCreditos()).toString();
        switch (idioma)
        {
            case "es":
                this.nombreAsignatura = r.getNombreAsigES();
                this.nombreAsignaturaPrevia = r.getNombreAsigPreviaES();
                this.itinerario = r.getItinerarioES();
                this.texto= r.getTextoES();
                break;
            case "en":
                this.nombreAsignatura = r.getNombreAsigEN();
                this.nombreAsignaturaPrevia = r.getNombreAsigPreviaEN();
                this.itinerario = r.getItinerarioEN();
                this.texto= r.getTextoEN();
                break;
            case "ca":
            default:
                this.nombreAsignatura = r.getNombreAsigCA();
                this.nombreAsignaturaPrevia = r.getNombreAsigPreviaCA();
                this.itinerario = r.getItinerarioCA();
                this.texto= r.getTextoCA();
                break;
        }
        this.creditosPorcentaje = r.getCreditosPorcentaje();
        this.excluirEEPTFG = r.getExcluirEEPTFG();
        this.caracter = r.getCaracter();
        this.creditosObItinerario = r.getCreditosObItinerario();
        this.excluirTFG = r.getExcluirTFG();
        this.incluirEEP = r.getIncluirEEP();

    }

    public static List<RequisitosMatriculaEEPUI> toUI(List<RequisitosMatriculaEEP> l, String idioma)
    {
        return l.stream()
                .map((rm) -> new RequisitosMatriculaEEPUI(rm, idioma))
                .collect(Collectors.toList());
    }

    public static Map<String, List<RequisitosMatriculaEEPUI>> toUIByAsignatura (List<RequisitosMatriculaEEP> l, String idioma)
    {
        return l.stream()
                .map(rm -> new RequisitosMatriculaEEPUI(rm, idioma))
                .collect(
                        Collectors.groupingBy(RequisitosMatriculaEEPUI::getAsignaturaId)
                );
    }

    private List<String []> getDescripcionRequisitos() {
        List<String []> ret = new ArrayList<>();

        boolean useComma = false;
        if(this.texto !=null){
                    ret.add(new String [] {"reqmatricula.eep.textoespecifico", this.texto});
            return ret;
        }

        if ( this.getCreditosPorcentaje() == null ) {
            ret.add(new String [] {"reqmatricula.eep.creditos.minimos", null});
        } else if (this.getCreditosPorcentaje() != null ) {

            ret.add(new String[] {"reqmatricula.eep.cred.porcentaje.min", this.getCreditosPorcentaje()});

            if ( this.getCreditosObItinerario().compareTo("S") == 0 ) {
                useComma = true;
                ret.add(new String [] {"reqmatricula.eep.cred.porcentaje.delitinerario", null});

            } else if (
                    this.excluirEEPTFG.compareTo("N") == 0 &&
                            (this.excluirTFG.compareTo("N") == 0 ||
                            this.incluirEEP.compareTo("S") == 0) ) {

                ret.add(new String [] {"reqmatricula.eep.cred.porcentaje.y", null});
            }
            if ( this.excluirEEPTFG.compareTo("N") == 0) {
                if (useComma) {
                    ret.add(new String [] {"reqmatricula.eep.cred.porcentaje.coma", null});
                }
                if (this.excluirTFG.compareTo("N") == 0)
                {
                    if (useComma) {
                        ret.add(new String [] {"reqmatricula.eep.cred.porcentaje.coma", null});
                    }
                    ret.add(new String [] {"reqmatricula.eep.cred.porcentaje.tfg", null});
                    if (this.incluirEEP.compareTo("S") == 0) {
                        ret.add(new String [] {"reqmatricula.eep.cred.porcentaje.y", null});
                        useComma = false;
                    }
                }

                if (this.incluirEEP.compareTo("S") == 0)
                {
                    if (useComma) {
                        ret.add(new String [] {"reqmatricula.eep.cred.porcentaje.coma", null});
                    }
                    ret.add(new String [] {"reqmatricula.eep.cred.porcentaje.eep", null});
                }
            }
        }
        if (this.asignaturaPreviaId != null) {
            ret.add(new String [] {"reqmatricula.eep.cred.asignaturaprevia", this.asignaturaPreviaId + " - " + this.nombreAsignaturaPrevia});
        }
        return ret;
    }

    public CajaInfoUI getCajaInfoUI()
    {
        CajaInfoReqMatriculaEEP caja = new CajaInfoReqMatriculaEEP(this.itinerario, this.getDescripcionRequisitos());
        caja.addInfo(this.creditos, "creditos");
        return caja;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombreAsignatura()
    {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura)
    {
        this.nombreAsignatura = nombreAsignatura;
    }

    public String getAsignaturaPreviaId()
    {
        return asignaturaPreviaId;
    }

    public void setAsignaturaPreviaId(String asignaturaPreviaId)
    {
        this.asignaturaPreviaId = asignaturaPreviaId;
    }

    public String getNombreAsignaturaPrevia()
    {
        return nombreAsignaturaPrevia;
    }

    public void setNombreAsignaturaPrevia(String nombreAsignaturaPrevia)
    {
        this.nombreAsignaturaPrevia = nombreAsignaturaPrevia;
    }

    public Long getItinerarioId()
    {
        return itinerarioId;
    }

    public void setItinerarioId(Long itinerarioId)
    {
        this.itinerarioId = itinerarioId;
    }

    public String getItinerario()
    {
        return itinerario;
    }

    public void setItinerario(String itinerario)
    {
        this.itinerario = itinerario;
    }

    public String getCreditos()
    {
        return creditos;
    }

    public void setCreditos(String creditos)
    {
        this.creditos = creditos;
    }

    public String getCreditosPorcentaje()
    {
        return creditosPorcentaje;
    }

    public String getExcluirEEPTFG()
    {
        return excluirEEPTFG;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getCreditosObItinerario()
    {
        return creditosObItinerario;
    }

    public String getExcluirTFG()
    {
        return excluirTFG;
    }

    public String getIncluirEEP()
    {
        return incluirEEP;
    }

    public String getTexto()
    {
        return texto;
    }

    public void setTexto(String texto)
    {
        this.texto = texto;
    }
}
