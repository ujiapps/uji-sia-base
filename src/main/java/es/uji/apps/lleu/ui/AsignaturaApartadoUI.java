package es.uji.apps.lleu.ui;


import es.uji.apps.lleu.model.AsignaturaApartado;

public class AsignaturaApartadoUI
{
    private Long id;
    private String nombre;

    public static AsignaturaApartadoUI toUI (AsignaturaApartado a, String idioma) {
        return new AsignaturaApartadoUI(a, idioma);
    }

    private AsignaturaApartadoUI(AsignaturaApartado a, String idioma) {
        this.id = a.getId();
        switch (idioma.toLowerCase()) {
            case "en":
                this.nombre = a.getNombreEN();
                break;
            case "es":
                this.nombre = a.getNombreES();
                break;
            default:
                this.nombre = a.getNombreCA();
        }
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
}
