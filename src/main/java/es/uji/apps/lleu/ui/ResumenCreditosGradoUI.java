package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.ResumenCreditosGrado;

public class ResumenCreditosGradoUI
{
    private String curso;
    private Integer ciclo;
    private String creditosTroncales;
    private String creditosObligatorios;
    private String creditosOptativos;
    private String creditosLibreEleccion;
    private String creditosPFG;
    private String creditosTotales;

    public static List<ResumenCreditosGradoUI> toUI (List<ResumenCreditosGrado> lrc) {
        return lrc.stream()
                .map((r) -> new ResumenCreditosGradoUI(r))
                .collect(Collectors.toList());
    }

    public ResumenCreditosGradoUI(ResumenCreditosGrado r) {

        if (r.getCurso() == 99) {
            this.curso = "";
        } else {
            this.curso = r.getCurso().toString();
        }

        this.ciclo = r.getCiclo();

        this.creditosTroncales = r.getCrdTr() == 0 ? "-" : new CreditoUI(r.getCrdTr()).toString();
        this.creditosObligatorios = r.getCrdOb() == 0 ? "-" : new CreditoUI(r.getCrdOb()).toString();
        this.creditosOptativos = r.getCrdOp() == 0 ? "-" : new CreditoUI(r.getCrdOp()).toString();
        this.creditosLibreEleccion = r.getCrdLe() == 0 ? "-" : new CreditoUI(r.getCrdLe()).toString();
        this.creditosPFG = r.getCrdPfg() == 0 ? "-" : new CreditoUI(r.getCrdPfg()).toString();

        Double totales = r.getCrdTr() + r.getCrdLe() + r.getCrdOb() + r.getCrdOp() + r.getCrdPfg();
        this.creditosTotales = new CreditoUI(totales).toString();
    }

    public String getCurso()
    {
        return curso;
    }

    public Integer getCiclo()
    {
        return ciclo;
    }

    public String getCreditosTroncales()
    {
        return creditosTroncales;
    }

    public String getCreditosObligatorios()
    {
        return creditosObligatorios;
    }

    public String getCreditosOptativos()
    {
        return creditosOptativos;
    }

    public String getCreditosLibreEleccion()
    {
        return creditosLibreEleccion;
    }

    public String getCreditosPFG()
    {
        return creditosPFG;
    }

    public String getCreditosTotales()
    {
        return creditosTotales;
    }
}
