package es.uji.apps.lleu.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uji.apps.lleu.model.Itinerario;

public class EstudioItineratioUI
{
    private Long itinerarioId;
    private String nombre;
    private String creditosOB;
    private String creditosOP;
    private List<ItineratioUI> itinerarios;


    public static Map<Long, EstudioItineratioUI> toUI(List<Itinerario> itinerarios, String idioma)
    {

        Map<Long, EstudioItineratioUI> res = new HashMap<>();

        for (Itinerario itinerario : itinerarios)
        {
            if (res.containsKey(itinerario.getItinerarioId()))
            {
                EstudioItineratioUI estUI = res.get(itinerario.getItinerarioId());
                List<ItineratioUI> est = estUI.getItinerarios();
                est.add(ItineratioUI.toUI(itinerario,idioma));
                estUI.setItinerarios(est);
                res.put(itinerario.getItinerarioId(),estUI);
            }
            else
            {
                EstudioItineratioUI estUI = new EstudioItineratioUI();
                estUI.setCreditosOB(new CreditoUI(itinerario.getCreditosOB()).toString());
                estUI.setCreditosOP(new CreditoUI(itinerario.getCreditosOP()).toString());
                switch (idioma.toLowerCase()) {
                    case "en":
                        estUI.setNombre(itinerario.getNombreEN());
                        break;
                    case "es":
                        estUI.setNombre(itinerario.getNombreES());
                        break;
                    default:
                        estUI.setNombre(itinerario.getNombreCA());
                }
                List<ItineratioUI> list = new ArrayList<>();
                list.add(ItineratioUI.toUI(itinerario,idioma));
                estUI.setItinerarios(list);
                res.put(itinerario.getItinerarioId(),estUI);
            }
        }
        return res;
    }

    public Long getItinerarioId()
    {
        return itinerarioId;
    }

    public void setItinerarioId(Long itinerarioId)
    {
        this.itinerarioId = itinerarioId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCreditosOB()
    {
        return creditosOB;
    }

    public void setCreditosOB(String creditosOB)
    {
        this.creditosOB = creditosOB;
    }

    public String getCreditosOP()
    {
        return creditosOP;
    }

    public void setCreditosOP(String creditosOP)
    {
        this.creditosOP = creditosOP;
    }

    public List<ItineratioUI> getItinerarios()
    {
        return itinerarios;
    }

    public void setItinerarios(List<ItineratioUI> itinerarios)
    {
        this.itinerarios = itinerarios;
    }

}
