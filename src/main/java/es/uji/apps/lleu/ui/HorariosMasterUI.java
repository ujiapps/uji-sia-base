package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.FicheroHorariosMaster;
import es.uji.apps.lleu.utils.AppInfo;

public class HorariosMasterUI
{
    private String url;
    private String descripcion;
    private String ficheroNombre;

    private String masterUrl;


    public static List<HorariosMasterUI> toUI (List<FicheroHorariosMaster> lhm, String idioma) {
        return lhm.stream()
                .map((hm) -> new HorariosMasterUI(hm, idioma))
                .collect(Collectors.toList());
    }


    private HorariosMasterUI(FicheroHorariosMaster he, String idioma) {

        this.url = AppInfo.URL_BASE + he.getCursoAca() + "/estudio/" + he.getEstudioId() + "/horarios/?download=1&p_id=" + he.getId();

        switch (idioma) {
            case "es":
                this.descripcion = he.getDescripcionES();
                break;
            case "en":
                this.descripcion = he.getDescripcionEN();
                break;
            default:
                this.descripcion = he.getDescripcionCA();
        }

        this.ficheroNombre = he.getFicheroNombre();


    }

    public String getUrl()
    {
        return url;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public String getFicheroNombre()
    {
        return ficheroNombre;
    }
}
