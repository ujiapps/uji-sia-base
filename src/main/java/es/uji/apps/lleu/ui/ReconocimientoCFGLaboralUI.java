package es.uji.apps.lleu.ui;

import es.uji.apps.lleu.model.EstudioRec;

public class ReconocimientoCFGLaboralUI {
    private Boolean tieneCFGyGrado;
    private Boolean tienePorExpericiaLaboral;
    private CreditoUI creditosExperiencia;
    private CreditoUI creditosCFGS;

    public static ReconocimientoCFGLaboralUI toUI(EstudioRec er) {
        return new ReconocimientoCFGLaboralUI(er);
    }

    private ReconocimientoCFGLaboralUI(EstudioRec er) {
        this.tieneCFGyGrado = er.getCfgs().equals("S");
        this.tienePorExpericiaLaboral = er.getExperiencia().equals("S");
        this.creditosExperiencia = er.getCreditosExperiencia() == null ? null : new CreditoUI(er.getCreditosExperiencia());
        this.creditosCFGS = er.getCreditosCFGS() == null ? null : new CreditoUI(er.getCreditosCFGS());

    }

    public Boolean getTieneCFGyGrado() {
        return tieneCFGyGrado;
    }

    public Boolean getTienePorExpericiaLaboral() {
        return tienePorExpericiaLaboral;
    }

    public CreditoUI getCreditosExperiencia() {
        return creditosExperiencia;
    }

    public CreditoUI getCreditosCFGS() {
        return creditosCFGS;
    }
}
