package es.uji.apps.lleu.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AulasExamenesGrado;
import es.uji.apps.lleu.model.ExamenesGrado;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class ExamenUI {

    private static final String DATEFORMAT_DAY = "dd";
    private static final String DATEFORMAT_MONTH = "MMMM";
    private static final String DATEFORMAT_YEAR = "yyyy";
    private static final String DATEFORMAT_TIME = "HH:mm";

    private String idioma;
    private Locale locale;

    private Date fecha;
    private Long convocatoriaId;
    private Long epocaId;
    private String semestre;
    private String inicio;
    private String tipo;
    private String parcial;
    private String fin;
    private String comentario;
    private String nombre;
    private String definitivo;
    private String aulasStr;

    private List<AulasExamenesGrado> aulas;

    public static List<ExamenUI> toUI(List<ExamenesGrado> l, String idioma) {
        return l.stream()
                .map((e) -> new ExamenUI(e, idioma))
                .collect(Collectors.toList());
    }

    private ExamenUI(ExamenesGrado e, String idioma) {

        this.idioma = idioma;

        if (e.getParcial().equals("S"))
        {
            this.nombre = "";

            switch (idioma)
            {
                case "es":
                    this.comentario = e.getComentarioES();
                    this.locale = new Locale("es", "ES");
                    break;

                case "en":
                    this.comentario = e.getComentarioEN();
                    this.locale = Locale.ENGLISH;
                    break;

                case "ca":
                default:
                    this.comentario = e.getComentarioCA();
                    this.locale = new Locale("ca", "ES");
                    break;
            }
        } else
        {
            switch (idioma)
            {
                case "es":
                    this.nombre = e.getNombreES().replace("Grado", "Junio-Julio");
                    this.comentario = e.getComentarioES();
                    this.locale = new Locale("es", "ES");
                    break;
                case "en":
                    this.nombre = e.getNombreEN().replace("Grade", "June-July");
                    this.comentario = e.getComentarioEN();
                    this.locale = Locale.ENGLISH;
                    break;
                case "ca":
                default:
                    this.nombre = e.getNombreCA().replace("Grau", "Juny-Juliol");
                    this.comentario = e.getComentarioCA();
                    this.locale = new Locale("ca", "ES");
                    break;
            }
        }

        this.fecha = e.getFecha();
        this.convocatoriaId = e.getConvocatoriaId();
        this.epocaId = e.getEpocaId();
        this.semestre = LocalizedStrings.getSemestreKey(e.getSemestre());
        this.tipo = LocalizedStrings.getTipoExamenKey(e.getTipo());
        this.parcial = e.getParcial();
        this.aulas = e.getAulas();
        this.aulasStr = e.getAulas().stream()
                .map(i -> i.getAula()).distinct()
                .collect(Collectors.joining(", "));
        this.definitivo = e.getDefinitivo();

        if (definitivo.equals("S"))
        {
            SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT_TIME, this.locale);
            this.inicio = sdf.format(e.getInicio());
        } else
        {
            this.inicio = " ";
        }

        if (this.definitivo.equals("S"))
        {
            SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT_TIME, this.locale);
            this.fin = sdf.format(e.getFin());
        } else
        {
            this.fin = " ";
        }
    }

    public String getFechaDia() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT_DAY, this.locale);
        return sdf.format(this.fecha);
    }

    public String getFechaMes() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT_MONTH, this.locale);
        return sdf.format(this.fecha);
    }

    public String getFechaAnyo() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT_YEAR, this.locale);
        return sdf.format(this.fecha);
    }

    public Long getConvocatoriaId() {
        return convocatoriaId;
    }

    public Long getEpocaId() {
        return epocaId;
    }

    public String getSemestre() {
        return semestre;
    }

    public String getInicio() {
        return this.inicio;
    }

    public String getTipo() {
        return this.tipo;
    }

    public String getParcial() {
        return parcial;
    }

    public String getFin() {
        return this.fin;
    }

    public String getComentario() {
        return comentario;
    }

    public String getNombre() {
        return nombre;
    }

    public List<AulasExamenesGrado> getAulas() {
        return aulas;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getDefinitivo() {
        return definitivo;
    }

    public String getAulasStr() {
        return aulasStr;
    }
}