package es.uji.apps.lleu.ui;


import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.ComplementoFormacion;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class ComplementoFormacionUI {

    private Long estudioId;
    private Integer cursoAca;

    private String asignaturaId;
    private String nombre;
    private String creditos;
    private String tipo;
    private Long grpId;
    private String semestre;
    private String soloConvalida;
    private String caracter;
    private String asignaturaCF;


    public static List<ComplementoFormacionUI> toUI (List<ComplementoFormacion> lcf, String idioma) {
        return lcf.stream()
                .map((cf) -> new ComplementoFormacionUI(cf, idioma))
                .collect(Collectors.toList());
    }

    public CajaAsignaturaUI getCajaAsignaturaUI () {
        CajaAsignaturaUI ret = new CajaAsignaturaUI(
                this.estudioId,
                this.cursoAca,
                this.nombre,
                this.asignaturaId,
                null,
                this.semestre,
                this.caracter,
                null,
                this.asignaturaCF
        );
        ret.addInfo(this.grpId.toString(), "grupo");
        ret.addInfo(this.creditos, "creditos");

        return ret;
    }

    private ComplementoFormacionUI(ComplementoFormacion cf, String idioma) {

        this.estudioId = cf.getEstudioId();
        this.cursoAca = cf.getCursoAca();

        this.asignaturaId = cf.getAsignaturaId();
        switch (idioma.toLowerCase()) {
            case "es":
                this.nombre = cf.getNombreES();
                break;
            case "en":
                this.nombre = cf.getNombreEN();
                break;
            default:
                this.nombre = cf.getNombreCA();
        }

        this.grpId = cf.getGrpId();
        this.creditos = new CreditoUI(cf.getCreditos()).toString();
        if (cf.getTipo().toUpperCase().equals("A")) {
            this.semestre = "A";
        } else {
            this.semestre = cf.getSemestre().toString();
        }
        this.semestre = LocalizedStrings.getSemestreKey(this.semestre);
        this.soloConvalida = cf.getSoloConvalida();
        this.asignaturaCF = cf.getAsignaturaCF();

        // TODO: definir el carácter de una asignatura de máster.
        this.caracter = null;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getCreditos()
    {
        return creditos;
    }

    public String getTipo()
    {
        return tipo;
    }

    public Long getGrpId()
    {
        return grpId;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public String getSoloConvalida()
    {
        return soloConvalida;
    }

    public String getCaracter()
    {
        return caracter;
    }
}
