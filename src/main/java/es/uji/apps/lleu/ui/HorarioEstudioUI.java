package es.uji.apps.lleu.ui;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.HorarioAsignaturaGeneral;
import es.uji.apps.lleu.model.HorarioEstudioCalendar;
import es.uji.apps.lleu.model.HorarioEstudioDetallado;
import es.uji.apps.lleu.model.HorarioEstudioGeneral;
import es.uji.apps.lleu.model.HorarioMasterDetallado;
import es.uji.apps.lleu.model.HorarioMasterGeneral;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class HorarioEstudioUI
{
    private String asignaturaId;
    private String nombreAsignatura;
    private Integer cursoAca;
    private Long estudioId;
    private Long curso;
    private String grupo;
    private String tipoSubgrupo;
    private Long subgrupo;
    private String caracter;
    private String semestre;
    private String aula;
    private Integer diaSemana;
    private Date ini;
    private Date fin;
    private String tipo;
    private String comentario;
    private String comentarioDiscont;
    private String edificio;

    public static List<HorarioEstudioUI> toUI (List<?> lhe, String idioma)
    {
        return lhe.stream().map((e) ->
        {
            if (e instanceof HorarioEstudioDetallado)
            {
                return new HorarioEstudioUI((HorarioEstudioDetallado) e, idioma);
            }
            else if (e instanceof HorarioAsignaturaGeneral)
            {
                return new HorarioEstudioUI((HorarioAsignaturaGeneral) e, idioma);

            }else if (e instanceof HorarioEstudioCalendar)
            {
                return new HorarioEstudioUI((HorarioEstudioCalendar) e, idioma);

            }else if (e instanceof HorarioMasterGeneral)
            {
                return new HorarioEstudioUI((HorarioMasterGeneral) e, idioma);

            }else if (e instanceof HorarioMasterDetallado)
            {
                return new HorarioEstudioUI((HorarioMasterDetallado) e, idioma);

            }
            else {
                return new HorarioEstudioUI((HorarioEstudioGeneral) e, idioma);
            }
        }).collect(Collectors.toList());
    }


    private HorarioEstudioUI(HorarioEstudioGeneral heg, String idioma) {
        this.asignaturaId = heg.getAsignaturaId();

        this.cursoAca = heg.getCursoAca();
        this.estudioId = heg.getEstudioId();
        this.curso = heg.getCurso();
        this.grupo = heg.getGrupo();
        this.tipoSubgrupo = heg.getTipoSubgrupo();
        this.subgrupo = heg.getSubgrupo();
        this.caracter = heg.getCaracter();
        this.semestre = LocalizedStrings.getSemestreKey(heg.getSemestre());
        this.diaSemana = heg.getDiaSemana();
        this.ini = heg.getIni();
        this.fin = heg.getFin();
        this.tipo = heg.getTipo();
        this.comentario = heg.getComentario();

        switch (idioma) {
            case "es":
                this.nombreAsignatura = heg.getNombreES();
                this.comentarioDiscont = heg.getComentarioDiscontES();
                break;
            case "en":
                this.nombreAsignatura = heg.getNombreEN();
                this.comentarioDiscont = heg.getComentarioDiscontEN();
                break;
            default:
                this.nombreAsignatura = heg.getNombreCA();
                this.comentarioDiscont = heg.getComentarioDiscontCA();
                break;
        }
    }

    private HorarioEstudioUI(HorarioAsignaturaGeneral he, String idioma) {
        this.asignaturaId = he.getAsignaturaId();

        switch (idioma) {
            case "es":
                this.nombreAsignatura = he.getAsignatura().getNombreES();
                this.comentarioDiscont = he.getComentarioES();
                break;
            case "en":
                this.nombreAsignatura = he.getAsignatura().getNombreEN();
                this.comentarioDiscont = he.getComentarioEN();
                break;
            default:
                this.nombreAsignatura = he.getAsignatura().getNombreCA();
                this.comentarioDiscont = he.getComentarioCA();
                break;
        }

        this.cursoAca = he.getCursoAca();
        this.estudioId = he.getEstudioId();
        this.curso = he.getCurso();
        this.grupo = he.getGrupo();
        this.tipoSubgrupo = he.getTipoSubgrupo();
        this.subgrupo = he.getSubgrupo();
        this.caracter = he.getCaracter();
        this.semestre = LocalizedStrings.getSemestreKey(he.getSemestre());
        this.diaSemana = he.getDiaSemana();
        this.ini = he.getIni();
        this.fin = he.getFin();
        this.tipo = he.getTipo();
        this.comentario = he.getComentario();
    }

    private HorarioEstudioUI (HorarioEstudioDetallado he, String idioma) {
        this.asignaturaId = he.getAsignaturaId();

        switch (idioma) {
            case "es":
                this.nombreAsignatura = he.getAsignatura().getNombreES();
                this.comentarioDiscont = he.getComentarioDiscontES();
                break;
            case "en":
                this.nombreAsignatura = he.getAsignatura().getNombreEN();
                this.comentarioDiscont = he.getComentarioDiscontEN();
                break;
            default:
                this.nombreAsignatura = he.getAsignatura().getNombreCA();
                this.comentarioDiscont = he.getComentarioDiscontCA();
                break;
        }

        this.cursoAca = he.getCursoAca();
        this.estudioId = he.getEstudioId();
        this.curso = he.getCurso();
        this.grupo = he.getGrupo();
        this.tipoSubgrupo = he.getTipoSubgrupo();
        this.subgrupo = he.getSubgrupo();
        this.caracter = he.getCaracter();
        this.semestre = LocalizedStrings.getSemestreKey(he.getSemestre());
        this.diaSemana = he.getDiaSemana();
        this.ini = he.getIni();
        this.fin = he.getFin();
        this.tipo = he.getTipo();
        this.comentario = he.getComentario();
        this.aula = he.getAula();
        this.edificio = he.getEdificio();
    }

    private HorarioEstudioUI (HorarioEstudioCalendar he, String idioma) {
        this.asignaturaId = he.getAsignaturaId();

        switch (idioma) {
            case "es":
                this.nombreAsignatura = he.getNombreAsignaturaES();
                break;
            case "en":
                this.nombreAsignatura = he.getNombreAsignaturaEN();
                break;
            default:
                this.nombreAsignatura = he.getNombreAsignaturaCA();
                break;
        }

        this.cursoAca = he.getCursoAca();
        this.estudioId = he.getEstudioId();
        this.grupo = he.getGrupo();
        this.tipoSubgrupo = he.getTipoSubgrupo();
        this.subgrupo = he.getSubgrupo();
        this.ini = he.getIni();
        this.fin = he.getFin();
        this.aula = he.getAula();
    }

    private HorarioEstudioUI(HorarioMasterGeneral hmg, String idioma) {
        this.asignaturaId = hmg.getAsignaturaId();

        this.cursoAca = hmg.getCursoAca();
        this.estudioId = hmg.getEstudioId();
        this.curso = hmg.getCurso();
        this.grupo = hmg.getGrupo();
        this.tipoSubgrupo = hmg.getTipoSubgrupo();
        this.subgrupo = hmg.getSubgrupo();
        this.caracter = hmg.getCaracter();
        this.semestre = LocalizedStrings.getSemestreKey(hmg.getSemestre());
        this.diaSemana = hmg.getDiaSemana();
        this.ini = hmg.getIni();
        this.fin = hmg.getFin();
        this.tipo = hmg.getTipo();
        this.comentario = hmg.getComentario();

        switch (idioma) {
            case "es":
                this.nombreAsignatura = hmg.getNombreES();
                break;
            case "en":
                this.nombreAsignatura = hmg.getNombreEN();
                break;
            default:
                this.nombreAsignatura = hmg.getNombreCA();
                break;
        }
    }

    private HorarioEstudioUI (HorarioMasterDetallado hm, String idioma) {
        this.asignaturaId = hm.getAsignaturaId();

        switch (idioma) {
            case "es":
                this.nombreAsignatura = hm.getNombreAsignaturaES();
                break;
            case "en":
                this.nombreAsignatura = hm.getNombreAsignaturaEN();
                break;
            default:
                this.nombreAsignatura = hm.getNombreAsignaturaCA();
                break;
        }

        this.cursoAca = hm.getCursoAca();
        this.estudioId = hm.getEstudioId();
        this.curso = hm.getCurso();
        this.grupo = hm.getGrupo();
        this.tipoSubgrupo = hm.getTipoSubgrupo();
        this.subgrupo = hm.getSubgrupo();
        this.caracter = hm.getCaracter();
        this.semestre = LocalizedStrings.getSemestreKey(hm.getSemestre());
        this.diaSemana = hm.getDiaSemana();
        this.ini = hm.getIni();
        this.fin = hm.getFin();
        this.tipo = hm.getTipo();
        this.comentario = hm.getComentario();
        this.aula = hm.getAula();
        this.edificio = hm.getEdificio();
    }


    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombreAsignatura()
    {
        return nombreAsignatura;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public Long getCurso()
    {
        return curso;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public String getTipoSubgrupo()
    {
        return tipoSubgrupo;
    }

    public Long getSubgrupo()
    {
        return subgrupo;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public Integer getDiaSemana()
    {
        return diaSemana;
    }

    public Date getIni()
    {
        return ini;
    }

    public Date getFin()
    {
        return fin;
    }

    public String getTipo()
    {
        return tipo;
    }

    public String getComentario()
    {
        return comentario;
    }

    public String getComentarioDiscont()
    {
        return comentarioDiscont;
    }

    public String getAula()
    {
        return aula;
    }

    public String getEdificio()
    {
        return edificio;
    }
}
