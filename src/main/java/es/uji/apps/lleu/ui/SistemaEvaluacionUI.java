package es.uji.apps.lleu.ui;

import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.SistemaEvaluacionGrado;
import es.uji.apps.lleu.model.SistemaEvaluacionMaster;

public class SistemaEvaluacionUI
{
    private String tipo;
    private Long evalId;
    private Integer cursoAca;
    private String asignaturaId;
    private String cursoAcaFin;
    private String nombre;
    private Float ponderacion;
    private Integer orden;

    private String lang;

    public static List<SistemaEvaluacionUI> toUI(List l, String idioma) {
        return (List<SistemaEvaluacionUI>) l.stream()
                .map((e) -> {
                    if (e instanceof SistemaEvaluacionGrado) {
                        return new SistemaEvaluacionUI((SistemaEvaluacionGrado) e, idioma);
                    }
                    return new SistemaEvaluacionUI((SistemaEvaluacionMaster) e, idioma);
                })
                .collect(Collectors.toList());
    }

    private SistemaEvaluacionUI(SistemaEvaluacionGrado seg, String idioma) {

        this.tipo = seg.getTipo();
        this.evalId = seg.getEvalId();
        this.cursoAca = seg.getCursoAca();
        this.asignaturaId = seg.getAsignaturaId();
        this.cursoAcaFin = seg.getCursoAcaFin();

        String nombre;
        switch (idioma) {
            case "es":
                nombre = seg.getNombreES();
                break;
            case "en":
                nombre = seg.getNombreUK();
                break;
            case "ca":
            default:
                nombre = seg.getNombreCA();
                break;
        }

        this.nombre = nombre;
        this.ponderacion = seg.getPonderacion();
        this.orden = seg.getOrden();
    }

    private SistemaEvaluacionUI(SistemaEvaluacionMaster sem, String idioma) {
        this.tipo = null;
        this.evalId = sem.getEvaluacionId();
        this.cursoAca = sem.getCursoAca();
        this.asignaturaId = sem.getAsignaturaId();

        this.lang = idioma;
        String nombre;
        switch (idioma) {
            case "es":
                nombre = sem.getNombreES();
                break;
            case "en":
                nombre = sem.getNombreEN();
                break;
            case "ca":
            default:
                nombre = sem.getNombreCA();
                break;
        }

        this.nombre = nombre;
        this.ponderacion = sem.getPonderacion();
        this.orden = sem.getOrden();
    }

    public String getTipo()
    {
        return tipo;
    }

    public Long getEvalId()
    {
        return evalId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getCursoAcaFin()
    {
        return cursoAcaFin;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getPonderacion()
    {
        return new DecimalFormat("#.##").format(this.ponderacion);
    }

    public Integer getOrden()
    {
        return orden;
    }
}
