package es.uji.apps.lleu.ui;

import es.uji.apps.lleu.model.ResultadosGuias;

import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ResultadosGuiasUI
{
    private String nombre;

    private static Pattern codeRegex;
    static {
        codeRegex = Pattern.compile("^([A-Z0-9\\-]+)( - .*)");
    }


    public static List<ResultadosGuiasUI> toUI (List<ResultadosGuias> lResultados, String idioma) {
        return lResultados.stream()
                .map(r -> new ResultadosGuiasUI(r, idioma)).sorted(Comparator.comparing(ResultadosGuiasUI::getNombre,Comparator.nullsLast(Comparator.reverseOrder())))
                .collect(Collectors.toList());
    }

    private ResultadosGuiasUI (ResultadosGuias r, String idioma) {
        switch (idioma.toLowerCase()) {
            case "es":
                this.nombre = r.getNombreES();
                break;
            case "en":
                this.nombre = r.getNombreEN();
                break;
            default:
                this.nombre = r.getNombreCA();
        }
        if(this.nombre!=null) {
            this.nombre = this.addCSSClassToCodeName(this.nombre);
        }
    }

    public String getNombre()
    {
        return nombre;
    }

    private String addCSSClassToCodeName(String name) {
        String ret;
        /**
         * Los códigos de las competencias van dentro del nombre, por lo que les asignaré la clase CSS codigo con una
         * expresión regular
         */
        Matcher m = codeRegex.matcher(name);
        if (m.find()) {
            ret = "<span class=\"codigo-asignatura\">" + m.group(1) + "</span> " + m.group(2);
        } else {
            ret = name;
        }

        return ret;
    }

}
