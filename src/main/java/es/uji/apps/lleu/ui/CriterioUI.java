package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.Criterios;

public class CriterioUI
{
    private Integer cursoAca;
    private String asignaturaId;
    private String texto;

    public static List<CriterioUI> toUI (List<Criterios> criterios, String idioma) {
        return criterios.stream()
                .map((c) -> new CriterioUI(c, idioma))
                .collect(Collectors.toList());
    }

    private CriterioUI (Criterios cg, String idioma) {
        this.cursoAca = cg.getCursoAca();
        this.asignaturaId = cg.getAsignaturaId();

        switch(idioma) {
            case "es":
                this.texto = cg.getTextoES();
                break;
            case "en":
                this.texto = cg.getTextoEN();
                break;
            case "ca":
            default:
                this.texto = cg.getTextoCA();
                break;
        }

    }

    public String getTexto()
    {
        return texto;
    }

    public void setTexto(String texto)
    {
        this.texto = texto;
    }
}
