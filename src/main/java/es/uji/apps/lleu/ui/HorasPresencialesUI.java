package es.uji.apps.lleu.ui;

/**
 * Representa una hora presencial o no.
 */
public class HorasPresencialesUI
{
    private Double x;

    public HorasPresencialesUI(Double x) {
        this.x = x;
    }
    public HorasPresencialesUI(Float x) {
        this.x = x.doubleValue();
    }
    public HorasPresencialesUI(Long x) {
        this.x = x.doubleValue();
    }
    public HorasPresencialesUI(Integer x) {
        this.x = x.doubleValue();
    }

    public String toString() {
        String ret;

        // Tenemos decimales
        if (Math.abs(this.x - Math.floor(this.x)) > 0.01) {
            ret = String.format("%.1f", this.x);
        } else {
            // no tenemos decimales
            ret = String.format("%.0f", this.x);
        }

        return ret;
    }
}
