package es.uji.apps.lleu.ui;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uji.apps.lleu.model.Foto;

public class FotoUI
{
    protected Long perId;
    protected String foto;
    protected String mimeType;

    protected String url;

    public static Map<Long, FotoUI> toUI (Map<Long, List<Foto>> fotos) {
        Map<Long, FotoUI> ret = new HashMap<>();
        for (Long perid : fotos.keySet()) {
            ret.put(perid, new FotoUI(fotos.get(perid).get(0)));
        }
        return ret;
    }

    public static FotoUI toUI ( Foto foto ) {
        return new FotoUI(foto);
    }

    private FotoUI ( Foto f ) {
        this.perId = f.getPerId();
        this.foto = new String(Base64.getEncoder().encode(f.getFoto()));
        this.mimeType = f.getMimeType();

        this.url = "data:" + this.mimeType.toLowerCase() + ";base64," + this.foto;
    }

    public FotoUI ( Long perId, String base64, String mimeType) {
        this.perId = perId;
        this.foto = base64;
        this.mimeType = mimeType;

        this.url = "data:" + this.mimeType.toLowerCase() + ";base64," + this.foto;
    }
    public FotoUI() {};

    public Long getPerId()
    {
        return perId;
    }

    public String getFoto()
    {
        return foto;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public String getUrl()
    {
        return url;
    }
}
