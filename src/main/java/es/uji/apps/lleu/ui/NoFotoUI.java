package es.uji.apps.lleu.ui;

public class NoFotoUI extends FotoUI
{
    public NoFotoUI(Long perid) {
        this.perId = perid;
        this.foto = "/sia/pix/nofoto.png";
        this.mimeType = "image/png";
        this.url = "/sia/pix/nofoto.png";
    }
}
