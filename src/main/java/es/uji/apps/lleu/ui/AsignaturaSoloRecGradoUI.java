package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaSoloRecGrado;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class AsignaturaSoloRecGradoUI
{
    private Long estudioId;
    private Long curId;
    private String asignaturaId;
    private String nombre;
    private CreditoUI creditos;
    private String caracter;
    private String eep;
    private String tipo;
    private String semestre;
    private Integer cursoAca;
    private String caracterStrKey;

    public static List<AsignaturaSoloRecGradoUI> toUI (List<AsignaturaSoloRecGrado> l, String idioma) {
        return l.stream().map(a -> new AsignaturaSoloRecGradoUI(a, idioma)).collect(Collectors.toList());
    }

    public CajaInfoUI getCajaAsignaturaUI()
    {
        CajaInfoUI caja = new CajaInfoUI(
                this.asignaturaId + " - " + this.nombre,
                null,
                null
        );
        return caja;
    }

    private AsignaturaSoloRecGradoUI(AsignaturaSoloRecGrado a, String idioma)
    {
        this.estudioId = a.getEstudioId();
        this.curId = a.getCurId();
        this.asignaturaId = a.getAsignaturaId();

        switch (idioma.toLowerCase())
        {
            case "es":
                this.nombre = a.getAsiNombreES();
                break;
            case "en":
                this.nombre = a.getAsiNombreEN();
                break;
            default:
                this.nombre = a.getAsiNombreCA();
                break;
        }

        this.creditos = new CreditoUI(a.getCreditos());
        this.caracter = a.getCaracter();
        this.eep = a.getEep();

        if (this.eep.equals("S") && this.caracter.equals("LC"))
        {
            this.caracterStrKey = LocalizedStrings.getCaracterKey("pr");
        }
        else
        {
            this.caracterStrKey = LocalizedStrings.getCaracterKey(this.caracter);
        }

        this.tipo = a.getTipo();
        if (this.tipo.equals("A"))
        {
            this.semestre = LocalizedStrings.getSemestreKey(this.tipo);

        }
        else
        {
            this.semestre = LocalizedStrings.getSemestreKey(a.getSemestre());
        }
        this.cursoAca = a.getCursoAca();
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public Long getCurId()
    {
        return curId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public CreditoUI getCreditos()
    {
        return creditos;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getEep()
    {
        return eep;
    }

    public String getTipo()
    {
        return tipo;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public String getCaracterStrKey()
    {
        return caracterStrKey;
    }
}
