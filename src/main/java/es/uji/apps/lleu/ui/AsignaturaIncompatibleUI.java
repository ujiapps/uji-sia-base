package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaIncompatible;

public class AsignaturaIncompatibleUI
{
    private String asignaturaId;
    private String nombre;

    public static List<AsignaturaIncompatibleUI> toUI (List<AsignaturaIncompatible> lai, String idioma) {
        return lai.stream()
                .map((ai) -> new AsignaturaIncompatibleUI(ai, idioma))
                .collect(Collectors.toList());
    }

    private AsignaturaIncompatibleUI(AsignaturaIncompatible ai, String idioma) {

        this.asignaturaId = ai.getAsignaturaId();
        switch (idioma.toLowerCase()) {
            case "es":
                this.nombre = this.asignaturaId + " - " + ai.getNombreES();
                break;
            case "en":
                this.nombre = this.asignaturaId + " - " + ai.getNombreEN();
                break;
            default:
                this.nombre = this.asignaturaId + " - " + ai.getNombreCA();
        }
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }
}
