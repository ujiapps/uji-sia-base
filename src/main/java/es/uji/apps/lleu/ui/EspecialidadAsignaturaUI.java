package es.uji.apps.lleu.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.EspecialidadAsignatura;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class EspecialidadAsignaturaUI {

    private Long estudioId;
    private Integer cursoAca;

    private String asignaturaId;
    private String nombre;
    private String creditos;
    private String caracter;

    public static HashMap<Long, List<EspecialidadAsignaturaUI>> toUI (Map<Long, List<EspecialidadAsignatura>> mEsa, String idioma) {

        HashMap<Long, List<EspecialidadAsignaturaUI>> ret = new HashMap<>();

        mEsa.keySet().forEach((k) -> {

            List<EspecialidadAsignaturaUI> lea = mEsa.get(k).stream()
                    .map(ea -> new EspecialidadAsignaturaUI(ea, idioma))
                    .collect(Collectors.toList());

            ret.put(k, lea);
        });

        return ret;
    }

    public CajaAsignaturaUI getCajaAsignaturaUI() {
        CajaAsignaturaUI ret = new CajaAsignaturaUI(
                this.estudioId,
                this.cursoAca,
                this.nombre,
                this.asignaturaId,
                null,
                null,
                this.caracter,
                null,null
        );
        ret.addInfo(this.creditos, "creditos");
        return ret;
    }

    private EspecialidadAsignaturaUI (EspecialidadAsignatura ea, String idioma) {

        this.cursoAca = ea.getCursoAca();
        this.estudioId = ea.getEstudioId();

        this.asignaturaId = ea.getAsignaturaId();

        switch ( idioma ) {
            case "es":
                this.nombre = ea.getNombreES();
                break;
            case "en":
                this.nombre = ea.getNombreEN();
                break;
            default:
                this.nombre = ea.getNombreCA();
        }

        this.creditos = new CreditoUI(ea.getCreditos()).toString();
        this.caracter = LocalizedStrings.getCaracterKey(ea.getCaracter());
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getCreditos()
    {
        return creditos;
    }

    public String getCaracter()
    {
        return caracter;
    }
}
