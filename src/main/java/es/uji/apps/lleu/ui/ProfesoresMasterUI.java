package es.uji.apps.lleu.ui;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.sun.research.ws.wadl.Param;
import es.uji.apps.lleu.model.ProfesorMaster;
import es.uji.commons.rest.ParamUtils;

public class ProfesoresMasterUI {
    private Long ubicacionId;
    private String nombreUbicacion;
    private List<ProfesorMasterUI> profesores;

    public ProfesoresMasterUI(Long ubicacionId, ProfesorMaster p, List<ProfesorMasterUI> profesorMasters, String idioma) {
        this.ubicacionId = ubicacionId;
        if (ubicacionId != -1 && ubicacionId != -2) {
            switch (idioma) {
                case "es":
                    this.nombreUbicacion = p.getUbicacionEstructural().getNombreES();
                    break;
                case "en":
                    this.nombreUbicacion = p.getUbicacionEstructural().getNombreEN();
                    break;
                default:
                    this.nombreUbicacion = p.getUbicacionEstructural().getNombreCA();
            }
        }
        this.profesores = profesorMasters;
    }

    public static Map<Long, ProfesoresMasterUI> toUI(List<ProfesorMaster> prof, String idioma) {

        Map<Long, ProfesoresMasterUI> res = new TreeMap<>(Collections.reverseOrder());
        prof.stream().collect(Collectors.groupingBy((r) -> ProfesoresMasterUI.getUbicacion(r)))
                .forEach((ubicacionId, profesorMasters) ->
                {
                    ProfesorMaster p = profesorMasters.get(0);
                    res.put(ubicacionId, new ProfesoresMasterUI(ubicacionId, p, ProfesorMasterUI.toUI(profesorMasters, idioma), idioma));

                });
        return res;
    }

    private static Long getUbicacion(ProfesorMaster p) {
        if (!ParamUtils.isNotNull(p.getUbicacionId())) {
            return (ParamUtils.isNotNull(p.getTipoExterno()) ? p.getTipoExterno() : -2L);
        }
        return p.getUbicacionId();
    }

    public Long getUbicacionId() {
        return ubicacionId;
    }

    public void setUbicacionId(Long ubicacionId) {
        this.ubicacionId = ubicacionId;
    }

    public String getNombreUbicacion() {
        return nombreUbicacion;
    }

    public void setNombreUbicacion(String nombreUbicacion) {
        this.nombreUbicacion = nombreUbicacion;
    }

    public List<ProfesorMasterUI> getProfesores() {
        return profesores;
    }

    public void setProfesores(List<ProfesorMasterUI> profesores) {
        this.profesores = profesores;
    }
}
