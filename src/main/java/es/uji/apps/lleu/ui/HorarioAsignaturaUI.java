package es.uji.apps.lleu.ui;

import es.uji.apps.lleu.model.HorarioAsigMasterDetallado;
import es.uji.apps.lleu.model.HorarioAsigMasterGeneral;
import es.uji.apps.lleu.model.HorarioAsignaturaDetallado;
import es.uji.apps.lleu.model.HorarioAsignaturaGeneral;
import es.uji.apps.lleu.utils.LocalizedStrings;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class HorarioAsignaturaUI
{
    private String asignaturaId;
    private String nombreAsignatura;
    private Integer cursoAca;
    private Long estudioId;
    private Long curso;
    private String grupo;
    private String tipoSubgrupo;
    private Long subgrupo;
    private String caracter;
    private String semestre;
    private String aula;
    private Integer diaSemana;
    private Date ini;
    private Date fin;
    private String tipo;
    private String comentario;
    private String comentarioDiscont;
    private String edificio;

    public static List<HorarioAsignaturaUI> toUI (List<?> lhe, String idioma)
    {
        return lhe.stream().map((e) -> {
            if (e instanceof HorarioAsignaturaGeneral) {
                return new HorarioAsignaturaUI((HorarioAsignaturaGeneral) e, idioma);
            } else if (e instanceof HorarioAsignaturaDetallado) {
                return new HorarioAsignaturaUI((HorarioAsignaturaDetallado) e, idioma);
            } else if (e instanceof HorarioAsigMasterGeneral) {
                return new HorarioAsignaturaUI((HorarioAsigMasterGeneral) e, idioma);
            }else if (e instanceof HorarioAsigMasterDetallado) {
                return new HorarioAsignaturaUI((HorarioAsigMasterDetallado) e, idioma);
            } else {
                    return null;
            }
        }).collect(Collectors.toList());
    }

    private HorarioAsignaturaUI(HorarioAsignaturaDetallado ha, String idioma) {

        this.asignaturaId = ha.getAsignaturaId();

        this.cursoAca = ha.getCursoACA();
        this.estudioId = ha.getEstudioId();
        this.grupo = ha.getGrupo();
        this.tipoSubgrupo = ha.getSubGrupoTipo();
        this.subgrupo = new Long(ha.getSubGrupoId());
        this.semestre = LocalizedStrings.getSemestreKey(ha.getSemestre());
        this.diaSemana = ha.getDia();
        this.ini = ha.getIni();
        this.fin = ha.getFin();
        this.comentario = ha.getComentario();

        // INFO: ComentarioDiscont no se usa pero usamos el campo para mostrar los comentarios calculados.
        switch (idioma) {
            case "es":
                this.comentarioDiscont = ha.getComentarioES();
                this.nombreAsignatura = ha.getNombreES();
                break;
            case "en":
                this.comentarioDiscont = ha.getComentarioEN();
                this.nombreAsignatura = ha.getNombreEN();
                break;
            default:
                this.comentarioDiscont = ha.getComentarioCA();
                this.nombreAsignatura = ha.getNombreCA();
                break;
        }

        this.aula = ha.getAula();
        this.edificio = ha.getEdificio();
    }

    private HorarioAsignaturaUI(HorarioAsignaturaGeneral h, String idioma) {
        this.asignaturaId = h.getAsignaturaId();

        this.cursoAca = h.getCursoAca();
        this.estudioId = h.getEstudioId();
        this.curso = h.getCurso();
        this.grupo = h.getGrupo();
        this.tipoSubgrupo = h.getTipoSubgrupo();
        this.subgrupo = h.getSubgrupo();
        this.caracter = h.getCaracter();
        this.semestre = LocalizedStrings.getSemestreKey(h.getSemestre());
        this.diaSemana = h.getDiaSemana();
        this.ini = h.getIni();
        this.fin = h.getFin();
        this.tipo = h.getTipo();

        this.comentario = h.getComentario();

        // INFO: ComentarioDiscont no se usa pero usamos el campo para mostrar los comentarios calculados.
        switch (idioma) {
            case "es":
                this.nombreAsignatura = h.getAsignatura().getNombreES();
                this.comentarioDiscont = h.getComentarioES();
                break;
            case "en":
                this.nombreAsignatura = h.getAsignatura().getNombreEN();
                this.comentarioDiscont = h.getComentarioEN();
                break;
            default:
                this.nombreAsignatura = h.getAsignatura().getNombreCA();
                this.comentarioDiscont = h.getComentarioCA();
                break;
        }

        this.aula = h.getAula();
        this.edificio = h.getEdificio();
    }

    private HorarioAsignaturaUI(HorarioAsigMasterGeneral h, String idioma) {
        this.asignaturaId = h.getAsignaturaId();

        this.cursoAca = h.getCursoAca();
        this.estudioId = h.getEstudioId();
        this.curso = h.getCurso();
        this.grupo = h.getGrupo();
        this.tipoSubgrupo = h.getTipoSubgrupo();
        this.subgrupo = h.getSubgrupo();
        this.caracter = h.getCaracter();
        this.semestre = LocalizedStrings.getSemestreKey(h.getSemestre());
        this.diaSemana = h.getDiaSemana();
        this.ini = h.getIni();
        this.fin = h.getFin();
        this.tipo = h.getTipo();

        this.comentario = h.getComentario();

        // TODO: Borrar campo en la vista comentarioDiscont
        switch (idioma) {
            case "es":
                this.nombreAsignatura = h.getNombreES();
                break;
            case "en":
                this.nombreAsignatura = h.getNombreEN();
                break;
            default:
                this.nombreAsignatura = h.getNombreCA();
                break;
        }

        this.aula = h.getAula();
        this.edificio = h.getEdificio();
    }

    private HorarioAsignaturaUI(HorarioAsigMasterDetallado ha, String idioma) {

        this.asignaturaId = ha.getAsignaturaId();

        this.cursoAca = ha.getCursoACA();
        this.estudioId = ha.getEstudioId();
        this.grupo = ha.getGrupo();
        this.tipoSubgrupo = ha.getSubGrupoTipo();
        this.tipo= ha.getTipo();
        this.caracter = ha.getCaracter();
        this.curso= ha.getCurso();
        this.subgrupo = new Long(ha.getSubGrupoId());
        this.semestre = LocalizedStrings.getSemestreKey(ha.getSemestre());
        this.diaSemana = ha.getDia();
        this.ini = ha.getIni();
        this.fin = ha.getFin();

        switch (idioma) {
            case "es":
                this.comentario = ha.getComentario();
                this.nombreAsignatura = ha.getNombreES();
                break;
            case "en":
                this.comentario = ha.getComentario();
                this.nombreAsignatura = ha.getNombreEN();
                break;
            default:
                this.comentario = ha.getComentario();
                this.nombreAsignatura = ha.getNombreCA();
                break;
        }

        this.aula = ha.getAula();
        this.edificio = ha.getEdificio();
    }


    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombreAsignatura()
    {
        return nombreAsignatura;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public Long getCurso()
    {
        return curso;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public String getTipoSubgrupo()
    {
        return tipoSubgrupo;
    }

    public Long getSubgrupo()
    {
        return subgrupo;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public Integer getDiaSemana()
    {
        return diaSemana;
    }

    public Date getIni()
    {
        return ini;
    }

    public Date getFin()
    {
        return fin;
    }

    public String getTipo()
    {
        return tipo;
    }

    public String getComentario()
    {
        return comentario;
    }

    public String getComentarioDiscont()
    {
        return comentarioDiscont;
    }

    public String getAula()
    {
        return aula;
    }

    public String getEdificio()
    {
        return edificio;
    }

}
