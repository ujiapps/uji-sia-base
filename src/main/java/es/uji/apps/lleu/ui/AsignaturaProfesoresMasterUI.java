package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaProfesorMaster;

public class AsignaturaProfesoresMasterUI
{

    private String asiId;
    private String grpId;
    private Integer cursoAca;
    private String idioma;
    private Long perId;
    private String personaNombre;
    private String mail;
    private String url;
    private Boolean noTieneTutoriasProfesorExterno;

    private AsignaturaProfesoresMasterUI(AsignaturaProfesorMaster asignaturaProfesor, String idioma)
    {

        this.asiId = asignaturaProfesor.getAsiId();
        this.grpId = asignaturaProfesor.getGrpId();
        this.cursoAca = asignaturaProfesor.getCursoAca();

        switch (idioma.toLowerCase())
        {
            case "en":
                this.idioma = asignaturaProfesor.getIdiomaEN();
                break;
            case "es":
                this.idioma = asignaturaProfesor.getIdiomaES();
                break;
            default:
                this.idioma = asignaturaProfesor.getIdiomaCA();
        }
        this.perId = asignaturaProfesor.getPerId();
        this.mail = asignaturaProfesor.getMail();
        this.personaNombre = asignaturaProfesor.getPersonaNombre();
        this.url = asignaturaProfesor.getUrl();
        this.noTieneTutoriasProfesorExterno = asignaturaProfesor.isNoTieneTutoriasProfesorExterno();
    }

    public static Map<String ,List<AsignaturaProfesoresMasterUI>> toUI(List<AsignaturaProfesorMaster> profesores, String idioma)
    {
        return profesores.stream()
                .map((ap) -> new AsignaturaProfesoresMasterUI(ap, idioma))
                .collect(Collectors.groupingBy(AsignaturaProfesoresMasterUI::getGrpId));
    }

    public String getAsiId()
    {
        return asiId;
    }

    public void setAsiId(String asiId)
    {
        this.asiId = asiId;
    }

    public String getGrpId()
    {
        return grpId;
    }

    public void setGrpId(String grpId)
    {
        this.grpId = grpId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public void setIdioma(String idioma)
    {
        this.idioma = idioma;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public String getPersonaNombre()
    {
        return personaNombre;
    }

    public void setPersonaNombre(String personaNombre)
    {
        this.personaNombre = personaNombre;
    }

    public String getMail()
    {
        return mail;
    }

    public void setMail(String mail)
    {
        this.mail = mail;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Boolean getNoTieneTutoriasProfesorExterno() {
        return noTieneTutoriasProfesorExterno;
    }

    public Boolean isNoTieneTutoriasProfesorExterno() {
        return noTieneTutoriasProfesorExterno;
    }

    public void setNoTieneTutoriasProfesorExterno(Boolean noTieneTutoriasProfesorExterno) {
        this.noTieneTutoriasProfesorExterno = noTieneTutoriasProfesorExterno;
    }
}