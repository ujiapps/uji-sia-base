package es.uji.apps.lleu.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaCircuito;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class AsignaturaCircuitoUI
{
    private String asignaturaId;
    private String nombre;
    private Integer curso;
    private String semestre;
    private String caracter;
    private String caracterStrKey;

    /**
     * Key es el tipo de subgrupo + id de subgrupo
     * VAlue es el tipo de subgrupo
     */
    private HashMap<String, String> subgrupos;
    private List<String> sortedKeys;


    private static HashMap<String, Integer> subgroupOrder;
    static {
        subgroupOrder = new HashMap<>();
        subgroupOrder.put("TE", 1);
        subgroupOrder.put("TP", 2);
        subgroupOrder.put("PR", 3);
        subgroupOrder.put("LA", 4);
        subgroupOrder.put("SE", 5);
        subgroupOrder.put("TU", 6);
        subgroupOrder.put("AV", 7);
    }

    public static List<AsignaturaCircuitoUI> toUI (List<AsignaturaCircuito> acs, String idioma) {

        ArrayList<AsignaturaCircuitoUI> ret = new ArrayList<>(acs.size());

        // Voy a agrupar las asignaturas para poder determinar los subgrupos. Key es asignaturaId.
        Map<String, List<AsignaturaCircuito>> asiMap =
                acs.stream()
                        .collect(Collectors.groupingBy(AsignaturaCircuito::getAsignaturaId));


        asiMap.keySet().forEach((asignaturaId) -> {
            AsignaturaCircuitoUI ui = new AsignaturaCircuitoUI();
            AsignaturaCircuito aux = asiMap.get(asignaturaId).get(0);

            ui.setAsignaturaId(aux.getAsignaturaId());
            switch (idioma) {
                case "es":
                    ui.setNombre(aux.getNombreAsignaturaES());
                    break;
                case "en":
                    ui.setNombre(aux.getNombreAsignaturaEN());
                    break;
                default:
                    ui.setNombre(aux.getNombreAsignaturaCA());
            }
            ui.setCurso(aux.getCurso());

            ui.setSemestre(LocalizedStrings.getSemestreKey(aux.getSemestre()));
            ui.setCaracter(aux.getCaracter());
            ui.setCaracterStrKey(LocalizedStrings.getCaracterKey(aux.getCaracter()));

            HashMap<String, String> subgrupos = new HashMap<>();
            asiMap.get(asignaturaId).forEach((a) -> {
                subgrupos.put(a.getSubgrupoTipo() + a.getSubgrupoId(), a.getSubgrupoTipo());
            });
            ui.setSubgrupos(subgrupos);

            // Mantengo una lista ordenada por tipo de subgrupo e id de subgrupo para, desde la plantilla,
            // iterar por los subgrupos en orden.

            List<String> sks = subgrupos.keySet()
                    .stream()
                    .sorted((e1, e2) -> {
                        Integer e1Order = subgroupOrder.get(e1.substring(0,2)) * 1000 +
                                          Integer.parseInt(e1.substring(2));

                        Integer e2Order = subgroupOrder.get(e2.substring(0,2)) * 1000 +
                                          Integer.parseInt(e2.substring(2));

                        if (e1Order > e2Order) {
                            return 1;
                        } else if (e1Order < e2Order) {
                            return -1;
                        } else {
                            return 0;
                        }
                    })
                    .collect(Collectors.toList());
            ui.setSortedKeys(sks);

            ret.add(ui);
        });

        return ret.stream()
                .sorted((e1, e2) -> e1.getAsignaturaId().compareToIgnoreCase(e2.getAsignaturaId()))
                .collect(Collectors.toList());
    }

    public AsignaturaCircuitoUI() {
        this.asignaturaId = null;
        this.nombre = null;
        this.curso = null;

        this.semestre = null;

        this.caracter = null;
        this.caracterStrKey = null;

        this.subgrupos = new HashMap<>();
        this.sortedKeys = new ArrayList<>();
    }

    /**
     * Ojo, este método no devuelve ninguna propiedad. Lo usamos para tener acceso fácil a las cadenas de traducción
     * de subgrupos desde la plantilla.
     *
     * @param key
     * @return
     */
    public String getSubgrupoStrKey(String key)
    {
        return LocalizedStrings.getSubgrupoKey(key);
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getCaracterStrKey() { return caracterStrKey; };

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public void setCurso(Integer curso)
    {
        this.curso = curso;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }


    public void setCaracterStrKey(String caracterStrKey)
    {
        this.caracterStrKey = caracterStrKey;
    }

    public HashMap<String, String> getSubgrupos()
    {
        return subgrupos;
    }

    public void setSubgrupos(HashMap<String, String> subgrupos)
    {
        this.subgrupos = subgrupos;
    }

    public List<String> getSortedKeys()
    {
        return sortedKeys;
    }

    public void setSortedKeys(List<String> sortedKeys)
    {
        this.sortedKeys = sortedKeys;
    }

}
