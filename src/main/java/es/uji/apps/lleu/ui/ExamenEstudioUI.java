package es.uji.apps.lleu.ui;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.ExamenesEstudio;
import es.uji.apps.lleu.model.ExamenesEstudioAulas;
import es.uji.apps.lleu.model.ExamenesEstudioAulasMaster;
import es.uji.apps.lleu.model.ExamenesEstudioMaster;

public class ExamenEstudioUI
{

    private static final String DATEFORMAT_TIME = "HH:mm";

    private Locale locale;

    private String nombreAsignatura;
    private String codigoAsignatura;
    private String fecha;
    private String ini;
    private String fin;
    private String aulas;
    private String tipo;
    private String parcial;
    private String definitivo;

    public static List<ExamenEstudioUI> toUI(List<?> l, String idioma) {
        return l.stream()
                .map((e) -> {
                    if (e instanceof ExamenesEstudioMaster)
                    {
                        return new ExamenEstudioUI((ExamenesEstudioMaster) e, idioma);

                    }
                    else {
                        return new ExamenEstudioUI((ExamenesEstudio) e, idioma);
                    }
                } )
                .collect(Collectors.toList());
    }

    public ExamenEstudioUI(ExamenesEstudio e, String idioma) {

        switch (idioma) {
            case "es":
                this.nombreAsignatura = e.getNombreAsignaturaES();
                this.locale = new Locale("es", "ES");
                break;
            case "en":
                this.nombreAsignatura = e.getNombreAsignaturaEN();
                this.locale = Locale.ENGLISH;
                break;
            default:
                this.nombreAsignatura = e.getNombreAsignaturaCA();
                this.locale =  new Locale("ca", "ES");
        }

        this.codigoAsignatura = e.getAsiId();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.fecha = sdf.format(e.getFecha());

        this.definitivo = e.getDefinitivo();

        if (definitivo.equals("S")) {
            SimpleDateFormat sdfh = new SimpleDateFormat(DATEFORMAT_TIME, this.locale);
            this.ini = sdfh.format(e.getIni());
        } else {
            this.ini = " ";
        }

        if (this.definitivo.equals("S"))
        {
            SimpleDateFormat sdfh = new SimpleDateFormat(DATEFORMAT_TIME, this.locale);
            this.fin = sdfh.format(e.getFin());
        } else {
            this.fin = " ";
        }



        this.tipo = e.getTipo();
        this.parcial = e.getParcial();

        this.aulas = "";
        Set<ExamenesEstudioAulas> aulas = e.getAulas();

        aulas.forEach((a) -> this.aulas = this.aulas + a.getAula() + ", ");

        if (this.aulas.length() > 0)
        {
            this.aulas = this.aulas.substring(0, this.aulas.length() - 2);
        }

    }

    public ExamenEstudioUI(ExamenesEstudioMaster e, String idioma) {

        switch (idioma) {
            case "es":
                this.nombreAsignatura = e.getNombreAsignaturaES();
                this.locale = new Locale("es", "ES");
                break;
            case "en":
                this.nombreAsignatura = e.getNombreAsignaturaEN();
                this.locale = Locale.ENGLISH;
                break;
            default:
                this.nombreAsignatura = e.getNombreAsignaturaCA();
                this.locale =  new Locale("ca", "ES");
        }

        this.codigoAsignatura = e.getAsiId();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        this.fecha = sdf.format(e.getFecha());

        this.definitivo = e.getDefinitivo();

        if (definitivo.equals("S")) {
            SimpleDateFormat sdfh = new SimpleDateFormat(DATEFORMAT_TIME, this.locale);
            this.ini = sdfh.format(e.getIni());
        } else {
            this.ini = " ";
        }

        if (this.definitivo.equals("S"))
        {
            SimpleDateFormat sdfh = new SimpleDateFormat(DATEFORMAT_TIME, this.locale);
            this.fin = sdfh.format(e.getFin());
        } else {
            this.fin = " ";
        }



        this.tipo = e.getTipo();
        this.parcial = e.getParcial();

        this.aulas = "";
        Set<ExamenesEstudioAulasMaster> aulas = e.getAulas();

        aulas.forEach((a) -> this.aulas = this.aulas + a.getAula() + ", ");

        if (this.aulas.length() > 0)
        {
            this.aulas = this.aulas.substring(0, this.aulas.length() - 2);
        }

    }

    public String getNombreAsignatura()
    {
        return nombreAsignatura;
    }

    public String getCodigoAsignatura()
    {
        return codigoAsignatura;
    }

    public String getFecha()
    {
        return fecha;
    }

    public String getIni()
    {
        return ini;
    }

    public String getFin()
    {
        return fin;
    }

    public String getAulas()
    {
        return aulas;
    }

    public String getTipo()
    {
        return tipo;
    }

    public String getParcial()
    {
        return parcial;
    }

    public Locale getLocale()
    {
        return locale;
    }

    public String getDefinitivo()
    {
        return definitivo;
    }
}
