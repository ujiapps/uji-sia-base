package es.uji.apps.lleu.ui;

import es.uji.apps.lleu.model.Circuito;

public class CircuitoUI {
    Integer cursoAca;
    Long estudioId;
    Long circuitoId;
    String nombre;
    String grupoId;
    String ingles;
    private String tienePlazas;


    public static CircuitoUI toUI(Circuito circuito, String idioma) {
        return new CircuitoUI(circuito, idioma);
    }


    private CircuitoUI(Circuito circuito, String idioma) {
        this.cursoAca = circuito.getCursoAca();
        this.estudioId = circuito.getEstudioId();
        this.circuitoId = circuito.getCircuitoId();
        this.nombre = circuito.getNombre();
        if (this.nombre.contains("IMEF")) {
            switch (idioma.toLowerCase()) {
                case "en":
                    this.nombre = this.nombre.concat(" - ENGLISH");
                    break;
                case "es":
                    this.nombre = this.nombre.concat(" - INGLÉS");
                    break;
                default:
                    this.nombre = this.nombre.concat(" - ANGLÉS");
            }
        }
        this.grupoId = circuito.getGrupoId();
        this.ingles = circuito.getIngles();
        this.tienePlazas = circuito.getTienePlazas();
    }

    public Integer getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Long estudioId) {
        this.estudioId = estudioId;
    }

    public Long getCircuitoId() {
        return circuitoId;
    }

    public void setCircuitoId(Long circuitoId) {
        this.circuitoId = circuitoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(String grupoId) {
        this.grupoId = grupoId;
    }

    public String getIngles() {
        return ingles;
    }

    public void setIngles(String ingles) {
        this.ingles = ingles;
    }

    public String getTienePlazas() {
        return tienePlazas;
    }

    public void setTienePlazas(String tienePlazas) {
        this.tienePlazas = tienePlazas;
    }
}
