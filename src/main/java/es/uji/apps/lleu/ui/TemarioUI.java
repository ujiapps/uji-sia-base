package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaTemario;

public class TemarioUI
{

    private String asignaturaId;

    private String texto;
    private String estado;

    private AsignaturaApartadoUI apartado;

    private Integer cursoACA;

    public static List<TemarioUI> toUI(List<AsignaturaTemario> temario, String idioma)
    {
        return temario.stream()
                .map((t) -> new TemarioUI(t, idioma))
                .collect(Collectors.toList());
    }

    public static TemarioUI toUI(AsignaturaTemario temario, String idioma)
    {
        return new TemarioUI(temario, idioma);
    }

    private TemarioUI (AsignaturaTemario temario, String idioma) {
        this.asignaturaId = temario.getAsignaturaId();
        this.estado = temario.getEstado();
        this.cursoACA = temario.getCursoACA();
        this.apartado = AsignaturaApartadoUI.toUI(temario.getApartado(), idioma);

        switch (idioma.toLowerCase()) {
            case "en":
                this.texto = temario.getTextoEN();
                break;
            case "es":
                this.texto = temario.getTextoES();
                break;
            default:
                this.texto = temario.getTextoCA();
        }
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getTexto()
    {
        return texto;
    }

    public String getEstado()
    {
        return estado;
    }

    public AsignaturaApartadoUI getApartado()
    {
        return apartado;
    }

    public Integer getCursoACA()
    {
        return cursoACA;
    }
}
