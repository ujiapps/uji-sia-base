package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaMetodologia;

public class MetodologiaUI
{
    private String asignaturaId;
    private String texto;
    private String estado;
    private AsignaturaApartadoUI apartado;
    private Integer cursoACA;

    public static List<MetodologiaUI> toUI(List<AsignaturaMetodologia> metodologia, String idioma)
    {
        return metodologia.stream()
                .map((a) -> new MetodologiaUI(a, idioma))
                .collect(Collectors.toList());
    }

    private MetodologiaUI (AsignaturaMetodologia metodologia, String idioma)
    {
        this.asignaturaId = metodologia.getAsignaturaId();
        this.estado = metodologia.getEstado();
        this.cursoACA = metodologia.getCursoACA();

        this.apartado = AsignaturaApartadoUI.toUI(metodologia.getApartado(), idioma);

        switch (idioma.toLowerCase()) {
            case "es":
                this.texto = metodologia.getTextoES();
                break;
            case "en":
                this.texto = metodologia.getTextoEN();
                break;
            default:
                this.texto = metodologia.getTextoCA();
        }
    }


    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getTexto()
    {
        return texto;
    }

    public String getEstado()
    {
        return estado;
    }

    public AsignaturaApartadoUI getApartado()
    {
        return apartado;
    }

    public Integer getCursoACA()
    {
        return cursoACA;
    }
}
