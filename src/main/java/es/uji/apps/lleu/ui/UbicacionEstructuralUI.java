package es.uji.apps.lleu.ui;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.UbicacionEstructural;
import es.uji.apps.lleu.model.enums.CSSClass;

public class UbicacionEstructuralUI
{
    private Long id;
    private String nombre;

    // caso de que sea una facultad, devuelve un css para poner el color de la escuela. si no devuelve null.
    private String css;

    public static List<UbicacionEstructuralUI> toUI (List<UbicacionEstructural> lu, String idioma) {
        return lu
                .parallelStream()
                .map(u -> new UbicacionEstructuralUI(u, idioma))
                .sorted(Comparator.comparing(UbicacionEstructuralUI::getNombre))
                .collect(Collectors.toList());
    }



    public UbicacionEstructuralUI (UbicacionEstructural u, String idioma) {
        this.id = u.getId();
        switch (idioma) {
            case "es":
                this.nombre = (u.getNombreES()!=null)?u.getNombreES():u.getNombreCA();
                break;
            case "en":
                this.nombre = (u.getNombreEN()!=null)?u.getNombreEN():u.getNombreCA();
                break;
            default:
                this.nombre = u.getNombreCA();
        }

        this.css = CSSClass.getCSSByCentro(u.getNombreES());
    }

    public Long getId()
    {
        return id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getCss()
    {
        return css;
    }
}
