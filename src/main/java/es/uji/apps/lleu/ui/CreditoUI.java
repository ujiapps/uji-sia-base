package es.uji.apps.lleu.ui;

/**
 * Representa un crédito
 */
public class CreditoUI
{
    private Double x;

    public CreditoUI(Double x) {
        this.x = x;
    }
    public CreditoUI(Float x) {
        this.x = x.doubleValue();
    }
    public CreditoUI(Long x) {
        this.x = x.doubleValue();
    }
    public CreditoUI(Integer x) {
        this.x = x.doubleValue();
    }

    public String toString() {
        String ret;

        // Tenemos decimales
        if (Math.abs(this.x - Math.floor(this.x)) > 0.01) {
            ret = String.format("%.1f", this.x);
        } else {
            // no tenemos decimales
            ret = String.format("%.0f", this.x);
        }

        return ret;
    }
}
