package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.RequisitosMatricula;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class RequisitosMatriculaUI
{
    private Integer cursoAca;
    private Long estudioId;

    private String asignaturaId;
    private String asignaturaId1;
    private String nombre;
    private Integer curso;
    private String semestre;
    private String caracter;
    private String tipo;

    /**
     * Devuelve un Map cuya clave es el id de asignatura con incompatibilidades y el valor es una lista con los
     * requisitos que debe cumplir
     *
     * @param rml
     * @param idioma
     * @return
     */
    public static Map<String, List<RequisitosMatriculaUI>> toUI (List<RequisitosMatricula> rml, String idioma) {
        return rml.stream()
                .map((rm) -> new RequisitosMatriculaUI(rm, idioma))
                .collect(Collectors.groupingBy(RequisitosMatriculaUI::getAsignaturaId));
    }

    public CajaAsignaturaUI getCajaAsignaturaUI () {
        return new CajaAsignaturaUI(
                this.estudioId,
                this.cursoAca,
                this.nombre,
                this.asignaturaId1,
                this.curso.toString(),
                this.semestre,
                this.caracter,
                null,null
        );
    }

    public RequisitosMatriculaUI (RequisitosMatricula rm, String idioma) {

        this.cursoAca = rm.getCursoAca();
        this.estudioId = rm.getEstudioId();

        this.asignaturaId = rm.getAsignaturaId();
        this.asignaturaId1 = rm.getAsignaturaId1();

        switch (idioma.toLowerCase()) {
            case "es":
                this.nombre = rm.getNombreES();
                break;
            case "en":
                this.nombre = rm.getNombreEN();
                break;
            default:
                this.nombre = rm.getNombreCA();
        }
        this.curso = rm.getCurso();
        this.semestre = LocalizedStrings.getSemestreKey(rm.getSemestre());
        this.caracter = LocalizedStrings.getCaracterKey(rm.getCaracter());

        this.tipo = rm.getTipo();
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getTipo()
    {
        return tipo;
    }
}
