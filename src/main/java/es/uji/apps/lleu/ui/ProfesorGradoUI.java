package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.ProfesorGrado;

public class ProfesorGradoUI
{
    private Long perId;
    private String nombre;
    private String departamento;
    private String categoria;
    private String url;

    /**
     * Devuelve un Map donde la clave es el nombre del departamento y el valor el listado de proresores de
     * ese departamento.
     *
     * @param lpg
     * @param idioma
     * @return
     */
    public static Map<String, List<ProfesorGradoUI>> toUI (List<ProfesorGrado> lpg, String idioma) {
        return lpg.stream()
                .map((pg) -> new ProfesorGradoUI(pg, idioma))
                .collect(Collectors.groupingBy(ProfesorGradoUI::getDepartamento));
    }

    private ProfesorGradoUI(ProfesorGrado p, String idioma) {

        this.perId = p.getPerId();
        this.nombre = p.getPerNombre();

        switch (idioma) {
            case "es":
                this.departamento = p.getDepartamentoES();
                this.categoria = p.getCategoriaES();
                break;
            case "en":
                this.departamento = p.getDepartamentoEN();
                this.categoria = p.getCategoriaCA();
                break;
            default:
                this.departamento = p.getDepartamentoCA();
                this.categoria = p.getCategoriaCA();
        }

        this.url = p.getUrl();

    }

    public String getNombre()
    {
        return nombre;
    }

    public String getDepartamento()
    {
        return departamento;
    }

    public String getCategoria()
    {
        return categoria;
    }

    public String getUrl()
    {
        return url;
    }

    public Long getPerId()
    {
        return perId;
    }
}
