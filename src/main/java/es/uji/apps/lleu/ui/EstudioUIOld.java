package es.uji.apps.lleu.ui;

import es.uji.apps.lleu.model.Estudio;
import es.uji.apps.lleu.model.EstudioTodo;
import es.uji.apps.lleu.model.EstudioUrl;
import es.uji.commons.rest.ParamUtils;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EstudioUIOld
{
    private Long id;
    private String nombre;
    private String tipo;
    private Long uestId;
    private Integer nivel;
    private String presentacion;
    private String faqURL;
    private String urlVerifica;
    private Integer cursos;
    private CreditoUI creditoItinerarioMin;
    private String calidadUrl;
    private String reconocimientoUrl;
    private String responsablesUrl;

    private Integer comentarioPortada;

    private EstudioUIOld(EstudioTodo e, EstudioUrl urls, String idioma, Integer anyo)
    {
        this.id = e.getId();

        switch (idioma)
        {
            case "es":
                this.nombre = e.getNombreES();
                break;
            case "en":
                this.nombre = e.getNombreEN();
                break;
            default:
                this.nombre = e.getNombreCA();
                break;

        }
        this.tipo = e.getTipo();
        this.uestId = this.normalizeUestId(e.getUestId());

        this.faqURL = e.getFaqUrl();
        this.calidadUrl = e.getCalidadUrl();
        this.reconocimientoUrl = e.getReconocimientoUrl();
        this.responsablesUrl = e.getResponsablesUrl();
        if (urls != null)
        {
            this.presentacion = urls.getUrl();
            this.urlVerifica = urls.getUrlVerifica();
        }
        else
        {
            this.presentacion = null;
            this.urlVerifica = null;
        }
        this.cursos = e.getCursos();
        this.nivel= ParamUtils.isNotNull(e.getNivel())? e.getNivel(): 1;
        this.comentarioPortada= e.getComentarioPortada();
        this.creditoItinerarioMin = e.getCreditoItinerarioMin() == null ? null : new CreditoUI(e.getCreditoItinerarioMin());
    }

    private Long normalizeUestId(Long uestId)
    {
        if (uestId == 318l || uestId == 3145l)
        {
            // instituto interuniversitario y uji se van a facultad de ciencias jurídicas de momento.
            uestId = 3l;
        }
        if (this.id == 42122l)
        {
            uestId = 2922l;
        }
        return uestId;
    }

    private EstudioUIOld (Estudio e, String idioma, Integer anyo) {
        this.id = e.getId();


        switch (idioma) {
            case "es":
                this.nombre = e.getNombreES();
                break;
            case "en":
                this.nombre = e.getNombreEN();
                break;
            default:
                this.nombre = e.getNombreCA();
                break;

        }
        this.tipo = e.getTipo();
        this.uestId = this.normalizeUestId(e.getUestId());

        this.nivel=ParamUtils.isNotNull(e.getNivel())? e.getNivel(): 1;
        this.comentarioPortada= e.getComentarioPortada();

        this.presentacion = e.getPresentacion();
        this.faqURL = e.getFaqURL();
        this.cursos = e.getCursos();
    }

    public static EstudioUIOld toUI(Estudio e, String idioma, Integer anyo)
    {
        return new EstudioUIOld(e, idioma, anyo);
    }

    public static List<EstudioUIOld> toUI(List<Estudio> le, String idioma, Integer anyo)
    {
        return le
                .stream()
                .map(e -> new EstudioUIOld(e, idioma, anyo))
                .sorted(Comparator.comparing(a -> a.nombre.toUpperCase()))
                .collect(Collectors.toList());
    }

    /**
     * Dado un listado de estudios, devuelve una agrupación basada en tipo de estudio, nivel y ubicación estructural.
     * Las claves del map son las siguientes:
     * <p>
     * * Tipo de estudio: "G", "M"
     * * Nivel: [1,2]
     * * ID de Ubicacinó estructural
     *
     * @param le
     * @param idioma
     * @param anyo
     * @return
     */
    public static Map<String, Map<Integer, Map<Long, List<EstudioUIOld>>>> toUIByEstudioNivelUbicacion(
            List<Estudio> le,
            String idioma,
            Integer anyo
    )
    {

        return le
                .stream()
                .map(e -> new EstudioUIOld(e, idioma, anyo))
                .sorted((a, b) -> {
                    if(a.nombre.contains(b.nombre) || b.nombre.contains(a.nombre)){
                        return b.id.compareTo(a.id);
                    }
                    return a.nombre.toUpperCase().compareTo(b.nombre.toUpperCase());
                })
                .collect(
                        // agrupo por tipo de estudio
                        Collectors.groupingBy(
                                EstudioUIOld::getTipo,
                                // agrupo por nivel --> con docencia / sin docencia
                                Collectors.groupingBy(
                                        EstudioUIOld::getNivel,
                                        // agrupo por ubicación
                                        Collectors.groupingBy(
                                                EstudioUIOld::getUestId

                                        )
                                )
                        )
                );


    }

    public static EstudioUIOld toUI(EstudioTodo estudioTodo, EstudioUrl urls, String idioma, Integer anyo)
    {
        return new EstudioUIOld(estudioTodo, urls, idioma, anyo);
    }

    public Long getId()
    {
        return id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getTipo()
    {
        return tipo;
    }

    public Long getUestId()
    {
        return uestId;
    }

    public Integer getNivel()
    {
        return nivel;
    }

    public String getPresentacion()
    {
        return presentacion;
    }

    public String getFaqURL()
    {
        return this.faqURL;
    }

    public Integer getCursos()
    {
        return cursos;
    }


    public String getUrlVerifica()
    {
        return urlVerifica;
    }

    public CreditoUI getCreditoItinerarioMin()
    {
        return creditoItinerarioMin;
    }

    public String getCalidadUrl()
    {
        return calidadUrl;
    }

    public String getReconocimientoUrl()
    {
        return reconocimientoUrl;
    }

    public Integer getComentarioPortada() {
        return comentarioPortada;
    }

    public String getResponsablesUrl() {
        return responsablesUrl;
    }
}
