package es.uji.apps.lleu.ui;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.uji.apps.lleu.model.Tutorias;

public class ProfesorUI
{
    private Long perId;
    private String personaNombre;
    private List<TutoriasUI> tutorias;
    private String virtuales;

    /**
     * Dado un listado de tutorias creamos estructura para mostrar en las plantillas
     * @param tutorias listado de tutorias
     * @param idioma idioma.
     * @return
     */
    public static List<ProfesorUI> toUI(List<Tutorias> tutorias, String idioma)
    {
        List<ProfesorUI> list = new ArrayList<>();
        for (Tutorias tutoria : tutorias)
        {
            TutoriasUI tut = TutoriasUI.toUI(tutoria, idioma);
            Boolean encontrado = false;
            for (ProfesorUI profesorUI : list)
            {
                if (profesorUI.getPerId().equals(tutoria.getPerId()))
                {
                    encontrado = true;
                    List<TutoriasUI> tutoriasUIList = new ArrayList<TutoriasUI>(profesorUI.getTutorias());
                    tutoriasUIList.add(tut);
                    profesorUI.setTutorias(tutoriasUIList);
                }
            }
            if (!encontrado)
            {
                ProfesorUI profNew = new ProfesorUI();
                profNew.setPerId(tutoria.getPerId());
                profNew.setPersonaNombre(tutoria.getPersonaNombre());
                profNew.setTutorias(Collections.singletonList(tut));
                profNew.setVirtuales(tutoria.getTutorias());
                list.add(profNew);
            }
        }
        return list;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public String getPersonaNombre()
    {
        return personaNombre;
    }

    public void setPersonaNombre(String personaNombre)
    {
        this.personaNombre = personaNombre;
    }

    public List<TutoriasUI> getTutorias()
    {
        return tutorias;
    }

    public void setTutorias(List<TutoriasUI> tutorias)
    {
        this.tutorias = tutorias;
    }

    public String getVirtuales()
    {
        return virtuales;
    }

    public void setVirtuales(String virtuales)
    {
        this.virtuales = virtuales;
    }
}
