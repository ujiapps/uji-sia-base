package es.uji.apps.lleu.ui;

import es.uji.apps.lleu.model.Itinerario;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class ItineratioUI
{
    private String asignaturaId;
    private String asignaturaNombre;
    private String caracter;
    private String creditos;

    public static ItineratioUI toUI(Itinerario itinerario, String idioma)
    {
        return new ItineratioUI(itinerario, idioma);
    }

    private ItineratioUI (Itinerario itinerario, String idioma) {
        this.asignaturaId = itinerario.getAsignaturaId();

        switch (idioma.toLowerCase()) {
            case "en":
                this.asignaturaNombre = itinerario.getNombreAsiEN();
                break;
            case "es":
                this.asignaturaNombre = itinerario.getNombreAsiES();
                break;
            default:
                this.asignaturaNombre = itinerario.getNombreAsiCA();
        }

        this.caracter = LocalizedStrings.getCaracterKey(itinerario.getCaracter());
        this.creditos = new CreditoUI(itinerario.getCreditos()).toString();
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getAsignaturaNombre()
    {
        return asignaturaNombre;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getCreditos()
    {
        return creditos;
    }
}
