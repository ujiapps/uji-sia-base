package es.uji.apps.lleu.ui;

import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.PresentacionCoordinadores;

public class PresentacionCoordinadoresUI
{
    private String nombre;
    private String departamento;

    public static List<PresentacionCoordinadoresUI> toUI (List<PresentacionCoordinadores> lpc, String idioma) {
        return lpc.stream()
                .map((pc) -> new PresentacionCoordinadoresUI(pc, idioma))
                .collect(Collectors.toList());
    }

    private PresentacionCoordinadoresUI(PresentacionCoordinadores pc, String idioma) {

        this.nombre = pc.getNombre();

        switch (idioma) {
            case "es":
                this.departamento = pc.getDepartamentoES();
                break;
            default:
                this.departamento = pc.getDepartamentoCA();
        }
    }

    public CajaInfoUI getCajaInfoUI()
    {
        return new CajaInfoUI(this.nombre, this.departamento);
    }

}
