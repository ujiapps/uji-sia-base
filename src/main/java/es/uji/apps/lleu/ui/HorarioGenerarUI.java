package es.uji.apps.lleu.ui;

import es.uji.apps.lleu.model.HorarioGenerar;
import es.uji.apps.lleu.utils.LocalizedStrings;

import java.util.*;
import java.util.stream.Collectors;

public class HorarioGenerarUI
{
    private String asignaturaId;
    private String nombreAsignatura;
    private Integer cursoAca;
    private Long estudioId;
    private Long curso;
    private String caracter;
    private String semestre;
    private String tipo;
    private Map<String, Map<String, Set<String>>> clasificacion;


    /**
     *
     * @param hg
     * @param idioma
     * @return Map donde las claves son el carácter de la asignatura y el id de asignatura.
     */
    public static Map<String, Map<String, List<HorarioGenerarUI>>> toUI(List<HorarioGenerar> hg, String idioma)
    {
        Map<String, Map<String, List<HorarioGenerarUI>>> result = new TreeMap<>();
        Map<String, Map<String, List<HorarioGenerar>>> caracteres = hg.stream().collect(
                Collectors.groupingBy(HorarioGenerarUI::getCaracterAsignatura,
                        Collectors.groupingBy(HorarioGenerar::getAsignaturaId)
                )
        );

        caracteres.keySet().forEach((as) ->
        {
            Map<String, List<HorarioGenerar>> asignaturas = caracteres.get(as);
            Map<String, List<HorarioGenerarUI>> ret = new TreeMap<>();
            asignaturas.keySet().forEach((asignaturaId) ->
            {
                HorarioGenerarUI ui = new HorarioGenerarUI(asignaturas.get(asignaturaId).get(0), idioma);

                ui.setClasificacion(extraeGrupos(asignaturas, asignaturaId));

                if (ret.get(asignaturaId) != null)
                {
                    ret.get(asignaturaId).add(ui);
                }
                else
                {
                    List<HorarioGenerarUI> l = new ArrayList<>();
                    l.add(ui);
                    ret.put(asignaturaId, l);
                }
            });
            result.put(as, ret);
        });
        return result;
    }

    private static Map<String, Map<String, Set<String>>> extraeGrupos(Map<String, List<HorarioGenerar>> asignaturas, String asignaturaId) {
        return asignaturas.get(asignaturaId)
                .stream()
                .collect(
                        Collectors.groupingBy(HorarioGenerar::getGrupo,
                                Collectors.groupingBy(HorarioGenerar::getSubGrupo,
                                Collectors.mapping(horarioGenerar -> horarioGenerar.getSubGrupoId().toString(),
                                        Collectors.toCollection(TreeSet::new)))));
    }

    public CajaAsignaturaUI getCajaAsignaturaUI() {
        CajaAsignaturaUI ret = new CajaAsignaturaUI(
                this.estudioId,
                this.cursoAca,
                this.nombreAsignatura,
                this.asignaturaId,
                this.curso.toString(),
                this.semestre,
                LocalizedStrings.getCaracterKey(this.caracter),
                null,
                null);

        return ret;
    }

    private HorarioGenerarUI(HorarioGenerar he, String idioma)
    {
        this.asignaturaId = he.getAsignaturaId();

        switch (idioma)
        {
            case "es":
                this.nombreAsignatura = he.getAsiNombreES();
                break;
            case "en":
                this.nombreAsignatura = he.getAsiNombreEN();
                break;
            default:
                this.nombreAsignatura = he.getAsiNombreCA();
                break;
        }

        this.cursoAca = he.getCursoAca();
        this.estudioId = he.getEstudioId();
        this.curso = he.getCurId();
        this.caracter = he.getCaracter();
        this.tipo = he.getTipo();
        if (this.tipo.equals("A"))
        {
            this.semestre = LocalizedStrings.getSemestreKey(this.tipo);

        }
        else
        {
            this.semestre = LocalizedStrings.getSemestreKey(he.getSemestre());
        }
    }


    private static String getAsignatura(HorarioGenerar r)
    {
        return r.getAsignaturaId();
    }

    private static String getCaracterAsignatura(HorarioGenerar h)
    {
        String ret;

        switch (h.getCaracter().toLowerCase())
        {
            case "tr":
            case "ob":
                ret = "personaliza.horarios.basica";
                break;
            case "lc":
            case "op":
                ret = "personaliza.horarios.optativa";
                break;
            case "pf":
                ret = "personaliza.horarios.practicas";
                break;
            default:
                ret = "caracter.desconocido";
        }

        return ret;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombreAsignatura()
    {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura)
    {
        this.nombreAsignatura = nombreAsignatura;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public void setClasificacion(Map<String, Map<String, Set<String>>> clasificacion) {
        this.clasificacion = clasificacion;
    }

    public Map<String, Map<String, Set<String>>> getClasificacion() {
        return clasificacion;
    }
}