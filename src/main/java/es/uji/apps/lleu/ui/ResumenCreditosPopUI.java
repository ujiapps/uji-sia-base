package es.uji.apps.lleu.ui;

import es.uji.apps.lleu.model.ResumenCreditosPop;

public class ResumenCreditosPopUI
{
    private String creditosTotal;
    private String creditosOb;
    private String creditosOp;
    private String creditosFb;
    private String creditosPe;
    private String creditosPf;

    private String creditosEspecificos;

    private String especialidadObligatoria;

    private ResumenCreditosPopUI(ResumenCreditosPop rcp)
    {
        this.creditosEspecificos=rcp.getCreditosEspecificos();
        if (rcp.getCreditosEspecificos().equals("N"))
        {
            this.creditosTotal = rcp.getCreditosTotal() == 0 ? "-" : new CreditoUI(rcp.getCreditosTotal()).toString();
            this.creditosOb = rcp.getCreditosOb() == 0 ? "-" : new CreditoUI(rcp.getCreditosOb()).toString();
            this.creditosOp = rcp.getCreditosOp() == 0 ? "-" : new CreditoUI(rcp.getCreditosOp()).toString();
            this.creditosFb = "-";
            this.creditosFb = "-";
            this.creditosFb = "-";
        }
        else
        {
            this.creditosTotal = rcp.getCreditosTotalEspecificos() == 0 ? "-" : new CreditoUI(rcp.getCreditosTotalEspecificos()).toString();
            this.creditosOb = rcp.getCreditosEspecificosOb() == 0 ? "-" : new CreditoUI(rcp.getCreditosEspecificosOb()).toString();
            this.creditosOp = rcp.getCreditosEspecificosOp() == 0 ? "-" : new CreditoUI(rcp.getCreditosEspecificosOp()).toString();
            this.creditosFb = rcp.getCreditosEspecificosFb() == 0 ? "-" : new CreditoUI(rcp.getCreditosEspecificosFb()).toString();
            this.creditosPe = rcp.getCreditosEspecificosPe() == 0 ? "-" : new CreditoUI(rcp.getCreditosEspecificosPe()).toString();
            this.creditosPf = rcp.getCreditosEspecificosPf() == 0 ? "-" : new CreditoUI(rcp.getCreditosEspecificosPf()).toString();
        }

        this.especialidadObligatoria = rcp.getEspecialidadOblicatoria();
    }

    public static ResumenCreditosPopUI toUI(ResumenCreditosPop rcp)
    {
        return new ResumenCreditosPopUI(rcp);
    }


    public String getCreditosTotal()
    {
        return creditosTotal;
    }

    public String getCreditosOb()
    {
        return creditosOb;
    }

    public String getCreditosOp()
    {
        return creditosOp;
    }

    public String getCreditosFb()
    {
        return creditosFb;
    }

    public String getCreditosPe()
    {
        return creditosPe;
    }

    public String getCreditosPf()
    {
        return creditosPf;
    }

    public String getCreditosEspecificos()
    {
        return creditosEspecificos;
    }

    public String getEspecialidadObligatoria() {
        return especialidadObligatoria;
    }
}
