package es.uji.apps.lleu.ui;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturasGrado;
import es.uji.apps.lleu.model.AsignaturasInformacion;

public class AsignaturaInformacionUI
{
    private String id;
    private String asignaturaId;
    private Integer cursoAca;
    private CreditoUI creditos;
    private String asignaturaDuracion;

    private CreditoUI creditosSuperados;
    private CreditoUI creditosTeoria;
    private CreditoUI creditosPractica;
    private CreditoUI creditosLaboratorio;
    private CreditoUI creditosSeminario;
    private CreditoUI creditosTutoria;
    private CreditoUI creditosEvaluacion;
    private CreditoUI ects;
    private CreditoUI horas;
    private CreditoUI horasAl1;
    private CreditoUI horasNoPresenciales;
    private CreditoUI incompatibilidades;

    private CreditoUI horasTotales;

    private String bloqueado;
    private Date fechaUltima;
    private String asignaturaConocimientos;
    private String asignaturaObjeticos;
    private String asignaturaObjetivo;
    private String asignaturaMetodolocia;
    private String asignaturaEvaluacion;
    private String corregida;
    private CreditoUI horasAl;

    private Set<AsignaturasGrado> asignaturasGrado;

    public static List<AsignaturaInformacionUI> toUI (List<AsignaturasInformacion> lai, String idioma) {
        return lai
                .stream()
                .map(ai -> new AsignaturaInformacionUI(ai, idioma))
                .collect(Collectors.toList());
    }

    public static AsignaturaInformacionUI toUI (AsignaturasInformacion ai, String idioma) {
        if (ai == null) {
            return null;
        }
        return new AsignaturaInformacionUI(ai, idioma);
    }

    private AsignaturaInformacionUI(AsignaturasInformacion ai, String idioma) {
        this.id = ai.getId();
        this.asignaturaId = ai.getAsignaturaId();
        this.cursoAca = ai.getCursoAca();
        this.creditos = new CreditoUI(ai.getCreditos());
        this.asignaturaDuracion = ai.getAsignaturaDuracion();

        this.creditosSuperados = ai.getCreditosSuperados() == null ? null : new CreditoUI(ai.getCreditosSuperados());
        this.creditosTeoria = ai.getCreditosTeoria() == null ? null : new CreditoUI(ai.getCreditosTeoria());
        this.creditosPractica = ai.getCreditosPractica() == null ? null : new CreditoUI(ai.getCreditosPractica());
        this.creditosLaboratorio = ai.getCreditosLaboratorio() == null ? null : new CreditoUI(ai.getCreditosLaboratorio());
        this.creditosSeminario = ai.getCreditosSeminario() == null ? null : new CreditoUI(ai.getCreditosSeminario());
        this.creditosTutoria = ai.getCreditosTutoria() == null ? null : new CreditoUI(ai.getCreditosTutoria());
        this.creditosEvaluacion = ai.getCreditosEvaluacion() == null ? null : new CreditoUI(ai.getCreditosEvaluacion());
        this.ects = ai.getEcts() == null ? null : new CreditoUI(ai.getEcts());
        this.horas = ai.getHoras() == null ? null : new CreditoUI(ai.getHoras());
        this.horasAl1 = ai.getHorasAl1() == null ? null : new CreditoUI(ai.getHorasAl1());
        this.horasNoPresenciales = ai.getHorasNoPresenciales() == null ? null : new CreditoUI(ai.getHorasNoPresenciales());
        this.incompatibilidades = ai.getIncompatibilidades() == null ? null : new CreditoUI(ai.getIncompatibilidades());

        this.horasTotales = new CreditoUI(ai.getHorasAl() + ai.getHorasNoPresenciales());

        this.bloqueado = ai.getBloqueado();
        this.fechaUltima = ai.getFechaUltima();
        this.asignaturaConocimientos = ai.getAsignaturaConocimientos();
        this.asignaturaObjeticos = ai.getAsignaturaObjeticos();
        this.asignaturaObjetivo = ai.getAsignaturaObjetivo();
        this.asignaturaMetodolocia = ai.getAsignaturaMetodolocia();
        this.asignaturaEvaluacion = ai.getAsignaturaEvaluacion();
        this.corregida = ai.getCorregida();
        this.horasAl = ai.getHorasAl() == null ? null : new CreditoUI(ai.getHorasAl());

        this.asignaturasGrado = ai.getAsignaturasGrado();
    }

    public String getId()
    {
        return id;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public CreditoUI getCreditos()
    {
        return creditos;
    }

    public String getAsignaturaDuracion()
    {
        return asignaturaDuracion;
    }

    public CreditoUI getCreditosSuperados()
    {
        return creditosSuperados;
    }

    public CreditoUI getCreditosTeoria()
    {
        return creditosTeoria;
    }

    public CreditoUI getCreditosPractica()
    {
        return creditosPractica;
    }

    public CreditoUI getCreditosLaboratorio()
    {
        return creditosLaboratorio;
    }

    public CreditoUI getCreditosSeminario()
    {
        return creditosSeminario;
    }

    public CreditoUI getCreditosTutoria()
    {
        return creditosTutoria;
    }

    public CreditoUI getCreditosEvaluacion()
    {
        return creditosEvaluacion;
    }

    public CreditoUI getEcts()
    {
        return ects;
    }

    public CreditoUI getHoras()
    {
        return horas;
    }

    public CreditoUI getHorasAl1()
    {
        return horasAl1;
    }

    public CreditoUI getHorasNoPresenciales()
    {
        return horasNoPresenciales;
    }

    public CreditoUI getIncompatibilidades()
    {
        return incompatibilidades;
    }

    public String getBloqueado()
    {
        return bloqueado;
    }

    public Date getFechaUltima()
    {
        return fechaUltima;
    }

    public String getAsignaturaConocimientos()
    {
        return asignaturaConocimientos;
    }

    public String getAsignaturaObjeticos()
    {
        return asignaturaObjeticos;
    }

    public String getAsignaturaObjetivo()
    {
        return asignaturaObjetivo;
    }

    public String getAsignaturaMetodolocia()
    {
        return asignaturaMetodolocia;
    }

    public String getAsignaturaEvaluacion()
    {
        return asignaturaEvaluacion;
    }

    public String getCorregida()
    {
        return corregida;
    }

    public CreditoUI getHorasAl()
    {
        return horasAl;
    }

    public Set<AsignaturasGrado> getAsignaturasGrado()
    {
        return asignaturasGrado;
    }

    public CreditoUI getHorasTotales()
    {
        return horasTotales;
    }
}
