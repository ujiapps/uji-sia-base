package es.uji.apps.lleu.ui;

import es.uji.apps.lleu.model.Asignatura;
import es.uji.apps.lleu.model.AsignaturasInformacion;

public class AsignaturaUI
{

    private String id;
    private String nombre;
    private AsignaturasInformacion asignaturasInformacion;

    public static AsignaturaUI toUI (Asignatura a, String idioma, Integer anyo) {
        return new AsignaturaUI(a, idioma);
    }

    private AsignaturaUI (Asignatura a, String idioma) {
        this.id = a.getId();
        switch (idioma.toLowerCase()) {
            case "es":
                this.nombre = a.getNombreES();
                break;
            case "en":
                this.nombre = a.getNombreEN();
                break;
            default:
                this.nombre = a.getNombreCA();
                break;
        }
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public AsignaturasInformacion getAsignaturasInformacion()
    {
        return asignaturasInformacion;
    }

    public void setAsignaturasInformacion(AsignaturasInformacion asignaturasInformacion)
    {
        this.asignaturasInformacion = asignaturasInformacion;
    }
}
