package es.uji.apps.lleu.ui;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturasMaster;
import es.uji.apps.lleu.utils.LocalizedStrings;

/**
 * Facade de Asignatura. La utilizamos para evitar tener que tratar con los idiomas en el campo nombre a nivel
 * de plantilla. Esta clase debe instanciarse haciendo uso de los métodos toUI().
 */
public class AsignaturaMasterUI
{

    private Long estudioId;
    private String asignaturaId;
    private String nombre;
    private String creditos;
    private String caracter;
    private String tipo;
    private String semestre;
    private String numeroSemestre;
    private Integer cursoAca;
    private Integer curso;
    private String soloConvalida;

    private String caracterStrKey;
    private String semestreStrKey;

    /**
     * Lo utilizamos para ordenar los subgrupos de la siguiente manera:
     *
     *    TE
     *    PR
     *    LA
     *    SE
     *    TU
     *    AV
     */
    private static class SubgrupoComparator implements Comparator<String> {

        private HashMap<String, Integer> orden;

        public SubgrupoComparator()
        {
            this.orden = new HashMap<>(6);
            this.orden.put("AV", 6);
            this.orden.put("TU", 5);
            this.orden.put("SE", 4);
            this.orden.put("LA", 3);
            this.orden.put("PR", 2);
            this.orden.put("TE", 1);
        }

        @Override
        public int compare(String o1, String o2)
        {
            int ret = 0;

            String tipo1 = o1.substring(0, 2);
            String tipo2 = o2.substring(0, 2);

            if (!this.orden.containsKey(tipo1) || !this.orden.containsKey(tipo2)) {
                return 0;
            }

            if (this.orden.get(tipo1) == this.orden.get(tipo2)) {
                Integer subgrupo1 = Integer.parseInt(o1.substring(2));
                Integer subgrupo2 = Integer.parseInt(o2.substring(2));
                if (subgrupo1 > subgrupo2) {
                    ret = 1;
                } else if (subgrupo1 < subgrupo2) {
                    ret = -1;
                }
            } else if (this.orden.get(tipo1) < this.orden.get(tipo2)) {
                ret = -1;
            } else {
                ret = 1;
            }

            return ret;
        }
    }

    /**
     * Dada una colección de asignaturaGrado y un idioma, devuelve una lista de asignaturaGrado UI. Si el idioma no está
     * soportado, utiliza por defecto CA. El listado de asignaturas debe pertenecer al mismo máster.
     *
     * @param asignaturasMaster normalmente un conjunto de asignaturasMaster a tratar
     * @param idioma           el idioma que necesitamos
     * @return una listado de AsignaturaGradoUI con nombre traducido al idioma especificado
     */
    public static List<AsignaturaMasterUI> toUI(Collection<AsignaturasMaster> asignaturasMaster, String idioma)
    {
        return asignaturasMaster.stream()
                .map((a) -> new AsignaturaMasterUI(a, idioma))
                .collect(Collectors.toList());
    }

    /**
     * Dada una asignatura y un idioma, devuelve una instancia de AsignaturaGradoUI. Si el idioma no está soportado,
     * utiliza por defecto, CA
     *
     * @param a      instancia de Asignatura
     * @param idioma el idioma ("CA", "EN", "ES")
     * @return instancia de AsignaturaGradoUI con nombre en el idioma especificado
     */
    public static AsignaturaMasterUI toUI(AsignaturasMaster a, String idioma)
    {
        return new AsignaturaMasterUI(a, idioma);
    }

    private AsignaturaMasterUI(AsignaturasMaster a, String idioma) {
        this.estudioId = a.getEstudioId();
        this.asignaturaId = a.getAsignaturaId();

        switch (idioma.toLowerCase()) {
            case "es":
                this.nombre = a.getNombreES();
                break;
            case "en":
                this.nombre = a.getNombreEN();
                break;
            default:
                this.nombre = a.getNombreCA();
                break;
        }

        this.creditos = new CreditoUI(a.getCreditos()).toString();
        this.caracter = a.getCaracter();
        this.caracterStrKey = LocalizedStrings.getCaracterKey(this.caracter);
        this.soloConvalida = a.getSoloConvalida();

        this.tipo = a.getTipo().toUpperCase();

        if(this.tipo.equals("A")){
            this.semestre = LocalizedStrings.getSemestreKey(this.tipo);
            this.numeroSemestre = "a";

        }else{
            this.semestre = LocalizedStrings.getSemestreKey(a.getSemestre());
            this.numeroSemestre = a.getSemestre().toString();
        }

        switch (this.tipo) {
            case "A":
                this.semestreStrKey = LocalizedStrings.getSemestreKey("a");
                break;

            case "S":
                this.semestreStrKey = LocalizedStrings.getSemestreKey(a.getSemestre());
                break;

            default:
                this.semestreStrKey = null;
        }
        this.cursoAca = a.getCursoAca();

        this.curso = a.getCurso();
    }

    public CajaAsignaturaUI getCajaAsignaturaUI(){

        CajaAsignaturaUI caja = new CajaAsignaturaUI(
                this.estudioId,
                this.cursoAca,
                this.nombre,
                this.asignaturaId,
                (this.curso != null) ? this.curso.toString() : null,
                this.semestreStrKey,
                this.caracterStrKey,
                this.soloConvalida,null
        );
        caja.addInfo(this.creditos, "creditos");
       return caja;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public String getCreditos()
    {
        return creditos;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getTipo()
    {
        return tipo;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public String getCaracterStrKey()
    {
        return caracterStrKey;
    }

    public String getSemestreStrKey()
    {
        return semestreStrKey;
    }

    public String getSoloConvalida()
    {
        return soloConvalida;
    }

    public String getNumeroSemestre() {
        return numeroSemestre;
    }
}
