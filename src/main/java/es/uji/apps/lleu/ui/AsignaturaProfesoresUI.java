package es.uji.apps.lleu.ui;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.AsignaturaProfesor;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class AsignaturaProfesoresUI
{

    private String asiId;
    private String grpId;
    private Integer sgrId;
    private String sgrTipoReal;
    private Integer cursoAca;
    private String sgrTipo;
    private String idioma;
    private Long perId;
    private String listSgrId;

    /**
     * Listado de subgrupoIds por idioma. La clave es el idioma, el valor es un string separado por comas con los
     * idiomas de docencia.
     */
    private Map<String, String> listSgrIdsIdioma;

    private String personaNombre;
    private String semestre;
    private String url;

    public AsignaturaProfesoresUI()
    {

    }

    private AsignaturaProfesoresUI(AsignaturaProfesor asignaturaProfesor, String idioma)
    {
        this.asiId = asignaturaProfesor.getAsiId();
        this.grpId = asignaturaProfesor.getGrpId();
        this.sgrId = asignaturaProfesor.getSgrId();
        this.sgrTipoReal = asignaturaProfesor.getSgrTipoReal();
        this.cursoAca = asignaturaProfesor.getCursoAca();
        this.sgrTipo = asignaturaProfesor.getSgrTipo();

        if (asignaturaProfesor.getCursoAca() < 2013)
        {
            this.idioma = AsignaturaProfesoresUI.getIdiomaOld(asignaturaProfesor.getIdiomaOld());
        }
        else
        {
            switch (idioma.toLowerCase())
            {
                case "en":
                    this.idioma = asignaturaProfesor.getIdiomaEN();
                    break;
                case "es":
                    this.idioma = asignaturaProfesor.getIdiomaES();
                    break;
                default:
                    this.idioma = asignaturaProfesor.getIdiomaCA();
            }
        }
        if (this.idioma != null) {
            this.idioma = this.idioma.replaceAll("<br />", " ");
        }
        this.perId = asignaturaProfesor.getPerId();
        this.personaNombre = asignaturaProfesor.getPersonaNombre();
        this.semestre = LocalizedStrings.getSemestreOnlyDataKey(asignaturaProfesor.getSemestre());
        this.url = asignaturaProfesor.getUrl();
    }

    private static String getIdiomaOld(Integer valor)
    {
        String ret;
        switch (valor)
        {
            case 0:
                ret = "";
                break;
            case 1:
                ret = "Valencià";
                break;
            case 2:
                ret = "Castellà";
                break;
            case 3:
                ret = "Anglés";
                break;
            case 4:
                ret = "Francés";
                break;
            case 5:
                ret = "Alemany";
                break;
            default:
                ret = "";
                break;
        }
        return ret;
    }

    /**
     * Funcion que devuelve el listado de profesores agrupado por grupo, tipo de subgrupo, perid, idioma y semestre en ese order.
     *
     * @param profesores
     * @param idioma
     * @return
     */

    public static Map<String, Map<String, Map<Long, Map<String, List<AsignaturaProfesoresUI>>>>> toUI(List<AsignaturaProfesor> profesores, String idioma)
    {
        //Ricardo tus ojos no veran esto, simplemente confia.
        return profesores.stream()

                .map((ap) -> new AsignaturaProfesoresUI(ap, idioma))

                .collect(Collectors.groupingBy(AsignaturaProfesoresUI::getGrpId, TreeMap::new,

                            Collectors.groupingBy(AsignaturaProfesoresUI::getSgrTipoReal, LinkedHashMap::new,

                                Collectors.groupingBy(AsignaturaProfesoresUI::getPerId,

                                        Collectors.collectingAndThen(Collectors.groupingBy(AsignaturaProfesoresUI::getSemestre), map ->
                                        {
                                            map.forEach((semestre, k) ->
                                            {
                                                List<AsignaturaProfesoresUI> profesoresUI = map.get(semestre);
                                                AsignaturaProfesoresUI profUI = profesoresUI.get(0);

                                                profUI.listSgrIdsIdioma = profesoresUI.stream().collect(
                                                        Collectors.groupingBy(
                                                                AsignaturaProfesoresUI::getIdioma,
                                                                Collectors.mapping(a -> ((AsignaturaProfesoresUI) a).getSgrId().toString(),
                                                                        Collectors.joining(",")
                                                                )
                                                        )
                                                );

                                                map.put(semestre, Collections.singletonList(profUI));
                                            });
                                            return map;
                                        })))));
    }

    public Integer getSgrId()
    {
        return this.sgrId;
    }

    private void copy(AsignaturaProfesoresUI asignaturaProfesor)
    {

        this.asiId = asignaturaProfesor.getAsiId();
        this.grpId = asignaturaProfesor.getGrpId();
        this.sgrId = asignaturaProfesor.getSgrId();
        this.sgrTipoReal = asignaturaProfesor.getSgrTipoReal();
        this.cursoAca = asignaturaProfesor.getCursoAca();
        this.sgrTipo = asignaturaProfesor.getSgrTipo();
        this.idioma = asignaturaProfesor.getIdioma();
        this.perId = asignaturaProfesor.getPerId();
        this.listSgrId = "";
        this.personaNombre = asignaturaProfesor.getPersonaNombre();
        this.semestre = asignaturaProfesor.getSemestre();
        this.url = asignaturaProfesor.getUrl();

        this.listSgrIdsIdioma = new HashMap<>();
    }

    public String getAsiId()
    {
        return asiId;
    }

    public String getGrpId()
    {
        return grpId;
    }

    public String getSgrTipoReal()
    {
        return sgrTipoReal;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public String getSgrTipo()
    {
        return sgrTipo;
    }

    public String getIdioma()
    {
        return idioma;
    }

    public Long getPerId()
    {
        return perId;
    }

    public String getPersonaNombre()
    {
        return personaNombre;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public String getUrl()
    {
        return url;
    }

    public String getListSgrId()
    {
        return listSgrId;
    }

    public void setListSgrId(String listSgrId)
    {
        this.listSgrId = listSgrId;
    }

    public Map<String, String> getListSgrIdsIdioma()
    {
        return listSgrIdsIdioma;
    }
}