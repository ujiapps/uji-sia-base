package es.uji.apps.lleu.ui;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Locale;

import es.uji.apps.lleu.model.Tutorias;
import es.uji.apps.lleu.utils.LocalizedStrings;

public class TutoriasUI implements Serializable
{

    private String horaInicio;
    private String horaFin;
    private String fechaIni;
    private String fechaFin;
    private String semestre;
    private String idDiaSemana;
    private String diaSemana;
    private Integer cursoAca;

    static TutoriasUI toUI(Tutorias tutoria, String idioma)
    {
        return new TutoriasUI(tutoria, idioma);
    }

    private TutoriasUI (Tutorias tutoria, String idioma) {
        this.horaInicio = tutoria.getHoraInicio();
        this.horaFin = tutoria.getHoraFin();

        SimpleDateFormat sdf;

        switch (idioma.toLowerCase())
        {
            case "en":
                sdf = new SimpleDateFormat("d MMMM yyyy", new Locale("en", "EN"));
                this.fechaIni = sdf.format(tutoria.getFechaIni());
                this.fechaFin = sdf.format(tutoria.getFechaFin());
                break;
            case "es":
                sdf = new SimpleDateFormat("d MMMM yyyy", new Locale("es", "ES"));
                this.fechaIni = sdf.format(tutoria.getFechaIni());
                this.fechaFin = sdf.format(tutoria.getFechaFin());
                break;
            default:
                sdf = new SimpleDateFormat("d MMMM yyyy", new Locale("ca", "ES"));
                this.fechaIni = sdf.format(tutoria.getFechaIni());
                this.fechaFin = sdf.format(tutoria.getFechaFin());
        }

        this.semestre = LocalizedStrings.getSemestreOnlyDataKey(tutoria.getSemestre());
        this.idDiaSemana = tutoria.getIdDiaSemana();
        this.diaSemana = tutoria.getIdDiaSemana();
        this.cursoAca = tutoria.getCursoAca();
    }

    public String getHoraInicio()
    {
        return horaInicio;
    }

    public String getHoraFin()
    {
        return horaFin;
    }

    public String getFechaIni()
    {
        return fechaIni;
    }

    public String getFechaFin()
    {
        return fechaFin;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public String getIdDiaSemana()
    {
        return idDiaSemana;
    }

    public String getDiaSemana()
    {
        return diaSemana;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }
}
