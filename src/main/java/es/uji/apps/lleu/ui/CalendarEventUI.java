package es.uji.apps.lleu.ui;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.apps.lleu.model.ExamenesEstudio;
import es.uji.apps.lleu.model.HorarioEstudioCalendar;
import es.uji.commons.rest.ParamUtils;

public class CalendarEventUI
{
    private String descripcion;
    private String summary;
    private String location;
    private String comment;
    private Date ini;
    private Date fin;

    public static List<CalendarEventUI> toUI (List<?> lhe, String idioma)
    {
        return lhe.stream().map((e) ->
                new CalendarEventUI((HorarioEstudioCalendar) e, idioma)).collect(Collectors.toList());
    }

    public static List<CalendarEventUI> toUI(List<ExamenesEstudio> examenes, Map<Long, ConvocatoriaUI> convocatorias, String idioma)
    {
        return examenes.stream().map((e) ->
                new CalendarEventUI(e,convocatorias.get(e.getConvocatoriaId().longValue()), idioma)).collect(Collectors.toList());
    }


    private CalendarEventUI(ExamenesEstudio he, ConvocatoriaUI convocatoriaUI, String idioma) {
        this.summary = he.getAsiId();
        if(ParamUtils.isNotNull(he.getTipo())){
            this.summary=this.summary.concat("-").concat(he.getTipo());
        }

        switch (idioma) {
            case "es":
                this.descripcion = this.summary.concat(" ").concat(he.getNombreAsignaturaES());
                break;
            case "en":
                this.descripcion = this.summary.concat(" ").concat(he.getNombreAsignaturaEN());
                break;
            default:
                this.descripcion = this.summary.concat(" ").concat(he.getNombreAsignaturaCA());
                break;
        }
        this.comment = convocatoriaUI.getNombre();
        this.location = he.getAulas().stream().map(a->a.getAula()).collect(Collectors.joining(","));
        this.ini = he.getIni();
        this.fin = he.getFin();
    }


    private CalendarEventUI(HorarioEstudioCalendar he, String idioma) {

        this.summary = he.getAsignaturaId();
        if(ParamUtils.isNotNull(he.getTipoSubgrupo()) && ParamUtils.isNotNull(he.getSubgrupo())){
            this.summary= this.summary.concat("-").concat(he.getGrupo()).concat("-").concat(he.getTipoSubgrupo().concat(he.getSubgrupo().toString()));
        }

        if(ParamUtils.isNotNull(he.getComentarioDiscontCA())){
            this.comment = he.getComentarioDiscontCA();
        }

        switch (idioma) {
            case "es":
                this.descripcion = this.summary.concat(" ").concat(he.getNombreAsignaturaES());
                break;
            case "en":
                this.descripcion = this.summary.concat(" ").concat(he.getNombreAsignaturaEN());
                break;
            default:
                this.descripcion = this.summary.concat(" ").concat(he.getNombreAsignaturaCA());
                break;
        }

        this.location = he.getAula();
        this.ini = he.getIni();
        this.fin = he.getFin();
    }



    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public Date getIni()
    {
        return ini;
    }

    public void setIni(Date ini)
    {
        this.ini = ini;
    }

    public Date getFin()
    {
        return fin;
    }

    public void setFin(Date fin)
    {
        this.fin = fin;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }
}
