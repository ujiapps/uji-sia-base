package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.Estudio;
import es.uji.apps.lleu.model.EstudioRec;
import es.uji.apps.lleu.model.EstudioRecMaster;
import es.uji.apps.lleu.model.QEstudioRec;
import es.uji.apps.lleu.model.QEstudioRecMaster;
import es.uji.apps.lleu.model.QEstudioTodos;
import es.uji.apps.lleu.model.QReconocimiento;
import es.uji.apps.lleu.model.Reconocimiento;
import es.uji.apps.lleu.model.enums.TipoDocencia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ReconocimientoDAO extends BaseDAODatabaseImpl
{
    private QReconocimiento qReconocimiento = QReconocimiento.reconocimiento;
    private QEstudioRec qEstudioRec = QEstudioRec.estudioRec;
    private QEstudioRecMaster qEstudioRecMaster = QEstudioRecMaster.estudioRecMaster;
    private QEstudioTodos qEstudioTodo = QEstudioTodos.estudioTodos;

    public List<Reconocimiento> getReconocimientos(String asignaturaId, Integer anyo, Long estudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qReconocimiento).join(qReconocimiento.estudioReconocido, qEstudioTodo).where(
                qReconocimiento.asigReconocida.eq(asignaturaId)
                        .and(qReconocimiento.cursoAca.eq(0).or(qReconocimiento.cursoAca.eq(anyo)))
                        .and(qReconocimiento.estudioReconocido.id.eq(estudioId))
        ).orderBy(qReconocimiento.tcoId.asc(),qReconocimiento.asigCursada.asc()).distinct().list(qReconocimiento);
    }

    public List<Reconocimiento> getReconocimientosEstudio(Integer anyo, Long estudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qReconocimiento).where(
            qReconocimiento.cursoAca.eq(anyo).or(qReconocimiento.cursoAca.eq(0)),
            qReconocimiento.estudioReconocido.id.eq(estudioId)
        ).orderBy(qReconocimiento.tcoId.asc(),qReconocimiento.asigCursada.asc()).distinct().list(qReconocimiento);
    }

    /**
     * Indica si un estudio tiene reconocimientos por experiencia laboral y CFGs/Grau
     * @param estudioId
     * @param anyo
     * @return
     */
    public EstudioRec getEstudioRec(Long estudioId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        QEstudioRec qEstudioRec2 = new QEstudioRec("estudiorec2");


        return query.from(qEstudioRec).where(
                qEstudioRec.estudioId.eq(estudioId)
                        .and(qEstudioRec.cursoAcaDesde.eq(new JPASubQuery().from(qEstudioRec2)
                                .where(qEstudioRec.estudioId.eq(qEstudioRec2.estudioId)
                                        .and(qEstudioRec2.cursoAcaDesde.loe(anyo))).unique(qEstudioRec2.cursoAcaDesde.max())))
        ).uniqueResult(qEstudioRec);

    }

    public EstudioRecMaster getEstudioRecMaster ( Long estudioId ) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qEstudioRecMaster).where(
                qEstudioRecMaster.estudioId.eq(estudioId)
        ).uniqueResult(qEstudioRecMaster);
    }

    public List<Reconocimiento> getReconocimientos(String asignaturaId, Integer anyo, Estudio estudio)
    {
        Integer cursoAca;
        if (estudio.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {
            cursoAca = 0;
        } else {
            cursoAca = anyo;
        }

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qReconocimiento).where(qReconocimiento.asigReconocida.eq(asignaturaId)
                .and(qReconocimiento.cursoAca.eq(cursoAca))
                .and(qReconocimiento.estudioReconocido.id.eq(estudio.getId())))
                .orderBy(qReconocimiento.tcoId.asc(),qReconocimiento.asigCursada.asc()).distinct().list(qReconocimiento);
    }
}
