package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.EstiloAsignatura;
import es.uji.apps.lleu.model.QEstiloAsignatura;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EstiloDAO extends BaseDAODatabaseImpl
{
    QEstiloAsignatura qEstiloAsignatura = QEstiloAsignatura.estiloAsignatura;

    public List<EstiloAsignatura> getAsignaturas (Long estudioId, Integer cursoAca) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qEstiloAsignatura).where(
                qEstiloAsignatura.estudioId.eq(estudioId),
                qEstiloAsignatura.cursoAca.eq(cursoAca)
        ).orderBy(qEstiloAsignatura.asignaturaId.asc()).list(qEstiloAsignatura);
    }
}
