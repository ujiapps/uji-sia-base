package es.uji.apps.lleu.dao;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.StringExpression;

import es.uji.apps.lleu.model.AsignaturaComentarios;
import es.uji.apps.lleu.model.AsignaturaSoloRecGrado;
import es.uji.apps.lleu.model.AsignaturasGrado;
import es.uji.apps.lleu.model.QAsignaturaComentarios;
import es.uji.apps.lleu.model.QAsignaturaSoloRecGrado;
import es.uji.apps.lleu.model.QAsignaturasGrado;
import es.uji.apps.lleu.model.QAsignaturasInformacion;
import es.uji.apps.lleu.model.QGrupoTeoriaGrado;
import es.uji.apps.lleu.model.QGuiasIngles;
import es.uji.apps.lleu.model.QVacantesGrupoGrado;
import es.uji.apps.lleu.model.QVacantesSubgrupoGrado;
import es.uji.apps.lleu.model.VacantesGrupoGrado;
import es.uji.apps.lleu.model.VacantesSubgrupoGrado;
import es.uji.apps.lleu.model.enums.CaracterAsignatura;
import es.uji.apps.lleu.model.enums.TipoComentario;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.InfoFiltro;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.StringUtils;

@Repository
public class AsignaturasGradoDAO extends BaseDAODatabaseImpl {
    private QAsignaturasGrado qAsignaturasGrado = QAsignaturasGrado.asignaturasGrado;
    private QVacantesSubgrupoGrado qVacantesSubgrupoGrado = QVacantesSubgrupoGrado.vacantesSubgrupoGrado;
    private QAsignaturasInformacion qAsignaturasInformacion = QAsignaturasInformacion.asignaturasInformacion;
    private QGrupoTeoriaGrado qGrupoTeoriaGrado = QGrupoTeoriaGrado.grupoTeoriaGrado;
    private QGuiasIngles qGuiasIngles = QGuiasIngles.guiasIngles;
    private QAsignaturaComentarios qAsignaturaComentarios = QAsignaturaComentarios.asignaturaComentarios;
    private QAsignaturaSoloRecGrado qAsignaturaSoloRecGrado = QAsignaturaSoloRecGrado.asignaturaSoloRecGrado;

    /**
     * Dado un estudioId de estudio y año, obtiene un listado de asignaturas sin filtrar.
     *
     * @param estudioId el estudioId de estudio
     * @param anyo      el año (curso académico) que queremos obtener
     * @return un listado de asignaturas
     */
    public List<AsignaturasGrado> getAsignaturasByIdAnyo(Long estudioId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturasGrado).where(qAsignaturasGrado.estudioId.eq(estudioId).and(qAsignaturasGrado.cursoAca.eq(anyo)));
        query.orderBy(qAsignaturasGrado.asignaturaId.asc());
        return query.distinct().list(qAsignaturasGrado);
    }

    /**
     * Devuelve una asignatura dada
     *
     * @param asignaturaId
     * @param estudioId
     * @param anyo
     * @return
     */
    public AsignaturasGrado getAsignatura(String asignaturaId, Long estudioId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        return query
                .from(qAsignaturasGrado)
                .join(qAsignaturasGrado.asignaturasInformacion, qAsignaturasInformacion).fetch()
                .where(
                        qAsignaturasGrado.asignaturaId.eq(asignaturaId),
                        qAsignaturasGrado.estudioId.eq(estudioId),
                        qAsignaturasGrado.cursoAca.eq(anyo)
                )
                .uniqueResult(qAsignaturasGrado);
    }

    public List<VacantesSubgrupoGrado> getVacantesSubgrupoGrado(Integer anyo, Long estudioId, List<String> asignaturaIds) {

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qVacantesSubgrupoGrado).where(
                qVacantesSubgrupoGrado.cusoAca.eq(anyo),
                qVacantesSubgrupoGrado.asignaturaId.in(asignaturaIds),
                qVacantesSubgrupoGrado.estudioId.eq(estudioId)
        ).list(qVacantesSubgrupoGrado);
    }

    /**
     * Dado un estudioId de estudio, un año e información de filtrado, devuelve el listado de asignaturas correspondiente.
     * OJO. Filtra los grupos V y W (en la propia vista).
     *
     * @param estudioId
     * @param anyo
     * @param filtro
     * @return listado de asignaturas filtradas.
     */
    public List<AsignaturasGrado> getAsignaturasByFilter(Long estudioId, Integer anyo, FiltroEstudio filtro) {

        BooleanExpression cond = qAsignaturasGrado.estudioId.eq(estudioId)
                .and(qAsignaturasGrado.cursoAca.eq(anyo));

        if (filtro.getCursos() != null) {
            cond = cond.and(qAsignaturasGrado.curId.in(filtro.getCursos()));
        }
        if (filtro.getSemestres() != null) {
            // las asignaturas anuales salen tanto si es primer semestre como sengundo
            cond = cond.and(
                    qAsignaturasGrado.semestre.in(
                            filtro.getSemestres()
                    ).or(qAsignaturasGrado.tipo.eq("A").or(qAsignaturasGrado.semestre.eq("12")))
            );
        }
        if (filtro.getNombre() != null) {
            StringExpression nombre;
            switch (filtro.getIdioma()) {
                case "es":
                    nombre = qAsignaturasGrado.asiNombreBusquedaES;
                    break;
                case "en":
                    nombre = qAsignaturasGrado.asiNombreBusquedaEN;
                    break;
                case "ca":
                default:
                    nombre = qAsignaturasGrado.asiNombreBusquedaCA;
                    break;
            }

            // Buscamos por nombre o por código
            String data = StringUtils.limpiaAcentos(filtro.getNombre().toUpperCase());
            cond = cond.and(
                    nombre.like("%" + data + "%").or(
                            qAsignaturasGrado.asignaturaId.like("%" + data + "%")
                    )
            );
        }
        if (filtro.getCaracteres() != null) {
            // Para el filtrado por caracteres hay que tener en cuenta la columna caracter y EEP
            // que indica estancia en prácticas.

            HashSet<String> sqlCaracter = new HashSet<>(5);

            // indica si añadir estancia en prácticas
            boolean addEEP = false;

            for (CaracterAsignatura ca : filtro.getCaracteres()) {
                for (String s : ca.getCaracterColumnValue()) {
                    sqlCaracter.add(s);
                    if (ca.equals(CaracterAsignatura.PRACTICAS)) {
                        addEEP = true;
                    }
                }
            }

            if (addEEP) {
                cond = cond.and(qAsignaturasGrado.caracter.in(sqlCaracter).or(qAsignaturasGrado.eep.eq("S")));
            } else {
                cond = cond.and(qAsignaturasGrado.caracter.in(sqlCaracter).and(qAsignaturasGrado.eep.eq("N")));
            }
        }


        JPAQuery query = new JPAQuery(entityManager);

        query.from(qAsignaturasGrado)
                .join(qAsignaturasGrado.asignaturasInformacion, qAsignaturasInformacion).fetch()
                .where(cond);

        if (filtro.getOrden() != null) {
            switch (filtro.getOrden().getValue()) {
                case "curso":
                    query.orderBy(qAsignaturasGrado.curId.asc(), qAsignaturasGrado.asignaturaId.asc());
                    break;
                case "codigo":
                    query.orderBy(qAsignaturasGrado.asignaturaId.asc(), qAsignaturasGrado.curId.asc());
                    break;
                default:
                    query.orderBy(qAsignaturasGrado.curId.asc(), qAsignaturasGrado.asignaturaId.asc());
            }
        } else {
            query.orderBy(qAsignaturasGrado.curId.asc(), qAsignaturasGrado.asignaturaId.asc());
        }
        return query.list(qAsignaturasGrado);
    }

    /**
     * Devuelve las asignaturas que sólo convalidan. Útil en reconocimientos.
     *
     * @param estudioId
     * @param anyo
     * @return
     */
    public List<AsignaturaSoloRecGrado> getAsignaturasSoloRec(Long estudioId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qAsignaturaSoloRecGrado).where(
                qAsignaturaSoloRecGrado.estudioId.eq(estudioId),
                qAsignaturaSoloRecGrado.cursoAca.eq(anyo)
        ).list(qAsignaturaSoloRecGrado);
    }

    /**
     * Devuelve un resumen de datos acerca de las asignaturas de un estudio. Lo utilizamos para los filtros.
     *
     * @param estudioId el estudio
     * @param anyo      el curso académico
     */

    public InfoFiltro getInfoFiltros(Long estudioId, Integer anyo) {
        // semestres
        JPAQuery query = new JPAQuery(entityManager);
        //List<Tuple> list = query.from(qAsignaturasGrado)

        // obtengo un listado de los semestres disponibles
        List<String> semestres = query
                .from(qAsignaturasGrado)
                .where(
                        qAsignaturasGrado.estudioId.eq(estudioId),
                        qAsignaturasGrado.cursoAca.eq(anyo)
                )
                .distinct()
                .orderBy(qAsignaturasGrado.semestre.asc())
                .list(qAsignaturasGrado.tipo, qAsignaturasGrado.semestre)
                .stream()
                .map(tupla -> {
                    String ret;
                    if (tupla.get(qAsignaturasGrado.tipo).equals("A")) {
                        ret = tupla.get(qAsignaturasGrado.tipo);
                    } else {
                        ret = tupla.get(qAsignaturasGrado.semestre);
                    }
                    return ret;
                })
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        // cursos
        query = new JPAQuery(entityManager);
        List<Long> cursos = query.from(qAsignaturasGrado)
                .where(
                        qAsignaturasGrado.estudioId.eq(estudioId),
                        qAsignaturasGrado.cursoAca.eq(anyo)
                ).distinct().orderBy(qAsignaturasGrado.curId.asc()).list(qAsignaturasGrado.curId);

        // grupos
        query = new JPAQuery(entityManager);
        List<String> grupos = query.from(qGrupoTeoriaGrado)
                .where(
                        qGrupoTeoriaGrado.estudioId.eq(estudioId),
                        qGrupoTeoriaGrado.cursoAca.eq(anyo),
                        qGrupoTeoriaGrado.curId.loe(6)
                ).distinct().orderBy(qGrupoTeoriaGrado.grupo.asc()).list(qGrupoTeoriaGrado.grupo);

        // caracteres
        query = new JPAQuery(entityManager);
        List<String> caracteres = query.from(qAsignaturasGrado)
                .where(
                        qAsignaturasGrado.estudioId.eq(estudioId),
                        qAsignaturasGrado.cursoAca.eq(anyo)
                ).distinct().list(qAsignaturasGrado.caracter);

        query = new JPAQuery(entityManager);
        boolean hasEEP = query.from(qAsignaturasGrado)
                .where(
                        qAsignaturasGrado.estudioId.eq(estudioId),
                        qAsignaturasGrado.cursoAca.eq(anyo),
                        qAsignaturasGrado.eep.eq("S")
                ).exists();

        List<CaracterAsignatura> lcaracteres = CaracterAsignatura.getInstancesFromSQLValues(caracteres);
        if (hasEEP && !lcaracteres.contains(CaracterAsignatura.PRACTICAS)) {
            lcaracteres.add(CaracterAsignatura.PRACTICAS);
        }

        return new InfoFiltro(cursos, grupos, semestres, lcaracteres);
    }

    /**
     * Indica si tiene una guía docente en inglés
     *
     * @param asignaturaId
     * @param cursoAca
     * @return
     */
    public boolean tieneGuiaDocenteEnIngles(String asignaturaId, Integer cursoAca) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qGuiasIngles).where(
                qGuiasIngles.asignaturaId.eq(asignaturaId),
                qGuiasIngles.cursoAcaIni.loe(cursoAca).and(
                        qGuiasIngles.cursoAcaFin.isNull().or(
                                qGuiasIngles.cursoAcaFin.goe(cursoAca)
                        )
                )).exists();
    }

    public List<VacantesGrupoGrado> getVacantesAsignatura(Integer anyo, List<AsignaturasGrado> asignaturasGrados) {
        // TODO: Sería interesante revisar los procedimientos de cálculo de vacantes en la vista LLEU_EXT_VACANTES_GRUPO_GRADO.
        List<String> ids = asignaturasGrados.stream().map(AsignaturasGrado::getAsignaturaId).collect(Collectors.toList());
        QVacantesGrupoGrado qVacanteGrupoGrado = QVacantesGrupoGrado.vacantesGrupoGrado;
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qVacanteGrupoGrado).where(
                qVacanteGrupoGrado.cusoAca.eq(anyo),
                qVacanteGrupoGrado.asignaturaId.in(ids)
        ).list(qVacanteGrupoGrado);
    }

    public List<VacantesGrupoGrado> getVacantesAsignatura(Integer anyo, String asignaturaId) {
        QVacantesGrupoGrado qVacanteGrupoGrado = QVacantesGrupoGrado.vacantesGrupoGrado;
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qVacanteGrupoGrado).where(
                qVacanteGrupoGrado.cusoAca.eq(anyo),
                qVacanteGrupoGrado.asignaturaId.eq(asignaturaId)
        ).list(qVacanteGrupoGrado);
    }

    public List<AsignaturaComentarios> getAsignaturaComentarios(AsignaturasGrado a, Long estudioId, Integer anyo, TipoComentario tipo) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturaComentarios).where(
                qAsignaturaComentarios.estudioId.eq(estudioId).or(qAsignaturaComentarios.estudioId.isNull()),
                qAsignaturaComentarios.asignaturaId.eq(a.getAsignaturaId()).or(qAsignaturaComentarios.asignaturaId.isNull()),
                qAsignaturaComentarios.cursoId.eq(a.getCurId()).or(qAsignaturaComentarios.cursoId.isNull()),
                qAsignaturaComentarios.cursoAca.eq(anyo).or(qAsignaturaComentarios.cursoAca.isNull()),
                qAsignaturaComentarios.tipo.eq(tipo.toString()));
        return query.orderBy(qAsignaturaComentarios.orden.asc()).list(qAsignaturaComentarios);
    }
}
