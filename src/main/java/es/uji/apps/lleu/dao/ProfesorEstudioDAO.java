package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.AsignaturaProfesorMaster;
import es.uji.apps.lleu.model.ProfesorGrado;
import es.uji.apps.lleu.model.ProfesorMaster;
import es.uji.apps.lleu.model.QAsignaturaProfesorMaster;
import es.uji.apps.lleu.model.QProfesorGrado;
import es.uji.apps.lleu.model.QProfesorMaster;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ProfesorEstudioDAO extends BaseDAODatabaseImpl
{
    private QProfesorMaster qProfesorMaster = QProfesorMaster.profesorMaster;
    private QProfesorGrado qProfesorGrado = QProfesorGrado.profesorGrado;

    private QAsignaturaProfesorMaster qAsignaturaProfesorMaster = QAsignaturaProfesorMaster.asignaturaProfesorMaster;

    public List<ProfesorMaster> getProfesoresMaster(Long estudioId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qProfesorMaster).where(qProfesorMaster.estudioId.eq(estudioId).and(qProfesorMaster.cursoAca.eq(anyo)));
        query.orderBy(qProfesorMaster.nombre.asc());
        return query.list(qProfesorMaster);
    }

    public List<ProfesorGrado> getProfesoresGrado(Long estudioId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qProfesorGrado)
                .where(
                        qProfesorGrado.estudioId.eq(estudioId),
                        qProfesorGrado.cursoAca.eq(anyo)
                ).orderBy(
                        qProfesorGrado.perNombre.asc()
                ).list(qProfesorGrado);
    }

    public List<AsignaturaProfesorMaster> getProfesoresMasterByAsig(String asignaturaId, Integer anyo)
    {
            JPAQuery query = new JPAQuery(entityManager);
            query.from(qAsignaturaProfesorMaster).where(qAsignaturaProfesorMaster.asiId.eq(asignaturaId).and(qAsignaturaProfesorMaster.cursoAca.eq(anyo)));
            query.orderBy(qAsignaturaProfesorMaster.personaNombre.asc());
            return query.list(qAsignaturaProfesorMaster);
    }
}
