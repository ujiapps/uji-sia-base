package es.uji.apps.lleu.dao;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import es.uji.commons.rest.ParamUtils;
import org.springframework.stereotype.Repository;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;

import es.uji.apps.lleu.model.HorarioAsignaturaGeneral;
import es.uji.apps.lleu.model.HorarioEstudioCalendar;
import es.uji.apps.lleu.model.HorarioEstudioDetallado;
import es.uji.apps.lleu.model.HorarioEstudioGeneral;
import es.uji.apps.lleu.model.QAsignaturaCircuito;
import es.uji.apps.lleu.model.QHorarioAsignaturaGeneral;
import es.uji.apps.lleu.model.QHorarioEstudioCalendar;
import es.uji.apps.lleu.model.QHorarioEstudioDetallado;
import es.uji.apps.lleu.model.QHorarioEstudioGeneral;
import es.uji.apps.lleu.model.enums.CaracterAsignatura;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.FiltroGenerarHorario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class HorariosEstudioDAO extends BaseDAODatabaseImpl {
    private QHorarioEstudioGeneral qHorarioEstudioGeneral = QHorarioEstudioGeneral.horarioEstudioGeneral;
    private QHorarioEstudioDetallado qHorariosEstudioDetallado = QHorarioEstudioDetallado.horarioEstudioDetallado;

    private QHorarioAsignaturaGeneral qHorarioAsignaturaGeneral = QHorarioAsignaturaGeneral.horarioAsignaturaGeneral;

    private QAsignaturaCircuito qAsignaturaCircuito = QAsignaturaCircuito.asignaturaCircuito;

    private QHorarioEstudioCalendar qHorariosEstudioCalendar = QHorarioEstudioCalendar.horarioEstudioCalendar;

    /**
     * Devuelve el horario general de un estudio (el que utilizamos en la vista general de los horarios).
     *
     * @param estudioId
     * @param cursoAca
     * @param fe
     * @return
     */
    public List<HorarioEstudioGeneral> getHorarioEstudioGeneral(Long estudioId, Integer cursoAca, FiltroEstudio fe) {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where = qHorarioEstudioGeneral.estudioId.eq(estudioId).and(
                qHorarioEstudioGeneral.cursoAca.eq(cursoAca).and(qHorarioEstudioGeneral.diaSemana.in(1, 2, 3, 4, 5, 6))
        );
        if (fe.getCursos() != null) {
            where = where.and(qHorarioEstudioGeneral.curso.in(fe.getCursos()));
        }
        if (fe.getSemestres() != null) {

            List<String> semestres = new ArrayList<String>(2);
            if (fe.getSemestres().contains("1")) {
                semestres.add("1");
            }
            if (fe.getSemestres().contains("2")) {
                semestres.add("2");
            }
            if (semestres.size() > 0) {
                where = where.and(qHorarioEstudioGeneral.semestre.in(semestres));
            } else if (fe.getSemestres().contains("a") || fe.getSemestres().contains("A")) {
                where = where.and(qHorarioEstudioGeneral.tipo.in("a", "A"));
            }

        }
        if (fe.getCaracteres() != null) {
            HashSet<String> sqlCaracter = new HashSet<>(5);
            for (CaracterAsignatura ca : fe.getCaracteres()) {
                for (String s : ca.getCaracterColumnValue()) {
                    sqlCaracter.add(s);
                }
            }
            where = where.and(qHorarioEstudioGeneral.caracter.in(sqlCaracter));
        }
        if (ParamUtils.isNotNull(fe.getGrupos())) {
            where = where.and(
                    qHorarioEstudioGeneral.grupo.in(
                            fe.getGrupos()
                    )
            );
        }
        query.from(qHorarioEstudioGeneral);
        query.where(where);
        List<HorarioEstudioGeneral> lista = query.orderBy(
                qHorarioEstudioGeneral.ini.asc(),
                qHorarioEstudioGeneral.asignaturaId.asc(),
                qHorarioEstudioGeneral.tipoSubgrupo.asc(),
                qHorarioEstudioGeneral.subgrupo.asc()).list(qHorarioEstudioGeneral);

        return lista;
    }

    /**
     * Devuelve un listado de horarios de estudio dado un id de estudio, un curso académico unas opciones de filtrado
     * y un intervalo de fechas
     *
     * @param estudioId
     * @param cursoAca
     * @param fe
     * @param from
     * @param to
     * @return
     */
    public List<HorarioEstudioDetallado> getHorariosEstudio(Long estudioId, Integer cursoAca, FiltroEstudio fe, Date from, Date to) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qHorariosEstudioDetallado);

        BooleanExpression where = qHorariosEstudioDetallado.estudioId.eq(estudioId).and(qHorariosEstudioDetallado.cursoAca.eq(cursoAca));

        // datos procedentes de filtros
        if (fe.getCursos() != null) {
            where = where.and(qHorariosEstudioDetallado.curso.in(fe.getCursos()));
        }
        if (fe.getSemestres() != null) {
            where = where.and(
                    qHorariosEstudioDetallado.semestre.in(
                            fe.getSemestres()
                    ).or(qHorariosEstudioDetallado.tipo.eq("A"))
            );
        }
        if (fe.getCaracteres() != null) {
            HashSet<String> sqlCaracter = new HashSet<>(5);
            for (CaracterAsignatura ca : fe.getCaracteres()) {
                for (String s : ca.getCaracterColumnValue()) {
                    sqlCaracter.add(s);
                }
            }
            where = where.and(qHorariosEstudioDetallado.caracter.in(sqlCaracter));
        }
        if (fe.getGrupos() != null) {
            where = where.and(
                    qHorariosEstudioDetallado.grupoTeoria.in(
                            fe.getGrupos()
                    )
            );
        }

        // intervalo temporal
        where = where.and(qHorariosEstudioDetallado.ini.goe(from)).and(qHorariosEstudioDetallado.fin.loe(to));
        return query.where(where).orderBy(
                qHorariosEstudioDetallado.ini.asc(),
                qHorariosEstudioDetallado.asignaturaId.asc(),
                qHorariosEstudioDetallado.tipoSubgrupo.asc(),
                qHorariosEstudioDetallado.subgrupo.asc()
        ).list(qHorariosEstudioDetallado);

    }

    public List<Long> getPrimerLectivos(Long estudioId, Integer anyo) {
        QHorarioEstudioDetallado qHorariosEstudio1 = new QHorarioEstudioDetallado("horariosEstudio");
        QHorarioEstudioDetallado qHorariosEstudio2 = new QHorarioEstudioDetallado("horariosEstudio");

        List<Long> list = new ArrayList<>();

        JPAQuery semestre1 = new JPAQuery(entityManager);

        semestre1.from(qHorariosEstudio1)
                .where(qHorariosEstudio1.estudioId.eq(estudioId)
                        .and(qHorariosEstudio1.cursoAca.eq(anyo))
                        .and(qHorariosEstudio1.semestre.eq("1")));

        if (semestre1.uniqueResult(qHorariosEstudio1.ini.min()) != null) {
            list.add(semestre1.uniqueResult(qHorariosEstudio1.ini.min()).getTime());
        } else {
            list.add((new Date()).getTime());
        }

        JPAQuery semestre2 = new JPAQuery(entityManager);
        semestre2.from(qHorariosEstudio2)
                .where(qHorariosEstudio2.estudioId.eq(estudioId)
                        .and(qHorariosEstudio2.cursoAca.eq(anyo))
                        .and(qHorariosEstudio2.semestre.eq("2")));

        if (semestre2.uniqueResult(qHorariosEstudio2.ini.min()) != null) {
            list.add(semestre2.uniqueResult(qHorariosEstudio2.ini.min()).getTime());
        } else {
            list.add((new Date()).getTime());
        }

        return list;
    }

    /**
     * Devuelve el timestamp del último día lectivo que tenemos para el estudio y curso académico dado.
     *
     * @param estudioId
     * @param anyo
     * @return
     */
    public Long getUltimoLectivo(Long estudioId, Integer anyo, FiltroEstudio fe) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qHorariosEstudioDetallado);

        BooleanExpression where = qHorariosEstudioDetallado.estudioId.eq(estudioId).and(qHorariosEstudioDetallado.cursoAca.eq(anyo));

        // datos procedentes de filtros
        if (fe.getCursos() != null) {
            where = where.and(qHorariosEstudioDetallado.curso.in(fe.getCursos()));
        }
        if (fe.getSemestres() != null) {
            where = where.and(
                    qHorariosEstudioDetallado.semestre.in(
                            fe.getSemestres()
                    ).or(qHorariosEstudioDetallado.tipo.eq("A"))
            );
        }
        if (fe.getCaracteres() != null) {
            HashSet<String> sqlCaracter = new HashSet<>(5);
            for (CaracterAsignatura ca : fe.getCaracteres()) {
                for (String s : ca.getCaracterColumnValue()) {
                    sqlCaracter.add(s);
                }
            }
            where = where.and(qHorariosEstudioDetallado.caracter.in(sqlCaracter));
        }
        if (fe.getGrupos() != null) {
            where = where.and(
                    qHorariosEstudioDetallado.grupoTeoria.in(
                            fe.getGrupos()
                    )
            );
        }

        Date ret = query.where(where).uniqueResult(qHorariosEstudioDetallado.ini.max());
        return ret == null ? 0l : ret.getTime();
    }


    public List<HorarioAsignaturaGeneral> getHorariosGenerados(Long estudioId, Integer cursoAca, List<FiltroGenerarHorario> filtros) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qHorarioAsignaturaGeneral);

        BooleanExpression where = qHorarioAsignaturaGeneral.estudioId.eq(estudioId).and(qHorarioAsignaturaGeneral.cursoAca.eq(cursoAca));
        BooleanBuilder queryFiltro = new BooleanBuilder();
        for (FiltroGenerarHorario filtro : filtros) {

            BooleanBuilder filtroSubGrupos = new BooleanBuilder();
            filtro.getSubgrupos().keySet().forEach(
                    (subgrupo) ->
                            filtroSubGrupos.or(qHorarioAsignaturaGeneral.tipoSubgrupo.eq(subgrupo).and(qHorarioAsignaturaGeneral.subgrupo.eq(filtro.getSubgrupos().get(subgrupo))))
            );
            filtroSubGrupos.or(qHorarioAsignaturaGeneral.tipoSubgrupo.eq("TE"));
            queryFiltro.or(
                    qHorarioAsignaturaGeneral.asignaturaId.eq(filtro.getAsignatura()).and(qHorarioAsignaturaGeneral.grupo.eq(filtro.getGrupo())).and(filtroSubGrupos)
            );
        }
        query.where(where.and(queryFiltro));
        return query.list(qHorarioAsignaturaGeneral);
    }


    /**
     * Devuelve los eventos de estudio asociados a un circuito concreto, en un estudio concreto y curso académico
     * concreto.
     *
     * @param estudioId
     * @param cursoAca
     * @param circuitoId
     * @param semestre
     * @return
     */
    public List<HorarioAsignaturaGeneral> getHorariosCircuito(Long estudioId, Integer cursoAca, Long circuitoId, Integer semestre) {

        JPAQuery query = new JPAQuery(entityManager);
        return query
                .from(qHorarioAsignaturaGeneral)
                .where(
                        qHorarioAsignaturaGeneral.estudioId.eq(estudioId),
                        qHorarioAsignaturaGeneral.cursoAca.eq(cursoAca),
                        qHorarioAsignaturaGeneral.semestre.eq(semestre.toString()),
                        qHorarioAsignaturaGeneral.asignaturaId.in(
                                new JPASubQuery()
                                        .from(qAsignaturaCircuito)
                                        .where(
                                                qAsignaturaCircuito.circuitoId.eq(circuitoId),
                                                qAsignaturaCircuito.estudioId.eq(estudioId),
                                                qAsignaturaCircuito.cursoAca.eq(cursoAca),
                                                qAsignaturaCircuito.grupo.eq(qHorarioAsignaturaGeneral.grupo),
                                                qAsignaturaCircuito.subgrupoId.eq(qHorarioAsignaturaGeneral.subgrupo),
                                                qAsignaturaCircuito.subgrupoTipo.eq(qHorarioAsignaturaGeneral.tipoSubgrupo)
                                        )
                                        .distinct()
                                        .list(qAsignaturaCircuito.asignaturaId)
                        )
                )
                .distinct()
                .list(qHorarioAsignaturaGeneral);

    }

    public List<HorarioEstudioDetallado> getHorariosCircuitoByDate(Long estudioId, Integer cursoAca, Long circuitoId, Integer semestre, LocalDate dateFrom, LocalDate dateTo) {
        JPAQuery query = new JPAQuery(entityManager);

        return query
                .from(qHorariosEstudioDetallado)
                .where(
                        qHorariosEstudioDetallado.estudioId.eq(estudioId),
                        qHorariosEstudioDetallado.cursoAca.eq(cursoAca),
                        qHorariosEstudioDetallado.semestre.eq(semestre.toString()),
                        qHorariosEstudioDetallado.ini.goe(Date.from(dateFrom.atStartOfDay(ZoneId.of("UTC")).toInstant())),
                        qHorariosEstudioDetallado.fin.loe(Date.from(dateTo.atStartOfDay(ZoneId.of("UTC")).toInstant())),

                        qHorariosEstudioDetallado.asignaturaId.in(
                                new JPASubQuery()
                                        .from(qAsignaturaCircuito)
                                        .where(
                                                qAsignaturaCircuito.circuitoId.eq(circuitoId),
                                                qAsignaturaCircuito.estudioId.eq(estudioId),
                                                qAsignaturaCircuito.cursoAca.eq(cursoAca),
                                                qAsignaturaCircuito.grupo.eq(qHorariosEstudioDetallado.grupo),
                                                qAsignaturaCircuito.subgrupoId.eq(qHorariosEstudioDetallado.subgrupo),
                                                qAsignaturaCircuito.subgrupoTipo.eq(qHorariosEstudioDetallado.tipoSubgrupo)
                                        )
                                        .distinct()
                                        .list(qAsignaturaCircuito.asignaturaId)
                        )

                )
                .distinct()
                .list(qHorariosEstudioDetallado);

    }

    public List<HorarioEstudioCalendar> getEventosCalendario(Long estudioId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qHorariosEstudioCalendar).where(qHorariosEstudioCalendar.estudioId.eq(estudioId).and(qHorariosEstudioCalendar.cursoAca.eq(anyo)));

        return query.list(qHorariosEstudioCalendar);
    }

    public Map<String, Long> getMinEventos(Long estudioId, Integer anyo, FiltroEstudio fe) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qHorariosEstudioDetallado);

        BooleanExpression where = qHorariosEstudioDetallado.estudioId.eq(estudioId).and(qHorariosEstudioDetallado.cursoAca.eq(anyo));

        // datos procedentes de filtros
        if (fe.getCursos() != null) {
            where = where.and(qHorariosEstudioDetallado.curso.in(fe.getCursos()));
        }
        if (fe.getSemestres() != null) {
            where = where.and(
                    qHorariosEstudioDetallado.semestre.in(
                            fe.getSemestres()
                    ).or(qHorariosEstudioDetallado.tipo.eq("A"))
            );
        }
        if (fe.getCaracteres() != null) {
            HashSet<String> sqlCaracter = new HashSet<>(5);
            for (CaracterAsignatura ca : fe.getCaracteres()) {
                for (String s : ca.getCaracterColumnValue()) {
                    sqlCaracter.add(s);
                }
            }
            where = where.and(qHorariosEstudioDetallado.caracter.in(sqlCaracter));
        }
        if (fe.getGrupos() != null) {
            where = where.and(
                    qHorariosEstudioDetallado.grupoTeoria.in(
                            fe.getGrupos()
                    )
            );
        }
        Tuple tuple = query.where(where).uniqueResult(qHorariosEstudioDetallado.curso.min(), qHorariosEstudioDetallado.semestre.min());
        Map<String, Long> res = new HashMap<>();
        res.put("curso", tuple.get(0, Long.class));

        try {
            res.put("semestre", Long.parseLong(tuple.get(1, String.class)));
        } catch (Exception e) {
            res.put("semestre", null);
        }
        return res;

    }
}
