package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.Circuito;
import es.uji.apps.lleu.model.QCircuito;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CircuitosDAO extends BaseDAODatabaseImpl
{
    QCircuito qCircuito = QCircuito.circuito;

    public List<Circuito> getCircuitos (Long estudioId, Integer cursoAca) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qCircuito).where(
                qCircuito.estudioId.eq(estudioId),
                qCircuito.cursoAca.eq(cursoAca)
        ).list(qCircuito);
    }
}
