package es.uji.apps.lleu.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.expr.StringExpression;

import es.uji.apps.lleu.model.AsignaturasMaster;
import es.uji.apps.lleu.model.HorarioAsigMasterDetallado;
import es.uji.apps.lleu.model.HorarioAsigMasterGeneral;
import es.uji.apps.lleu.model.QAsignaturasMaster;
import es.uji.apps.lleu.model.QEstudio;
import es.uji.apps.lleu.model.QGrupoTeoriaMaster;
import es.uji.apps.lleu.model.QGuiasIngles;
import es.uji.apps.lleu.model.QHorarioAsigMasterDetallado;
import es.uji.apps.lleu.model.QHorarioAsigMasterGeneral;
import es.uji.apps.lleu.model.QHorarioMasterDetallado;
import es.uji.apps.lleu.model.enums.CaracterAsignatura;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.InfoFiltro;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.StringUtils;

import static java.lang.Math.toIntExact;


@Repository
public class AsignaturasMasterDAO extends BaseDAODatabaseImpl
{
    private QAsignaturasMaster qAsignaturasMaster = QAsignaturasMaster.asignaturasMaster;
    private QGuiasIngles qGuiasIngles = QGuiasIngles.guiasIngles;
    private QEstudio qEstudio = QEstudio.estudio;
    private QHorarioAsigMasterGeneral qHorarioAsigMasterGeneral = QHorarioAsigMasterGeneral.horarioAsigMasterGeneral;
    private QHorarioAsigMasterDetallado qHorarioAsigMasterDetallado = QHorarioAsigMasterDetallado.horarioAsigMasterDetallado;
    private QGrupoTeoriaMaster qGrupoTeoriaMaster = QGrupoTeoriaMaster.grupoTeoriaMaster;


    /**
     * Dado un estudioId de estudio y año, obtiene un listado de asignaturas sin filtrar.
     *
     * @param estudioId el estudioId de estudio
     * @param anyo el año (curso académico) que queremos obtener
     * @return un listado de asignaturas
     */
    public List<AsignaturasMaster> getAsignaturasByIdAnyo(Long estudioId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturasMaster).where(qAsignaturasMaster.estudioId.eq(estudioId).and(qAsignaturasMaster.cursoAca.eq(anyo))
        .and(qAsignaturasMaster.caracter.ne("CF")));
        query.orderBy(qAsignaturasMaster.asignaturaId.asc());
        return query.distinct().list(qAsignaturasMaster);
    }


    /**
     * Dado un estudioId de estudio, un año e información de filtrado, devuelve el listado de asignaturas correspondiente.
     *
     * @param estudioId
     * @param anyo
     * @param filtro
     * @param solsConvalida si es null, devuelve cualquier valor para solsConvalida. Si no filtra también por ese valor.
     * @return listado de asignaturas filtradas.
     */
    public List<AsignaturasMaster> getAsignaturasByFilter(Long estudioId, Integer anyo, FiltroEstudio filtro, String solsConvalida) {

        BooleanExpression cond = qAsignaturasMaster.estudioId.eq(estudioId)
                                .and(qAsignaturasMaster.cursoAca.eq(anyo).and(qAsignaturasMaster.caracter.ne("CF")));

        if ( filtro.getCursos() != null ) {
            List<Integer> cursos = filtro.getCursos().stream().map(l -> toIntExact(l)).collect(Collectors.toList());
            cond = cond.and(qAsignaturasMaster.curso.in(cursos));
        }
        if (filtro.getSemestres() != null) {
            // Los semestres >= 3 los mapeo a 1 y 2 (son asignaturas que duran más de un curso)
            List<String> semestres = filtro.getSemestres().stream().map((s) -> {
                if (s.equals("a")) {
                    return s;
                }
                return Integer.toString(Math.floorMod(Integer.parseInt(s) - 1, 2) + 1 );
            }).collect(Collectors.toList());

            cond = cond.and(
                    qAsignaturasMaster.semestre.in(semestres).or(qAsignaturasMaster.tipo.eq("A"))
            );
        }
        if (filtro.getNombre() != null) {
            StringExpression nombre;
            switch (filtro.getIdioma()) {
                case "es":
                    nombre = qAsignaturasMaster.nombreES;
                    break;
                case "en":
                    nombre = qAsignaturasMaster.nombreEN;
                    break;
                case "ca":
                default:
                    nombre = qAsignaturasMaster.nombreCA;
                    break;
            }

            // Buscamos por nombre o por código
            String data = StringUtils.limpiaAcentos(filtro.getNombre().toUpperCase());
            cond = cond.and(
                        nombre.upper().like("%" + data + "%").or(
                                qAsignaturasMaster.asignaturaId.like("%" + data + "%")
                        )
                    );
        }
        if (filtro.getCaracteres() != null)
        {
            // Para el filtrado por caracteres hay que tener en cuenta la columna caracter y EEP
            // que indica estancia en prácticas.

            HashSet<String> sqlCaracter = new HashSet<>(5);

            // indica si añadir estancia en prácticas
            boolean addEEP = false;

            for (CaracterAsignatura ca : filtro.getCaracteres()) {
                for (String s : ca.getCaracterColumnValue()) {
                    sqlCaracter.add(s);
                }
            }
            cond = cond.and(qAsignaturasMaster.caracter.in(sqlCaracter));
        }

        if (solsConvalida != null) {
            cond = cond.and(qAsignaturasMaster.soloConvalida.eq(solsConvalida.toUpperCase()));
        }

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturasMaster)
                .where(cond);
        query.orderBy(qAsignaturasMaster.asignaturaId.asc());

        return query.distinct().list(qAsignaturasMaster);
    }

    /**
     * Devuelve un resumen de datos acerca de las asignaturas de un estudio. Lo utilizamos para los filtros.
     *
     * @param estudioId el estudio
     * @param anyo el curso académico
     */

    public InfoFiltro getInfoFiltros(Long estudioId, Integer anyo)
    {
        // semestres
        JPAQuery query = new JPAQuery(entityManager);
        List<Tuple> list = query.from(qAsignaturasMaster)
            .where(
                    qAsignaturasMaster.estudioId.eq(estudioId),
                    qAsignaturasMaster.cursoAca.eq(anyo)
            ).distinct().orderBy(qAsignaturasMaster.semestre.asc()).list(qAsignaturasMaster.tipo,qAsignaturasMaster.semestre);
        List<String> semestres = new ArrayList<>();
        for(Tuple tupla: list){
            if(tupla.get(qAsignaturasMaster.tipo).equals("A")){
                semestres.add(tupla.get(qAsignaturasMaster.tipo));
            }else{
                semestres.add(tupla.get(qAsignaturasMaster.semestre));
            }

        }
        semestres=semestres.parallelStream().distinct().sorted().collect(Collectors.toList());

        // grupos
        query = new JPAQuery(entityManager);
        List<String> grupos = query.from(qGrupoTeoriaMaster)
                .where(
                        qGrupoTeoriaMaster.estudioId.eq(estudioId),
                        qGrupoTeoriaMaster.cursoAca.eq(anyo),
                        qGrupoTeoriaMaster.curId.loe(6)
                ).distinct().orderBy(qGrupoTeoriaMaster.grupo.asc()).list(qGrupoTeoriaMaster.grupo);

        // caracteres
        query = new JPAQuery(entityManager);
        List<String> caracteres = query.from(qAsignaturasMaster)
                .where(
                        qAsignaturasMaster.estudioId.eq(estudioId),
                        qAsignaturasMaster.cursoAca.eq(anyo)
                ).distinct().list(qAsignaturasMaster.caracter);

        List<CaracterAsignatura> lcaracteres = CaracterAsignatura.getInstancesFromSQLValues(caracteres);
        if (!lcaracteres.contains(CaracterAsignatura.PRACTICAS)) {
            lcaracteres.add(CaracterAsignatura.PRACTICAS);
        }

        // cursos
        query = new JPAQuery(entityManager);
        List<Long> cursos = query.from(qAsignaturasMaster).where(
                qAsignaturasMaster.estudioId.eq(estudioId)
        ).distinct().list(qAsignaturasMaster.curso).stream().map(c -> new Long(c)).collect(Collectors.toList());

        return new InfoFiltro(
                cursos,
                grupos,
                semestres,
                lcaracteres
        );
    }

    public InfoFiltro getInfoFiltros(Long estudioId, String asignaturaId, Integer anyo)
    {
        // grupos
        JPAQuery query  = new JPAQuery(entityManager);
        List<String> grupos = query.from(qHorarioAsigMasterDetallado)
                .where(
                        qHorarioAsigMasterDetallado.estudioId.eq(estudioId),
                        qHorarioAsigMasterDetallado.asignaturaId.eq(asignaturaId),
                        qHorarioAsigMasterDetallado.cursoACA.eq(anyo)
                ).distinct().orderBy(qHorarioAsigMasterDetallado.grupo.asc()).list(qHorarioAsigMasterDetallado.grupo);
        //Los tipos de grupos
        query = new JPAQuery(entityManager);
        List<String> tiposSubgrupo = query.from(qHorarioAsigMasterDetallado)
                .where(
                        qHorarioAsigMasterDetallado.asignaturaId.eq(asignaturaId),
                        qHorarioAsigMasterDetallado.estudioId.eq(estudioId),
                        qHorarioAsigMasterDetallado.cursoACA.eq(anyo)
                ).distinct().orderBy(qHorarioAsigMasterDetallado.subGrupoTipo.asc()).list(qHorarioAsigMasterDetallado.subGrupoTipo);

        return new InfoFiltro(grupos,tiposSubgrupo);

    }


    public AsignaturasMaster getAsignatura(String asignaturaId, Long estudioId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturasMaster).where(qAsignaturasMaster.estudioId.eq(estudioId)
                .and(qAsignaturasMaster.cursoAca.eq(anyo))
                .and(qAsignaturasMaster.asignaturaId.eq(asignaturaId)));
        return query.uniqueResult(qAsignaturasMaster);
    }

    /**
     * Indica si tiene una guía docente en inglés
     *
     * @param asignaturaId
     * @param cursoAca
     * @return
     */
    public boolean tieneGuiaDocenteEnIngles ( String asignaturaId, Integer cursoAca ){
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qGuiasIngles).where(
                qGuiasIngles.asignaturaId.eq(asignaturaId),
                qGuiasIngles.cursoAcaIni.loe(cursoAca).and(
                        qGuiasIngles.cursoAcaFin.isNull().or(
                                qGuiasIngles.cursoAcaFin.goe(cursoAca)
                        )
                )).exists();
    }

    public List<Long> getPrimerLectivos(Long estudioId, String asignaturaId, Integer anyo)
    {
        QHorarioMasterDetallado qHorarioMasterDetallado1 = new QHorarioMasterDetallado("horarios1");
        QHorarioMasterDetallado qHorarioMasterDetallado2 = new QHorarioMasterDetallado("horarios2");

        List<Long> list = new ArrayList<>(2);

        String primerGrupoSemestre1 = new JPAQuery(entityManager).from(qHorarioMasterDetallado1).where(
                qHorarioMasterDetallado1.estudioId.eq(estudioId),
                qHorarioMasterDetallado1.asignaturaId.eq(asignaturaId),
                qHorarioMasterDetallado1.cursoAca.eq(anyo),
                qHorarioMasterDetallado1.semestre.eq("1")
        ).uniqueResult(qHorarioMasterDetallado1.grupo.min());

        JPAQuery semestre1 = new JPAQuery(entityManager);

        if ( primerGrupoSemestre1 == null) {
            semestre1.from(qHorarioMasterDetallado1)
                    .where(
                            qHorarioMasterDetallado1.estudioId.eq(estudioId),
                            qHorarioMasterDetallado1.asignaturaId.eq(asignaturaId),
                            qHorarioMasterDetallado1.cursoAca.eq(anyo),
                            qHorarioMasterDetallado1.semestre.eq("1")
                    );
        } else {
            semestre1.from(qHorarioMasterDetallado1)
                    .where(
                            qHorarioMasterDetallado1.estudioId.eq(estudioId),
                            qHorarioMasterDetallado1.asignaturaId.eq(asignaturaId),
                            qHorarioMasterDetallado1.cursoAca.eq(anyo),
                            qHorarioMasterDetallado1.semestre.eq("1"),
                            qHorarioMasterDetallado1.grupo.eq(primerGrupoSemestre1)
                    );

        }

        Date aux = semestre1.uniqueResult(qHorarioMasterDetallado1.fin.min());
        if (aux == null ) {
            list.add(null);
        } else {
            list.add(aux.getTime());
        }

        JPAQuery semestre2 = new JPAQuery(entityManager);

        semestre2.from(qHorarioMasterDetallado2)
                .where(qHorarioMasterDetallado2.estudioId.eq(estudioId),
                        qHorarioMasterDetallado2.asignaturaId.eq(asignaturaId),
                        qHorarioMasterDetallado2.cursoAca.eq(anyo),
                        qHorarioMasterDetallado2.semestre.eq("2")
                );

        aux = semestre2.uniqueResult(qHorarioMasterDetallado2.fin.min());
        if (aux == null) {
            list.add(null);
        } else {
            list.add(aux.getTime());
        }

        return list;
    }

    public Long getUltimoLectivo(Long estudioId, String asignaturaId, Integer anyo, FiltroEstudio filtro)
    {
        QHorarioMasterDetallado qHorarioMasterDetallado = QHorarioMasterDetallado.horarioMasterDetallado;
        BooleanExpression cond = qHorarioMasterDetallado.estudioId.eq(estudioId).and(qHorarioMasterDetallado.cursoAca.eq(anyo));
        if (filtro.getGrupos() != null)
        {
            cond = cond.and(qHorarioMasterDetallado.grupo.in(filtro.getGrupos()));

        }
        if (filtro.getSemestres() != null)
        {
            // las asignaturas anuales salen tanto si es primer semestre como sengundo.
            cond = cond.and(qHorarioMasterDetallado.semestre.in(filtro.getSemestres()));
        }

        JPAQuery query = new JPAQuery(entityManager);
        Date ultimo = query.from(qHorarioMasterDetallado)
                .where(
                        cond,
                        qHorarioMasterDetallado.asignaturaId.eq(asignaturaId)
                )
                .uniqueResult(qHorarioMasterDetallado.fin.max());

        return ultimo == null ? 0l : ultimo.getTime();
    }

    public List<HorarioAsigMasterGeneral> getHorarioGeneralAsignatura(Long estudioId, String asignaturaId, Integer cursoAca, FiltroEstudio filtro) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qHorarioAsigMasterGeneral);


        BooleanExpression where = qHorarioAsigMasterGeneral.estudioId.eq(estudioId)
                .and(qHorarioAsigMasterGeneral.asignaturaId.eq(asignaturaId))
                .and(qHorarioAsigMasterGeneral.cursoAca.eq(cursoAca));

        if (filtro.getGrupos() != null) {
            where = where.and(qHorarioAsigMasterGeneral.grupo.in(filtro.getGrupos()));
        }
        if (filtro.getSemestres() != null) {
            where = where.and(qHorarioAsigMasterGeneral.semestre.in(filtro.getSemestres()));
        }

        return query.where(where).list(qHorarioAsigMasterGeneral);
    }

    public List<HorarioAsigMasterDetallado> getHorarioDetalladoAsignatura(Long estudioId, String asignaturaId, Integer anyo, FiltroEstudio filtro, Date from, Date to)
    {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where = qHorarioAsigMasterDetallado.estudioId.eq(estudioId).and(qHorarioAsigMasterDetallado.cursoACA.eq(anyo));
        if (filtro.getGrupos() != null) {
            where = where.and(qHorarioAsigMasterDetallado.grupo.in(filtro.getGrupos()));
        }
        if (filtro.getSemestres() != null) {
            // las asignaturas anuales salen tanto si es primer semestre como sengundo.
            where = where.and(qHorarioAsigMasterDetallado.semestre.in(filtro.getSemestres()));
        }

        return query.from(qHorarioAsigMasterDetallado)
                .where(
                        where,
                        qHorarioAsigMasterDetallado.asignaturaId.eq(asignaturaId),
                        qHorarioAsigMasterDetallado.cursoACA.eq(anyo),
                        qHorarioAsigMasterDetallado.fHorIni.goe(from),
                        qHorarioAsigMasterDetallado.fHorFin.lt(to)
                )
                .distinct()
                .list(qHorarioAsigMasterDetallado);
    }
}
