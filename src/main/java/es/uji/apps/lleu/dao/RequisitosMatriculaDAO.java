package es.uji.apps.lleu.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.AsignaturaIncompatible;
import es.uji.apps.lleu.model.QAsignaturaIncompatible;
import es.uji.apps.lleu.model.QRequisitosMatricula;
import es.uji.apps.lleu.model.QRequisitosMatriculaEEP;
import es.uji.apps.lleu.model.RequisitosMatricula;
import es.uji.apps.lleu.model.RequisitosMatriculaEEP;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class RequisitosMatriculaDAO extends BaseDAODatabaseImpl
{
    QRequisitosMatricula qRequisitosMatricula = QRequisitosMatricula.requisitosMatricula;
    QRequisitosMatriculaEEP qRequisitosMatriculaEEP = QRequisitosMatriculaEEP.requisitosMatriculaEEP;
    QAsignaturaIncompatible qAsignaturaIncompatible = QAsignaturaIncompatible.asignaturaIncompatible;

    public List<RequisitosMatricula> getRequisitosMatricula(Long estudioId, Integer cursoAca) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qRequisitosMatricula).where(
                qRequisitosMatricula.cursoAca.eq(cursoAca),
                qRequisitosMatricula.estudioId.eq(estudioId)
        ).orderBy(qRequisitosMatricula.asignaturaId1.asc()).list(qRequisitosMatricula);
    }


    public List<AsignaturaIncompatible> getAsignaturasACursar (Long estudioId, Integer cursoAca) {
        JPAQuery q = new JPAQuery(entityManager);
        List<AsignaturaIncompatible> asigs = q.from(qAsignaturaIncompatible).where(
                qAsignaturaIncompatible.estudioId.eq(estudioId),
                qAsignaturaIncompatible.cursoAca.eq(cursoAca)
        ).list(qAsignaturaIncompatible);

        q = new JPAQuery(entityManager);
        List<RequisitosMatriculaEEP> req = q.from(qRequisitosMatriculaEEP).where(
                qRequisitosMatriculaEEP.estudioId.eq(estudioId),
                qRequisitosMatriculaEEP.cursoAca.eq(cursoAca),
                qRequisitosMatriculaEEP.cursoAcaIni.isNull().or(
                        qRequisitosMatriculaEEP.cursoAcaIni.loe(cursoAca)
                                //Estos son especiales empiezan en el 2020
                                .or(qRequisitosMatriculaEEP.asignaturaId.in("MP1847","MI1847"))
                ),
                qRequisitosMatriculaEEP.cursoAcaFin.isNull().or(
                    qRequisitosMatriculaEEP.cursoAcaFin.goe(cursoAca)
                )
        ).distinct().list(QRequisitosMatriculaEEP.create(
                qRequisitosMatriculaEEP.cursoAca,
                qRequisitosMatriculaEEP.estudioId,
                qRequisitosMatriculaEEP.asignaturaId,
                qRequisitosMatriculaEEP.nombreAsigES,
                qRequisitosMatriculaEEP.nombreAsigCA,
                qRequisitosMatriculaEEP.nombreAsigEN
        ));

        List<AsignaturaIncompatible> asigsEEP = req.parallelStream().map(a -> new AsignaturaIncompatible(
                a.getCursoAca(),
                a.getEstudioId(),
                a.getAsignaturaId(),
                a.getNombreAsigES(),
                a.getNombreAsigCA(),
                a.getNombreAsigEN()
        )).collect(Collectors.toList());

        List<AsignaturaIncompatible> ret = new ArrayList<>(asigs);
        ret.addAll(asigsEEP);

        return ret.parallelStream().sorted(
                (a,b) -> a.getAsignaturaId().compareTo(b.getAsignaturaId())
        ).distinct().collect(Collectors.toList());

    }

    public List<RequisitosMatriculaEEP> getRequisitosMatriculaEEP(Long estudioId, Integer cursoAca)
    {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qRequisitosMatriculaEEP).where(
                qRequisitosMatriculaEEP.estudioId.eq(estudioId),
                qRequisitosMatriculaEEP.cursoAca.eq(cursoAca),
                qRequisitosMatriculaEEP.cursoAcaIni.isNull().or(
                        qRequisitosMatriculaEEP.cursoAcaIni.loe(cursoAca)
                                //Estos son especiales empiezan en el 2020
                                .or(qRequisitosMatriculaEEP.asignaturaId.in("MP1847","MI1847"))
                ),
                qRequisitosMatriculaEEP.cursoAcaFin.isNull().or(
                        qRequisitosMatriculaEEP.cursoAcaFin.goe(cursoAca)
                )
        ).distinct().list(qRequisitosMatriculaEEP);
    }
}
