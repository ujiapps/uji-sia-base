package es.uji.apps.lleu.dao;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.Foto;
import es.uji.apps.lleu.model.QFoto;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class FotoDAO extends BaseDAODatabaseImpl
{
    private QFoto qFoto = QFoto.foto1;

    public List<Foto> getFotos (Set<Long> perIds ) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qFoto).where(
                qFoto.perId.in(perIds)
        ).list(qFoto);
    }
}
