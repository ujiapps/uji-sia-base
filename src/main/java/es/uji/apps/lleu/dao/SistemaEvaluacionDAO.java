package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.Criterios;
import es.uji.apps.lleu.model.EstadoAsignatura;
import es.uji.apps.lleu.model.ExamenesGrado;
import es.uji.apps.lleu.model.ExamenesMaster;
import es.uji.apps.lleu.model.QCriterios;
import es.uji.apps.lleu.model.QEstadoAsignatura;
import es.uji.apps.lleu.model.QExamenesGrado;
import es.uji.apps.lleu.model.QExamenesMaster;
import es.uji.apps.lleu.model.QSistemaEvaluacionGrado;
import es.uji.apps.lleu.model.QSistemaEvaluacionMaster;
import es.uji.apps.lleu.model.SistemaEvaluacionGrado;
import es.uji.apps.lleu.model.SistemaEvaluacionMaster;
import es.uji.apps.lleu.model.enums.TipoSistemaEvaluacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SistemaEvaluacionDAO extends BaseDAODatabaseImpl
{
    QSistemaEvaluacionGrado qSistemaEvaluacionGrado = QSistemaEvaluacionGrado.sistemaEvaluacionGrado;
    QSistemaEvaluacionMaster qSistemaEvaluacionMaster = QSistemaEvaluacionMaster.sistemaEvaluacionMaster;
    QCriterios qCriteriosGrado = QCriterios.criterios;
    QExamenesGrado qExamenesGrado = QExamenesGrado.examenesGrado;
    QExamenesMaster qExamenesMaster = QExamenesMaster.examenesMaster;
    QEstadoAsignatura qEstadoAsignatura = QEstadoAsignatura.estadoAsignatura;

    public List<SistemaEvaluacionGrado> getSistemasEvaluacion (String asignaturaId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);

        EstadoAsignatura e = query.from(qEstadoAsignatura).where(
                qEstadoAsignatura.cursoACa.eq(anyo),
                qEstadoAsignatura.asignaturaId.eq(asignaturaId)
        ).uniqueResult(qEstadoAsignatura);

        String tipo;
        if (anyo < 2011 || (!e.getEstado().equals("T") && !e.getEstado().equals("F")) ) {
            tipo = TipoSistemaEvaluacion.LISTAPRUEBAS.toString();
        } else {
            tipo = TipoSistemaEvaluacion.LISTAEVALUACION.toString();
        }

        query = new JPAQuery(entityManager);

        return query.from(qSistemaEvaluacionGrado).where(
                qSistemaEvaluacionGrado.tipo.eq(tipo),
                qSistemaEvaluacionGrado.cursoAca.eq(anyo),
                qSistemaEvaluacionGrado.asignaturaId.eq(asignaturaId)
        ).orderBy(qSistemaEvaluacionGrado.ponderacion.desc()).list(qSistemaEvaluacionGrado);
    }

    /**
     * Devuelve los sistemas de evaluación de máster
     *
     * @param asignaturaId
     * @param anyo
     * @return
     */
    public List<SistemaEvaluacionMaster> getSistemasEvaluacionMaster(String asignaturaId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qSistemaEvaluacionMaster).where(
                qSistemaEvaluacionMaster.cursoAca.eq(anyo),
                qSistemaEvaluacionMaster.asignaturaId.eq(asignaturaId)
        ).orderBy(qSistemaEvaluacionMaster.ponderacion.desc()).list(qSistemaEvaluacionMaster);
    }

    /**
     * Devuelve los criterios de evaluación tanto de grado como de máster
     *
     * @param asignaturaId
     * @param anyo
     * @return
     */
    public List<Criterios> getCriterios(String asignaturaId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qCriteriosGrado).where(
                qCriteriosGrado.cursoAca.eq(anyo),
                qCriteriosGrado.asignaturaId.eq(asignaturaId)
        ).list(qCriteriosGrado);
    }

    public List<ExamenesGrado> getExamenesGrado(String asignaturaId, Integer anyo) {

        JPAQuery query = new JPAQuery(entityManager);
        List<ExamenesGrado> ret = query.from(qExamenesGrado).where(
                qExamenesGrado.cursoAca.eq(anyo),
                qExamenesGrado.asignaturaId.eq(asignaturaId)
        ).orderBy(qExamenesGrado.Fecha.asc()).list(qExamenesGrado);

        return ret;
    }

    public List<ExamenesMaster> getExamenesMaster(String asignaturaId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qExamenesMaster).where(
                qExamenesMaster.cursoAca.eq(anyo),
                qExamenesMaster.asignaturaId.eq(asignaturaId)
        ).orderBy(qExamenesMaster.Fecha.asc()).list(qExamenesMaster);
    }
}
