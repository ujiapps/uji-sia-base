package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.QResultadosGuias;
import es.uji.apps.lleu.model.ResultadosGuias;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ResultadoGuiaDAO extends BaseDAODatabaseImpl
{
    QResultadosGuias qResultadosGuias = QResultadosGuias.resultadosGuias;

    public List<ResultadosGuias> getResultados(String asignaturaId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qResultadosGuias).where(
                qResultadosGuias.asignaturaId.eq(asignaturaId),
                qResultadosGuias.cursoAca.eq(anyo)
        ).list(qResultadosGuias);
    }
}
