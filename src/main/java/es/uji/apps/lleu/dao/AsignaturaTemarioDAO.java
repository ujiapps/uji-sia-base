package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.AsignaturaTemario;
import es.uji.apps.lleu.model.QAsignaturaTemario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AsignaturaTemarioDAO extends BaseDAODatabaseImpl
{
    private QAsignaturaTemario qAsignaturaTemario = QAsignaturaTemario.asignaturaTemario;

    public List<AsignaturaTemario> getTemario(String asignaturaId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturaTemario).where(qAsignaturaTemario.codigoBusqueda.eq(asignaturaId)
                .and(qAsignaturaTemario.cursoACA.eq(anyo))
                .and(qAsignaturaTemario.apartado.id.eq(9L)));
        query.orderBy(qAsignaturaTemario.apartado.id.asc());


        List<AsignaturaTemario> list = query.list(qAsignaturaTemario);
        return list;
    }
}
