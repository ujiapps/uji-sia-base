package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.ComplementoFormacion;
import es.uji.apps.lleu.model.QComplementoFormacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ComplementoFormacionDAO extends BaseDAODatabaseImpl {

    QComplementoFormacion qComplementoFormacion = QComplementoFormacion.complementoFormacion;

    public List<ComplementoFormacion> getComplementos(Long estudioId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qComplementoFormacion).where(
                qComplementoFormacion.estudioId.eq(estudioId),
                qComplementoFormacion.cursoAca.eq(anyo)
        ).orderBy(qComplementoFormacion.asignaturaId.asc()).list(qComplementoFormacion);
    }
}
