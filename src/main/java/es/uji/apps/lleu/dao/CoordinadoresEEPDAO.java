package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.CoordinadoresEEP;
import es.uji.apps.lleu.model.QCoordinadoresEEP;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CoordinadoresEEPDAO extends BaseDAODatabaseImpl
{
   private QCoordinadoresEEP qCoordinadoresEEP = QCoordinadoresEEP.coordinadoresEEP;


    public List<CoordinadoresEEP> getCoordinadoresEEP(Long estudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCoordinadoresEEP).where(qCoordinadoresEEP.estudioId.eq(estudioId));

        return query.list(qCoordinadoresEEP);

    }
}
