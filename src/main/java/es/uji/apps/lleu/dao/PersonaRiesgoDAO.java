package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.QPersonaRiesgo;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PersonaRiesgoDAO extends BaseDAODatabaseImpl {

    private QPersonaRiesgo qPersonaRiesgo = QPersonaRiesgo.personaRiesgo;

    public List<Long> getPersonasRiesgo() {
        JPAQuery query = new JPAQuery(entityManager);


        return query.from(qPersonaRiesgo).list(qPersonaRiesgo.perId);
    }

}