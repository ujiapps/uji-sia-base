package es.uji.apps.lleu.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.Asignatura;
import es.uji.apps.lleu.model.QAsignatura;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AsignaturaDAO extends BaseDAODatabaseImpl
{
    private QAsignatura qAsignatura = QAsignatura.asignatura;

    /**
     * Devuelve una asignatura por id de asignatura
     * @param id
     * @return
     */
    public Asignatura getById(String id) {

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qAsignatura).where(qAsignatura.id.eq(id)).uniqueResult(qAsignatura);

    }
}
