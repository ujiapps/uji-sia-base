package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.QAsignaturaProfesor;
import es.uji.apps.lleu.model.QAsignaturaProfesorMaster;
import es.uji.apps.lleu.model.QTutorias;
import es.uji.apps.lleu.model.Tutorias;
import es.uji.commons.db.BaseDAODatabaseImpl;


@Repository
public class TutoriasDAO extends BaseDAODatabaseImpl
{

    private QTutorias qTutorias = QTutorias.tutorias1;
    private QAsignaturaProfesor qAsignaturaProfesor = QAsignaturaProfesor.asignaturaProfesor;
    private QAsignaturaProfesorMaster qAsignaturaProfesorMaster = QAsignaturaProfesorMaster.asignaturaProfesorMaster;

    public List<Tutorias> getTutoriasGrado(String asignaturaId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPASubQuery subQuery = new JPASubQuery();

            query.from(qTutorias).where(qTutorias.cursoAca.eq(anyo).and(qTutorias.perId.in(
                    subQuery.from(qAsignaturaProfesor).where(qAsignaturaProfesor.asiId.eq(asignaturaId).and(qAsignaturaProfesor.cursoAca.eq(anyo)))
                            .list(qAsignaturaProfesor.perId))
            ));

        query.orderBy(qTutorias.semestre.asc(), qTutorias.fechaIni.asc(), qTutorias.diaSemana.asc(),qTutorias.horaInicio.asc());

        return query.list(qTutorias);
    }

    public List<Tutorias> getTutoriasMaster(String asignaturaId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        JPASubQuery subQuery = new JPASubQuery();
            query.from(qTutorias).where(qTutorias.cursoAca.eq(anyo).and(qTutorias.perId.in(
                    subQuery.from(qAsignaturaProfesorMaster).where(qAsignaturaProfesorMaster.asiId.eq(asignaturaId).and(qAsignaturaProfesorMaster.cursoAca.eq(anyo)))
                            .list(qAsignaturaProfesorMaster.perId))
            ));

        query.orderBy(qTutorias.semestre.asc(), qTutorias.fechaIni.asc(), qTutorias.diaSemana.asc(),qTutorias.horaInicio.asc());

        return query.list(qTutorias);
    }
}
