package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.QResumenCreditosGrado;
import es.uji.apps.lleu.model.QResumenCreditosPop;
import es.uji.apps.lleu.model.ResumenCreditosGrado;
import es.uji.apps.lleu.model.ResumenCreditosPop;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ResumenCreditosDAO extends BaseDAODatabaseImpl
{
    QResumenCreditosGrado qResumenCreditosGrado = QResumenCreditosGrado.resumenCreditosGrado;
    QResumenCreditosPop qResumenCreditosPop = QResumenCreditosPop.resumenCreditosPop;

    public List<ResumenCreditosGrado> getResumenCreditosGrado (Long estudioId) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qResumenCreditosGrado).where(
                qResumenCreditosGrado.estudioId.eq(estudioId)
        ).orderBy(qResumenCreditosGrado.ciclo.asc(), qResumenCreditosGrado.curso.asc()).list(qResumenCreditosGrado);
    }

    public ResumenCreditosPop getResumenCreditoPop (Long estudioId) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qResumenCreditosPop).where(
                qResumenCreditosPop.estudioId.eq(estudioId)
        ).uniqueResult(qResumenCreditosPop);
    }
}
