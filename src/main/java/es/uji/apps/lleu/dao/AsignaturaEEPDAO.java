package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.AsignaturaEEP;
import es.uji.apps.lleu.model.AsignaturaEEPExtra;
import es.uji.apps.lleu.model.QAsignaturaEEP;
import es.uji.apps.lleu.model.QAsignaturaEEPExtra;
import es.uji.apps.lleu.model.enums.TipoDocencia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AsignaturaEEPDAO extends BaseDAODatabaseImpl
{
    private QAsignaturaEEP qAsignaturaEEP = QAsignaturaEEP.asignaturaEEP;
    private QAsignaturaEEPExtra qAsignaturaEEPExtra = QAsignaturaEEPExtra.asignaturaEEPExtra;


    public List<AsignaturaEEP> getAsignaturasEEP(Long estudioId, Integer anyo, TipoDocencia tipo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qAsignaturaEEP).where(qAsignaturaEEP.estudioId.eq(estudioId)
                .and(qAsignaturaEEP.cursoAca.eq(anyo))
        .and(qAsignaturaEEP.tipo.eq(tipo.getValue())));

        return query.list(qAsignaturaEEP);

    }

    public List<AsignaturaEEPExtra> getAsignaturasEEPExtra(Long estudioId, TipoDocencia tipo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qAsignaturaEEPExtra).where(qAsignaturaEEPExtra.estudioId.eq(estudioId)
                .and(qAsignaturaEEPExtra.tipo.eq(tipo.getValue())));

        return query.list(qAsignaturaEEPExtra);
    }
}
