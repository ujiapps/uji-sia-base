package es.uji.apps.lleu.dao;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.Actividad;
import es.uji.apps.lleu.model.QActividad;
import es.uji.commons.db.BaseDAODatabaseImpl;


@Repository
public class ActividadDAO extends BaseDAODatabaseImpl {

    QActividad qActividad = QActividad.actividad;

    public List<Actividad> getActividades(String asignaturaId, Integer anyo) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qActividad).where(
                qActividad.cursoAca.eq(anyo),
                qActividad.asignaturaId.eq(asignaturaId))
                .orderBy(qActividad.orden.asc())
                .list(qActividad);
    }
}
