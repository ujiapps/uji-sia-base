package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.Especialidad;
import es.uji.apps.lleu.model.EspecialidadAsignatura;
import es.uji.apps.lleu.model.QEspecialidad;
import es.uji.apps.lleu.model.QEspecialidadAsignatura;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EspecialidadDAO extends BaseDAODatabaseImpl
{
    QEspecialidad qEspecialidad = QEspecialidad.especialidad;
    QEspecialidadAsignatura qEspecialidadAsignatura = QEspecialidadAsignatura.especialidadAsignatura;

    public List<Especialidad> getEspecialidades (Long estudioId, Integer cursoAca ) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qEspecialidad).where(
                qEspecialidad.estudioId.eq(estudioId)
                /*
                qEspecialidad.cursoIni.isNull().or(qEspecialidad.cursoIni.loe(cursoAca)),
                qEspecialidad.cursoFin.isNull().or(qEspecialidad.cursoFin.goe(cursoAca))
                */
        ).list(qEspecialidad);
    }

    public List<EspecialidadAsignatura> getEspecialidadAsignaturas (Long estudioID, Integer cursoAca) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qEspecialidadAsignatura).where(
                qEspecialidadAsignatura.estudioId.eq(estudioID),
                qEspecialidadAsignatura.cursoAca.eq(cursoAca)
        ).orderBy(qEspecialidadAsignatura.asignaturaId.asc()).list(qEspecialidadAsignatura);
    }
}
