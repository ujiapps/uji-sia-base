package es.uji.apps.lleu.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;

import es.uji.apps.lleu.model.FicheroHorariosMaster;
import es.uji.apps.lleu.model.HorarioMasterDetallado;
import es.uji.apps.lleu.model.HorarioMasterGeneral;
import es.uji.apps.lleu.model.HorariosMasterUrl;
import es.uji.apps.lleu.model.QFicheroHorariosMaster;
import es.uji.apps.lleu.model.QHorarioMasterDetallado;
import es.uji.apps.lleu.model.QHorarioMasterGeneral;
import es.uji.apps.lleu.model.QHorariosMasterUrl;
import es.uji.apps.lleu.model.enums.CaracterAsignatura;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EstudioMasterDAO extends BaseDAODatabaseImpl {

    private QFicheroHorariosMaster qFicheroHorariosMaster = QFicheroHorariosMaster.ficheroHorariosMaster;
    private QHorariosMasterUrl qHorariosMasterUrl = QHorariosMasterUrl.horariosMasterUrl;
    private QHorarioMasterGeneral qHorarioMasterGeneral = QHorarioMasterGeneral.horarioMasterGeneral;
    private QHorarioMasterDetallado qHorarioMasterDetallado = QHorarioMasterDetallado.horarioMasterDetallado;


    /**
     * Devuelve la URL URL de los horarios de un máster
     *
     * @param estudioId
     * @return
     */
    public HorariosMasterUrl getHorarioMasterUrl(Long estudioId) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qHorariosMasterUrl).where(
                qHorariosMasterUrl.estudioId.eq(estudioId)
        ).uniqueResult(qHorariosMasterUrl);
    }

    /**
     * Devovemos una tupla con los campos que necesitamos. En este caso en concreto lo que podemos hacer es
     *
     * @param estudioId
     * @param cursoAca
     * @return
     */
    public List<FicheroHorariosMaster> getFicherosHorariosMaster(Long estudioId, Integer cursoAca) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qFicheroHorariosMaster).where(
                qFicheroHorariosMaster.estudioId.eq(estudioId),
                qFicheroHorariosMaster.cursoAca.eq(cursoAca)
        ).list(QFicheroHorariosMaster.create(
                qFicheroHorariosMaster.cursoAca,
                qFicheroHorariosMaster.estudioId,
                qFicheroHorariosMaster.id,
                qFicheroHorariosMaster.descripcionCA,
                qFicheroHorariosMaster.descripcionES,
                qFicheroHorariosMaster.descripcionEN)
        );
    }

    /**
     * Devuelve el contenido del fichero de un horario de máster
     *
     * @param id
     * @param estudioId
     * @param cursoAca
     * @return
     */
    public FicheroHorariosMaster getHorarioMaster(Long id, Long estudioId, Integer cursoAca) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qFicheroHorariosMaster).where(
                qFicheroHorariosMaster.id.eq(id),
                qFicheroHorariosMaster.estudioId.eq(estudioId),
                qFicheroHorariosMaster.cursoAca.eq(cursoAca)
        ).uniqueResult(qFicheroHorariosMaster);
    }

    public List<Long> getPrimerLectivos(Long estudioId, Integer anyo) {
        QHorarioMasterDetallado qHorariosEstudio1 = new QHorarioMasterDetallado("horariosEstudio1");
        QHorarioMasterDetallado qHorariosEstudio2 = new QHorarioMasterDetallado("horariosEstudio2");

        List<Long> list = new ArrayList<>();

        JPAQuery semestre1 = new JPAQuery(entityManager);

        semestre1.from(qHorariosEstudio1)
                .where(qHorariosEstudio1.estudioId.eq(estudioId)
                        .and(qHorariosEstudio1.cursoAca.eq(anyo))
                        .and(qHorariosEstudio1.semestre.eq("1")));

        Date dateSemestre1 = semestre1.uniqueResult(qHorariosEstudio1.ini.min());

        if (dateSemestre1 != null) {
            list.add(dateSemestre1.getTime());
        } else {
            list.add((new Date()).getTime());
        }

        JPAQuery semestre2 = new JPAQuery(entityManager);
        semestre2.from(qHorariosEstudio2)
                .where(qHorariosEstudio2.estudioId.eq(estudioId)
                        .and(qHorariosEstudio2.cursoAca.eq(anyo))
                        .and(qHorariosEstudio2.semestre.eq("2")));

        Date dateSemestre2 = semestre2.uniqueResult(qHorariosEstudio2.ini.min());

        if (dateSemestre2 != null) {
            list.add(dateSemestre2.getTime());
        } else {
            list.add((new Date()).getTime());
        }

        return list;
    }

    /**
     * Devuelve el horario general de un estudio (el que utilizamos en la vista general de los horarios).
     *
     * @param estudioId
     * @param cursoAca
     * @param fe
     * @return
     */
    public List<HorarioMasterGeneral> getHorarioEstudioMaster(Long estudioId, Integer cursoAca, FiltroEstudio fe) {

        JPAQuery query = new JPAQuery(entityManager);
        BooleanExpression where = qHorarioMasterGeneral.estudioId.eq(estudioId).and(
                qHorarioMasterGeneral.cursoAca.eq(cursoAca).and(qHorarioMasterGeneral.diaSemana.in(1, 2, 3, 4, 5, 6))
        );
        if (fe.getCursos() != null) {
            where = where.and(qHorarioMasterGeneral.curso.in(fe.getCursos()));
        }
        if (fe.getSemestres() != null) {

            List<String> semestres = new ArrayList<String>(2);
            if (fe.getSemestres().contains("1")) {
                semestres.add("1");
            }
            if (fe.getSemestres().contains("2")) {
                semestres.add("2");
            }

            if (fe.getSemestres().contains("a") || fe.getSemestres().contains("A")) {
                if (fe.getSemestres().contains("1") || fe.getSemestres().contains("2")){
                    where = where.and(qHorarioMasterGeneral.tipo.in("a", "A").or(qHorarioMasterGeneral.semestre.in(semestres)));
                } else {
                    where = where.and(qHorarioMasterGeneral.tipo.in("a", "A"));
                }
            } else if (semestres.size() > 0) {
                where = where.and(qHorarioMasterGeneral.semestre.in(semestres));
            }

        }
        if (fe.getCaracteres() != null) {
            HashSet<String> sqlCaracter = new HashSet<>(5);
            for (CaracterAsignatura ca : fe.getCaracteres()) {
                for (String s : ca.getCaracterColumnValue()) {
                    sqlCaracter.add(s);
                }
            }
            where = where.and(qHorarioMasterGeneral.caracter.in(sqlCaracter));
        }

        if (fe.getGrupos() != null) {
            where = where.and(
                    qHorarioMasterGeneral.grupo.in(
                            fe.getGrupos()
                    )
            );
        }
        return query.from(qHorarioMasterGeneral).where(where).orderBy(
                qHorarioMasterGeneral.ini.asc(),
                qHorarioMasterGeneral.asignaturaId.asc(),
                qHorarioMasterGeneral.tipoSubgrupo.asc(),
                qHorarioMasterGeneral.subgrupo.asc())
                .list(qHorarioMasterGeneral);
    }


    /**
     * Devuelve el timestamp del último día lectivo que tenemos para el master y curso académico dado.
     *
     * @param estudioId
     * @param anyo
     * @return
     */
    public Long getUltimoLectivo(Long estudioId, Integer anyo, FiltroEstudio fe) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qHorarioMasterDetallado);

        BooleanExpression where = qHorarioMasterDetallado.estudioId.eq(estudioId).and(qHorarioMasterDetallado.cursoAca.eq(anyo));

        // datos procedentes de filtros
        if (fe.getCursos() != null) {
            where = where.and(qHorarioMasterDetallado.curso.in(fe.getCursos()));
        }
        if (fe.getSemestres() != null) {
            where = where.and(
                    qHorarioMasterDetallado.semestre.in(
                            fe.getSemestres()
                    ).or(qHorarioMasterDetallado.tipo.eq("A"))
            );
        }
        if (fe.getCaracteres() != null) {
            HashSet<String> sqlCaracter = new HashSet<>(5);
            for (CaracterAsignatura ca : fe.getCaracteres()) {
                for (String s : ca.getCaracterColumnValue()) {
                    sqlCaracter.add(s);
                }
            }
            where = where.and(qHorarioMasterDetallado.caracter.in(sqlCaracter));
        }

        if (fe.getGrupos() != null) {
            where = where.and(
                    qHorarioMasterDetallado.grupo.in(
                            fe.getGrupos()
                    )
            );
        }


        Date ret = query.where(where).uniqueResult(qHorarioMasterDetallado.ini.max());
        return ret == null ? 0l : ret.getTime();
    }

    public Map<String, Long> getMinEventos(Long estudioId, Integer anyo, FiltroEstudio fe) {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qHorarioMasterDetallado);

        BooleanExpression where = qHorarioMasterDetallado.estudioId.eq(estudioId).and(qHorarioMasterDetallado.cursoAca.eq(anyo));

        // datos procedentes de filtros
        if (fe.getCursos() != null) {
            where = where.and(qHorarioMasterDetallado.curso.in(fe.getCursos()));
        }
        if (fe.getSemestres() != null) {
            where = where.and(
                    qHorarioMasterDetallado.semestre.in(
                            fe.getSemestres()
                    ).or(qHorarioMasterDetallado.tipo.eq("A"))
            );
        }
        if (fe.getCaracteres() != null) {
            HashSet<String> sqlCaracter = new HashSet<>(5);
            for (CaracterAsignatura ca : fe.getCaracteres()) {
                for (String s : ca.getCaracterColumnValue()) {
                    sqlCaracter.add(s);
                }
            }
            where = where.and(qHorarioMasterDetallado.caracter.in(sqlCaracter));
        }

        Tuple tuple = query.where(where).uniqueResult(qHorarioMasterDetallado.curso.min(), qHorarioMasterDetallado.semestre.min());
        Map<String, Long> res = new HashMap<>();
        res.put("curso", tuple.get(0, Long.class));

        try {
            res.put("semestre", Long.parseLong(tuple.get(1, String.class)));
        }
        catch (Exception e) {
            res.put("semestre", null);
        }
        return res;

    }

    /**
     * Devuelve un listado de horarios de master dado un id de master, un curso académico unas opciones de filtrado
     * y un intervalo de fechas
     *
     * @param estudioId
     * @param cursoAca
     * @param fe
     * @param from
     * @param to
     * @return
     */
    public List<HorarioMasterDetallado> getHorariosEstudio(Long estudioId, Integer cursoAca, FiltroEstudio fe, Date from, Date to) {

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qHorarioMasterDetallado);

        BooleanExpression where = qHorarioMasterDetallado.estudioId.eq(estudioId).and(qHorarioMasterDetallado.cursoAca.eq(cursoAca));

        // datos procedentes de filtros
        if (fe.getCursos() != null) {
            where = where.and(qHorarioMasterDetallado.curso.in(fe.getCursos()));
        }
        if (fe.getSemestres() != null) {
            where = where.and(
                    qHorarioMasterDetallado.semestre.in(
                            fe.getSemestres()
                    ).or(qHorarioMasterDetallado.tipo.eq("A"))
            );
        }
        if (fe.getCaracteres() != null) {
            HashSet<String> sqlCaracter = new HashSet<>(5);
            for (CaracterAsignatura ca : fe.getCaracteres()) {
                for (String s : ca.getCaracterColumnValue()) {
                    sqlCaracter.add(s);
                }
            }
            where = where.and(qHorarioMasterDetallado.caracter.in(sqlCaracter));
        }
        if (fe.getGrupos() != null) {
            where = where.and(
                    qHorarioMasterDetallado.grupo.in(
                            fe.getGrupos()
                    )
            );
        }

        // intervalo temporal
        where = where.and(qHorarioMasterDetallado.ini.goe(from)).and(qHorarioMasterDetallado.fin.loe(to));
        return query.where(where).orderBy(
                qHorarioMasterDetallado.ini.asc(),
                qHorarioMasterDetallado.asignaturaId.asc(),
                qHorarioMasterDetallado.tipoSubgrupo.asc(),
                qHorarioMasterDetallado.subgrupo.asc()
        ).list(qHorarioMasterDetallado);

    }
}
