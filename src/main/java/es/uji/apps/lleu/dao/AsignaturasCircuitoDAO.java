package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.AsignaturaCircuito;
import es.uji.apps.lleu.model.QAsignaturaCircuito;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AsignaturasCircuitoDAO extends BaseDAODatabaseImpl
{
    QAsignaturaCircuito qAsignaturaCircuito = QAsignaturaCircuito.asignaturaCircuito;

    public List<AsignaturaCircuito> getAsignaturasCircuito (Long estudioId, Long circuitoId, Integer cursoAca) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qAsignaturaCircuito).where(
                qAsignaturaCircuito.estudioId.eq(estudioId),
                qAsignaturaCircuito.circuitoId.eq(circuitoId),
                qAsignaturaCircuito.cursoAca.eq(cursoAca)
        ).list(qAsignaturaCircuito);
    }
}
