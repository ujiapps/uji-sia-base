package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.Itinerario;
import es.uji.apps.lleu.model.QItinerario;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ItinerarioDAO extends BaseDAODatabaseImpl
{
    private QItinerario qItinerarios = QItinerario.itinerario;

    public List<Itinerario> getItinerariosFuturos(Long estudioId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qItinerarios).where(qItinerarios.titulacionId.eq(estudioId)
                .and((qItinerarios.cursoAcaIni.loe(anyo).and(qItinerarios.cursoAcaFin.isNull().or(qItinerarios.cursoAcaFin.goe(anyo))))
                        .or(qItinerarios.cursoAcaIni.goe(anyo)))
                .and(qItinerarios.mencion.eq("N")));
        query.orderBy(qItinerarios.titulacionId.asc(),qItinerarios.asignaturaId.asc());

        return query.list(qItinerarios);
    }

    public List<Itinerario> getItinerarios(Long estudioId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qItinerarios).where(qItinerarios.titulacionId.eq(estudioId)
                .and(qItinerarios.cursoAcaIni.loe(anyo).and(qItinerarios.cursoAcaFin.isNull().or(qItinerarios.cursoAcaFin.goe(anyo))))
                .and(qItinerarios.mencion.eq("N")));
        query.orderBy(qItinerarios.titulacionId.asc(),qItinerarios.asignaturaId.asc());

        return query.list(qItinerarios);
    }

    public List<Itinerario> getMencionesFuturas(Long estudioId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qItinerarios).where(qItinerarios.titulacionId.eq(estudioId)
                .and((qItinerarios.cursoAcaIni.loe(anyo).and(qItinerarios.cursoAcaFin.isNull().or(qItinerarios.cursoAcaFin.goe(anyo)))
                        .or(qItinerarios.cursoAcaIni.goe(anyo))))
                .and(qItinerarios.mencion.eq("S")));
        query.orderBy(qItinerarios.titulacionId.asc(),qItinerarios.asignaturaId.asc());
        return query.list(qItinerarios);
    }

    public List<Itinerario> getMenciones(Long estudioId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qItinerarios).where(qItinerarios.titulacionId.eq(estudioId)
                .and(qItinerarios.cursoAcaIni.loe(anyo).and(qItinerarios.cursoAcaFin.isNull().or(qItinerarios.cursoAcaFin.goe(anyo))))
                .and(qItinerarios.mencion.eq("S")));
        query.orderBy(qItinerarios.titulacionId.asc(),qItinerarios.asignaturaId.asc());
        return query.list(qItinerarios);
    }

    public Itinerario isMencionesFuturasOb(Long estudioId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qItinerarios).where(qItinerarios.titulacionId.eq(estudioId)
                .and((qItinerarios.cursoAcaIni.loe(anyo).and(qItinerarios.cursoAcaFin.isNull().or(qItinerarios.cursoAcaFin.goe(anyo)))
                        .or(qItinerarios.cursoAcaIni.goe(anyo))))
                .and(qItinerarios.mencion.eq("S"))
                .and(qItinerarios.caracter.eq("OB")));
        query.orderBy(qItinerarios.titulacionId.asc(),qItinerarios.asignaturaId.asc());
        return query.singleResult(qItinerarios);
    }
}
