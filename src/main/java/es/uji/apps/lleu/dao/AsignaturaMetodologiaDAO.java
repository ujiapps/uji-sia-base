package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.AsignaturaMetodologia;
import es.uji.apps.lleu.model.QAsignaturaMetodologia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AsignaturaMetodologiaDAO extends BaseDAODatabaseImpl
{
    private QAsignaturaMetodologia qAsignaturaMetodologia = QAsignaturaMetodologia.asignaturaMetodologia;

    public List<AsignaturaMetodologia> getMetodologia(String asignaturaId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturaMetodologia).where(qAsignaturaMetodologia.codigoBusqueda.eq(asignaturaId).and(qAsignaturaMetodologia.cursoACA.eq(anyo)));
        query.orderBy(qAsignaturaMetodologia.apartado.id.asc());


        List<AsignaturaMetodologia> list = query.list(qAsignaturaMetodologia);
        return list;
    }
}
