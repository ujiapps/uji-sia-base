package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.ExamenesEstudioMaster;
import es.uji.apps.lleu.model.QExamenesEstudioMaster;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ExamenesMasterDAO extends BaseDAODatabaseImpl
{
    QExamenesEstudioMaster qExamenesEstudioMaster = QExamenesEstudioMaster.examenesEstudioMaster;

//    public List<ExamenesEstudio> getExamenes(Long estudioId, Integer cursoAca)
//    {
//        JPAQuery query = new JPAQuery(this.entityManager);
//
//        return query.from(qExamenesEstudio).where(
//                qExamenesEstudio.cursoAca.eq(cursoAca),
//                qExamenesEstudio.estudioId.eq(estudioId)
//        ).orderBy(
//                qExamenesEstudio.fecha.asc(),
//                qExamenesEstudio.ini.asc()
//        ).list(qExamenesEstudio);
//    }

    /**
     * Indica si hay exámenes definidos para un master / curso académico concreto
     *
     * @param estudioId el id de estudio
     * @param cursoAca el curso académico a considerar
     * @return boolean true si hay exámanes y false si no
     */
    public boolean hayExamenes (Long estudioId, Integer cursoAca) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qExamenesEstudioMaster).where(
                qExamenesEstudioMaster.cursoAca.eq(cursoAca),
                qExamenesEstudioMaster.estudioId.eq(estudioId)
        ).exists();
    }

    /**
     * Obtiene los exámenes por fecha
     *
     * @param estudioId
     * @param cursoAca
     * @param convocatoriaId
     * @return
     */
    public List<ExamenesEstudioMaster> getExamenesByDate(Long estudioId, Integer cursoAca, Integer convocatoriaId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qExamenesEstudioMaster).where(
                qExamenesEstudioMaster.cursoAca.eq(cursoAca),
                qExamenesEstudioMaster.estudioId.eq(estudioId),
                qExamenesEstudioMaster.convocatoriaId.eq(convocatoriaId)
        ).orderBy(
                qExamenesEstudioMaster.fecha.asc(),
                qExamenesEstudioMaster.ini.asc()
        ).list(qExamenesEstudioMaster);
    }
}
