package es.uji.apps.lleu.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.BooleanExpression;
import es.uji.apps.lleu.model.*;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.InfoFiltro;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class HorarioDAO extends BaseDAODatabaseImpl
{
    private QHorarioAsignaturaDetallado qHorarioAsignaturaDetallado = QHorarioAsignaturaDetallado.horarioAsignaturaDetallado;
    private QHorarioAsignaturaGeneral qHorarioAsignaturaGeneral = QHorarioAsignaturaGeneral.horarioAsignaturaGeneral;
    private QHorarioGenerar qHorarioGenerar = QHorarioGenerar.horarioGenerar;

    public List<HorarioAsignaturaDetallado> getHorarioDetalladoAsignatura(Long estudioId, String asignaturaId, Integer anyo, FiltroEstudio filtro, Date from, Date to)
    {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where = qHorarioAsignaturaDetallado.estudioId.eq(estudioId).and(qHorarioAsignaturaDetallado.cursoACA.eq(anyo));
        if (filtro.getGrupos() != null) {
            where = where.and(qHorarioAsignaturaDetallado.grupo.in(filtro.getGrupos()));
        }
        if (filtro.getSemestres() != null) {
            // las asignaturas anuales salen tanto si es primer semestre como sengundo.
            where = where.and(qHorarioAsignaturaDetallado.semestre.in(filtro.getSemestres()));
        }

        return query.from(qHorarioAsignaturaDetallado)
                .where(
                        where,
                        qHorarioAsignaturaDetallado.asignaturaId.eq(asignaturaId),
                        qHorarioAsignaturaDetallado.cursoACA.eq(anyo),
                        qHorarioAsignaturaDetallado.fHorIni.goe(from),
                        qHorarioAsignaturaDetallado.fHorFin.loe(to)
                )
                .distinct()
                .list(qHorarioAsignaturaDetallado);
    }

    public List<HorarioAsignaturaGeneral> getHorarioGeneralAsignatura(Long estudioId, String asignaturaId, Integer cursoAca, FiltroEstudio filtro) {
        JPAQuery query = new JPAQuery(entityManager);

        BooleanExpression where = qHorarioAsignaturaGeneral.estudioId.eq(estudioId).and(qHorarioAsignaturaGeneral.asignaturaId.eq(asignaturaId)).and(qHorarioAsignaturaGeneral.cursoAca.eq(cursoAca));

        if (filtro.getGrupos() != null) {
            where = where.and(qHorarioAsignaturaGeneral.grupo.in(filtro.getGrupos()));
        }
        if (filtro.getSemestres() != null) {
            where = where.and(qHorarioAsignaturaGeneral.semestre.in(filtro.getSemestres()));
        }

        return query.from(qHorarioAsignaturaGeneral).where(where).list(qHorarioAsignaturaGeneral);
    }

    /**
     * Devuelve InfoFiltro con información necesaria para representar los filtros.
     *
     * @param asignaturaId
     * @param anyo
     * @return
     */
    public InfoFiltro getInfoFiltros(String asignaturaId, Integer anyo)
    {
        // grupos
        JPAQuery query = new JPAQuery(entityManager);
        List<String> grupos = query.from(qHorarioAsignaturaDetallado)
                .where(
                        qHorarioAsignaturaDetallado.asignaturaId.eq(asignaturaId),
                        qHorarioAsignaturaDetallado.cursoACA.eq(anyo)
                ).distinct().orderBy(qHorarioAsignaturaDetallado.grupo.asc()).list(qHorarioAsignaturaDetallado.grupo);

        // los tipos de subgrupo
        query = new JPAQuery(entityManager);
        List<String> tiposSubgrupo = query.from(qHorarioAsignaturaDetallado)
                .where(
                        qHorarioAsignaturaDetallado.asignaturaId.eq(asignaturaId),
                        qHorarioAsignaturaDetallado.cursoACA.eq(anyo)
                ).distinct().list(qHorarioAsignaturaDetallado.subGrupoTipo);

        return new InfoFiltro(grupos,tiposSubgrupo);
    }

    public List<Long> getPrimerLectivos(Long estudioId, String asignaturaId, Integer anyo)
    {
        QHorarioAsignaturaDetallado qHorarioAsignatura1 = new QHorarioAsignaturaDetallado("horarios");
        QHorarioAsignaturaDetallado qHorarioAsignatura2 = new QHorarioAsignaturaDetallado("horarios");

        List<Long> list = new ArrayList<>(2);

        JPAQuery semestre1 = new JPAQuery(entityManager);
        semestre1.from(qHorarioAsignatura1)
                .where(
                        qHorarioAsignatura1.estudioId.eq(estudioId),
                        qHorarioAsignatura1.asignaturaId.eq(asignaturaId),
                        qHorarioAsignatura1.cursoACA.eq(anyo),
                        qHorarioAsignatura1.semestre.eq("1")
                );
        Date aux = semestre1.uniqueResult(qHorarioAsignatura1.fHorIni.min());
        if (aux == null) {
            list.add(null);
        } else {
            list.add(aux.getTime());
        }

        JPAQuery semestre2 = new JPAQuery(entityManager);

        semestre2.from(qHorarioAsignatura2)
                .where(qHorarioAsignatura2.estudioId.eq(estudioId),
                        qHorarioAsignatura2.asignaturaId.eq(asignaturaId),
                        qHorarioAsignatura2.cursoACA.eq(anyo),
                        qHorarioAsignatura2.semestre.eq("2")
                );

        aux = semestre2.uniqueResult(qHorarioAsignatura2.fHorIni.min());
        if (aux == null) {
            list.add(null);
        } else {
            list.add(aux.getTime());
        }

        return list;
    }

    public List<HorarioGenerar> getAsignaturas(Long estudioId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qHorarioGenerar).where(
                qHorarioGenerar.estudioId.eq(estudioId),
                qHorarioGenerar.cursoAca.eq(anyo),
                qHorarioGenerar.subGrupo.in("LA", "PR", "SE", "TU")
                );
        return query.distinct().orderBy(qHorarioGenerar.asignaturaId.asc()).list(qHorarioGenerar);
    }


    public Long getUltimoLectivo(Long estudioId, String asignaturaId, Integer anyo, FiltroEstudio filtro)
    {
        BooleanExpression cond = qHorarioAsignaturaDetallado.estudioId.eq(estudioId).and(qHorarioAsignaturaDetallado.cursoACA.eq(anyo));
        if (filtro.getGrupos() != null)
        {
            cond = cond.and(qHorarioAsignaturaDetallado.grupo.in(filtro.getGrupos()));

        }
        if (filtro.getSemestres() != null)
        {
            // las asignaturas anuales salen tanto si es primer semestre como sengundo.
            cond = cond.and(qHorarioAsignaturaDetallado.semestre.in(filtro.getSemestres()));
        }

        JPAQuery query = new JPAQuery(entityManager);
        Date ultimo = query.from(qHorarioAsignaturaDetallado)
                .where(
                        cond,
                        qHorarioAsignaturaDetallado.asignaturaId.eq(asignaturaId)
                )
                .uniqueResult(qHorarioAsignaturaDetallado.fHorIni.max());

        return ultimo == null ? 0l : ultimo.getTime();
    }
}
