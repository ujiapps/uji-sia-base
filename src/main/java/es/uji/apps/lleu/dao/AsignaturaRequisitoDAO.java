package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.AsignaturaRequisito;
import es.uji.apps.lleu.model.QAsignaturaRequisito;
import es.uji.apps.lleu.model.QRequisitosMatriculaEEP;
import es.uji.apps.lleu.model.RequisitosMatriculaEEP;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AsignaturaRequisitoDAO extends BaseDAODatabaseImpl
{
    private QAsignaturaRequisito qAsignaturaRequisito = QAsignaturaRequisito.asignaturaRequisito;
    private QRequisitosMatriculaEEP qRequisitosMatriculaEEP = QRequisitosMatriculaEEP.requisitosMatriculaEEP;

    /**
     * Dado un id de asignatura, titulación y año, devuelve información sobre requisitos de asignatura.
     *
     * @param asignaturaId
     * @param anyo
     * @return
     */
    public List<AsignaturaRequisito> getRequisitos(String asignaturaId, Long estudioId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);

        List<AsignaturaRequisito> ret = query.from(qAsignaturaRequisito).where(
                qAsignaturaRequisito.asignaturaId.eq(asignaturaId),
                qAsignaturaRequisito.estudioId.eq(estudioId),
                qAsignaturaRequisito.cursoAca.eq(anyo)
        ).orderBy(qAsignaturaRequisito.asignaturaIdIncompatible.asc()).distinct().list(qAsignaturaRequisito);

        return ret;
    }


    public List<RequisitosMatriculaEEP> getRequisitosEEP(String asignaturaId, Long estudioId,Integer cursoAca)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qRequisitosMatriculaEEP).where(
                qRequisitosMatriculaEEP.asignaturaId.eq(asignaturaId),
                qRequisitosMatriculaEEP.estudioId.eq(estudioId),
                qRequisitosMatriculaEEP.cursoAca.eq(cursoAca)
//                (qRequisitosMatriculaEEP.cursoAcaIni.isNull().or(qRequisitosMatriculaEEP.cursoAcaIni.loe(cursoAca))
                        //Estas asignaturas con especiales empiezan en el 2020
                        .or(qRequisitosMatriculaEEP.asignaturaId.in("MP1847","MI1847"))
//                (qRequisitosMatriculaEEP.cursoAcaFin.isNull().or(qRequisitosMatriculaEEP.cursoAcaFin.goe(cursoAca)))
        );
        return query.list(qRequisitosMatriculaEEP);
    }
}
