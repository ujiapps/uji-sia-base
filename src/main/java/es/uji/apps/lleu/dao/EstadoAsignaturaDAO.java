package es.uji.apps.lleu.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.EstadoAsignatura;
import es.uji.apps.lleu.model.QEstadoAsignatura;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EstadoAsignaturaDAO extends BaseDAODatabaseImpl
{
    QEstadoAsignatura qGdoEstadoAsignatura = QEstadoAsignatura.estadoAsignatura;

    public EstadoAsignatura getEstadoAsignatura(String asignaturaId, Integer anyo) {

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qGdoEstadoAsignatura).where(
                qGdoEstadoAsignatura.asignaturaId.eq(asignaturaId),
                qGdoEstadoAsignatura.cursoACa.eq(anyo)
        ).uniqueResult(qGdoEstadoAsignatura);

    }
}
