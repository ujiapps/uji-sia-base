package es.uji.apps.lleu.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.Convocatoria;
import es.uji.apps.lleu.model.QConvocatoria;
import es.uji.apps.lleu.model.QExamenesEstudio;
import es.uji.apps.lleu.model.QExamenesEstudioMaster;
import es.uji.apps.lleu.model.enums.TipoDocencia;
import es.uji.commons.db.BaseDAODatabaseImpl;

import static java.lang.Math.abs;

@Repository
public class ConvocatoriaDAO extends BaseDAODatabaseImpl
{

    private QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
    private QExamenesEstudio qExamenesEstudio = QExamenesEstudio.examenesEstudio;
    private QExamenesEstudioMaster qExamenesEstudioMaster = QExamenesEstudioMaster.examenesEstudioMaster;

    /**
     * Devuelve las convocatorias de examenes para Grados.
     * @return
     */
    public List<Convocatoria> getConvocatoriasGrados()
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qConvocatoria)
                .where(qConvocatoria.tipo.eq("G"))
                .orderBy(qConvocatoria.orden.desc())
                .distinct()
                .list(qConvocatoria);
    }
    /**
     * Devuelve las convocatorias de examenes para Masters.
     * @return
     */
    public List<Convocatoria> getConvocatoriasMasters()
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qConvocatoria)
                .where(qConvocatoria.tipo.eq("M"))
                .orderBy(qConvocatoria.orden.desc())
                .distinct()
                .list(qConvocatoria);
    }

    /**
     * Devuelve el id de convocatoria activa.
     *
     * @param estudioId
     * @param cursoAca
     * @return
     */
    public Integer getConvocatoriaActiva(Long estudioId, Integer cursoAca, TipoDocencia tipoDocencia){
        if(tipoDocencia.equals(TipoDocencia.GRADO)){
            return getConvocatoriaActivaGrado(estudioId,cursoAca);
        }else {
            return getConvocatoriaActivaMaster(estudioId,cursoAca);
        }
    }

    public Integer getConvocatoriaActivaGrado(Long estudioId, Integer cursoAca)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        List<Tuple> list = query.from(qExamenesEstudio)
                .where(
                        qExamenesEstudio.cursoAca.eq(cursoAca),
                        qExamenesEstudio.estudioId.eq(estudioId)
                )
                .distinct().orderBy(qExamenesEstudio.fecha.asc())
                .list(qExamenesEstudio.fecha, qExamenesEstudio.convocatoriaId);

        if (list.isEmpty()) {
            return 0;
        }

        Date today = new Date();
        long minFecha = abs(list.get(0).get(qExamenesEstudio.fecha).getTime()-today.getTime());
        Integer convocatoria = list.get(0).get(qExamenesEstudio.convocatoriaId);

        for(Tuple t : list){
            Date fecha = t.get(qExamenesEstudio.fecha);
            if(abs(fecha.getTime()-today.getTime()) < minFecha){
                minFecha=abs(fecha.getTime()-today.getTime());
                convocatoria = t.get(qExamenesEstudio.convocatoriaId);
            }
        }
        return convocatoria;
    }

    public Integer getConvocatoriaActivaMaster(Long estudioId, Integer cursoAca)
    {
        JPAQuery query = new JPAQuery(this.entityManager);

        List<Tuple> list = query.from(qExamenesEstudioMaster)
                .where(
                        qExamenesEstudioMaster.cursoAca.eq(cursoAca),
                        qExamenesEstudioMaster.estudioId.eq(estudioId)
                )
                .distinct().orderBy(qExamenesEstudioMaster.fecha.asc())
                .list(qExamenesEstudioMaster.fecha, qExamenesEstudioMaster.convocatoriaId);

        if (list.isEmpty()) {
            return 0;
        }

        Date today = new Date();
        long minFecha = abs(list.get(0).get(qExamenesEstudioMaster.fecha).getTime()-today.getTime());
        Integer convocatoria = list.get(0).get(qExamenesEstudioMaster.convocatoriaId);

        for(Tuple t : list){
            Date fecha = t.get(qExamenesEstudioMaster.fecha);
            if(abs(fecha.getTime()-today.getTime()) < minFecha){
                minFecha=abs(fecha.getTime()-today.getTime());
                convocatoria = t.get(qExamenesEstudioMaster.convocatoriaId);
            }
        }
        return convocatoria;
    }

}
