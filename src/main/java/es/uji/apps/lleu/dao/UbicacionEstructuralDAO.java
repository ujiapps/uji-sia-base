package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.QUbicacionEstructural;
import es.uji.apps.lleu.model.UbicacionEstructural;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class UbicacionEstructuralDAO extends BaseDAODatabaseImpl
{

    private QUbicacionEstructural qUbicacionEstructural = QUbicacionEstructural.ubicacionEstructural;

    /**
     * Devuelve ubicaciones estructurales por id
     *
     * @param ids
     * @return
     */
    public List<UbicacionEstructural> getByIds (List<Long> ids) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qUbicacionEstructural).where(
                qUbicacionEstructural.id.in(ids)
        ).list(qUbicacionEstructural);
    }

    public UbicacionEstructural getById (Long id) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qUbicacionEstructural).where(
                qUbicacionEstructural.id.eq(id),
                qUbicacionEstructural.id.in(2,3,4,2922)
        ).uniqueResult(qUbicacionEstructural);
    }
}
