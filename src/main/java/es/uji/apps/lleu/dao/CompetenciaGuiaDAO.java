package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.Competencia;
import es.uji.apps.lleu.model.QCompetencia;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CompetenciaGuiaDAO extends BaseDAODatabaseImpl
{
    QCompetencia qCompetencia = QCompetencia.competencia;

    public List<Competencia> getCompetencias(String asignaturaId, Integer anyo) {
        JPAQuery q = new JPAQuery(entityManager);
        return q.from(qCompetencia).where(
                qCompetencia.cursoACa.eq(anyo),
                qCompetencia.asignaturaId.eq(asignaturaId)
        ).list(qCompetencia);
    }
}
