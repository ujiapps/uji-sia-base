package es.uji.apps.lleu.dao;

import com.mysema.query.jpa.JPASubQuery;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.lleu.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EstudioDAO extends BaseDAODatabaseImpl
{
    private QEstudio qEstudio = QEstudio.estudio;
    private QEstudioTodo qEstudioTodo = QEstudioTodo.estudioTodo;
    private QEstudioUrl qEstudioUrl = QEstudioUrl.estudioUrl;
    private QEstudiosPlanesVW qEstudiosPlanesVW = QEstudiosPlanesVW.estudiosPlanesVW;

    public List<Estudio> getEstudios(Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query
                .from(qEstudio)
                .where(
                        qEstudio.cursoAca.isNull().or(
                                qEstudio.cursoAca.eq(anyo)
                        ),
                        qEstudio.cursoAcaIni.loe(anyo).and(
                                qEstudio.cursoAcaFin.isNull().or(
                                        qEstudio.cursoAcaFin.goe(anyo)
                                )
                        )
                )
                .list(qEstudio);
    }

    public EstudioTodo getById(Long id, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query
                .from(qEstudioTodo)
                .where(
                        qEstudioTodo.id.eq(id),
                        qEstudioTodo.cursoAca.eq(-1).or(
                                qEstudioTodo.cursoAca.eq(anyo)
                        )
                )
                .uniqueResult(qEstudioTodo);
    }

    public EstudioUrl getEstudioUrls(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);

        return query
                .from(qEstudioUrl)
                .where(
                        qEstudioUrl.estudioId.eq(id)
                )
                .uniqueResult(qEstudioUrl);

    }

    /**
     * Devuelve los estudios que necesitamos con los datos estrictamente necesarios para pintar la portada de estudios
     * del LLEU.
     *
     * @param anyo
     * @return
     */
    public List<Estudio> getEstudiosPortadaOld(Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        return query
                .from(qEstudio)
                .where(
                        qEstudio.cursoAca   .isNull().or(
                                qEstudio.cursoAca.eq(anyo)
                        ),
                        qEstudio.cursoAcaIni.loe(anyo).and(
                                qEstudio.cursoAcaFin.isNull().or(
                                        qEstudio.cursoAcaFin.goe(anyo)
                                )
                        )
                )
                .list(QEstudio.create(
                        qEstudio.id,
                        qEstudio.cursoAcaFinDocencia,
                        qEstudio.nombreCA,
                        qEstudio.nombreEN,
                        qEstudio.nombreES,
                        qEstudio.tipo,
                        qEstudio.uestId,
                        qEstudio.nivel,
                        qEstudio.comentarioPortada,
                        qEstudio.responsablesUrl
                ));
    }

    public List<EstudioPortada> getEstudiosPortada(Integer anyo)
    {
        JPAQuery query= new JPAQuery(entityManager)
                .from(qEstudiosPlanesVW)
                .where(qEstudiosPlanesVW.cursoAca.eq(anyo));
        return query.list(qEstudiosPlanesVW.estudioId,
                        qEstudiosPlanesVW.tipoEstudio,
                        qEstudiosPlanesVW.nombreEstudioCa,
                        qEstudiosPlanesVW.nombreEstudioEn,
                        qEstudiosPlanesVW.nombreEstudioEs,
                        qEstudiosPlanesVW.cursoAca,
                        qEstudiosPlanesVW.planId,
                        qEstudiosPlanesVW.nombrePlanCa,
                        qEstudiosPlanesVW.nombrePlanEs,
                        qEstudiosPlanesVW.nombrePlanEn,
                        qEstudiosPlanesVW.ubicacionId,
                        qEstudiosPlanesVW.ubicacionCa,
                        qEstudiosPlanesVW.ubicacionEs,
                        qEstudiosPlanesVW.ubicacionEn)
                .stream().map(tuple -> {
                    EstudioPortada estudioPortada = new EstudioPortada();
                    estudioPortada.setEstudioId(tuple.get(qEstudiosPlanesVW.estudioId));
                    estudioPortada.setTipoEstudio(tuple.get(qEstudiosPlanesVW.tipoEstudio));
                    estudioPortada.setNombreCa(tuple.get(qEstudiosPlanesVW.nombreEstudioCa));
                    estudioPortada.setNombreEn(tuple.get(qEstudiosPlanesVW.nombreEstudioEn));
                    estudioPortada.setNombreEs(tuple.get(qEstudiosPlanesVW.nombreEstudioEs));
                    estudioPortada.setCursoAca(tuple.get(qEstudiosPlanesVW.cursoAca));
                    estudioPortada.setPlanId(tuple.get(qEstudiosPlanesVW.planId));
                    estudioPortada.setNombrePLanCa(tuple.get(qEstudiosPlanesVW.nombrePlanCa));
                    estudioPortada.setNombrePlanEs(tuple.get(qEstudiosPlanesVW.nombrePlanEs));
                    estudioPortada.setNombrePlanEn(tuple.get(qEstudiosPlanesVW.nombrePlanEn));
                    estudioPortada.setUbicacionId(tuple.get(qEstudiosPlanesVW.ubicacionId));
                    estudioPortada.setUbicacionCa(tuple.get(qEstudiosPlanesVW.ubicacionCa));
                    estudioPortada.setUbicacionEs(tuple.get(qEstudiosPlanesVW.ubicacionEs));
                    estudioPortada.setUbicacionEn(tuple.get(qEstudiosPlanesVW.ubicacionEn));
                    return estudioPortada;
                }).collect(Collectors.toList());
    }

    public Long isExtinto(Long estudioId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        Integer finDocencia = query
                .from(qEstudio)
                .where(
                        qEstudio.cursoAca.isNull().or(
                                qEstudio.cursoAca.eq(anyo)
                        ),
                        qEstudio.cursoAcaIni.loe(anyo).and(
                                qEstudio.cursoAcaFin.isNull().or(
                                        qEstudio.cursoAcaFin.goe(anyo)
                                )
                        ), qEstudio.id.eq(estudioId)
                ).singleResult(qEstudio.cursoAcaFinDocencia);
        if (finDocencia == null || finDocencia >= anyo)
        {
            return 0L;
        }
        else
        {
            return 1L;
        }
    }
}
