package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.PresentacionCoordinadores;
import es.uji.apps.lleu.model.PresentacionMaster;
import es.uji.apps.lleu.model.QPresentacionCoordinadores;
import es.uji.apps.lleu.model.QPresentacionMaster;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PresentacionEstudioDAO extends BaseDAODatabaseImpl
{
    QPresentacionCoordinadores qPresentacionCoordinadores = QPresentacionCoordinadores.presentacionCoordinadores;
    QPresentacionMaster qPresentacionMaster = QPresentacionMaster.presentacionMaster;

    public List<PresentacionCoordinadores> getCoordinadores (Long estudioId) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPresentacionCoordinadores).where(
                qPresentacionCoordinadores.estudioId.eq(estudioId)
        ).orderBy(qPresentacionCoordinadores.nombre.asc()).list(qPresentacionCoordinadores);
    }

    public PresentacionMaster getPresentacionEstudio (Long estudioId, Integer anyo) {
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qPresentacionMaster).where(
                qPresentacionMaster.estudioId.eq(estudioId),
                qPresentacionMaster.cursoAca.eq(anyo)
        ).uniqueResult(qPresentacionMaster);
    }
}
