package es.uji.apps.lleu.dao;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.AsignaturaProfesor;
import es.uji.apps.lleu.model.QAsignaturaProfesor;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AsignaturaProfesorDAO extends BaseDAODatabaseImpl
{
    private QAsignaturaProfesor qAsignaturaProfesor = QAsignaturaProfesor.asignaturaProfesor;

    public List<AsignaturaProfesor> getProfesores(String asignaturaId, Integer anyo)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAsignaturaProfesor).where(qAsignaturaProfesor.asiId.eq(asignaturaId).and(qAsignaturaProfesor.cursoAca.eq(anyo)));
        query.orderBy(qAsignaturaProfesor.orden.asc());
        return query.list(qAsignaturaProfesor);
    }
}
