package es.uji.apps.lleu.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.lleu.model.EstudioCredito;
import es.uji.apps.lleu.model.QEstudioCredito;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class EstudioCreditoDAO extends BaseDAODatabaseImpl
{
    private QEstudioCredito qEstudioCredito = QEstudioCredito.estudioCredito;

    public List<EstudioCredito> getResumenCreditos(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qEstudioCredito).where(qEstudioCredito.id.eq(id));
        return query.distinct().list(qEstudioCredito);
    }

}
