package es.uji.apps.lleu.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.AsignaturasMasterDAO;
import es.uji.apps.lleu.dao.SistemaEvaluacionDAO;
import es.uji.apps.lleu.model.AsignaturasMaster;
import es.uji.apps.lleu.model.HorarioAsigMasterDetallado;
import es.uji.apps.lleu.model.HorarioAsigMasterGeneral;
import es.uji.apps.lleu.model.SistemaEvaluacionMaster;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.InfoFiltro;

@Service
public class AsignaturasMasterService
{
    private AsignaturasMasterDAO asignaturasMasterDAO;
    private SistemaEvaluacionDAO sistemaEvaluacionDAO;

    @Autowired
    public AsignaturasMasterService(AsignaturasMasterDAO asignaturasMasterDAO,
                                    SistemaEvaluacionDAO sistemaEvaluacionDAO)
    {
        this.asignaturasMasterDAO = asignaturasMasterDAO;
        this.sistemaEvaluacionDAO = sistemaEvaluacionDAO;
    }

    public List<AsignaturasMaster> getAll()
    {
        return asignaturasMasterDAO.get(AsignaturasMaster.class);
    }

    public AsignaturasMaster getById(String id)
    {
        return asignaturasMasterDAO.get(AsignaturasMaster.class, id).get(0);
    }


    public List<AsignaturasMaster> getAsignaturasByIdAnyo(Long id, Integer anyo)
    {
        return asignaturasMasterDAO.getAsignaturasByIdAnyo(id,anyo);
    }


    /**
     * Devuelve un listado de asignturas para un estudio dado, en un año dado y dadas unas condiciones de filtrado.
     *
     * @param estudioId el id de estudio a considerar
     * @param anyo el curso académico a considerar
     * @param filtro especificación de filtrado a aplicar
     * @param solsConvalida si es null devuelve cualquier valor de sols convalida. Si no, indica el valor que debe tener.
     * @return
     */
    public List<AsignaturasMaster> getAsignaturasByFilter(Long estudioId, Integer anyo, FiltroEstudio filtro, String solsConvalida) {
        return asignaturasMasterDAO.getAsignaturasByFilter(estudioId, anyo, filtro, solsConvalida);
    }

    public InfoFiltro getFiltroInfo(Long estudioId, Integer anyo) {
        return this.asignaturasMasterDAO.getInfoFiltros(estudioId, anyo);
    }

    /**
     * Devuelve información sobre las asignaturas de un estudio dado
     *
     * @param asignaturaId el id del estudio que queremos consultar
     * @param anyo el curso académico que queremos consultar
     * @return información sobre los estudios
     */
    public InfoFiltro getFiltroInfo (Long estudioId, String asignaturaId, Integer anyo) {
        return this.asignaturasMasterDAO.getInfoFiltros(estudioId, asignaturaId, anyo);
    }


    /**
     * Devuelve una asignatura de master
     * @param asignaturaId
     * @param estudioId
     * @param anyo
     * @return
     */
    public AsignaturasMaster getAsignatura(String asignaturaId, Long estudioId, Integer anyo)
    {
        return this.asignaturasMasterDAO.getAsignatura(asignaturaId,estudioId,anyo);
    }

    public List<SistemaEvaluacionMaster> getSistemasEvaluacion(String asignaturaId, Integer anyo) {
        return this.sistemaEvaluacionDAO.getSistemasEvaluacionMaster(asignaturaId, anyo);
    }

    public List<Long> getPrimerDiaLectivo (Long estudioId, String asignaturaId, Integer anyo) {
        return asignaturasMasterDAO.getPrimerLectivos(estudioId, asignaturaId, anyo);
    }

    public Long getUltimoLectivo (Long estudioId, String asignaturaId, Integer anyo, FiltroEstudio fe) {
        return asignaturasMasterDAO.getUltimoLectivo(estudioId,asignaturaId, anyo, fe);
    }

    public List<HorarioAsigMasterGeneral> getHorarioGeneralAsignatura(Long estudioId, String asignaturaId, Integer cursoAca, FiltroEstudio fe) {
        return asignaturasMasterDAO.getHorarioGeneralAsignatura(estudioId, asignaturaId, cursoAca, fe);
    }

    public List<HorarioAsigMasterDetallado> getHorarioDetalladoAsignatura(Long estudioId, String asignaturaId, Integer anyo, FiltroEstudio fe, Date from, Date to)
    {
        return asignaturasMasterDAO.getHorarioDetalladoAsignatura(estudioId,asignaturaId, anyo, fe, from ,to);
    }
}
