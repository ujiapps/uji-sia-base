package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.PersonaRiesgoDAO;
import es.uji.apps.lleu.dao.ProfesorEstudioDAO;
import es.uji.apps.lleu.model.AsignaturaProfesorMaster;
import es.uji.apps.lleu.model.ProfesorGrado;
import es.uji.apps.lleu.model.ProfesorMaster;

@Service
public class ProfesoresEstudioService
{
    private ProfesorEstudioDAO profesorEstudioDAO;

    private PersonaRiesgoDAO personaRiesgoDAO;

    @Autowired
    public ProfesoresEstudioService(ProfesorEstudioDAO profesorEstudioDAO, PersonaRiesgoDAO personaRiesgoDAO)
    {
        this.profesorEstudioDAO = profesorEstudioDAO;
        this.personaRiesgoDAO = personaRiesgoDAO;
    }

    public List<ProfesorMaster> getProfesoresMasterByIdAnyo(Long estudioId, Integer anyo)
    {
        return profesorEstudioDAO.getProfesoresMaster(estudioId,anyo);
    }

    public List<ProfesorGrado> getProfesoresGradoByIdAnyo (Long estudioId, Integer anyo) {
        return profesorEstudioDAO.getProfesoresGrado(estudioId, anyo);
    }

    public List<AsignaturaProfesorMaster> getProfesoresMaster(String asignaturaId, Integer anyo)
    {
       return profesorEstudioDAO.getProfesoresMasterByAsig(asignaturaId,anyo);
    }

    public List<Long> getPersonasRiesgo(){
        return personaRiesgoDAO.getPersonasRiesgo();
    }

}