package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.RequisitosMatriculaDAO;
import es.uji.apps.lleu.model.AsignaturaIncompatible;
import es.uji.apps.lleu.model.RequisitosMatricula;
import es.uji.apps.lleu.model.RequisitosMatriculaEEP;

@Service
public class RequisitosMatriculaService
{
    private RequisitosMatriculaDAO requisitosMatriculaDAO;

    @Autowired
    public RequisitosMatriculaService( RequisitosMatriculaDAO requisitosMatriculaDAO)
    {
        this.requisitosMatriculaDAO = requisitosMatriculaDAO;
    }
    public List<AsignaturaIncompatible> getAsignaturas (Long estudioId, Integer cursoAca) {
        return this.requisitosMatriculaDAO.getAsignaturasACursar(estudioId, cursoAca);
    }
    public List<RequisitosMatricula> getRequisitosMatricula (Long estudioId, Integer cursoAca) {
        return this.requisitosMatriculaDAO.getRequisitosMatricula(estudioId, cursoAca);
    }

    public List<RequisitosMatriculaEEP> getRequisitosMatriculaEEP(Long estudioId, Integer cursoAca)
    {
        return this.requisitosMatriculaDAO.getRequisitosMatriculaEEP(estudioId, cursoAca);
    }
}
