package es.uji.apps.lleu.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.lleu.model.AsignaturasGrado;
import es.uji.apps.lleu.model.AsignaturasMaster;
import es.uji.apps.lleu.model.VacantesGrupoGrado;
import es.uji.apps.lleu.model.VacantesSubgrupoGrado;
import es.uji.apps.lleu.model.enums.TipoDocencia;
import es.uji.apps.lleu.services.AsignaturasGradoService;
import es.uji.apps.lleu.services.AsignaturasMasterService;
import es.uji.apps.lleu.services.EstudioService;
import es.uji.apps.lleu.ui.AsignaturaGradoUI;
import es.uji.apps.lleu.ui.AsignaturaMasterUI;
import es.uji.apps.lleu.ui.EstudioUI;
import es.uji.apps.lleu.ui.VacantesGrupoGradoUI;
import es.uji.apps.lleu.ui.VacantesSubgrupoGradoUI;
import es.uji.apps.lleu.utils.AppInfo;
import es.uji.apps.lleu.utils.CursoAcademico;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.PeriodoMatricula;
import es.uji.commons.web.template.Template;


public class EstudioAsignaturasResource
{
    @PathParam("anyo")
    private Integer anyo;

    @InjectParam
    private AsignaturasGradoService asignaturasGradoService;

    @InjectParam
    private AsignaturasMasterService asignaturasMasterService;

    @InjectParam
    private EstudioService estudioService;


    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template getAsignaturas(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                 @PathParam("id") Long estudioId,
                                 @QueryParam("nombre") @DefaultValue("") String filtroNombre,
                                 @QueryParam("cursos") @DefaultValue("") String filtroCursos,
                                 @QueryParam("grupos") @DefaultValue("") String filtroGrupos,
                                 @QueryParam("semestres") @DefaultValue("") String filtroSemestres,
                                 @QueryParam("caracteres") @DefaultValue("") String filtroCaracteres,
                                 @QueryParam("orden") @DefaultValue("") String orden
    ) throws ParseException
    {
        FiltroEstudio fe = FiltroEstudio.getInstanceFromQueryStringParams(
                idioma,
                filtroNombre,
                filtroCursos,
                filtroGrupos,
                filtroSemestres,
                filtroCaracteres,
                orden
        );
        Template template = null;
        EstudioUI estudioUI = estudioService.getById(estudioId,idioma ,this.anyo);

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {
            template = AppInfo.buildPaginaForAjax("sia/estudio/asignaturas-contenido", this.anyo, this.anyo + "/estudio/" + estudioId  , idioma, String.join(" ", "SIA","-",estudioUI.getNombre()));

            List<AsignaturasGrado> asignaturasGrados = asignaturasGradoService.getAsignaturasByFilter(estudioId, this.anyo, fe);
            List<VacantesGrupoGrado> vacantesAsignaturas = new ArrayList<>();
            if(PeriodoMatricula.isPeriodoAsignaturas() && anyo == CursoAcademico.getCurrent()) {
                vacantesAsignaturas = asignaturasGradoService.getVacantesAsignaturas(anyo, asignaturasGrados );
            }else{
                template.put("periodoAsignaturas", false);
            }
            template.put("listadoAsignaturas", AsignaturaGradoUI.toUI(asignaturasGrados,vacantesAsignaturas,idioma));

        }else{
            template = AppInfo.buildPaginaForAjax("sia/master/asignaturas-contenido", this.anyo, this.anyo + "/estudio/" + estudioId  , idioma,String.join(" ", "SIA","-",estudioUI.getNombre()));

            List<AsignaturasMaster> asignaturasMaster = asignaturasMasterService.getAsignaturasByFilter(estudioId, this.anyo, fe, "N");
            template.put("listadoAsignaturas", AsignaturaMasterUI.toUI(asignaturasMaster,idioma));

        }
        template.put("estudio", estudioUI);

        return template;
    }

    @GET
    @Path("vacantes")
    @Produces(MediaType.TEXT_HTML)
    public Template getVacantesSubgrupo(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                        @PathParam("id") Long estudioId,
                                        @QueryParam("asignatura") @DefaultValue("") String asignaturaId)
            throws ParseException
    {
        List<VacantesSubgrupoGrado> vacantes = this.asignaturasGradoService.getVacantesSubgrupoGrado(anyo,estudioId, Collections.singletonList(asignaturaId));
        VacantesGrupoGradoUI vacantesAsignatura = VacantesGrupoGradoUI.toUI(asignaturasGradoService.getVacantesAsignatura(anyo, asignaturaId));

        VacantesSubgrupoGradoUI vacantesSubgrupoGradoUI = VacantesSubgrupoGradoUI.toUI(vacantes);

        Template template = null;

        template = AppInfo.buildPaginaForAjax("sia/fragments/info-periodo-matricula", this.anyo, this.anyo + "/estudio/" + estudioId  , idioma,"SIA");
        template.put("vacantesGrupos", vacantesAsignatura.getVacantesGrupos());
        template.put("vacantesNuevosGrupos", vacantesAsignatura.getVacantesNuevosGrupos());

        template.put("vacantesSubgrupos", vacantesSubgrupoGradoUI.getVacantesSubgrupos());
        template.put("vacantesNuevosSubgrupos", vacantesSubgrupoGradoUI.getVacantesNuevosSubgrupos());


        return template;
    }
}