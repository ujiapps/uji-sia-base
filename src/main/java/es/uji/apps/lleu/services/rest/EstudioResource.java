package es.uji.apps.lleu.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.lleu.model.*;
import es.uji.apps.lleu.model.enums.*;
import es.uji.apps.lleu.services.*;
import es.uji.apps.lleu.ui.*;
import es.uji.apps.lleu.utils.*;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Path("estudio")
public class EstudioResource {
    @InjectParam
    ReconocimientoService reconocimientoService;
    @InjectParam
    RequisitosMatriculaService requisitosMatriculaService;
    @InjectParam
    AsignaturaProfesorService asignaturaProfesorService;
    @PathParam("anyo")
    private Integer anyo;
    @InjectParam
    private EstudioService estudioService;
    @InjectParam
    private AsignaturasGradoService asignaturasGradoService;
    @InjectParam
    private EstudioCreditoService estudioCreditoService;
    @InjectParam
    private ComplementosService complementosService;
    @InjectParam
    AsignaturasMasterService asignaturasMasterService;
    @InjectParam
    ProfesoresEstudioService profesoresEstudioService;
    @InjectParam
    EspecialidadService especialidadService;
    @InjectParam
    ConvocatoriaService convocatoriaService;
    @InjectParam
    UbicacionesEstructuralesService ubicacionesEstructuralesService;
    @InjectParam
    private CalendarOutputStream calendarOutput;

    @InjectParam
    FotoService fotosService;

    @InjectParam
    private EstudioMasterService estudioMasterService;

    /**
     * Para llamada AJAX de obtención de asignaturas de un estudio
     *
     * @param estudioAsignaturasResource
     * @return
     */
    @Path("{id}/asignaturas")
    public EstudioAsignaturasResource getPlatformItem(
            @InjectParam EstudioAsignaturasResource estudioAsignaturasResource) {
        return estudioAsignaturasResource;
    }

    /**
     * Para ver información de asignaturas
     *
     * @param asignaturaResource
     * @return
     */
    @Path("{id}/asignatura")
    public AsignaturaResource getPlatformItem(
            @InjectParam AsignaturaResource asignaturaResource) {
        return asignaturaResource;
    }

    /**
     * Para ver generar los horarios del estudio
     *
     * @param estudioHorarioResource
     * @return
     */
    @Path("{id}/horarios")
    public EstudioHorarioResource getPlatformItem(
            @InjectParam EstudioHorarioResource estudioHorarioResource) {
        return estudioHorarioResource;
    }


    @GET
    @Path("{id}")
    @Produces(MediaType.TEXT_HTML)
    public Template get(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                        @PathParam("id") Long estudioId,
                        @QueryParam("nombre") @DefaultValue("") String filtroNombre,
                        @QueryParam("cursos") @DefaultValue("") String filtroCursos,
                        @QueryParam("grupos") @DefaultValue("") String filtroGrupos,
                        @QueryParam("semestres") @DefaultValue("") String filtroSemestres,
                        @QueryParam("caracteres") @DefaultValue("") String filtroCaracteres,
                        @QueryParam("orden") @DefaultValue("") String orden,
                        @QueryParam("pm") Boolean periodoAsignaturas

    ) throws MalformedURLException,
            ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);

        // Parseamos los filtros que tenemos que aplicar
        FiltroEstudio fe = FiltroEstudio.getInstanceFromQueryStringParams(
                idioma,
                filtroNombre,
                filtroCursos,
                filtroGrupos,
                filtroSemestres,
                filtroCaracteres,
                orden
        );

        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId, this.anyo, idioma, String.join(" ", "SIA","-",estudioUI.getNombre()),false, periodoAsignaturas);
        template.put("contenido", "sia/estudio/base");
        template.put("estudio", estudioUI);
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("contenidoestudio", "sia/estudio/contenido");
        template.put("pageclass", "page-asignaturas");

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {
            // Calculamos los filtros que deben estar activos
            InfoFiltro ifs = asignaturasGradoService.getFiltroInfo(estudioId, this.anyo);
            ifs.calculateActiveValues(fe);

            // Obtenemos un listado de asignaturas filtradas
            List<AsignaturasGrado> asignaturasGrados = asignaturasGradoService.getAsignaturasByFilter(estudioId, this.anyo, fe);
            List<VacantesGrupoGrado> vacantesGrupoGrados = new ArrayList<>();
            if ((periodoAsignaturas != null || PeriodoMatricula.isPeriodoAsignaturas()) && anyo == CursoAcademico.getCurrent()) {
                vacantesGrupoGrados = asignaturasGradoService.getVacantesAsignaturas(anyo, asignaturasGrados);
            } else {
                template.put("periodoAsignaturas", false);
            }
            HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);

            template.put("asignaturascontenido", "sia/estudio/asignaturas-contenido");
            template.put("infofiltroestudio", ifs);
            template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
            template.put("listadoAsignaturas", AsignaturaGradoUI.toUI(asignaturasGrados, vacantesGrupoGrados, idioma));
        } else {
            // Calculamos los filtros que deben estar activos
            InfoFiltro ifs = asignaturasMasterService.getFiltroInfo(estudioId, this.anyo);
            ifs.calculateActiveValues(fe);

            // Obtenemos un listado de asignaturas filtradas
            List<AsignaturasMaster> asignaturasMaster = asignaturasMasterService.getAsignaturasByFilter(estudioId, this.anyo, fe, "N");
            List<AsignaturaMasterUI> asignaturasMasterUI = AsignaturaMasterUI.toUI(asignaturasMaster, idioma);

            ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));

            template.put("totalCreditos", resumen.getCreditosTotal());
            template.put("asignaturascontenido", "sia/master/asignaturas-contenido");
            template.put("infofiltroestudio", ifs);
            template.put("listadoAsignaturas", asignaturasMasterUI);
        }
        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }

    /**
     * Metodo que devuelve un template de los reconocimientos de un estudio.
     *
     * @param idioma
     * @param estudioId
     * @return Template
     * @throws ParseException
     */
    @GET
    @Path("{id}/reconocimientos")
    @Produces(MediaType.TEXT_HTML)
    public Template getReconocimientos(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                       @PathParam("id") Long estudioId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        List<Reconocimiento> reconocimientos = reconocimientoService.getReconocimientosEstudio(anyo, estudioId);

        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/reconocimientos", this.anyo, idioma, String.join(" ", "SIA","-",estudioUI.getNombre()),false, false);

        Map<Long, EstudioReconocimientoUI> lreconocimientos = EstudioReconocimientoUI.toUI(reconocimientos, idioma);

        template.put("contenido", "sia/estudio/base");

        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("estudioReconocimiento", lreconocimientos);
        template.put("pageclass", "page-reconocimientos");

        if (estudioUI.getTipo().equals("G")) {
            EstudioRec er = reconocimientoService.getEstudioRec(estudioId, this.anyo);
            if (er != null) {
                template.put("reclaboralcfgs", ReconocimientoCFGLaboralUI.toUI(er));
            }

            List<AsignaturaSoloRecGrado> asignaturasSolsConvalida = asignaturasGradoService.getAsignaturasSoloReconocimiento(estudioId, anyo);

            template.put("soloconvalida", AsignaturaSoloRecGradoUI.toUI(asignaturasSolsConvalida, idioma));
            template.put("contenidoestudio", "sia/estudio/reconocimientos");
        } else if (estudioUI.getTipo().equals("M")) {

            // reconocimientos de máster.
            EstudioRecMaster erm = reconocimientoService.getEstudioRecMaster(estudioId);
            if (erm != null) {
                template.put("recmaster", EstudioRecMasterUI.toUI(erm));
            }

            // asignaturas que sólo convalidan
            List<AsignaturasMaster> asignaturasSolsConvalida = asignaturasMasterService.getAsignaturasByFilter(
                    estudioId,
                    anyo,
                    new FiltroEstudio(idioma),
                    "S");
            ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));

            template.put("totalCreditos", resumen.getCreditosTotal());
            template.put("soloconvalida", AsignaturaMasterUI.toUI(asignaturasSolsConvalida, idioma));
            template.put("contenidoestudio", "sia/master/reconocimientos");

        }

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }


    @GET
    @Path("{id}/examenes")
    @Produces(MediaType.TEXT_HTML)
    @Consumes("text/calendar")
    public Response getExamenes(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                @PathParam("id") Long estudioId, @HeaderParam("Content-Type") MediaType type
    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);

        if (ParamUtils.isNotNull(type) && type.toString().equals("text/calendar")) {
            List<Convocatoria> convocatorias = convocatoriaService.getConvocatoriasGrados();
            Map<Long, ConvocatoriaUI> map = ConvocatoriaUI.toUI(convocatorias, idioma).stream()
                    .collect(Collectors.toMap(ConvocatoriaUI::getId, Function.identity()));


            calendarOutput.setExamenesCalendarData(estudioUI, anyo, map, idioma);

            return Response
                    .ok(calendarOutput)
                    .header("Content-Disposition",
                            "inline; filename=\"" + calendarOutput.getCalendario() + ".ics\"")
                    .type("text/calendar; charset=UTF-8").build();
        }
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/examenes", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);

        template.put("contenido", "sia/estudio/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {

            template.put("contenidoestudio", "sia/estudio/examenes");
            template.put("pageclass", "page-examenesestudio");

            boolean hayExamenes = estudioService.hayExamenes(estudioId, anyo);
            template.put("hayexamenes", hayExamenes);
            if (hayExamenes) {
                List<Convocatoria> convocatorias = convocatoriaService.getConvocatoriasGrados();

                template.put("convocatorias", ConvocatoriaUI.toUI(convocatorias, idioma));
                template.put("convocatoriaActiva", convocatoriaService.getConvocatoriaActiva(estudioId, this.anyo, TipoDocencia.GRADO));
            }


        } else {

            // asignaturas que sólo convalidan
            ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));
            template.put("totalCreditos", resumen.getCreditosTotal());
            template.put("contenidoestudio", "sia/master/examenes");
            template.put("pageclass", "page-examenesmaster");


            boolean hayExamenes = estudioMasterService.hayExamenes(estudioId, anyo);
            template.put("hayexamenes", hayExamenes);
            if (hayExamenes) {
                List<Convocatoria> convocatorias = convocatoriaService.getConvocatoriasMasters();

                template.put("convocatorias", ConvocatoriaUI.toUI(convocatorias, idioma));
                template.put("convocatoriaActiva", convocatoriaService.getConvocatoriaActiva(estudioId, this.anyo, TipoDocencia.MASTERES));
            }

        }
        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));
        return Response.ok(template, MediaType.TEXT_HTML).build();
//        return template;
    }


    @GET
    @Path("{id}/examenes/json")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getExamenesJson(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                          @PathParam("id") Long estudioId,
                                          @QueryParam("convocatoria") @DefaultValue("0") Integer convocatoria

    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {
            List<ExamenesEstudio> examenes = estudioService.getExamenesByDate(estudioId, this.anyo, convocatoria);
            return UIEntity.toUI(ExamenEstudioUI.toUI(examenes, idioma));
        } else {
            List<ExamenesEstudioMaster> examenes = estudioMasterService.getExamenesByDate(estudioId, this.anyo, convocatoria);
            return UIEntity.toUI(ExamenEstudioUI.toUI(examenes, idioma));
        }
    }

    @GET
    @Path("{id}/circuitos")
    @Produces(MediaType.TEXT_HTML)
    public Template getCircuitos(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                 @PathParam("id") Long estudioId,
                                 @QueryParam("pm") Boolean periodoAsignaturas
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/circuitos", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, periodoAsignaturas);

        List<CircuitoUI> circuitos = this.estudioService.getCircuitos(estudioId, anyo).stream().map(c -> CircuitoUI.toUI(c, idioma)).collect(Collectors.toList());

        template.put("contenido", "sia/estudio/base");
        template.put("contenidoestudio", "sia/estudio/circuitos");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-circuitos");
        template.put("circuitos", circuitos);
        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        FiltroEstudio fe = new FiltroEstudio(idioma);
        fe.addSemestre("1");
        Long ultimoSemestre1 = this.estudioService.getUltimoLectivo(estudioId, anyo, fe);
        template.put("ultimoLectivoSemestre1", ultimoSemestre1);

        fe = new FiltroEstudio(idioma);
        fe.addSemestre("2");
        Long ultimoSemestre2 = this.estudioService.getUltimoLectivo(estudioId, anyo, fe);
        template.put("ultimoLectivoSemestre2", ultimoSemestre2);

        List<Long> primerLectivos = this.estudioService.getPrimerLectivos(estudioId, anyo);
        template.put("primerdiasemestre1", primerLectivos.get(0));
        template.put("primerdiasemestre2", primerLectivos.get(1));

        return template;
    }

    @GET
    @Path("{id}/circuito/{cid}/asignaturas")
    @Produces(MediaType.TEXT_HTML)
    public Template getExamenesJson(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                    @PathParam("id") Long estudioId,
                                    @PathParam("cid") Long circuitoId

    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        List<AsignaturaCircuito> acs = this.estudioService.getAsignaturasCircuito(estudioId, circuitoId, anyo);

        Template template = AppInfo.buildPaginaForAjax("sia/estudio/circuitos-contenido", this.anyo, this.anyo + "/estudio/" + estudioId, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()));
        template.put("estudio", estudioUI);
        template.put("asignaturasCircuito", CajaAsignaturaUI.toUI(acs, idioma));

        return template;
    }

    @GET
    @Path("{id}/itinerarios")
    @Produces(MediaType.TEXT_HTML)
    public Template getItinerios(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                 @PathParam("id") Long estudioId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/itinerarios", this.anyo, idioma, String.join(" ", "SIA","-",estudioUI.getNombre()),false, false);


        Map<Long, EstudioItineratioUI> itineratios = EstudioItineratioUI.toUI(estudioService.getItinerarios(estudioId, anyo), idioma);
        Map<Long, EstudioItineratioUI> itineratiosFuturos = EstudioItineratioUI.toUI(estudioService.getItinerariosFuturos(estudioId, anyo), idioma);
        Map<Long, EstudioItineratioUI> menciones = EstudioItineratioUI.toUI(estudioService.getMenciones(estudioId, anyo), idioma);
        Map<Long, EstudioItineratioUI> mencionesFuturas = EstudioItineratioUI.toUI(estudioService.getMencionesFuturas(estudioId, anyo), idioma);
        boolean isMencionesFuturasOb = estudioService.isMencionesFuturasOb(estudioId, anyo);

        template.put("contenido", "sia/estudio/base");
        template.put("contenidoestudio", "sia/estudio/itinerarios");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-itinerarios");
        template.put("itinerarios", itineratios);
        template.put("itinerariosFuturos", !itineratiosFuturos.isEmpty());
        template.put("menciones", menciones);
        template.put("mencionesFuturas", !mencionesFuturas.isEmpty());
        template.put("mencionEspecialTodas", estudioUI.getId() == 245);
        template.put("mencionEspecialUna", (estudioUI.getId() == 250 || estudioUI.getId() == 251 || estudioUI.getId() == 252 || estudioUI.getId() == 249) && !isMencionesFuturasOb);
        template.put("mencionEspecialUnaObl", (estudioUI.getId() == 250 || estudioUI.getId() == 251 || estudioUI.getId() == 252 || estudioUI.getId() == 249) && isMencionesFuturasOb);


        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }


    @GET
    @Path("{id}/requisitos")
    @Produces(MediaType.TEXT_HTML)
    public Template getRequisitos(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                  @PathParam("id") Long estudioId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/requisitos", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);

        List<AsignaturaIncompatible> asignaturas = requisitosMatriculaService.getAsignaturas(estudioId, anyo);

        List<RequisitosMatricula> requisitos = requisitosMatriculaService.getRequisitosMatricula(estudioId, anyo);
        List<RequisitosMatriculaEEP> requisitosEEP = requisitosMatriculaService.getRequisitosMatriculaEEP(estudioId, anyo);

        Map<String, List<RequisitosMatriculaEEPUI>> reepUI = RequisitosMatriculaEEPUI.toUIByAsignatura(requisitosEEP, idioma);

        template.put("contenido", "sia/estudio/base");
        template.put("contenidoestudio", "sia/estudio/requisitos-matricula");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-requisitos-matricula");

        template.put("asignaturas", AsignaturaIncompatibleUI.toUI(asignaturas, idioma));
        template.put("requisitos", RequisitosMatriculaUI.toUI(requisitos, idioma));
        template.put("requisitosEEP", reepUI);

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }

    @GET
    @Path("{id}/informacion")
    @Produces(MediaType.TEXT_HTML)
    public Template getInformacion(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                   @PathParam("id") Long estudioId
    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/informacion", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);


        template.put("contenido", "sia/estudio/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-informacion-estudio");

        // los enlaces son una lista de arrays de dos elementos:
        //     el elemento 0 contiene la url
        //     el elemento 1 contiene la clave de la etiqueta

        List<String[]> informacion = new ArrayList<>();
        List<String[]> normativas = new ArrayList<>();

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {

            // información
            informacion.add(new String[]{"https://www.uji.es/perfils/futurs/grau/", "estudio.informacion.futuroEstudiante"});
            informacion.add(new String[]{"https://www.uji.es/perfils/estudiantat/uji/graus/", "estudio.informacion.estudiante"});
            informacion.add(new String[]{"https://www.uji.es/cau/manuals/guies/alumnat/", "estudio.informacion.estudiante.guia"});

            // normativas
            normativas.add(new String[]{"https://www.uji.es/transparencia/normativa/normpropia/estudiantat/Grau/", "estudio.normativa.url"});
            normativas.add(new String[]{"https://www.uji.es/organs/ouag/sg/docs/politiques/gen/NormativaIResolucions/CriterisAcademics/", "estudio.criterios.tratamiento.personales.url"});
            normativas.add(new String[]{"https://www.uji.es/organs/ouag/sg/docs/politiques/gen/NormativaIResolucions/criteristesis/", "estudio.criterios.tratamiento.tfg.url"});
            normativas.add(new String[]{"https://www.uji.es/transparencia/normativa/normpropia", "estudio.normativa.propia.url"});
        } else {
            // informacion

            informacion.add(new String[]{"https://www.uji.es/perfils/futurs/universitaris/", "estudio.informacion.futuroEstudiante"});
            informacion.add(new String[]{"https://www.uji.es/perfils/estudiantat/uji/masters/", "estudio.informacion.estudiante"});
            informacion.add(new String[]{"https://www.uji.es/cau/manuals/guies/alumnat/", "estudio.informacion.estudiante.guia"});


            // normativas
            normativas.add(new String[]{"https://www.uji.es/transparencia/normativa/normpropia/estudiantat/master/", "master.informacion.normaticasestudiomaster.url"});
            normativas.add(new String[]{"https://www.uji.es/organs/ouag/sg/docs/politiques/gen/NormativaIResolucions/CriterisAcademics/", "estudio.criterios.tratamiento.personales.url"});
            normativas.add(new String[]{"https://www.uji.es/organs/ouag/sg/docs/politiques/gen/NormativaIResolucions/criteristesis/", "estudio.criterios.tratamiento.tfg.url"});

            normativas.add(new String[]{"http://www.uji.es/transparencia/normativa/normpropia/", "estudio.normativa.propia.url"});
            ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));

            template.put("totalCreditos", resumen.getCreditosTotal());
        }

        template.put("contenidoestudio", "sia/estudio/informacion");
        template.put("enlacesinformacion", informacion);
        template.put("enlacesnormativas", normativas);

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }

    @GET
    @Path("{id}/estilo")
    @Produces(MediaType.TEXT_HTML)
    public Template getEstilo(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                              @PathParam("id") Long estudioId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/estilo", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);

        List<EstiloAsignatura> asignaturas = this.estudioService.getAsignaturasEstilo(estudioId, anyo);

        template.put("contenido", "sia/estudio/base");
        template.put("contenidoestudio", "sia/estudio/estilo");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-informacion-estilo");
        template.put("asignaturas", EstiloAsignaturaUI.toUI(asignaturas, idioma));
        template.put("ambitos", EstiloAsignaturaUI.getAmbitos(asignaturas));

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }


    @GET
    @Path("{id}/resumencreditos")
    @Produces(MediaType.TEXT_HTML)
    public Template getResumenCreditos(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                       @PathParam("id") Long estudioId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/resumencreditos", this.anyo, idioma, String.join(" ", "SIA","-",estudioUI.getNombre()),false, false);
        String estudioNombre = LocalizedStrings.getEstudioCompacto(estudioUI.getNombre(), idioma);

        template.put("contenido", "sia/estudio/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioNombre", estudioNombre);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-resumen-creditos");

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));


        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {
            // estudio de grado
            List<ResumenCreditosGrado> resumen = this.estudioService.getResumenCreditosGrado(estudioId);
            Double totalCreditos = resumen
                    .parallelStream()
                    .mapToDouble((r) -> r.getCrdLe() + r.getCrdPfg() + r.getCrdOp() + r.getCrdOb() + r.getCrdTr())
                    .sum();

            template.put("contenidoestudio", "sia/estudio/resumen-creditos");
            template.put("resumen", ResumenCreditosGradoUI.toUI(resumen));
            template.put("totalCreditos", totalCreditos);

        } else {
            // estudios de máster
            ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));

            template.put("contenidoestudio", "sia/master/resumen-creditos");
            template.put("resumen", resumen);
            template.put("totalCreditos", resumen.getCreditosTotal());
        }

        return template;
    }

    @GET
    @Path("{id}/eep")
    @Produces(MediaType.TEXT_HTML)
    public Template getPracticasExternas(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                         @PathParam("id") Long estudioId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);

        Template template = AppInfo.buildPagina(
                this.anyo + "/estudio/" + estudioId + "/epp",
                this.anyo,
                idioma,
                String.join(" ", "SIA","-",estudioUI.getNombre()),
                false,
                false
        );

        List<CoordinadoresEEP> coordinadoresEEP = this.estudioService.getCoordinadoresEEP(estudioId);

        template.put("contenido", "sia/estudio/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("pageclass", "page-eep");


        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {
            // estudio de grado
            HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
            List<AsignaturaEEP> asignaturasEEP = this.estudioService.getAsignaturasEEP(estudioId, anyo, TipoDocencia.GRADO);

            template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
            template.put("contenidoestudio", "sia/estudio/eep");
            template.put("asignaturasEEP", AsignaturaEEPUI.toUI(asignaturasEEP, idioma));
            template.put("requisitos", 1);
            //En caso de Medicina y enfermeria añadimos las asignaturas extra.
            if (estudioId.equals(229L) || estudioId.equals(230L)) {
                List<AsignaturaEEPExtra> asignaturasEEPextra = this.estudioService.getAsignaturasEEPExtra(estudioId, TipoDocencia.GRADO);
                template.put("asignaturasEEPExtra", AsignaturaEEPUI.toExtraUI(asignaturasEEPextra, idioma));
                if (estudioId.equals(230L)) {
                    template.put("requisitos", 0);
                }
            }
            template.put("coordinadores", coordinadoresEEP);

        } else {
            // estudios de máster
            List<AsignaturaEEP> asignaturasEEP = this.estudioService.getAsignaturasEEP(estudioId, anyo, TipoDocencia.MASTERES);
            template.put("contenidoestudio", "sia/master/eep");
            template.put("asignaturasEEP", AsignaturaEEPUI.toUI(asignaturasEEP, idioma));
            ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));

            template.put("totalCreditos", resumen.getCreditosTotal());

            List<AsignaturaEEPExtra> asignaturasEEPextra = this.estudioService.getAsignaturasEEPExtra(estudioId, TipoDocencia.MASTERES);
            template.put("asignaturasEEPExtra", AsignaturaEEPUI.toExtraUI(asignaturasEEPextra, idioma));

        }

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }

    @GET
    @Path("{id}/presentacion")
    @Produces(MediaType.TEXT_HTML)
    public Template getPresentacion(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                    @PathParam("id") Long estudioId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/presentacion", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);

        PresentacionMaster pm = estudioService.getPresentacion(estudioId, anyo);
        List<PresentacionCoordinadores> coordinadores = estudioService.getPresentacionCoordinadores(estudioId);

        template.put("contenido", "sia/estudio/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-presentacion");
        template.put("contenidoestudio", "sia/master/presentacion");
        template.put("presentacion", PresentacionMasterUI.toUI(pm, idioma));
        template.put("coordinadores", PresentacionCoordinadoresUI.toUI(coordinadores, idioma));

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }

    @GET
    @Path("{id}/complementos")
    @Produces(MediaType.TEXT_HTML)
    public Template getComplementos(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                    @PathParam("id") Long estudioId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/complementos", this.anyo, idioma, String.join(" ", "SIA","-",estudioUI.getNombre()),false, false);

        List<ComplementoFormacion> complementos = complementosService.getComplementos(estudioId, anyo);

        template.put("contenido", "sia/estudio/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-complementos");
        template.put("contenidoestudio", "sia/master/complementos");
        template.put("complementos", ComplementoFormacionUI.toUI(complementos, idioma));
        ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));

        template.put("totalCreditos", resumen.getCreditosTotal());

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }

    @GET
    @Path("{id}/intercambio")
    @Produces(MediaType.TEXT_HTML)
    public Template getIntercambio(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                   @PathParam("id") Long estudioId
    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/intercambio", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);

        template.put("contenido", "sia/estudio/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-intercambio");
        template.put("contenidoestudio", "sia/master/intercambio");

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }

    /**
     * Profesores.
     *
     * @param idioma
     * @param estudioId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/profesorado")
    @Produces(MediaType.TEXT_HTML)
    public Template getProfesores(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                  @PathParam("id") Long estudioId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);

        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/profesorado", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);

        template.put("contenido", "sia/estudio/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("pageclass", "page-profesores-estudio");

        if (estudioUI.getTipo().compareTo(TipoDocencia.MASTERES.getValue()) == 0) {
            // estudio de máster
            List<ProfesorMaster> profesores = profesoresEstudioService.getProfesoresMasterByIdAnyo(estudioId, anyo);
            Map<Long, ProfesoresMasterUI> profesorMasterUIMap = ProfesoresMasterUI.toUI(profesores, idioma);
            // estudios de máster
            ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));

            template.put("totalCreditos", resumen.getCreditosTotal());

            template.put("contenidoestudio", "sia/master/profesores");
            template.put("profesores", profesorMasterUIMap);
            template.put("fotos", fotosService.getFotos(profesores));
        } else {
            // estudio de grado

            HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
            List<ProfesorGrado> profesores = profesoresEstudioService.getProfesoresGradoByIdAnyo(estudioId, anyo);
            Map<String, List<ProfesorGradoUI>> profesorGradoUIMap = ProfesorGradoUI.toUI(profesores, idioma);

            template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
            template.put("contenidoestudio", "sia/estudio/profesores");
            template.put("profesores", profesorGradoUIMap);
            template.put("fotos", fotosService.getFotos(profesores));
        }

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }


    @GET
    @Path("{id}/especialidades")
    @Produces(MediaType.TEXT_HTML)
    public Template getEspecialidades(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                      @PathParam("id") Long estudioId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/especialidades/", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);

        List<Especialidad> lEspecialidades = this.especialidadService.getEspecialidades(estudioId, this.anyo);

        Map<Long, List<EspecialidadAsignatura>> mEspecialidades =
                this.especialidadService.getEspecialidadAsignaturasByPieId(estudioId, this.anyo);
        Map<Long, List<EspecialidadAsignaturaUI>> mEspecialidadAsignaturaUI = EspecialidadAsignaturaUI.toUI(mEspecialidades, idioma);

        template.put("contenido", "sia/estudio/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-especialidades");
        template.put("contenidoestudio", "sia/master/especialidades");

        template.put("especialidades", EspecialidadUI.toUI(lEspecialidades, idioma));
        template.put("asignaturas", mEspecialidadAsignaturaUI);

        // estudios de máster
        ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));

        template.put("totalCreditos", resumen.getCreditosTotal());
        template.put("especialidadObligatoria", resumen.getEspecialidadObligatoria());

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }

    @GET
    @Path("{id}/adendas")
    @Produces(MediaType.TEXT_HTML)
    public Template getAdendas(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                               @PathParam("id") Long estudioId
    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + estudioId + "/adendas", this.anyo, idioma, String.join(" ", "SIA","-",estudioUI.getNombre()),false, false);


        template.put("contenido", "sia/estudio/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("pageclass", "page-adendas-estudio");
        template.put("contenidoestudio", "sia/estudio/adendas");


        switch (anyo) {
            case 2019:
                template.put("adenda1", ModificacionCOVID2019.valueOfNumber(estudioId.intValue()) != null ? ModificacionCOVID2019.valueOfNumber(estudioId.intValue()).getDescripcion() : null);
                break;
            case 2020:
                template.put("adenda1", ModificacionCOVID2020_1.valueOfNumber(estudioId.intValue()) != null ? ModificacionCOVID2020_1.valueOfNumber(estudioId.intValue()).getDescripcion() : null);
                template.put("adenda2", ModificacionCOVID2020_2.valueOfNumber(estudioId.intValue()) != null ? ModificacionCOVID2020_2.valueOfNumber(estudioId.intValue()).getDescripcion() : null);
                break;
            default:
                break;
        }
        if (estudioUI.getTipo().compareTo(TipoDocencia.MASTERES.getValue()) == 0) {
            // estudios de máster
            ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));
            template.put("totalCreditos", resumen.getCreditosTotal());
        }
            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
            ));


        return template;
    }


}