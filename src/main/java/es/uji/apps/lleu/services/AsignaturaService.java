package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.AsignaturaDAO;
import es.uji.apps.lleu.dao.AsignaturaRequisitoDAO;
import es.uji.apps.lleu.model.Asignatura;
import es.uji.apps.lleu.model.AsignaturaRequisito;

@Service
public class AsignaturaService
{
    private AsignaturaDAO asignaturaDAO;
    private AsignaturaRequisitoDAO asignaturaRequisitoDAO;

    @Autowired
    public AsignaturaService (
            AsignaturaDAO asignaturaDAO,
            AsignaturaRequisitoDAO asignaturaRequisitoDAO
    ) {
        this.asignaturaDAO = asignaturaDAO;
        this.asignaturaRequisitoDAO = asignaturaRequisitoDAO;
    }

    /**
     * Devuelve una asignatura por ID
     *
     * @param id
     * @return
     */
    public Asignatura getAsignaturaById(String id) {
        return this.asignaturaDAO.getById(id);
    }

    /**
     * Devuelve las incompatibilidades. Requisitos e incompatibilidades son intercambiables.
     *
     * @param asignaturaId
     * @param anyo
     * @return
     */
    public List<AsignaturaRequisito> getIncompatibilidades (String asignaturaId, Long estudioId, Integer anyo) {
        return this.asignaturaRequisitoDAO.getRequisitos(asignaturaId, estudioId, anyo);
    }
}
