package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.AsignaturaMetodologiaDAO;
import es.uji.apps.lleu.model.AsignaturaMetodologia;

@Service
public class AsignaturasMetodologiaService
{
    private AsignaturaMetodologiaDAO asignaturaMetodologiaDAO;


    @Autowired
    public AsignaturasMetodologiaService(
            AsignaturaMetodologiaDAO asignaturaMetodologiaDAO
    )
    {
        this.asignaturaMetodologiaDAO = asignaturaMetodologiaDAO;
    }


    /**
     * Devuelve un listado con la metodoligia de una asignatura
     * @param asignaturaId el id de la asignatura que queremos consultar
     * @return
     */
    public List<AsignaturaMetodologia> getMetodologia(String asignaturaId, Integer anyo){
        return asignaturaMetodologiaDAO.getMetodologia(asignaturaId,anyo);
    }


}
