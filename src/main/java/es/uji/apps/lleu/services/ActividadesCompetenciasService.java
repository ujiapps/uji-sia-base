package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.uji.apps.lleu.dao.ActividadDAO;
import es.uji.apps.lleu.dao.CompetenciaGuiaDAO;
import es.uji.apps.lleu.dao.EstadoAsignaturaDAO;
import es.uji.apps.lleu.dao.ResultadoGuiaDAO;
import es.uji.apps.lleu.model.ResultadosGuias;
import es.uji.apps.lleu.ui.ActividadUI;
import es.uji.apps.lleu.ui.CompetenciaUI;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ActividadesCompetenciasService extends BaseDAODatabaseImpl
{
    private ActividadDAO actividadDAO;
    private CompetenciaGuiaDAO competenciaGuiaDAO;
    private ResultadoGuiaDAO resultadosGuiaDAO;
    private EstadoAsignaturaDAO estadoAsignaturaDAO;

    @Autowired
    public ActividadesCompetenciasService(

            ActividadDAO actividadDAO,

            CompetenciaGuiaDAO competenciaGuiaDAO,
            ResultadoGuiaDAO resultadoGuiaDAO,
            EstadoAsignaturaDAO estadoAsignaturaDAO)
    {

        this.actividadDAO = actividadDAO;
        this.competenciaGuiaDAO = competenciaGuiaDAO;
        this.resultadosGuiaDAO = resultadoGuiaDAO;
        this.estadoAsignaturaDAO = estadoAsignaturaDAO;
    }

    /**
     * Devuelve las actividades
     *
     * @param asignaturaId
     * @param anyo
     * @return
     */
    public List<ActividadUI> getActividades (String asignaturaId, Integer anyo, String idioma) {
        return ActividadUI.toUI(
                this.actividadDAO.getActividades(asignaturaId, anyo),
                idioma
        );
    }

    /**
     * Devuelve las competencias de una asignatura
     *
     * @param asignaturaId
     * @param anyo
     * @return
     */
    public List<CompetenciaUI> getCompetencias(String asignaturaId, Integer anyo, String idioma) {
        return CompetenciaUI.toUI(
                    this.competenciaGuiaDAO.getCompetencias(asignaturaId, anyo),
                    idioma
            );
    }

    /**
     * Devuelve los resultados de una asignatura (apartado de requisitos y competencias)
     * @param asignaturaId
     * @param anyo
     * @return
     */
    public List<ResultadosGuias> getResultados(String asignaturaId, Integer anyo) {
        return this.resultadosGuiaDAO.getResultados(asignaturaId, anyo);
    }

}
