package es.uji.apps.lleu.services;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.component.VEvent;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.lleu.dao.ExamenesEstudioDAO;
import es.uji.apps.lleu.dao.HorariosEstudioDAO;
import es.uji.apps.lleu.ui.CalendarEventUI;
import es.uji.apps.lleu.ui.ConvocatoriaUI;
import es.uji.apps.lleu.ui.EstudioUI;

@Component
public class CalendarOutputStream implements StreamingOutput
{

    private String calendario;
    private List<CalendarEventUI> events;

    @Autowired
    private CalendarFactoryService calendarFactory;

    @Autowired
    private HorariosEstudioDAO horariosEstudioDAO;

    @Autowired
    private ExamenesEstudioDAO examenesEstudioDAO;

    public CalendarOutputStream()
    {
    }

    public void write(OutputStream output) throws IOException, WebApplicationException
    {
        Calendar calendar = calendarFactory.createCalendar(calendario);

        for (CalendarEventUI event : events)
        {
            VEvent calendarEvent = calendarFactory.createNewCalendarEvent(event);
            calendar.getComponents().add(calendarEvent);
        }

        output.write(calendar.toString().getBytes());
    }

    public void setHorarioCalendarData(EstudioUI estudio, Integer anyo, String idioma)
    {
        calendario = estudio.getNombre();
        events = CalendarEventUI.toUI(horariosEstudioDAO.getEventosCalendario(estudio.getId(), anyo), idioma);
    }

    public void setExamenesCalendarData(EstudioUI estudio, Integer anyo, Map<Long, ConvocatoriaUI> convocatorias, String idioma)
    {
        calendario = estudio.getNombre();
        events = CalendarEventUI.toUI(examenesEstudioDAO.getExamenes(estudio.getId(), anyo),convocatorias, idioma);
    }

    public String getCalendario()
    {
        return calendario;
    }
}
