package es.uji.apps.lleu.services;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.EspecialidadDAO;
import es.uji.apps.lleu.model.Especialidad;
import es.uji.apps.lleu.model.EspecialidadAsignatura;

@Service
public class EspecialidadService
{

    private EspecialidadDAO especialidadDAO;

    @Autowired
    public EspecialidadService (EspecialidadDAO especialidadDAO) {
        this.especialidadDAO = especialidadDAO;
    }

    public List<Especialidad> getEspecialidades (Long estudioId, Integer cursoAca) {
        return this.especialidadDAO.getEspecialidades(estudioId, cursoAca);
    }

    public List<EspecialidadAsignatura> getEspecialidadAsignaturas (Long estudioId, Integer cursoAca) {
        return this.especialidadDAO.getEspecialidadAsignaturas(estudioId, cursoAca);
    }

    /**
     * Devuelve el listado de especialidades de asignatura de un máster en concreto agrupadas por el identificador
     * de estudio
     *
     * @param estudioId
     * @param cursoAca
     * @return
     */
    public Map<Long, List<EspecialidadAsignatura>> getEspecialidadAsignaturasByPieId (Long estudioId, Integer cursoAca) {
        List<EspecialidadAsignatura> l = this.especialidadDAO.getEspecialidadAsignaturas(estudioId, cursoAca);
        return l.stream().collect(Collectors.groupingBy(EspecialidadAsignatura::getPieId));
    }
}
