package es.uji.apps.lleu.services;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Comment;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.model.property.Version;
import net.fortuna.ical4j.model.property.XProperty;

import java.net.SocketException;
import java.util.GregorianCalendar;
import java.util.UUID;

import org.springframework.stereotype.Component;

import es.uji.apps.lleu.ui.CalendarEventUI;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Component
public class CalendarFactoryService extends BaseDAODatabaseImpl
{

    private static final String TIMEZONE_ID = "Europe/Madrid";

    public Calendar createCalendar(String calendarName)
    {
        Calendar calendar = new Calendar();
        calendar.getProperties().add(
                new ProdId("-//UJI//SIA Calendar::" + calendarName + "//EN"));
        calendar.getProperties().add(Version.VERSION_2_0);
        calendar.getProperties().add(CalScale.GREGORIAN);
        calendar.getProperties().add(new XProperty("X-WR-CALNAME", calendarName));
        calendar.getProperties().add(new XProperty("X-WR-TIMEZONE", TIMEZONE_ID));

        return calendar;
    }

    public VEvent createNewCalendarEvent(CalendarEventUI event) throws SocketException
    {
        return createRegularEvent(getTimeZone(), event);
    }

    private TimeZone getTimeZone()
    {
        TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
        return registry.getTimeZone(CalendarFactoryService.TIMEZONE_ID);
    }

    private VEvent createRegularEvent(TimeZone timezone, CalendarEventUI event)
            throws SocketException
    {
        VTimeZone tz = timezone.getVTimeZone();

        java.util.Calendar startDate = new GregorianCalendar();
        startDate.setTimeZone(timezone);
        startDate.setTime(event.getIni());

        java.util.Calendar endDate = new GregorianCalendar();
        endDate.setTimeZone(timezone);
        endDate.setTime(event.getFin());

        DateTime start = new DateTime(startDate.getTime());
        start.setTimeZone(timezone);

        DateTime end = new DateTime(endDate.getTime());
        end.setTimeZone(timezone);

        VEvent meeting = new VEvent(start, end, event.getSummary());
        meeting.getProperties().add(tz.getTimeZoneId());

        if (event.getLocation() != null)
        {
            meeting.getProperties().add(new Location(event.getLocation()));
        }

        if (event.getDescripcion() != null)
        {
            meeting.getProperties().add(new Description(event.getDescripcion()));
        }
        if (event.getComment() != null)
        {
            meeting.getProperties().add(new Comment(event.getComment()));
        }

        meeting.getProperties().add(new Uid(UUID.randomUUID().toString()));

        return meeting;
    }

}
