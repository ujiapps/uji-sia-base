package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.ComplementoFormacionDAO;
import es.uji.apps.lleu.model.ComplementoFormacion;

@Service
public class ComplementosService {

    private ComplementoFormacionDAO complementoFormacionDAO;

    @Autowired
    public ComplementosService (
            ComplementoFormacionDAO complementoFormacionDAO
    ) {
        this.complementoFormacionDAO = complementoFormacionDAO;
    }


    public List<ComplementoFormacion> getComplementos (Long estudioId, Integer anyo) {
        return this.complementoFormacionDAO.getComplementos(estudioId, anyo);
    }

}

