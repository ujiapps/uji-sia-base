package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.AsignaturaRequisitoDAO;
import es.uji.apps.lleu.dao.AsignaturasGradoDAO;
import es.uji.apps.lleu.dao.EstadoAsignaturaDAO;
import es.uji.apps.lleu.dao.SistemaEvaluacionDAO;
import es.uji.apps.lleu.model.AsignaturaComentarios;
import es.uji.apps.lleu.model.AsignaturaRequisito;
import es.uji.apps.lleu.model.AsignaturaSoloRecGrado;
import es.uji.apps.lleu.model.AsignaturasGrado;
import es.uji.apps.lleu.model.Criterios;
import es.uji.apps.lleu.model.ExamenesGrado;
import es.uji.apps.lleu.model.ExamenesMaster;
import es.uji.apps.lleu.model.RequisitosMatriculaEEP;
import es.uji.apps.lleu.model.SistemaEvaluacionGrado;
import es.uji.apps.lleu.model.VacantesGrupoGrado;
import es.uji.apps.lleu.model.VacantesSubgrupoGrado;
import es.uji.apps.lleu.model.enums.TipoComentario;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.InfoFiltro;

@Service
public class AsignaturasGradoService
{
    private AsignaturasGradoDAO asignaturasGradoDAO;
    private AsignaturaRequisitoDAO asignaturaRequisitoDAO;
    private SistemaEvaluacionDAO sistemaEvaluacionDAO;

    @Autowired
    public AsignaturasGradoService(
            AsignaturasGradoDAO asignaturasGradoDAO,
            AsignaturaRequisitoDAO asignaturaRequisitoDAO,
            EstadoAsignaturaDAO estadoAsignaturaDAO,
            SistemaEvaluacionDAO sistemaEvaluacionDAO
    )
    {
        this.asignaturasGradoDAO = asignaturasGradoDAO;
        this.asignaturaRequisitoDAO = asignaturaRequisitoDAO;
        this.sistemaEvaluacionDAO = sistemaEvaluacionDAO;
    }

    public List<AsignaturasGrado> getAll()
    {
        return asignaturasGradoDAO.get(AsignaturasGrado.class);
    }

    public AsignaturasGrado getById(String id)
    {
        return asignaturasGradoDAO.get(AsignaturasGrado.class, id).get(0);
    }

    public AsignaturasGrado getAsignatura(String asignaturaId, Long estudioId, Integer anyo) {
        return asignaturasGradoDAO.getAsignatura(asignaturaId, estudioId, anyo);
    }

    public List<AsignaturasGrado> getAsignaturasByIdAnyo(Long id, Integer anyo)
    {
        return asignaturasGradoDAO.getAsignaturasByIdAnyo(id,anyo);
    }


    /**
     * Devuelve un listado de asignturas para un estudio dado, en un año dado y dadas unas condiciones de filtrado.
     * Filtra los grupos W y V.
     *
     * @param estudioId el id de estudio a considerar
     * @param anyo el curso académico a considerar
     * @param filtro especificación de filtrado a aplicar
     * @return
     */
    public List<AsignaturasGrado> getAsignaturasByFilter(Long estudioId, Integer anyo, FiltroEstudio filtro) {
        return asignaturasGradoDAO.getAsignaturasByFilter(estudioId, anyo, filtro);
    }

    /**
     * Devuelve las asignaturas activas sólo a efectos de reconocimiento. Para grado.
     *
     * @param estudioId
     * @param anyo
     * @return
     */
    public List<AsignaturaSoloRecGrado> getAsignaturasSoloReconocimiento(Long estudioId, Integer anyo) {
        return asignaturasGradoDAO.getAsignaturasSoloRec(estudioId, anyo);
    }

    /**
     * Devuelve información sobre las asignaturas de un estudio dado
     *
     * @param estudioId el id del estudio que queremos consultar
     * @param anyo el curso académico que queremos consultar
     * @return información sobre los estudios
     */
    public InfoFiltro getFiltroInfo (Long estudioId, Integer anyo) {
        return this.asignaturasGradoDAO.getInfoFiltros(estudioId, anyo);
    }
    public List<AsignaturaRequisito> getRequisitos (String asignaturaId, Long estudioId, Integer anyo) {
        return this.asignaturaRequisitoDAO.getRequisitos(asignaturaId, estudioId, anyo);
    }
    public List<ExamenesGrado> getExamenesGrado (String asignaturaId, Integer anyo) {
        return this.sistemaEvaluacionDAO.getExamenesGrado(asignaturaId, anyo);
    }

    public List<ExamenesMaster> getExamenesMaster (String asignaturaId, Integer anyo) {
        return this.sistemaEvaluacionDAO.getExamenesMaster(asignaturaId, anyo);
    }

    public List<Criterios> getCriteriosEvaluacion (String asignaturaId, Integer anyo) {
        return this.sistemaEvaluacionDAO.getCriterios(asignaturaId, anyo);
    }
    public List<SistemaEvaluacionGrado> getSistemasEvaluacion(String asignaturaId, Integer anyo) {
        return this.sistemaEvaluacionDAO.getSistemasEvaluacion(asignaturaId, anyo);
    }
    public boolean tieneGuiaDocenteEnIngles(String asignaturaId, Integer anyo) {
        return this.asignaturasGradoDAO.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
    }


    public List<VacantesSubgrupoGrado> getVacantesSubgrupoGrado(Integer anyo, Long estudioId, List<String> asignaturaIds)
    {
        return this.asignaturasGradoDAO.getVacantesSubgrupoGrado(anyo,estudioId, asignaturaIds);
    }


    public List<VacantesGrupoGrado> getVacantesAsignaturas(Integer anyo, List<AsignaturasGrado> asignaturasGrados)
    {
        return this.asignaturasGradoDAO.getVacantesAsignatura(anyo,asignaturasGrados);
    }
    public List<VacantesGrupoGrado> getVacantesAsignatura(Integer anyo, String asignaturaId)
    {
        return this.asignaturasGradoDAO.getVacantesAsignatura(anyo,asignaturaId);
    }

    public List<AsignaturaComentarios> getAsignaturaComentarios(AsignaturasGrado a, Long estudioId, Integer anyo , TipoComentario tipo)
    {
        return this.asignaturasGradoDAO.getAsignaturaComentarios(a,estudioId,anyo, tipo);
    }

    public List<RequisitosMatriculaEEP> getRequisitosEEP(String asignaturaId, Long estudioId, Integer cursoAca)
    {

        return this.asignaturaRequisitoDAO.getRequisitosEEP(asignaturaId, estudioId,cursoAca);
    }
}
