package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.AsignaturaTemarioDAO;
import es.uji.apps.lleu.model.AsignaturaTemario;

@Service
public class AsignaturaTemarioService
{
    private AsignaturaTemarioDAO asignaturaTemarioDAO;

    @Autowired
    public AsignaturaTemarioService(
            AsignaturaTemarioDAO asignaturaTemarioDAO
    )
    {
        this.asignaturaTemarioDAO = asignaturaTemarioDAO;
    }

    /**
     * Devuelve un listado con el temario de una asignatura
     * @param asignaturaId el id de la asignatura que queremos consultar
     * @return
     */
    public List<AsignaturaTemario> getTemario(String asignaturaId, Integer anyo){
        return asignaturaTemarioDAO.getTemario(asignaturaId,anyo);
    }
}
