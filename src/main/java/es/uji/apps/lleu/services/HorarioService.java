package es.uji.apps.lleu.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.HorarioDAO;
import es.uji.apps.lleu.model.HorarioAsignaturaDetallado;
import es.uji.apps.lleu.model.HorarioAsignaturaGeneral;
import es.uji.apps.lleu.model.HorarioGenerar;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.InfoFiltro;

@Service
public class HorarioService
{
    private HorarioDAO horarioDAO;

    @Autowired
    public HorarioService(HorarioDAO horarioDAO)
    {
        this.horarioDAO = horarioDAO;
    }

    public List<HorarioAsignaturaDetallado> getHorarioDetalladoAsignatura(Long estudioId, String asignaturaId, Integer anyo, FiltroEstudio fe, Date from, Date to)
    {
        return horarioDAO.getHorarioDetalladoAsignatura(estudioId,asignaturaId, anyo, fe, from ,to);
    }

    public List<HorarioAsignaturaGeneral> getHorarioGeneralAsignatura(Long estudioId, String asignaturaId, Integer cursoAca, FiltroEstudio fe) {
        return horarioDAO.getHorarioGeneralAsignatura(estudioId, asignaturaId, cursoAca, fe);
    }

    public Long getUltimoLectivo (Long estudioId, String asignaturaId, Integer anyo, FiltroEstudio fe) {
        return horarioDAO.getUltimoLectivo(estudioId,asignaturaId, anyo, fe);
    }

    public InfoFiltro getFiltroInfo(String asignaturaId, Integer anyo)
    {
        return horarioDAO.getInfoFiltros(asignaturaId,anyo);
    }

    public List<Long> getPrimerDiaLectivo (Long estudioId, String asignaturaId, Integer anyo) {
        return horarioDAO.getPrimerLectivos(estudioId, asignaturaId, anyo);
    }

    public List<HorarioGenerar> getAsignaturas(Long estudioId, Integer anyo)
    {

        return horarioDAO.getAsignaturas(estudioId,anyo);
    }
}