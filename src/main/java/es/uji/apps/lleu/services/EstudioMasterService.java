package es.uji.apps.lleu.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.EstudioMasterDAO;
import es.uji.apps.lleu.dao.ExamenesMasterDAO;
import es.uji.apps.lleu.model.ExamenesEstudioMaster;
import es.uji.apps.lleu.model.FicheroHorariosMaster;
import es.uji.apps.lleu.model.HorarioMasterDetallado;
import es.uji.apps.lleu.model.HorarioMasterGeneral;
import es.uji.apps.lleu.utils.FiltroEstudio;

@Service
public class EstudioMasterService {
    private EstudioMasterDAO estudioMasterDAO;
    private ExamenesMasterDAO examenesMasterDAO;

    @Autowired
    public EstudioMasterService(EstudioMasterDAO estudioMasterDAO, ExamenesMasterDAO examenesMasterDAO
    ) {
        this.estudioMasterDAO = estudioMasterDAO;
        this.examenesMasterDAO = examenesMasterDAO;
    }

    public List<HorarioMasterGeneral> getHorarioEstudioMaster(Long estudioId, Integer anyo, FiltroEstudio fe) {
        return this.estudioMasterDAO.getHorarioEstudioMaster(estudioId, anyo, fe);
    }

    /**
     * Devuelve un fichero concreto de horario de máster.
     *
     * @param id        el id del fichero
     * @param estudioId
     * @param anyo
     * @return
     */
    public FicheroHorariosMaster getHorarioMaster(Long id, Long estudioId, Integer anyo) {
        return this.estudioMasterDAO.getHorarioMaster(id, estudioId, anyo);
    }

    /**
     * Metodo que el primer dia lectivo para un estudio.
     *
     * @param estudioId Id del estudio
     * @param anyo      Año del curso
     * @return Date del primer dia lectivo
     */
    public List<Long> getPrimerLectivos(Long estudioId, Integer anyo) {
        return estudioMasterDAO.getPrimerLectivos(estudioId, anyo);
    }

    public Long getUltimoLectivo(Long estudioId, Integer anyo, FiltroEstudio fe) {
        return estudioMasterDAO.getUltimoLectivo(estudioId, anyo, fe);
    }

    /**
     * Funcion que devuelve el curso y el semestre min de los eventos disponibles
     *
     * @param estudioId
     * @param anyo
     * @param fe
     * @return
     */
    public Map<String, Long> getMinEventos(Long estudioId, Integer anyo, FiltroEstudio fe) {
        return estudioMasterDAO.getMinEventos(estudioId, anyo, fe);
    }

    public List<HorarioMasterDetallado> getHorarios(Long estudioId, Integer anyo, FiltroEstudio fe, Date ini, Date fin) {
        return this.estudioMasterDAO.getHorariosEstudio(estudioId, anyo, fe, ini, fin);
    }


    public boolean hayExamenes (Long estudioId, Integer anyo) {
        return this.examenesMasterDAO.hayExamenes(estudioId, anyo);
    }

    public List<ExamenesEstudioMaster> getExamenesByDate (Long estudioId, Integer anyo, Integer convocatoriaId) {
        return this.examenesMasterDAO.getExamenesByDate(estudioId, anyo,convocatoriaId);
    }
}
