package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.ConvocatoriaDAO;
import es.uji.apps.lleu.model.Convocatoria;
import es.uji.apps.lleu.model.enums.TipoDocencia;

@Service
public class ConvocatoriaService
{
    private ConvocatoriaDAO convocatoriaDAO;

    @Autowired
    public ConvocatoriaService(ConvocatoriaDAO convocatoriaDAO)
    {
        this.convocatoriaDAO = convocatoriaDAO;
    }

    public List<Convocatoria> getConvocatoriasGrados()
    {
        return convocatoriaDAO.getConvocatoriasGrados();
    }

    public List<Convocatoria> getConvocatoriasMasters()
    {
        return convocatoriaDAO.getConvocatoriasMasters();
    }

    public Integer getConvocatoriaActiva(Long estudioId, Integer cursoAca, TipoDocencia tipoDocencia)
    {
        return convocatoriaDAO.getConvocatoriaActiva(estudioId, cursoAca,tipoDocencia);
    }
}