package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.UbicacionEstructuralDAO;
import es.uji.apps.lleu.model.UbicacionEstructural;

@Service
public class UbicacionesEstructuralesService
{
    private UbicacionEstructuralDAO ubicacionEstructuralDAO;

    @Autowired
    public UbicacionesEstructuralesService ( UbicacionEstructuralDAO ubicacionEstructuralDAO) {
        this.ubicacionEstructuralDAO = ubicacionEstructuralDAO;
    }

    public List<UbicacionEstructural> getUbicacionesEstructurales (List<Long> ids ) {
        return this.ubicacionEstructuralDAO.getByIds(ids);
    }
    public UbicacionEstructural getUbicacionEstructural (Long id) {
        return null;
    }


}
