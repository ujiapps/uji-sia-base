package es.uji.apps.lleu.services;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;

import es.uji.apps.lleu.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.AsignaturaEEPDAO;
import es.uji.apps.lleu.dao.AsignaturasCircuitoDAO;
import es.uji.apps.lleu.dao.CircuitosDAO;
import es.uji.apps.lleu.dao.CoordinadoresEEPDAO;
import es.uji.apps.lleu.dao.EstiloDAO;
import es.uji.apps.lleu.dao.EstudioDAO;
import es.uji.apps.lleu.dao.ExamenesEstudioDAO;
import es.uji.apps.lleu.dao.HorariosEstudioDAO;
import es.uji.apps.lleu.dao.ItinerarioDAO;
import es.uji.apps.lleu.dao.PresentacionEstudioDAO;
import es.uji.apps.lleu.dao.ResumenCreditosDAO;
import es.uji.apps.lleu.model.enums.TipoDocencia;
import es.uji.apps.lleu.ui.EstudioUI;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.FiltroGenerarHorario;
import es.uji.apps.lleu.utils.InfoFiltro;

@Service
public class EstudioService
{
    private EstudioDAO estudioDAO;
    private HorariosEstudioDAO horariosEstudioDAO;
    private ExamenesEstudioDAO examenesEstudioDAO;
    private CircuitosDAO circuitosDAO;
    private AsignaturasCircuitoDAO asignaturasCircuitoDAO;
    private ItinerarioDAO itinerarioDAO;
    private EstiloDAO estiloDAO;
    private ResumenCreditosDAO resumenCreditosDAO;
    private AsignaturaEEPDAO asignaturaEEPDAO;
    private CoordinadoresEEPDAO coordinadoresEEPDAO;
    private PresentacionEstudioDAO presentacionMasterDAO;

    @Autowired
    public EstudioService(EstudioDAO estudioDAO,
                          HorariosEstudioDAO horariosEstudioDAO,
                          ExamenesEstudioDAO examenesEstudioDAO,
                          CircuitosDAO circuitosDAO,
                          AsignaturasCircuitoDAO asignaturasCircuitoDAO,
                          ItinerarioDAO itinerarioDAO,
                          EstiloDAO estiloDAO,
                          ResumenCreditosDAO resumenCreditosDAO,
                          AsignaturaEEPDAO asignaturaEEPDAO,
                          CoordinadoresEEPDAO coordinadoresEEPDAO,
                          PresentacionEstudioDAO presentacionMasterDAO)
    {
        this.estudioDAO = estudioDAO;
        this.horariosEstudioDAO = horariosEstudioDAO;
        this.examenesEstudioDAO = examenesEstudioDAO;
        this.circuitosDAO = circuitosDAO;
        this.asignaturasCircuitoDAO = asignaturasCircuitoDAO;
        this.itinerarioDAO=itinerarioDAO;
        this.estiloDAO = estiloDAO;
        this.resumenCreditosDAO = resumenCreditosDAO;
        this.asignaturaEEPDAO=asignaturaEEPDAO;
        this.coordinadoresEEPDAO= coordinadoresEEPDAO;
        this.presentacionMasterDAO = presentacionMasterDAO;
    }

    public List<Estudio> getAll()
    {
        return estudioDAO.get(Estudio.class);
    }

    public EstudioUI getById(Long id, String idioma, Integer anyo)
    {
        EstudioTodo estudioTodo = estudioDAO.getById(id, anyo);
        EstudioUrl estudioUrl = estudioDAO.getEstudioUrls(id);

        EstudioUI estudioUI = EstudioUI.toUI(estudioTodo, estudioUrl, idioma,anyo);

        return estudioUI;

    }

    public Estudio insert(Estudio estudio)
    {
        return estudioDAO.insert(estudio);
    }

    public Estudio update(Estudio estudio)
    {
        return estudioDAO.update(estudio);
    }

    public void delete(Long id)
    {
        estudioDAO.delete(Estudio.class, id);
    }

    public List<Estudio> getEstudios(Integer anyo)
    {
        return estudioDAO.getEstudios(anyo);
    }


    public List<EstudioPortada> getEstudiosPortada(Integer anyo) {
        return estudioDAO.getEstudiosPortada(anyo);
    }

    public List<Estudio> getEstudiosPortadaOld(Integer anyo) {
        return estudioDAO.getEstudiosPortadaOld(anyo);
    }

    public InfoFiltro getFiltroInfo (Long estudioId, Integer anyo) {
        return null;
    }

    public List<HorarioEstudioDetallado> getHorarios (Long estudioId, Integer anyo, FiltroEstudio fe, Date ini, Date fin) {
        return this.horariosEstudioDAO.getHorariosEstudio(estudioId, anyo, fe, ini, fin);
    }
    /**
     * Metodo que el primer dia lectivo para un estudio.
     * @param estudioId Id del estudio
     * @param anyo Año del curso
     * @return Date del primer dia lectivo
     */
    public List<Long> getPrimerLectivos(Long estudioId, Integer anyo)
    {
        return horariosEstudioDAO.getPrimerLectivos(estudioId, anyo);
    }

    public Long getUltimoLectivo (Long estudioId, Integer anyo, FiltroEstudio fe) {
        return horariosEstudioDAO.getUltimoLectivo(estudioId, anyo, fe);
    }

    /**
     * Devuelve un listado de los exámenes de un estudio.
     *
     * @param estudioId
     * @param anyo
     * @return
     */
    public List<ExamenesEstudio> getExamenes(Long estudioId, Integer anyo) {
        return this.examenesEstudioDAO.getExamenes(estudioId, anyo);
    }

    public List<ExamenesEstudio> getExamenesByDate (Long estudioId, Integer anyo, Integer convocatoriaId) {
        return this.examenesEstudioDAO.getExamenesByDate(estudioId, anyo,convocatoriaId);
    }
    public boolean hayExamenes (Long estudioId, Integer anyo) {
        return this.examenesEstudioDAO.hayExamenes(estudioId, anyo);
    }

    public List<Circuito> getCircuitos (Long estudioId, Integer anyo) {
        return this.circuitosDAO.getCircuitos(estudioId, anyo);
    }
    public List<AsignaturaCircuito> getAsignaturasCircuito (Long estudioId, Long circuitoId, Integer anyo) {
        return this.asignaturasCircuitoDAO.getAsignaturasCircuito(estudioId, circuitoId, anyo);
    }

    /**
     * Devuelve un listado de itinerarios para un estudio en un curso academico.
     * @param estudioId
     * @param anyo
     * @return
     */
    public List<Itinerario> getItinerarios(Long estudioId, Integer anyo)
    {
        return itinerarioDAO.getItinerarios(estudioId,anyo);
    }

    public List<Itinerario> getItinerariosFuturos(Long estudioId, Integer anyo)
    {
        return itinerarioDAO.getItinerariosFuturos(estudioId,anyo);
    }

    public List<Itinerario> getMenciones(Long estudioId, Integer anyo)
    {
        return itinerarioDAO.getMenciones(estudioId,anyo);
    }

    public List<Itinerario> getMencionesFuturas(Long estudioId, Integer anyo)
    {
        return itinerarioDAO.getMencionesFuturas(estudioId,anyo);
    }

    public boolean isMencionesFuturasOb(Long estudioId, Integer anyo)
    {
        Itinerario aux = itinerarioDAO.isMencionesFuturasOb(estudioId,anyo);
        return aux != null;
    }

    public List<EstiloAsignatura> getAsignaturasEstilo (Long estudioId, Integer anyo) {
        return this.estiloDAO.getAsignaturas(estudioId, anyo);
    }

    public List<ResumenCreditosGrado> getResumenCreditosGrado (Long estudioId) {
        return this.resumenCreditosDAO.getResumenCreditosGrado(estudioId);
    }
    public ResumenCreditosPop getResumenCreditosPop (Long estudioId) {
        return this.resumenCreditosDAO.getResumenCreditoPop(estudioId);
    }

    /**
     * Devuelve un listado de las asignaturas de EEP para una titulacion
     * @param estudioId
     * @param anyo
     * @return
     */
    public List<AsignaturaEEP> getAsignaturasEEP(Long estudioId, Integer anyo , TipoDocencia tipo)
    {
        return this.asignaturaEEPDAO.getAsignaturasEEP(estudioId,anyo, tipo);
    }

    public List<AsignaturaEEPExtra> getAsignaturasEEPExtra(Long estudioId, TipoDocencia tipo)
    {
        return this.asignaturaEEPDAO.getAsignaturasEEPExtra(estudioId , tipo);
    }

    public List<CoordinadoresEEP> getCoordinadoresEEP(Long estudioId)
    {
        return this.coordinadoresEEPDAO.getCoordinadoresEEP(estudioId);
    }

    public PresentacionMaster getPresentacion(Long estudioId, Integer anyo) {
        return this.presentacionMasterDAO.getPresentacionEstudio(estudioId, anyo);
    }
    public List<PresentacionCoordinadores> getPresentacionCoordinadores(Long estudioId) {
        return this.presentacionMasterDAO.getCoordinadores(estudioId);
    }

    public List<HorarioAsignaturaGeneral> getHorariosGenerados(Long estudioId, Integer anyo, List<FiltroGenerarHorario> filtros)
    {
        return this.horariosEstudioDAO.getHorariosGenerados( estudioId,  anyo,  filtros);
    }

    /**
     * Devuelve el horario de un circuito concreto.
     *
     * @param estudioId
     * @param anyo
     * @param circuitoId
     * @param semestre
     */
    public List<HorarioAsignaturaGeneral> getHorariosCircuito(Long estudioId, Integer anyo, Long circuitoId, Integer semestre) {
        return this.horariosEstudioDAO.getHorariosCircuito(estudioId, anyo, circuitoId, semestre);
    }
    public List<HorarioEstudioDetallado> getHorariosCircuitoByDate (Long estudioId, Integer anyo, Long circuitoId, Integer semestre, Long from) {
        LocalDate dateFrom = Instant.ofEpochMilli(from).atZone(ZoneId.of("UTC")).toLocalDate();
        LocalDate dateTo = dateFrom.plusDays(5);
        return this.horariosEstudioDAO.getHorariosCircuitoByDate(estudioId, anyo, circuitoId, semestre, dateFrom, dateTo);
    }

    public List<HorarioEstudioGeneral> getHorarioEstudioGeneral(Long estudioId, Integer anyo, FiltroEstudio fe)
    {
        return this.horariosEstudioDAO.getHorarioEstudioGeneral(estudioId, anyo, fe);
    }

    /**
     * Funcion que devuelve el curso y el semestre min de los eventos disponivles
     * @param estudioId
     * @param anyo
     * @param fe
     * @return
     */
    public Map<String, Long> getMinEventos(Long estudioId, Integer anyo, FiltroEstudio fe)
    {
        return horariosEstudioDAO.getMinEventos(estudioId,anyo,fe);
    }
    public Long isExtinto(Long estudioId,Integer anyo) {
        return estudioDAO.isExtinto(estudioId, anyo);
    }
}
