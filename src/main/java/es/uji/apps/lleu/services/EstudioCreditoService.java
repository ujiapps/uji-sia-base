package es.uji.apps.lleu.services;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.EstudioCreditoDAO;
import es.uji.apps.lleu.model.EstudioCredito;

import static java.lang.StrictMath.max;

@Service
public class EstudioCreditoService
{
    private EstudioCreditoDAO estudioCreditoDAO;

    @Autowired
    public EstudioCreditoService(EstudioCreditoDAO estudioCreditoDAO)
    {
        this.estudioCreditoDAO = estudioCreditoDAO;
    }

    public List<EstudioCredito> getAll()
    {
        return estudioCreditoDAO.get(EstudioCredito.class);
    }

    public EstudioCredito getById(Long id)
    {
        return estudioCreditoDAO.get(EstudioCredito.class, id).get(0);
    }

    /**
     * Dado los creditos de un estudio devuelve un resumen.
     *
     * @param id  id del estudio
     * @return  HashMap con los valores de ciclo, cursos y creditos
     */
    public HashMap<String, Float> getResumenCreditos(Long id)
    {
        List<EstudioCredito> estudioCreditos= estudioCreditoDAO.getResumenCreditos(id);
        HashMap<String, Float> resumen = new HashMap<>();

        Float total = 0f;
        Float cursos = 0f;
        Float ciclo = 0f;

        for (EstudioCredito estudioCredito : estudioCreditos){
            total += estudioCredito.getTotal();
            cursos += 1;
            ciclo = max(ciclo,estudioCredito.getCiclo());
        }
        resumen.put("ciclo", ciclo);
        resumen.put("cursos", cursos);
        resumen.put("creditos", total);
        return resumen;
    }
}
