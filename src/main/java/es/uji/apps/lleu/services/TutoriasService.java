package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.TutoriasDAO;
import es.uji.apps.lleu.model.Tutorias;
import es.uji.apps.lleu.model.enums.TipoDocencia;

@Service
public class TutoriasService
{
    private TutoriasDAO tutoriasDAO;

    @Autowired
    public TutoriasService(TutoriasDAO tutoriasDAO)
    {
        this.tutoriasDAO = tutoriasDAO;
    }

    public List<Tutorias> getAll()
    {
        return tutoriasDAO.get(Tutorias.class);
    }

    public Tutorias getById(String id)
    {
        return tutoriasDAO.get(Tutorias.class, id).get(0);
    }

    public List<Tutorias> getTutorias(String asignaturaId, Integer anyo, TipoDocencia tipo)
    {
        if(tipo.getValue().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {
            return tutoriasDAO.getTutoriasGrado(asignaturaId, anyo);
        }else{
            return tutoriasDAO.getTutoriasMaster(asignaturaId, anyo);

        }
    }
}
