package es.uji.apps.lleu.services;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.FotoDAO;
import es.uji.apps.lleu.model.Foto;
import es.uji.apps.lleu.ui.FotoUI;
import es.uji.apps.lleu.ui.NoFotoUI;
import es.uji.apps.lleu.utils.IPersona;

@Service
public class FotoService
{
    private FotoDAO fotoDAO;
    private static FotoUI noFotoInstance = null;

    /**
     * Devuelve una instancia de foto para una persona que no tenga foto o no autorice a poner foto.
     *
     * @param p
     * @return
     */
    private static FotoUI getNoFoto(Long perId) {
        if (noFotoInstance == null) {
            noFotoInstance = new NoFotoUI(perId);
        }
        return noFotoInstance;
    }

    @Autowired
    public FotoService( FotoDAO fotoDAO ) {
        this.fotoDAO= fotoDAO;
    }

    /**
     * Dado un listado de personas, devolvemos su foto
     *
     * @param lap
     * @return
     */
    public Map<Long, FotoUI> getFotos(List<? extends IPersona> lap)
    {
        // La clave es el perid, el valor la foto
        Map<Long, FotoUI> ret = lap
                .parallelStream()
                .map(IPersona::getPerId)
                .distinct()
                .collect(Collectors.toMap(
                        perid -> perid,
                        perid -> getNoFoto(perid)
                ));

        // ahora sacamos las fotos de la base de datos y actualizamos el valor de vuelta

        List<Foto> fotos = this.fotoDAO.getFotos(ret.keySet());
        fotos.forEach(f -> ret.put(f.getPerId(), FotoUI.toUI(f)));

        return ret;
    }
}
