package es.uji.apps.lleu.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.lleu.model.AsignaturaComentarios;
import es.uji.apps.lleu.model.AsignaturaMetodologia;
import es.uji.apps.lleu.model.AsignaturaProfesor;
import es.uji.apps.lleu.model.AsignaturaProfesorMaster;
import es.uji.apps.lleu.model.AsignaturaRequisito;
import es.uji.apps.lleu.model.AsignaturaTemario;
import es.uji.apps.lleu.model.AsignaturasGrado;
import es.uji.apps.lleu.model.AsignaturasMaster;
import es.uji.apps.lleu.model.Criterios;
import es.uji.apps.lleu.model.ExamenesGrado;
import es.uji.apps.lleu.model.ExamenesMaster;
import es.uji.apps.lleu.model.HorarioAsigMasterDetallado;
import es.uji.apps.lleu.model.HorarioAsigMasterGeneral;
import es.uji.apps.lleu.model.HorarioAsignaturaDetallado;
import es.uji.apps.lleu.model.HorarioAsignaturaGeneral;
import es.uji.apps.lleu.model.Reconocimiento;
import es.uji.apps.lleu.model.RequisitosMatriculaEEP;
import es.uji.apps.lleu.model.ResultadosGuias;
import es.uji.apps.lleu.model.SistemaEvaluacionGrado;
import es.uji.apps.lleu.model.SistemaEvaluacionMaster;
import es.uji.apps.lleu.model.Tutorias;
import es.uji.apps.lleu.model.enums.CSSClass;
import es.uji.apps.lleu.model.enums.TipoComentario;
import es.uji.apps.lleu.model.enums.TipoDocencia;
import es.uji.apps.lleu.services.ActividadesCompetenciasService;
import es.uji.apps.lleu.services.AsignaturaProfesorService;
import es.uji.apps.lleu.services.AsignaturaService;
import es.uji.apps.lleu.services.AsignaturaTemarioService;
import es.uji.apps.lleu.services.AsignaturasGradoService;
import es.uji.apps.lleu.services.AsignaturasMasterService;
import es.uji.apps.lleu.services.AsignaturasMetodologiaService;
import es.uji.apps.lleu.services.EstudioService;
import es.uji.apps.lleu.services.FotoService;
import es.uji.apps.lleu.services.HorarioService;
import es.uji.apps.lleu.services.ProfesoresEstudioService;
import es.uji.apps.lleu.services.ReconocimientoService;
import es.uji.apps.lleu.services.TutoriasService;
import es.uji.apps.lleu.ui.ActividadUI;
import es.uji.apps.lleu.ui.AsignaturaComentariosUI;
import es.uji.apps.lleu.ui.AsignaturaGradoUI;
import es.uji.apps.lleu.ui.AsignaturaMasterUI;
import es.uji.apps.lleu.ui.AsignaturaProfesoresMasterUI;
import es.uji.apps.lleu.ui.AsignaturaProfesoresUI;
import es.uji.apps.lleu.ui.AsignaturaRequisitoUI;
import es.uji.apps.lleu.ui.BreadCrumb;
import es.uji.apps.lleu.ui.CompetenciaUI;
import es.uji.apps.lleu.ui.CriterioUI;
import es.uji.apps.lleu.ui.EstudioUI;
import es.uji.apps.lleu.ui.ExamenMasterUI;
import es.uji.apps.lleu.ui.ExamenUI;
import es.uji.apps.lleu.ui.HorarioAsignaturaUI;
import es.uji.apps.lleu.ui.LeyendaSubgrupoUI;
import es.uji.apps.lleu.ui.MetodologiaUI;
import es.uji.apps.lleu.ui.ProfesorUI;
import es.uji.apps.lleu.ui.ReconocimientoUI;
import es.uji.apps.lleu.ui.RequisitosMatriculaEEPUI;
import es.uji.apps.lleu.ui.ResultadosGuiasUI;
import es.uji.apps.lleu.ui.SistemaEvaluacionUI;
import es.uji.apps.lleu.ui.TemarioUI;
import es.uji.apps.lleu.utils.AppInfo;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.InfoFiltro;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;

@Path("asignatura")
public class AsignaturaResource {
    @InjectParam
    EstudioService estudioService;
    @InjectParam
    AsignaturaService asignaturaService;
    @InjectParam
    AsignaturasGradoService asignaturasGradoService;
    @InjectParam
    AsignaturaProfesorService asignaturaProfesorService;
    @InjectParam
    TutoriasService tutoriasService;
    @InjectParam
    AsignaturasMetodologiaService asignaturasMetodologiaService;
    @InjectParam
    HorarioService horarioService;
    @InjectParam
    ReconocimientoService reconocimientoService;
    @InjectParam
    AsignaturasMasterService asignaturasMasterService;
    @InjectParam
    AsignaturaTemarioService asignaturaTemarioService;
    @InjectParam
    ActividadesCompetenciasService actividadesCompetenciasService;
    @InjectParam
    ProfesoresEstudioService profesoresEstudioService;

    @InjectParam
    FotoService fotoService;

    @PathParam("anyo")
    private Integer anyo;
    @PathParam("id")
    private Long estudioId;

    /**
     * Petición inicial.
     *
     * @param idioma
     * @param asignaturaId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}")
    @Produces(MediaType.TEXT_HTML)
    public Template get(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                        @PathParam("id") String asignaturaId
    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);

        Template template = AppInfo.buildPagina(
                this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId,
                this.anyo,
                idioma,
                String.join(" ", "SIA","-",estudioUI.getNombre()),
                false,
                false);

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {
            // Calculamos los filtros que deben estar activos.
            InfoFiltro ifs = horarioService.getFiltroInfo(asignaturaId, this.anyo);

            AsignaturasGrado a = asignaturasGradoService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            List<AsignaturaComentarios> comentarios = asignaturasGradoService.getAsignaturaComentarios(a, this.estudioId, this.anyo, TipoComentario.PRINCIPALASIG);

            List<Long> diasLectivos = horarioService.getPrimerDiaLectivo(estudioId, asignaturaId, anyo);
            AsignaturaGradoUI asignaturaGradoUI = AsignaturaGradoUI.toUI(a, idioma);

            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            template.put("contenido", "sia/asignatura/base");
            template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
            template.put("estudio", estudioUI);
            template.put("infofiltroestudio", ifs);
            template.put("leyendasubgrupos", LeyendaSubgrupoUI.toUI(ifs.getTiposSubgrupo()));
            template.put("asignatura", asignaturaGradoUI);
            template.put("comentarios", AsignaturaComentariosUI.toUI(comentarios, idioma));
            template.put("asignaturaSeccion", "sia/asignatura/principal");
            template.put("pageclass", "page-asignaturaprincipal");
            template.put("primerLectivos", diasLectivos);
            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaGradoUI.getAsignaturaId() + " - " + asignaturaGradoUI.getNombre(), false)
            ));
        } else
        {
            // Obtenemos un listado de asignaturas filtradas.
            AsignaturasMaster asignaturasMaster = asignaturasMasterService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            AsignaturaMasterUI asignaturaMasterUI = AsignaturaMasterUI.toUI(asignaturasMaster, idioma);
            InfoFiltro ifs = asignaturasMasterService.getFiltroInfo(estudioId, asignaturaId, anyo);
            List<Long> diasLectivos = asignaturasMasterService.getPrimerDiaLectivo(estudioId, asignaturaId, anyo);

//            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
//            template.put("tieneguiaingles", tieneGuia);

            template.put("contenido", "sia/master/asignatura/base");
            template.put("asignaturaSeccion", "sia/master/asignatura/principal");
            template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
            template.put("estudio", estudioUI);
            template.put("asignatura", AsignaturaMasterUI.toUI(asignaturasMaster, idioma));

            template.put("infofiltroestudio", ifs);
            template.put("leyendasubgrupos", LeyendaSubgrupoUI.toUI(ifs.getTiposSubgrupo()));
            template.put("pageclass", "page-asigmasterprincipal");
            template.put("primerLectivos", diasLectivos);

            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaMasterUI.getAsignaturaId() + " - " + asignaturaMasterUI.getNombre(), false)
            ));
        }

        return template;
    }

    /**
     * Actividades, Competencias y Resultados
     *
     * @param idioma
     * @param asignaturaId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/incompatibilidades")
    @Produces(MediaType.TEXT_HTML)
    public Template getIncompatibilidades(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                          @PathParam("id") String asignaturaId
    ) throws ParseException {
        AsignaturasGrado a = asignaturasGradoService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);
        List<AsignaturaRequisito> incompatibilidades = asignaturaService.getIncompatibilidades(asignaturaId, this.estudioId, this.anyo);

        Template template = AppInfo.buildPagina(
                this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/incompatibilidades",
                this.anyo,
                idioma,
                String.join(" ", "SIA","-",estudioUI.getNombre()),
                false,
                false);


        AsignaturaGradoUI asignaturaGradoUI = AsignaturaGradoUI.toUI(a, idioma);

        boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
        template.put("tieneguiaingles", tieneGuia);

        template.put("contenido", "sia/asignatura/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("asignatura", asignaturaGradoUI);
        template.put("asignaturaSeccion", "sia/asignatura/incompatibilidades");

        template.put("incompatibilidades", AsignaturaRequisitoUI.toUI(incompatibilidades, idioma));

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaGradoUI.getAsignaturaId() + " - " + asignaturaGradoUI.getNombre(), false),
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/incompatibilidades", "incompatibilidades", true)
        ));

        return template;
    }

    /**
     * Actividades, Competencias y Resultados
     *
     * @param idioma
     * @param asignaturaId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/evaluacion")
    @Produces(MediaType.TEXT_HTML)
    public Template getEvaluacion(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                  @PathParam("id") String asignaturaId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);
        List<Criterios> criterios = asignaturasGradoService.getCriteriosEvaluacion(asignaturaId, anyo);
        Template template = AppInfo.buildPagina(
                this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/evaluacion",
                this.anyo,
                idioma,
                String.join(" ", "SIA","-",estudioUI.getNombre()),
                false,
                false);

        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("asignaturaSeccion", "sia/asignatura/evaluacion");
        template.put("criterios", CriterioUI.toUI(criterios, idioma));

        // los grados además tienen exámenes
        String nombreAsignatura = "";
        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {

            List<SistemaEvaluacionGrado> sistemas = asignaturasGradoService.getSistemasEvaluacion(asignaturaId, anyo);
            AsignaturasGrado a = asignaturasGradoService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            AsignaturaGradoUI agui = AsignaturaGradoUI.toUI(a, idioma);

            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);


            template.put("sistemas", SistemaEvaluacionUI.toUI(sistemas, idioma));
            template.put("contenido", "sia/asignatura/base");
            template.put("asignatura", agui);

            nombreAsignatura = agui.getAsignaturaId() + " - " + agui.getNombre();

        } else if (estudioUI.getTipo().compareTo(TipoDocencia.MASTERES.getValue()) == 0)
        {

            List<SistemaEvaluacionMaster> sistemas = asignaturasMasterService.getSistemasEvaluacion(asignaturaId, anyo);
            AsignaturasMaster a = asignaturasMasterService.getAsignatura(asignaturaId, estudioId, this.anyo);
            AsignaturaMasterUI amui = AsignaturaMasterUI.toUI(a, idioma);

            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            template.put("sistemas", SistemaEvaluacionUI.toUI(sistemas, idioma));
            template.put("contenido", "sia/master/asignatura/base");
            template.put("asignatura", amui);
            nombreAsignatura = amui.getAsignaturaId() + " - " + amui.getNombre();

        }

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, nombreAsignatura, false),
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/evaluacion", "evaluacion", true)
        ));

        return template;
    }


    /**
     * Actividades, Competencias y Resultados
     *
     * @param idioma
     * @param asignaturaId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/actividades")
    @Produces(MediaType.TEXT_HTML)
    public Template getActividades(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                   @PathParam("id") String asignaturaId
    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);

        List<ActividadUI> actividades = actividadesCompetenciasService.getActividades(asignaturaId, anyo, idioma);
        List<CompetenciaUI> competencias = actividadesCompetenciasService.getCompetencias(asignaturaId, anyo, idioma);
        List<ResultadosGuias> resultados = actividadesCompetenciasService.getResultados(asignaturaId, anyo);

        Template template = AppInfo.buildPagina(
                this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId,
                this.anyo,
                idioma,
                String.join(" ", "SIA","-",estudioUI.getNombre()),
                false,
                false);


        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);

        template.put("actividades", actividades);
        template.put("competencias", competencias);
        template.put("resultados", ResultadosGuiasUI.toUI(resultados, idioma));
        template.put("asignaturaSeccion", "sia/asignatura/actividades-competencias");

        String nombreAsignatura;
        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {
            AsignaturasGrado a = asignaturasGradoService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            AsignaturaGradoUI asignaturaGradoUI = AsignaturaGradoUI.toUI(a, idioma);

            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            template.put("asignatura", asignaturaGradoUI);
            template.put("contenido", "sia/asignatura/base");

            nombreAsignatura = asignaturaGradoUI.getAsignaturaId() + " - " + asignaturaGradoUI.getNombre();

        } else if (estudioUI.getTipo().compareTo(TipoDocencia.MASTERES.getValue()) == 0)
        {
            AsignaturasMaster am = asignaturasMasterService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            AsignaturaMasterUI asignaturaMasterUI = AsignaturaMasterUI.toUI(am, idioma);

            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            template.put("asignatura", asignaturaMasterUI);
            template.put("contenido", "sia/master/asignatura/base");

            nombreAsignatura = asignaturaMasterUI.getAsignaturaId() + " - " + asignaturaMasterUI.getNombre();
        } else
        {
            nombreAsignatura = "";
        }

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, nombreAsignatura, false),
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/actividades", "actividadescompetencias", true)
        ));

        return template;
    }


    /**
     * Requisitos de matrícula
     *
     * @param idioma
     * @param asignaturaId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/reqmatricula")
    @Produces(MediaType.TEXT_HTML)
    public Template getReqMatricula(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                    @PathParam("id") String asignaturaId
    ) throws ParseException {
        AsignaturasGrado a = asignaturasGradoService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);

        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/reqmatricula",
                this.anyo,
                idioma,
                String.join(" ", "SIA","-",estudioUI.getNombre()),
                false,
                false);
        if (a.getEep().equals("S"))
        {
            List<RequisitosMatriculaEEP> matriculaEEP = asignaturasGradoService.getRequisitosEEP(asignaturaId, this.estudioId, this.anyo);
            template.put("requisitosEEP", RequisitosMatriculaEEPUI.toUI(matriculaEEP, idioma));
        }
        List<AsignaturaRequisito> requisitos = asignaturasGradoService.getRequisitos(asignaturaId, this.estudioId, this.anyo);
        template.put("requisitos", AsignaturaRequisitoUI.toUI(requisitos, idioma));

        AsignaturaGradoUI asignaturaGradoUI = AsignaturaGradoUI.toUI(a, idioma);
        List<AsignaturaComentarios> comentarios = asignaturasGradoService.getAsignaturaComentarios(a, this.estudioId, this.anyo, TipoComentario.REQMATRICULAASIG);

        boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
        template.put("tieneguiaingles", tieneGuia);

        // TODO: quitar mainurl y usar página
        template.put("mainurl", "/sia/rest/publicacion/" + this.anyo);

        template.put("contenido", "sia/asignatura/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("asignatura", asignaturaGradoUI);
        template.put("comentarios", AsignaturaComentariosUI.toUI(comentarios, idioma));
        template.put("asignaturaSeccion", "sia/asignatura/requisitosmatricula");
        template.put("pageclass", "page-requisitosmatriculaasig");

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaGradoUI.getAsignaturaId() + " - " + asignaturaGradoUI.getNombre(), false),
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/reqmatricula", "requisitosmatricula", true)
        ));

        return template;
    }

    /**
     * Temario.
     *
     * @param idioma
     * @param asignaturaId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/temario")
    @Produces(MediaType.TEXT_HTML)
    public Template getTemario(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                               @PathParam("id") String asignaturaId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);


        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/temario", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);

        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {

            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            AsignaturasGrado a = asignaturasGradoService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            AsignaturaGradoUI asignaturaGradoUI = AsignaturaGradoUI.toUI(a, idioma);
            List<AsignaturaTemario> temario = asignaturaTemarioService.getTemario(asignaturaId, anyo);
            template.put("contenido", "sia/asignatura/base");
            template.put("asignatura", asignaturaGradoUI);
            template.put("temario", TemarioUI.toUI(temario, idioma));
            template.put("asignaturaSeccion", "sia/asignatura/temario");
            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaGradoUI.getAsignaturaId() + " - " + asignaturaGradoUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/temario", "asignaturas.contenido.detalle.temario", true)
            ));

        } else
        {
            AsignaturasMaster a = asignaturasMasterService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            AsignaturaMasterUI asignaturaMasterUI = AsignaturaMasterUI.toUI(a, idioma);
            List<AsignaturaTemario> temario = asignaturaTemarioService.getTemario(asignaturaId, anyo);

            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            template.put("contenido", "sia/master/asignatura/base");
            template.put("asignatura", asignaturaMasterUI);
            template.put("temario", TemarioUI.toUI(temario, idioma));
            template.put("asignaturaSeccion", "sia/master/asignatura/temario");
            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaMasterUI.getAsignaturaId() + " - " + asignaturaMasterUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/temario", "asignaturas.contenido.detalle.temario", true)
            ));
        }

        return template;
    }

    /**
     * Metodologia.
     *
     * @param idioma
     * @param asignaturaId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/metodologia")
    @Produces(MediaType.TEXT_HTML)
    public Template getMetodologia(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                   @PathParam("id") String asignaturaId
    ) throws ParseException {

        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);
        AsignaturasGrado a = asignaturasGradoService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
        List<AsignaturaMetodologia> metodologia = asignaturasMetodologiaService.getMetodologia(asignaturaId, anyo);

        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/metodologia", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);


        AsignaturaGradoUI asignaturaGradoUI = AsignaturaGradoUI.toUI(a, idioma);


        boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
        template.put("tieneguiaingles", tieneGuia);

        template.put("contenido", "sia/asignatura/base");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("asignatura", asignaturaGradoUI);

        template.put("metodologia", MetodologiaUI.toUI(metodologia, idioma));
        template.put("asignaturaSeccion", "sia/asignatura/metodologia");

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaGradoUI.getAsignaturaId() + " - " + asignaturaGradoUI.getNombre(), false),
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/metodologia", "metodologiadocente", true)
        ));

        return template;
    }

    /**
     * Profesores.
     *
     * @param idioma
     * @param asignaturaId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/profesores")
    @Produces(MediaType.TEXT_HTML)
    public Template getProfesores(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                  @PathParam("id") String asignaturaId
    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);

        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/profesores", this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("pageclass", "page-profesores");

        template.put("riesgo", profesoresEstudioService.getPersonasRiesgo());

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {
            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            AsignaturasGrado a = asignaturasGradoService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            List<AsignaturaProfesor> profesores = asignaturaProfesorService.getProfesores(asignaturaId, anyo);
            List<Tutorias> tutorias = tutoriasService.getTutorias(asignaturaId, anyo, TipoDocencia.GRADO);

            AsignaturaGradoUI asignaturaGradoUI = AsignaturaGradoUI.toUI(a, idioma);

            template.put("contenido", "sia/asignatura/base");
            template.put("asignatura", asignaturaGradoUI);
            template.put("profesores", AsignaturaProfesoresUI.toUI(profesores, idioma));
            template.put("tutorias", ProfesorUI.toUI(tutorias, idioma));
            template.put("asignaturaSeccion", "sia/asignatura/profesores");
            template.put("fotos", fotoService.getFotos(profesores));

            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaGradoUI.getAsignaturaId() + " - " + asignaturaGradoUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/profesores", "profesorado", true)
            ));

        } else
        {

            AsignaturasMaster a = asignaturasMasterService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            AsignaturaMasterUI asignaturaMasterUI = AsignaturaMasterUI.toUI(a, idioma);
            List<AsignaturaProfesorMaster> profesores = profesoresEstudioService.getProfesoresMaster(asignaturaId, anyo);
            List<Tutorias> tutorias = tutoriasService.getTutorias(asignaturaId, anyo, TipoDocencia.MASTERES);

            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            template.put("contenido", "sia/master/asignatura/base");
            template.put("asignatura", asignaturaMasterUI);
            template.put("profesores", AsignaturaProfesoresMasterUI.toUI(profesores, idioma));
            template.put("tutorias", ProfesorUI.toUI(tutorias, idioma));
            template.put("asignaturaSeccion", "sia/master/asignatura/profesores");
            template.put("fotos", fotoService.getFotos(profesores));

            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaMasterUI.getAsignaturaId() + " - " + asignaturaMasterUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/profesores", "profesorado", true)
            ));

        }

        return template;
    }

    /**
     * Función que nos devuelve los horarios de una asignatura
     *
     * @param idioma
     * @param asignaturaId
     * @param filtroNombre
     * @param filtroCursos
     * @param filtroGrupos
     * @param filtroSemestres
     * @param filtroCaracteres
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/horarios")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getHorario(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                     @PathParam("id") String asignaturaId,
                                     @QueryParam("nombre") @DefaultValue("") String filtroNombre,
                                     @QueryParam("cursos") @DefaultValue("") String filtroCursos,
                                     @QueryParam("grupos") @DefaultValue("") String filtroGrupos,
                                     @QueryParam("semestres") @DefaultValue("") String filtroSemestres,
                                     @QueryParam("caracteres") @DefaultValue("") String filtroCaracteres,
                                     @QueryParam("from") @DefaultValue("-1") Long fromTimestamp

    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);
        List<UIEntity> ret;

        FiltroEstudio fe = FiltroEstudio.getInstanceFromQueryStringParams(
                idioma,
                filtroNombre,
                filtroCursos,
                filtroGrupos,
                filtroSemestres,
                filtroCaracteres,
                null
        );
        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {
            if (fromTimestamp == -1l)
            {
                List<HorarioAsignaturaGeneral> horarioGeneral = horarioService.getHorarioGeneralAsignatura(estudioId, asignaturaId, anyo, fe);
                ret = UIEntity.toUI(HorarioAsignaturaUI.toUI(horarioGeneral, idioma));
            } else
            {
                Date from = new Date(fromTimestamp);
                from.setHours(0);
                from.setMinutes(0);
                from.setSeconds(0);
                Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                c.setTimeInMillis(fromTimestamp);
                c.add(Calendar.DATE, 5);
                Date to = new Date(c.getTimeInMillis());
                to.setHours(0);
                to.setMinutes(0);
                to.setSeconds(0);

                List<HorarioAsignaturaDetallado> horarioAsignaturaDetallado = horarioService.getHorarioDetalladoAsignatura(this.estudioId, asignaturaId, this.anyo, fe, from, to);
                ret = UIEntity.toUI(HorarioAsignaturaUI.toUI(horarioAsignaturaDetallado, idioma));
            }
        } else
        {
            if (fromTimestamp == -1l)
            {
                List<HorarioAsigMasterGeneral> horarioGeneral = asignaturasMasterService.getHorarioGeneralAsignatura(estudioId, asignaturaId, anyo, fe);
                ret = UIEntity.toUI(HorarioAsignaturaUI.toUI(horarioGeneral, idioma));
            } else
            {
                Date from = new Date(fromTimestamp);
                from.setHours(0);
                from.setMinutes(0);
                from.setSeconds(0);
                Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                c.setTimeInMillis(fromTimestamp);
                c.add(Calendar.DATE, 5);
                Date to = new Date(c.getTimeInMillis());
                to.setHours(0);
                to.setMinutes(0);
                to.setSeconds(0);

                List<HorarioAsigMasterDetallado> horarioAsignaturaDetallado = asignaturasMasterService.getHorarioDetalladoAsignatura(this.estudioId, asignaturaId, this.anyo, fe, from, to);
                ret = UIEntity.toUI(HorarioAsignaturaUI.toUI(horarioAsignaturaDetallado, idioma));
            }

        }
        return ret;
    }

    @GET
    @Path("{id}/horarios/ultimolectivo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUltimoLectivo(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                     @PathParam("id") String asignaturaId,
                                     @QueryParam("nombre") @DefaultValue("") String filtroNombre,
                                     @QueryParam("cursos") @DefaultValue("") String filtroCursos,
                                     @QueryParam("grupos") @DefaultValue("") String filtroGrupos,
                                     @QueryParam("semestres") @DefaultValue("") String filtroSemestres,
                                     @QueryParam("caracteres") @DefaultValue("") String filtroCaracteres

    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);
        FiltroEstudio fe = FiltroEstudio.getInstanceFromQueryStringParams(
                idioma,
                filtroNombre,
                filtroCursos,
                filtroGrupos,
                filtroSemestres,
                filtroCaracteres,
                null
        );


        Map<String, Long> results = new HashMap<>();
        results.put("extinto", estudioService.isExtinto(estudioId, anyo));

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {
            Long ultimo = horarioService.getUltimoLectivo(this.estudioId, asignaturaId, this.anyo, fe);
            results.put("ultimo", ultimo);
        } else
        {
            Long ultimo = asignaturasMasterService.getUltimoLectivo(this.estudioId, asignaturaId, this.anyo, fe);
            results.put("ultimo", ultimo);
        }
        HashMap<String, Object> ret = new HashMap<>();
        ret.put("sucess", true);
        ret.put("data", results);
        return Response.ok(ret).build();
    }


    /**
     * Metodo que devuelve el template de reconicimientos para una asignatura.
     *
     * @param idioma
     * @param asignaturaId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/reconocimientos")
    @Produces(MediaType.TEXT_HTML)
    public Template getReconocimientos(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                       @PathParam("id") String asignaturaId
    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);

        List<Reconocimiento> reconocimientos = reconocimientoService.getReconocimientos(asignaturaId, anyo, this.estudioId);

        Template template = AppInfo.buildPagina(
                this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/reconocimientos",
                this.anyo,
                idioma,
                String.join(" ", "SIA","-",estudioUI.getNombre()),
                false,
                false);

        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("reconocimientos", ReconocimientoUI.toUI(reconocimientos, idioma));
        template.put("pageclass", "page-reconocimientos");

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {
            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            AsignaturasGrado a = asignaturasGradoService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            AsignaturaGradoUI asignaturaGradoUI = AsignaturaGradoUI.toUI(a, idioma);

            template.put("contenido", "sia/asignatura/base");
            template.put("asignatura", asignaturaGradoUI);
            template.put("asignaturaSeccion", "sia/asignatura/reconocimientos");
            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaGradoUI.getAsignaturaId() + " - " + asignaturaGradoUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/reconocimientos", "reconocimientos", true)
            ));
        } else
        {
            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            AsignaturasMaster a = asignaturasMasterService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            AsignaturaMasterUI asignaturaMasterUI = AsignaturaMasterUI.toUI(a, idioma);
            template.put("contenido", "sia/master/asignatura/base");
            template.put("asignatura", asignaturaMasterUI);
            template.put("asignaturaSeccion", "sia/master/asignatura/reconocimientos");
            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaMasterUI.getAsignaturaId() + " - " + asignaturaMasterUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/temario", "reconocimientos", true)
            ));
        }

        return template;
    }


    /**
     * Examenes asignatura.
     *
     * @param idioma
     * @param asignaturaId
     * @return
     * @throws ParseException
     */
    @GET
    @Path("{id}/examenes")
    @Produces(MediaType.TEXT_HTML)
    public Template getExamenes(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                @PathParam("id") String asignaturaId
    ) throws ParseException {
        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);

        Template template = AppInfo.buildPagina(
                this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/examenes",
                this.anyo,
                idioma,
                String.join(" ", "SIA","-",estudioUI.getNombre()),
                false,
                false);

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0)
        {
            AsignaturasGrado a = asignaturasGradoService.getAsignatura(asignaturaId, this.estudioId, this.anyo);

            AsignaturaGradoUI asignaturaGradoUI = AsignaturaGradoUI.toUI(a, idioma);

            List<ExamenesGrado> examenes = asignaturasGradoService.getExamenesGrado(asignaturaId, anyo);
            List<ExamenesGrado> parciales;
            if (examenes != null)
            {
                parciales = examenes.stream().filter(e -> e.getParcial().equals("S")).collect(Collectors.toList());
                examenes = examenes.stream().filter(e -> e.getParcial().equals("N")).collect(Collectors.toList());
            } else
            {
                parciales = new ArrayList<>(0);
            }

            List<ExamenUI> examenesUI = ExamenUI.toUI(examenes, idioma);

            boolean tieneGuia = this.asignaturasGradoService.tieneGuiaDocenteEnIngles(asignaturaId, anyo);
            template.put("tieneguiaingles", tieneGuia);

            template.put("contenido", "sia/asignatura/base");
            template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
            template.put("estudio", estudioUI);
            template.put("asignatura", asignaturaGradoUI);
            template.put("asignaturaSeccion", "sia/asignatura/examenes");
            template.put("examenes", examenesUI);
            template.put("parciales", ExamenUI.toUI(parciales, idioma));
            template.put("pageclass", "page-asignaturaexamenes");
        } else
        {
            AsignaturasMaster a = asignaturasMasterService.getAsignatura(asignaturaId, this.estudioId, this.anyo);
            AsignaturaMasterUI asignaturaMasterUI = AsignaturaMasterUI.toUI(a, idioma);

            List<ExamenesMaster> examenes = asignaturasGradoService.getExamenesMaster(asignaturaId, anyo);
            List<ExamenesMaster> parciales;
            if (examenes != null)
            {
                parciales = examenes.stream().filter(e -> e.getParcial().equals("S")).collect(Collectors.toList());
                examenes = examenes.stream().filter(e -> e.getParcial().equals("N")).collect(Collectors.toList());
            } else
            {
                parciales = new ArrayList<>(0);
            }

            List<ExamenMasterUI> examenesUI = ExamenMasterUI.toUI(examenes, idioma);

            template.put("contenido", "sia/master/asignatura/base");
            template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
            template.put("estudio", estudioUI);
            template.put("asignatura", asignaturaMasterUI);
            template.put("asignaturaSeccion", "sia/master/asignatura/examenes");
            template.put("examenes", examenesUI);
            template.put("parciales", ExamenMasterUI.toUI(parciales, idioma));
            template.put("pageclass", "page-asignaturamasterexamenes");
            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId, asignaturaMasterUI.getAsignaturaId() + " - " + asignaturaMasterUI.getNombre(), false),
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId + "/asignatura/" + asignaturaId + "/examenes", "examenes", true)
            ));
        }
        return template;
    }
}