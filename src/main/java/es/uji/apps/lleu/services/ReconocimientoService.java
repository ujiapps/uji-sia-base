package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.ReconocimientoDAO;
import es.uji.apps.lleu.model.Estudio;
import es.uji.apps.lleu.model.EstudioRec;
import es.uji.apps.lleu.model.EstudioRecMaster;
import es.uji.apps.lleu.model.Reconocimiento;

@Service
public class ReconocimientoService
{
    private ReconocimientoDAO reconocimientoDAO;

    @Autowired
    public ReconocimientoService(ReconocimientoDAO reconocimientoDAO)
    {
        this.reconocimientoDAO = reconocimientoDAO;
    }

    public List<Reconocimiento> getAll()
    {
        return reconocimientoDAO.get(Reconocimiento.class);
    }

    public Reconocimiento getById(String id)
    {
        return reconocimientoDAO.get(Reconocimiento.class, id).get(0);
    }

    public List<Reconocimiento> getReconocimientos(String asignaturaId, Integer anyo, Long estudioId)
    {
        return reconocimientoDAO.getReconocimientos(asignaturaId,anyo,estudioId);
    }
    public List<Reconocimiento> getReconocimientosEstudio(Integer anyo, Long estudioId)
    {
        return reconocimientoDAO.getReconocimientosEstudio(anyo,estudioId);
    }

    public List<Reconocimiento> getReconocimientos(String asignaturaId, Integer anyo, Estudio estudio)
    {
        return reconocimientoDAO.getReconocimientos(asignaturaId, anyo, estudio);
    }



    public EstudioRec getEstudioRec(Long estudioId, Integer anyo) {
        return reconocimientoDAO.getEstudioRec(estudioId, anyo);
    }
    public EstudioRecMaster getEstudioRecMaster ( Long estudioId ) {
        return reconocimientoDAO.getEstudioRecMaster(estudioId);
    }

}
