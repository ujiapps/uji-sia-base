package es.uji.apps.lleu.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.lleu.dao.AsignaturaProfesorDAO;
import es.uji.apps.lleu.model.AsignaturaProfesor;

@Service
public class AsignaturaProfesorService
{
    private AsignaturaProfesorDAO asignaturaProfesorDAO;

    @Autowired
    public AsignaturaProfesorService(AsignaturaProfesorDAO asignaturaProfesorDAO)
    {
        this.asignaturaProfesorDAO = asignaturaProfesorDAO;
    }

    public List<AsignaturaProfesor> getAll()
    {
        return asignaturaProfesorDAO.get(AsignaturaProfesor.class);
    }

    public AsignaturaProfesor getById(String id)
    {
        return asignaturaProfesorDAO.get(AsignaturaProfesor.class, id).get(0);
    }

    public List<AsignaturaProfesor> getProfesores(String asignaturaId, Integer anyo)
    {
        return asignaturaProfesorDAO.getProfesores(asignaturaId,anyo);
    }
}
