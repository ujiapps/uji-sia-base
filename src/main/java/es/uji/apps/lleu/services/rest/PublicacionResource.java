package es.uji.apps.lleu.services.rest;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.lleu.model.Estudio;
import es.uji.apps.lleu.model.EstudioPortada;
import es.uji.apps.lleu.model.UbicacionEstructural;
import es.uji.apps.lleu.services.EstudioService;
import es.uji.apps.lleu.services.UbicacionesEstructuralesService;
import es.uji.apps.lleu.ui.EstudioUI;
import es.uji.apps.lleu.ui.EstudioUIOld;
import es.uji.apps.lleu.ui.UbicacionEstructuralUI;
import es.uji.apps.lleu.utils.AppInfo;
import es.uji.apps.lleu.utils.CursoAcademico;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.web.template.Template;

@Path("publicacion")
public class PublicacionResource
{

    @InjectParam
    private EstudioService estudioService;

    @InjectParam
    UbicacionesEstructuralesService ubicacionesEstructuralesService;

    @Path("{anyo}/estudio")
    public EstudioResource getPlatformItem(
            @InjectParam EstudioResource estudioResource)
    {
        return estudioResource;
    }


    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getDefault(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws URISyntaxException
    {
       return Response.seeOther(new URI("/publicacion/"+CursoAcademico.getCurrent())).build();
    }

    @GET
    @Path("{anyo}")
    @Produces(MediaType.TEXT_HTML)
    public Template get(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                        @PathParam("anyo") Integer anyo,
                        @QueryParam("cb") Boolean chatBot) throws MalformedURLException,
            ParseException {

        ParamUtils.checkNotNull(anyo);

        Template template = AppInfo.buildPagina(
                anyo.toString(),
                anyo,
                idioma,
                "SIA",
                true,
                null,chatBot
        );

        if (anyo >= 2024) {
            // Obtenemos los estudios.
            List<EstudioPortada> estudiosPortada = estudioService.getEstudiosPortada(anyo);
            // Obtenemos los centros a los que pertenecen los estudios.
            List<Long> lUestIds = estudiosPortada.stream().map(e -> e.getUbicacionId()).distinct().collect(Collectors.toList());
            List<UbicacionEstructural> centros = ubicacionesEstructuralesService.getUbicacionesEstructurales(lUestIds);

            // Estudios clasificados por tipo de estudio, nivel y ubicación estructural.
            Map<String, Map<Long, Map<Long, List<EstudioUI>>>> estudiosUI = EstudioUI.toUIByEstudioNivelUbicacion(
                    estudiosPortada,
                    idioma,
                    anyo
            );

            template.put("contenido", "sia/contenido");
            template.put("estudios", estudiosUI);
            template.put("centros", UbicacionEstructuralUI.toUI(centros, idioma));
        } else {
            // Obtenemos los estudios.
            List<Estudio> estudios = estudioService.getEstudiosPortadaOld(anyo);

            // Obtenemos los centros a los que pertenecen los estudios.
            List<Long> lUestIds = estudios.stream().map(e -> e.getUestId()).distinct().collect(Collectors.toList());
            List<UbicacionEstructural> centros =  ubicacionesEstructuralesService.getUbicacionesEstructurales(lUestIds);

            // Estudios clasificados por tipo de estudio, nivel y ubicación estructural.
            Map<String, Map<Integer, Map<Long, List<EstudioUIOld>>>> estudiosUI= EstudioUIOld.toUIByEstudioNivelUbicacion(
                    estudios,
                    idioma,
                    anyo
            );

            template.put("contenido", "sia/contenido_old");
            template.put("estudios", estudiosUI);
            template.put("centros", UbicacionEstructuralUI.toUI(centros, idioma));
        }

        template.put("seccion", "sia/listado");
        template.put("pageclass", "page-publicacion");

        return template;
    }

}