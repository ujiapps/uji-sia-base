package es.uji.apps.lleu.services.rest;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.lleu.model.FicheroHorariosMaster;
import es.uji.apps.lleu.model.HorarioAsignaturaGeneral;
import es.uji.apps.lleu.model.HorarioEstudioDetallado;
import es.uji.apps.lleu.model.HorarioEstudioGeneral;
import es.uji.apps.lleu.model.HorarioGenerar;
import es.uji.apps.lleu.model.HorarioMasterDetallado;
import es.uji.apps.lleu.model.HorarioMasterGeneral;
import es.uji.apps.lleu.model.enums.CSSClass;
import es.uji.apps.lleu.model.enums.TipoDocencia;
import es.uji.apps.lleu.services.AsignaturasGradoService;
import es.uji.apps.lleu.services.AsignaturasMasterService;
import es.uji.apps.lleu.services.CalendarOutputStream;
import es.uji.apps.lleu.services.EstudioCreditoService;
import es.uji.apps.lleu.services.EstudioMasterService;
import es.uji.apps.lleu.services.EstudioService;
import es.uji.apps.lleu.services.HorarioService;
import es.uji.apps.lleu.ui.BreadCrumb;
import es.uji.apps.lleu.ui.EstudioUI;
import es.uji.apps.lleu.ui.HorarioEstudioUI;
import es.uji.apps.lleu.ui.HorarioGenerarUI;
import es.uji.apps.lleu.ui.ResumenCreditosCabeceraUI;
import es.uji.apps.lleu.ui.ResumenCreditosPopUI;
import es.uji.apps.lleu.utils.AppInfo;
import es.uji.apps.lleu.utils.FiltroEstudio;
import es.uji.apps.lleu.utils.FiltroGenerarHorario;
import es.uji.apps.lleu.utils.InfoFiltro;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;

public class EstudioHorarioResource {
    @PathParam("anyo")
    private Integer anyo;

    @PathParam("id")
    private Long estudioId;

    @InjectParam
    private EstudioCreditoService estudioCreditoService;

    @InjectParam
    private AsignaturasGradoService asignaturasGradoService;


    @InjectParam
    private HorarioService horarioService;

    @InjectParam
    private EstudioService estudioService;

    @InjectParam
    private EstudioMasterService estudioMasterService;

    @InjectParam
    private CalendarOutputStream calendarOutput;

    @InjectParam
    private AsignaturasMasterService asignaturasMasterService;

    /**
     * Plantilla de horarios de grado / máster y doctorado. Este método se encarga de servir las plantillas de horaios
     * de máster y de grado. También de la descarga de los ficheros de horario de máster. El método puede devolver
     * contenido de cualquier tipo Mime en principio, por eso lo gestionamos todo con un objeto Response.
     * <p>
     * En caso de recibir un Content-type: text-calendar se genera un fichero ics.
     *
     * @param idioma
     * @param download  para un estudio de máster, indica que debemos descargar el fichero con el horario
     * @param estudioId
     * @return
     * @throws MalformedURLException
     * @throws ParseException
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    @Consumes({MediaType.TEXT_XML, "text/calendar"})
    public Response getHorarios(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                @QueryParam("download") @DefaultValue("0") Integer download,
                                @QueryParam("p_id") @DefaultValue("-1") Long pId,
                                @PathParam("id") Long estudioId, @HeaderParam("Content-Type") MediaType type
    ) throws MalformedURLException,
            ParseException {

        Response.ResponseBuilder rb;

        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);

        /**
         * Si se trata de un estudio de máster y piden descarga de fichero.
         */
        if (estudioUI.getTipo().compareTo(TipoDocencia.MASTERES.getValue()) == 0 &&
                download == 1 &&
                pId != -1l) {

            // me piden la descarga de un fichero de horario de máster

            FicheroHorariosMaster h = estudioMasterService.getHorarioMaster(pId, estudioId, this.anyo);

            MediaType mediaType;
            try {
                mediaType = MediaType.valueOf(h.getContentType());
            }
            catch (IllegalArgumentException e) {
                mediaType = MediaType.APPLICATION_OCTET_STREAM_TYPE;
            }

            CacheControl cc = new CacheControl();
            cc.setNoCache(true);
            cc.setNoStore(true);
            cc.setPrivate(true);

            rb = Response.ok(h.getFichero(), mediaType);
            rb.cacheControl(cc);
            rb.header("Content-Disposition", "attachment; filename=\"" + h.getFicheroNombre().replace("\"", "") + "\"");

            return rb.build();
        }

        if (ParamUtils.isNotNull(type) && type.toString().equals("text/calendar")) {
            calendarOutput.setHorarioCalendarData(estudioUI, anyo, idioma);

            return Response
                    .ok(calendarOutput)
                    .header("Content-Disposition",
                            "inline; filename=\"" + calendarOutput.getCalendario() + ".ics\"")
                    .type("text/calendar; charset=UTF-8").build();
        }

        // Si se trata de un estudio de grado o de máster (sin petición de descarga de fichero de máster)

        Template template = AppInfo.buildPagina(
                this.anyo + "/estudio/" + estudioId,
                this.anyo,
                idioma,
                String.join(" ", "SIA","-",estudioUI.getNombre()),
                false,
                false
        );


        template.put("contenido", "sia/estudio/base");
        template.put("estudio", estudioUI);

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {

            HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(estudioId);
            InfoFiltro ifs = asignaturasGradoService.getFiltroInfo(estudioId, anyo);
            List<Long> primerLectivos = estudioService.getPrimerLectivos(estudioId, anyo);

            template.put("contenidoestudio", "sia/estudio/horarios");
            template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
            template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
            template.put("primerLectivos", primerLectivos);
            template.put("infofiltroestudio", ifs);
            template.put("pageclass", "page-horariosestudio");

            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
            ));

            rb = Response.ok(template, MediaType.TEXT_HTML_TYPE);

        } else {
            // estudio de máster sin descarga de fichero

//            List<FicheroHorariosMaster> ficheroHorariosMaster = estudioService.getFicherosHorariosMaster(estudioId, anyo);
//            HorariosMasterUrl hUrl = estudioService.getHorarioMasterUrl(estudioId);

            ResumenCreditosPopUI resumen = ResumenCreditosPopUI.toUI(this.estudioService.getResumenCreditosPop(estudioId));

            template.put("totalCreditos", resumen.getCreditosTotal());

            InfoFiltro ifs = asignaturasMasterService.getFiltroInfo(estudioId, anyo);
            List<Long> primerLectivos = estudioMasterService.getPrimerLectivos(estudioId, anyo);
            if(ifs.getCaracteres().get("BASICA") == null) {
                if(ifs.getCaracteres().get("OPTATIVAS") == null)
                    ifs.getCaracteres().put("PRACTICAS", true);
                else ifs.getCaracteres().put("OPTATIVAS", true);
            }

            template.put("contenido", "sia/estudio/base");
            template.put("contenidoestudio", "sia/master/horarios");
            template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
            template.put("estudio", estudioUI);
            template.put("primerLectivos", primerLectivos);
            template.put("infofiltroestudio", ifs);
            template.put("pageclass", "page-horariosmaster");

//            template.put("ficheros", HorariosMasterUI.toUI(ficheroHorariosMaster, idioma));
//            template.put("horariomaster", hUrl);

            template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                    new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + estudioId, estudioUI.getNombre(), false)
            ));

            rb = Response.ok(template, MediaType.TEXT_HTML_TYPE);

        }


        return rb.build();
    }

    @GET
    @Path("general")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getHorarioEstudioGeneral(
            @CookieParam("uji-lang") @DefaultValue("ca") String idioma,
            @QueryParam("nombre") @DefaultValue("") String filtroNombre,
            @QueryParam("cursos") @DefaultValue("") String filtroCursos,
            @QueryParam("grupos") @DefaultValue("") String filtroGrupos,
            @QueryParam("semestres") @DefaultValue("1") String filtroSemestres,
            @QueryParam("caracteres") @DefaultValue("") String filtroCaracteres
    ) throws MalformedURLException,
            ParseException {
        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);

        FiltroEstudio fe = FiltroEstudio.getInstanceFromQueryStringParams(
                idioma,
                "",
                filtroCursos,
                filtroGrupos,
                filtroSemestres,
                filtroCaracteres,
                ""
        );
        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {
            List<HorarioEstudioGeneral> l = estudioService.getHorarioEstudioGeneral(estudioId, anyo, fe);
            return UIEntity.toUI(HorarioEstudioUI.toUI(l, idioma));
        } else {
            List<HorarioMasterGeneral> l = estudioMasterService.getHorarioEstudioMaster(estudioId, anyo, fe);
            return UIEntity.toUI(HorarioEstudioUI.toUI(l, idioma));
        }
    }

    /**
     * Metodo que devuelve un json con las clases de un estudio en un rango de fechas.
     *
     * @param idioma           Idioma
     * @param filtroNombre     Filtro por nombre
     * @param filtroCursos     Filtro por curso
     * @param filtroGrupos     Filtro por Grupo por defecto de carga el grupo A
     * @param filtroSemestres  Filtro por semestre por defecto se carga el semestre 1
     * @param filtroCaracteres Filtro por caracter de la asignatura.
     * @param fromTimestamp    Timestamp a partir de la cual se quiere buscar, incluido. Debe estar en milisegundos
     * @return Se devuelve un json de clases.
     * @throws MalformedURLException
     * @throws ParseException
     */
    @GET
    @Path("json")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getHorariosJson(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                          @QueryParam("nombre") @DefaultValue("") String filtroNombre,
                                          @QueryParam("cursos") @DefaultValue("") String filtroCursos,
                                          @QueryParam("grupos") @DefaultValue("") String filtroGrupos,
                                          @QueryParam("semestres") @DefaultValue("1") String filtroSemestres,
                                          @QueryParam("caracteres") @DefaultValue("") String filtroCaracteres,
                                          @QueryParam("from") Long fromTimestamp

    ) throws MalformedURLException,
            ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);

        FiltroEstudio fe = FiltroEstudio.getInstanceFromQueryStringParams(
                idioma,
                "",
                filtroCursos,
                filtroGrupos,
                filtroSemestres,
                filtroCaracteres,
                ""
        );

        Date from = new Date(fromTimestamp);
        from.setHours(0);
        from.setMinutes(0);
        from.setSeconds(0);
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        c.setTimeInMillis(fromTimestamp);
        c.add(Calendar.DATE, 6);
        Date to = new Date(c.getTimeInMillis());
        to.setHours(0);
        to.setMinutes(0);
        to.setSeconds(0);


        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {
            List<HorarioEstudioDetallado> l = estudioService.getHorarios(estudioId, anyo, fe, from, to);
            return UIEntity.toUI(HorarioEstudioUI.toUI(l, idioma));
        } else {
            List<HorarioMasterDetallado> l = estudioMasterService.getHorarios(estudioId, anyo, fe, from, to);
            return UIEntity.toUI(HorarioEstudioUI.toUI(l, idioma));
        }

    }

    @GET
    @Path("ultimolectivo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUltimoLectivo(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                     @QueryParam("nombre") @DefaultValue("") String filtroNombre,
                                     @QueryParam("cursos") @DefaultValue("") String filtroCursos,
                                     @QueryParam("grupos") @DefaultValue("") String filtroGrupos,
                                     @QueryParam("semestres") @DefaultValue("") String filtroSemestres,
                                     @QueryParam("caracteres") @DefaultValue("") String filtroCaracteres

    ) throws MalformedURLException,
            ParseException {

        EstudioUI estudioUI = estudioService.getById(estudioId, idioma, this.anyo);

        FiltroEstudio fe = FiltroEstudio.getInstanceFromQueryStringParams(
                idioma,
                "",
                filtroCursos,
                filtroGrupos,
                filtroSemestres,
                filtroCaracteres,
                ""
        );

        Map<String, Long> results = new HashMap<>();
        results.put("extinto", estudioService.isExtinto(estudioId, anyo));

        if (estudioUI.getTipo().compareTo(TipoDocencia.GRADO.getValue()) == 0) {
            Long ultimo = estudioService.getUltimoLectivo(estudioId, anyo, fe);
            results.put("ultimo", ultimo);
            results.putAll(estudioService.getMinEventos(estudioId, anyo, fe));
        } else {
            Long ultimo = estudioMasterService.getUltimoLectivo(estudioId, anyo, fe);
            results.put("ultimo", ultimo);
            results.putAll(estudioMasterService.getMinEventos(estudioId, anyo, fe));
        }

        HashMap<String, Object> ret = new HashMap<>();
        ret.put("sucess", true);
        ret.put("data", results);
        return Response.ok(ret).build();
    }

    @GET
    @Path("generar")
    @Produces(MediaType.TEXT_HTML)
    public Template get(@CookieParam("uji-lang") @DefaultValue("ca") String idioma) throws MalformedURLException,
            ParseException {

        EstudioUI estudioUI = estudioService.getById(this.estudioId, idioma, this.anyo);
        HashMap<String, Float> estudioCreditos = estudioCreditoService.getResumenCreditos(this.estudioId);
        Template template = AppInfo.buildPagina(this.anyo + "/estudio/" + this.estudioId, this.anyo, idioma,String.join(" ", "SIA","-",estudioUI.getNombre()), false, false);

        InfoFiltro ifs = asignaturasGradoService.getFiltroInfo(estudioId, anyo);

        List<HorarioGenerar> asignaturas = this.horarioService.getAsignaturas(estudioId, anyo);
        Map<String, Map<String, List<HorarioGenerarUI>>> mapAsignaturasUI = HorarioGenerarUI.toUI(asignaturas, idioma);
        List<HorarioGenerarUI> lAsignaturasUI = new ArrayList<>();
        for (String c : mapAsignaturasUI.keySet()) {
            for (String asignaturaId : mapAsignaturasUI.get(c).keySet()) {
                lAsignaturasUI.addAll(mapAsignaturasUI.get(c).get(asignaturaId));
            }
        }

        template.put("contenido", "sia/estudio/base");
        template.put("contenidoestudio", "sia/estudio/personaliza-horario");
        template.put("centroCSS", CSSClass.getCSSByCentro(estudioUI.getUestId()));
        template.put("estudio", estudioUI);
        template.put("estudioCreditos", ResumenCreditosCabeceraUI.toUI(estudioCreditos));
        template.put("infofiltroestudio", ifs);
        template.put("pageclass", "page-generar-horarios");
        template.put("asignaturas", HorarioGenerarUI.toUI(asignaturas, idioma));
        template.put("listadoAsignaturas", lAsignaturasUI);

        template.put("breadcrumbs", BreadCrumb.getBreadcrumbList(
                new BreadCrumb(AppInfo.URL_BASE + this.anyo + "/estudio/" + this.estudioId, estudioUI.getNombre(), false)
        ));

        return template;
    }

    @POST
    @Path("generar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> get(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, List<FiltroGenerarHorario> filtros) throws MalformedURLException,
            ParseException {
        List<HorarioAsignaturaGeneral> l = estudioService.getHorariosGenerados(estudioId, anyo, filtros);
        return UIEntity.toUI(HorarioEstudioUI.toUI(l, idioma));
    }


    /**
     * Devuelve los eventos asociados a los eventos de un circuito determinado.
     *
     * @param circuitoId
     * @param semestre
     * @param from
     * @param idioma
     * @return
     */
    @GET
    @Path("circuito/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<UIEntity> getHorarioCircuito(
            @PathParam("id") Long circuitoId,
            @QueryParam("semestre") @DefaultValue("1") Integer semestre,
            @QueryParam("from") @DefaultValue("-1") Long from,
            @CookieParam("uji-lang") @DefaultValue("ca") String idioma
    ) {
        List<UIEntity> ret;

        if (from == -1) {
            List<HorarioAsignaturaGeneral> l = this.estudioService.getHorariosCircuito(estudioId, anyo, circuitoId, semestre);
            ret = UIEntity.toUI(HorarioEstudioUI.toUI(l, idioma));
        } else {
            List<HorarioEstudioDetallado> lHorarioEstudioDetallado = this.estudioService.getHorariosCircuitoByDate(estudioId, anyo, circuitoId, semestre, from);
            ret = UIEntity.toUI(HorarioEstudioUI.toUI(lHorarioEstudioDetallado, idioma));
        }
        return ret;
    }
}