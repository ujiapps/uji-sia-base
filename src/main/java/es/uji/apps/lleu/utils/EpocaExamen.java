package es.uji.apps.lleu.utils;

import java.util.Calendar;
import java.util.Date;

public class EpocaExamen {

    private Integer epoca;
    private Integer year;

    public EpocaExamen(Integer epoca, Integer anyo) {
        this.epoca = epoca;
        this.year = anyo;
    }

    public Date getFrom() {
        Date ret = null;

        Calendar cal = Calendar.getInstance();
        if (this.epoca == 7) {
            cal.set(this.year+1, 0, 1, 0, 0, 0);
            ret = cal.getTime();
        } else if (this.epoca == 8) {
            cal.set(this.year+1, 4, 1, 0, 0, 0);
            ret = cal.getTime();
        } else {
            cal.set(this.year+1, 5, 1, 0, 0, 0);
            ret = cal.getTime();
        }
        return ret;
    }

    public Date getTo() {
        Date ret = null;
        int maxDay;

        Calendar cal = Calendar.getInstance();
        if (this.epoca == 7) {
            cal.set(this.year+1, 1, 1, 23, 59, 59);
            maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(this.year+1, 1, maxDay, 23, 59, 59);
        } else if (this.epoca == 8) {
            cal.set(this.year+1, 5, 1, 23, 59, 59);
            maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(this.year+1, 5, maxDay, 23, 59, 59);
        } else {
            cal.set(this.year+1, 6, 1, 23, 59, 59);
            maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(this.year+1, 6, maxDay, 23, 59, 59);
        }

        return cal.getTime();
    }



}