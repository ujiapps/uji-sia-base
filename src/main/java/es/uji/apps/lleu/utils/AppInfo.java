package es.uji.apps.lleu.utils;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.Pagina;

@Component
public class AppInfo
{
    public static String URL_BASE;

    /**
     * Número a strings. Lo utilizo para poner correctamente las clases #-up de foundation.
     */
    public static final String[] numberToString = {
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",
            "twelve"
    };


    private static String HOST;

    @Value("${uji.lleu.host}")
    private void setHost(String host) { HOST = host; };

    //  TODO: esto es más bien un hack para poder acceder al app.properties. Revisar/Repensar/Refactorizar/Re.*
    @Value("${uji.lleu.basePath}")
    public void setURL_BASE(String urlBase) {
        URL_BASE = urlBase;
    }

    public static Template buildPagina(String url, Integer anyo, String idioma,String titulo, Boolean portada, Boolean periodoAsignaturas) throws ParseException
    {
        return buildPagina(url,anyo,idioma, titulo,portada,periodoAsignaturas,false);
    }


    public static Template buildPagina(String url, Integer anyo, String idioma, String titulo, Boolean portada, Boolean periodoAsignaturas,Boolean chatBot) throws ParseException
    {



        Template template = new HTMLTemplate("sia/base", new Locale(idioma), "sia");
        template.put("pagina", new Pagina(AppInfo.URL_BASE, AppInfo.URL_BASE + url, idioma, titulo));
        if(periodoAsignaturas!=null){
            template.put("periodoAsignaturas", periodoAsignaturas);
            template.put("periodoCircuitos", periodoAsignaturas);

        }else{
            template.put("periodoAsignaturas", PeriodoMatricula.isPeriodoAsignaturas() && anyo == CursoAcademico.getCurrent());
            template.put("periodoCircuitos", PeriodoMatricula.isPeriodoCircuitos() && anyo == CursoAcademico.getCurrent());
        }
        template.put("portada", portada);
        template.put("cursoMin", CursoAcademico.getCurrent());
        template.put("maxCurso", CursoAcademico.getCurrent());

        template.put("chatBot",chatBot);
        List<Integer> cursosAcademicosActivos = CursoAcademico.getCursosAcademicosActivos();
        template.put("cursoActual", anyo);
        template.put("esCursoActivo", cursosAcademicosActivos.contains(anyo));
        if(!cursosAcademicosActivos.contains(anyo)){
            cursosAcademicosActivos.add(anyo);
        }
        template.put("cursosAcademicos", cursosAcademicosActivos);



        template.put("numberToString", AppInfo.numberToString);
        template.put("server", AppInfo.HOST);

        Date hoy = new Date();
        template.put("nc", hoy.getTime());

        return template;
    }

    /**
     * Construye el template para las peticiones AJAX que devuelven HTML
     *
     * @param templateName
     * @param url
     * @param idioma
     * @return
     * @throws ParseException
     */
    public static Template buildPaginaForAjax(String templateName, Integer anyo, String url, String idioma,String titulo) throws ParseException
    {
        Template template = new HTMLTemplate(templateName, new Locale(idioma), "sia");
        template.put("pagina", new Pagina(AppInfo.URL_BASE, AppInfo.URL_BASE + url, idioma, titulo));
        template.put("cursoActual", anyo);
        template.put("periodoAsignaturas", PeriodoMatricula.isPeriodoAsignaturas() && anyo == CursoAcademico.getCurrent());
        template.put("periodoCircuitos", PeriodoMatricula.isPeriodoCircuitos() && anyo == CursoAcademico.getCurrent());

        template.put("server", AppInfo.HOST);
        return template;
    }
}