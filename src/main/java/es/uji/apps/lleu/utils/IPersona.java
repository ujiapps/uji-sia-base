package es.uji.apps.lleu.utils;

/**
 * Representa una persona. Utilizaremos esta interfaz en algunos modelos. El más común es en los modelos que representan
 * profesorado: ASignaturaProfesor, ProfesorMaster, etc.
 */
public interface IPersona
{
    public Long getPerId();
}
