package es.uji.apps.lleu.utils;

import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CursoAcademico
{
    private static LocalDate inicioNuevoCurso;
    private static LocalDate finCursoAnterior;

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

    @Value("${uji.sia.inicioNuevoCurso}")
    public void setInicioNuevoCurso(String inicioNuevoCurso)
    {
        CursoAcademico.inicioNuevoCurso = LocalDate.parse(inicioNuevoCurso.concat("/").concat(Year.now().toString()), formatter);
    }

    @Value("${uji.sia.finCursoAnterior}")
    public void setFinCursoAnterior(String finCursoAnterior)
    {
        CursoAcademico.finCursoAnterior = LocalDate.parse(finCursoAnterior.concat("/").concat(Year.now().toString()), formatter);
    }

    public static LocalDate getInicioNuevoCurso()
    {
        return inicioNuevoCurso;
    }

    public static int getCurrent()
    {
        LocalDate now = LocalDate.now();
        if (now.isBefore(inicioNuevoCurso)) {
            return inicioNuevoCurso.getYear() - 1;
        } else {
            return inicioNuevoCurso.getYear();
        }
    }

    public static List<Integer> getCursosAcademicosActivos() {
        List<Integer> ret = new ArrayList<>(2);
        LocalDate now = LocalDate.now();

        ret.add(getCurrent());
        if ((now.isEqual(inicioNuevoCurso) || now.isAfter(inicioNuevoCurso)) && (now.isEqual(finCursoAnterior) || now.isBefore(finCursoAnterior))) {
            ret.add(getCurrent()-1);
        }
        return ret;
    }
}
