package es.uji.apps.lleu.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import es.uji.apps.lleu.model.enums.CaracterAsignatura;

/**
 * Contiene información acerca de los semestres, grupos y caracteres disponibles para un estudio en concreto.
 */
public class InfoFiltro
{
    private TreeMap<Long, Boolean> cursos;
    private HashMap<String, Boolean> semestres;
    private TreeMap<String, Boolean> grupos;
    private HashMap<String, Boolean> caracteres;
    private List<String> tiposSubgrupo;

    /**
     * Inicializa los parámetros internamente e indica que no están activos.
     *
     * @param semestres los semestres del estudio
     * @param grupos los grupos del estudio
     * @param cursos cuantos cursos tiene el estudio
     */
    public InfoFiltro(
            List<Long> cursos,
            List<String> grupos,
            List<String> semestres,
            List<CaracterAsignatura> caracteres
    )
    {
        this.semestres = new LinkedHashMap<>(semestres.size());
        this.cursos = new TreeMap<>();
        this.grupos = new TreeMap<>();
        this.caracteres = new HashMap<>(caracteres.size());

        for (String s : semestres) {
            this.semestres.put(s.toLowerCase(), false);
        }
        for (String g : grupos) {
            this.grupos.put(g, false);
        }
        for (Long c : cursos) {
            this.cursos.put(c, false);
        }
        for (CaracterAsignatura ca : caracteres) {
            this.caracteres.put(ca.toString(), false);
        }
        this.tiposSubgrupo = null;
    }

    public InfoFiltro(List<String> grupos, List<String> tiposSubgrupo)
    {
        this.grupos = new TreeMap<>();
        for (String g : grupos) {
            this.grupos.put(g, false);
        }
        this.tiposSubgrupo = tiposSubgrupo;
    }

    /**
     * Después de instanciar esta clase, es necesario llamar a calculateActiveValues() para indicar qué filtros estan activos
     * y qué filtros no están activos.
     *
     * @param fe
     */
    public void calculateActiveValues(FiltroEstudio fe) {
        if (fe.getSemestres()!=null) {
            for (String s : fe.getSemestres())
            {
                if (this.semestres.containsKey(s)) {
                    this.semestres.put(s, true);
                }
            }
        }
        if (fe.getGrupos()!=null) {
            for (String g : fe.getGrupos()) {
                if (this.grupos.containsKey(g))
                {
                    this.grupos.put(g, true);
                }
            }
        }
        if (fe.getCursos() != null) {
            for (Long c : fe.getCursos()) {
                if (this.cursos.containsKey(c))
                {
                    this.cursos.put(c, true);
                }
            }
        }
        if (fe.getCaracteres() != null) {
            for (CaracterAsignatura ca : fe.getCaracteres()) {
                if (this.caracteres.containsKey(ca.toString())) {
                    this.caracteres.put(ca.toString(), true);
                }
            }
        }
    }

    public HashMap<String, Boolean> getSemestres()
    {
        return this.semestres;
    }
    public TreeMap<String, Boolean> getGrupos() { return this.grupos; }
    public TreeMap<Long, Boolean> getCursos() { return this.cursos; }
    public HashMap<String, Boolean> getCaracteres() { return this.caracteres; }

    public List<String> getTiposSubgrupo()
    {
        return tiposSubgrupo;
    }
}
