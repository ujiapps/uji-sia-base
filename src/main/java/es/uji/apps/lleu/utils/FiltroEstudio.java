package es.uji.apps.lleu.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import es.uji.apps.lleu.model.enums.CaracterAsignatura;
import es.uji.apps.lleu.model.enums.Orden;
import es.uji.commons.rest.ParamUtils;

/**
 * Esta clase permite especificar el filtrado que queremos realizar sobre las asignaturas de un estudio en
 * concreto.
 */
public class FiltroEstudio
{

    /**
     * Como medida de seguridad, no admitimos más de 10 valores por criterio de filtrado
     */
    public static final int SPLIT_LIMIT = 15;

    /**
     * Indica el idioma en el que tenemos que buscar los nombres de asignatura :-(
     */
    protected String idioma;

    /**
     * Filtro por nombre de asignatura
     */
    protected String nombre;

    /**
     * Listado de nombres de grupo: A,B,C,...
     */
    protected List<String> grupos;

    /**
     * Listado de cursos: 1,2,3...
     */
    protected List<Long> cursos;
    /**
     * Semetres: 1,2
     */
    protected List<String> semestres;

    /**
     * El caracteres de asignaturas que queremos obtener
     */
    protected List<CaracterAsignatura> caracteres;

    /**
     * El orden en el que se deben devolver las asingaturas
     */
    protected Orden orden;

    /**
     * Devuelve una instancia de FiltroEstudio a partir de los query strings recibidos en una petición REST. El formato
     * que esperamos para cada parámetro:
     *
     *    idioma: (ca|en|es)
     *    filtroNombre: \p{Alpha}
     *    filtroCursos: [0-9](,[0-9])*
     *    filtroGrupos: [A-Za-z](,[A-Za-z])*
     *    filtroSemestres: [12](,[12])*
     *    filtroCaracter: (fbasica|optativas|practicas)
     *    orden: (optativas|practicas)
     *
     * No utilizar este método para instanciar filtros de la forma habitual
     *
     * @return FiltroEstudio una nueva instancia de FiltroEstudio con los filtros configurados
     */
    public static FiltroEstudio getInstanceFromQueryStringParams(
            String idioma,
            String filtroNombre,
            String filtroCursos,
            String filtroGrupos,
            String filtroSemestres,
            String filtroCaracter,
            String orden)
    {
        filtroNombre = (filtroNombre == null) ? "" : filtroNombre.trim();
        filtroCursos = (filtroCursos == null) ? "" : filtroCursos.trim();
        filtroGrupos = (filtroGrupos == null) ? "" : filtroGrupos.trim();
        filtroSemestres = (filtroSemestres == null) ? "" : filtroSemestres.trim();
        filtroCaracter = (filtroCaracter == null) ? "" : filtroCaracter.trim();

        Pattern regex = Pattern.compile(",");
        String []tokens;

        FiltroEstudio fe = new FiltroEstudio(idioma);

        // nombre
        if (filtroNombre.length() > 0) {
            fe.setNombre(filtroNombre);
        }
        // cursos
        if (filtroCursos.length() > 0) {
            tokens = regex.split(filtroCursos, SPLIT_LIMIT);
            for (String t : tokens) {
                try
                {
                    fe.addCurso(Long.parseLong(t.trim()));
                } catch (NumberFormatException e) {
                    // si algún token no es un número no filtramos por ese token
                }
            }
        }

        // grupos
        if (filtroGrupos.length() > 0) {
            tokens = regex.split(filtroGrupos, SPLIT_LIMIT);
            for (String t : tokens) {
                fe.addGrupo(t);
            }
        }

        // semestres
        if (filtroSemestres.length() > 0) {
            tokens = regex.split(filtroSemestres, SPLIT_LIMIT);
            for (String t : tokens) {
                fe.addSemestre(t);
            }
        }

        // carácter
        if (filtroCaracter.length() > 0) {
            tokens = regex.split(filtroCaracter, SPLIT_LIMIT);
            for (String t : tokens) {
                switch (t) {
                    case "fbasica":
                        fe.addCaracter(CaracterAsignatura.BASICA);
                        break;
                    case "optativas":
                        fe.addCaracter(CaracterAsignatura.OPTATIVAS);
                        break;
                    case "practicas":
                        fe.addCaracter(CaracterAsignatura.PRACTICAS);
                        break;
                }
            }
        }

        // orden
        if (ParamUtils.isNotNull(orden) && orden.length() > 0) {
            switch (orden) {
                case "fecha":
                    fe.addOrden(Orden.CURSO);
                    break;
                case "codigo":
                    fe.addOrden(Orden.CODIGO);
                    break;
            }
        }
        return fe;
    }

    /**
     * Inicializa un filtro sin condición. Instanciado directamente implica obtener todas las asignaturas.
     */
    public FiltroEstudio(String idioma)
    {
        idioma = idioma.trim().toLowerCase();
        switch (idioma)
        {
            case "es":
            case "en":
                this.idioma = idioma;
                break;
            default:
                this.idioma = "ca";
        }

        this.nombre = null;
        this.grupos = null;
        this.cursos = null;
        this.semestres = null;
        this.caracteres = null;
        this.orden = null;
    }

    /**
     * Establece el filtro por nombre de asignatura.
     *
     * @param nombre el nombre de la asignatura
     */
    public FiltroEstudio setNombre(String nombre)
    {
        if (nombre == null) {
            this.nombre = null;
        } else {
            this.nombre = nombre.trim();
        }
        return this;
    }

    /**
     * Añade un grupo al filtro
     *
     * @param grupo
     * @return
     */
    public FiltroEstudio addGrupo(String grupo) {
        if (this.grupos == null) {
            this.grupos = new ArrayList<>();
        }
        this.grupos.add(grupo.trim());
        return this;
    }

    /**
     * Añade un curso al filtro
     *
     * @param curso
     * @return
     */
    public FiltroEstudio addCurso(Long curso) {
        if (this.cursos == null) {
            this.cursos = new ArrayList<>();
        }
        this.cursos.add(curso);
        return this;
    }

    /**
     * Añade un semestre al filtro
     * @param semestre
     * @return
     */
    public FiltroEstudio addSemestre(String semestre) {
        if (this.semestres == null) {
            this.semestres = new ArrayList<>();
        }
        this.semestres.add(semestre.trim());
        return this;
    }

    /**
     * Añade un caracteres de formación al filtro
     *
     * @param tipo
     * @return
     */
    public FiltroEstudio addCaracter(CaracterAsignatura tipo) {
        if (this.caracteres == null) {
            this.caracteres = new ArrayList<>();
        }
        this.caracteres.add(tipo);
        return this;
    }

    /**
     * Añade orden al filtro
     * @param orden
     * @return
     */
    public void addOrden(Orden orden) {
        this.orden = orden;
    }

    /**
     * Devuelve el idioma en lowercase
     *
     * @return
     */
    public String getIdioma()
    {
        return this.idioma.toLowerCase();
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public List<String> getGrupos()
    {
        return this.grupos;
    }

    public List<Long> getCursos()
    {
        return this.cursos;
    }

    public List<String> getSemestres()
    {
        return this.semestres;
    }

    public List<CaracterAsignatura> getCaracteres()
    {
        return this.caracteres;
    }

    public Orden getOrden() { return this.orden; }
}
