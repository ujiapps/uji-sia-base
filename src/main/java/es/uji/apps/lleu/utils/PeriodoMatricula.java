package es.uji.apps.lleu.utils;

import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PeriodoMatricula
{

    private static String periodoVacantesAsignatura;
    private static String periodoVacantesCircuitos;

    @Value("${uji.sia.periodos.vacantes.asignaturas}")
    public void setPeriodoVacantesAsignatura(String periodoVacantesAsignatura)
    {
        this.periodoVacantesAsignatura = periodoVacantesAsignatura;
    }

    @Value("${uji.sia.periodos.vacantes.circuitos}")
    public void setPeriodoVacantesCircuitos(String periodoVacantesCircuitos)
    {
        this.periodoVacantesCircuitos = periodoVacantesCircuitos;
    }

    private static class Periodo{
        private LocalDate inicio;
        private LocalDate fin;
        private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");


        public Periodo(String inicio, String fin)
        {
            this.inicio = LocalDate.parse(inicio.concat("/").concat(Year.now().toString()), formatter);
            this.fin = LocalDate.parse(fin.concat("/").concat(Year.now().toString()), formatter);
        }

        public LocalDate getInicio()
        {
            return inicio;
        }

        public void setInicio(LocalDate inicio)
        {
            this.inicio = inicio;
        }

        public LocalDate getFin()
        {
            return fin;
        }

        public void setFin(LocalDate fin)
        {
            this.fin = fin;
        }

        private static List<Periodo> parseaPeriodos(String periodo){
            String[] pe = periodo.split(",");

            List<Periodo> periodos = Arrays.stream(pe).map(string -> string.split("\\|"))
                    .map(array  -> new Periodo(array[0], array[1])).collect(Collectors.toList());
            return periodos;
        }
    }

    public static Boolean isPeriodoAsignaturas()
    {
        LocalDate currentDate = LocalDate.now();
        List<Periodo> listPeriodos= parseaPeriodosAsignaturas();

        for(Periodo pe: listPeriodos){
            if((currentDate.isAfter(pe.getInicio()) && currentDate.isBefore(pe.getFin())) || currentDate.isEqual(pe.getInicio()) || currentDate.isEqual(pe.getFin())){
                return true;
            }
        }

        return false;
    }

    public static Boolean isPeriodoCircuitos()
    {
        LocalDate currentDate = LocalDate.now();
        List<Periodo> listPeriodos= parseaPeriodosCircuitos();

        for(Periodo pe: listPeriodos){
            if((currentDate.isAfter(pe.getInicio()) && currentDate.isBefore(pe.getFin())) || currentDate.isEqual(pe.getInicio()) || currentDate.isEqual(pe.getFin())){
                return true;
            }
        }

        return false;
    }

    private static List<Periodo> parseaPeriodosAsignaturas(){
        return Periodo.parseaPeriodos(PeriodoMatricula.periodoVacantesAsignatura);
    }

    private static List<Periodo> parseaPeriodosCircuitos(){
        return Periodo.parseaPeriodos(PeriodoMatricula.periodoVacantesCircuitos);
    }
}
