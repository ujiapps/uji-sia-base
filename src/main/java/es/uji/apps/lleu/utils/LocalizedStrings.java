package es.uji.apps.lleu.utils;

import es.uji.commons.rest.ParamUtils;

public class LocalizedStrings
{
    /**
     * Dado el carácter de una asignatura, devuelve la cadena thymeleaf de traducción correspondiente.
     *
     * @param caracter
     * @return
     */
    public static String getCaracterKey(String caracter)
    {

        String ret;

        switch (caracter.toLowerCase())
        {
            case "tr":
            case "ob":
            case "lc":
            case "pf":
            case "op":
            case "pr":
                ret = "caracter." + caracter.toLowerCase();
                break;
            default:
                ret = "caracter.desconocido";
        }

        return ret;
    }


    /**
     * Dado un tipo de subgrupo devuelve la clave de la cadena Thymeleaf que lo traduce.
     *
     * @param tipoSubgrupo
     * @return
     */
    public static String getSubgrupoKey(String tipoSubgrupo)
    {
        String ret;

        switch (tipoSubgrupo)
        {
            case "TE":
                ret = "teoria";
                break;
            case "TP":
                ret = "teoriayproblemas";
                break;
            case "PR":
                ret = "problemas";
                break;
            case "LA":
                ret = "laboratorio";
                break;
            case "TU":
                ret = "tutoria";
                break;
            case "SE":
                ret = "seminario";
                break;
            case "AV":
                ret = "evaluacion";
                break;
            case "ERA":
                ret = "era";
                break;
            default:
                ret = "desconocido";
        }

        return ret;
    }

    /**
     * Dado una especificación de semestre, devuelve la cadena de asigntura que se debe devolver. Semestre puede tomar
     * los siguientes valores: '1', '2', 'A' , '12'
     * <p>
     * La cadena devuelve "Semestre 1", "Semestre 2", "Anual", "Semestre 1 - Semestre 2"
     *
     * @param semestre
     * @return
     */
    public static String getSemestreKey(String semestre)
    {

        String ret;

        switch (semestre.toLowerCase())
        {
            case "1":
            case "2":
            case "12":
            case "a":
                ret = "semestre." + semestre.toLowerCase();
                break;
            default:
                ret = "desconocido";
        }

        return ret;
    }

    /**
     * Dado una especificación de semestre, devuelve la cadena de asigntura que se debe devolver. Semestre puede tomar
     * los siguientes valores: '1', '2', 'A' , '12'
     * <p>
     * La cadena devuelve "1", "2", "A"
     *
     * @param semestre
     * @return
     */
    public static String getSemestreOnlyDataKey(String semestre)
    {
        String ret;

        switch (semestre.toLowerCase())
        {
            case "1":
            case "2":
            case "a":
                ret = "semestre.data." + semestre.toLowerCase();
                break;
            case "1,2":
                ret = "semestre.data.a";
                break;
            default:
                ret = "desconocido";
        }

        return ret;
    }

    /**
     * Dado un tipo de examen, devuelve la cadena de traducción correspondiente.
     *
     * @param tipo
     * @return
     */
    public static String getTipoExamenKey(String tipo)
    {
        String ret = "examen.tipo.";

        switch (tipo.toLowerCase())
        {
            case "teo":
            case "pro":
            case "lab":
            case "pra":
            case "alt":
            case "ora":
                ret += tipo.toLowerCase();
                break;
            default:
                ret += "unknown";
        }

        return ret;
    }

    public static String getSoloConvalida(String soloConvalida)
    {
        String ret = "soloConvalida.";

        switch (soloConvalida.toLowerCase())
        {
            case "s":
                ret += "si";
                break;
            case "n":
                ret += "no";
                break;
            default:
                ret += "no";
        }
        return ret;
    }

    public static String getDoctor(String doctor)
    {
        String ret = "master.doctor.";
        if (!ParamUtils.isNotNull(doctor))
        {
            ret += "no";
        }
        else
        {

            switch (doctor.toLowerCase())
            {
                case "s":
                    ret += "si";
                    break;
                case "n":
                    ret += "no";
                    break;
                default:
                    ret += "no";
            }
        }
        return ret;
    }

    public static String getSuperarStr(Float obligatorios, Float optativos)
    {
        String ret;
        if (obligatorios == 1 && optativos == 1)
        {
            ret = "especialidades.superar.singular";
        }
        else if (obligatorios == 1 && (optativos > 1 || optativos < 1))
        {
            ret = "especialidades.superar.singular.plural";
        }
        else if ((obligatorios > 1 || obligatorios < 1) && optativos == 1)
        {
            ret = "especialidades.plural.singular";
        }
        else
        {
            ret = "especialidades.superar.plural";
        }
        return ret;
    }

    public static String getEstudioCompacto(String nombre, String idioma)
    {
        String ret;

        switch (idioma.toLowerCase())
        {
            case "en":
                ret = nombre.replace("Bachelor's Degree in","");
                break;
            case "es":
                ret = nombre.replace("Grado en","");
                break;
            default:
                ret = nombre.replace("Grau en","");
        }

        return ret;

    }
}
