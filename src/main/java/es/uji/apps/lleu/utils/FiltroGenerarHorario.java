package es.uji.apps.lleu.utils;

import java.io.Serializable;
import java.util.Map;


public class FiltroGenerarHorario implements Serializable
{

    private String asignatura;
    private String grupo;
    private Map<String, Long> subgrupos;

    public String getAsignatura()
    {
        return asignatura;
    }

    public void setAsignatura(String asignatura)
    {
        this.asignatura = asignatura;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public Map<String, Long> getSubgrupos()
    {
        return subgrupos;
    }

    public void setSubgrupos(Map<String, Long> subgrupos)
    {
        this.subgrupos = subgrupos;
    }
}
