package es.uji.apps.lleu.model;

import com.mysema.query.annotations.QueryProjection;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UJI_PLANES")
public class Planes implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "ESTUDIO_ID")
    private EstudioUji estudioId;

    @Id
    @Column(name = "TIT_ID")
    private Long titulacionId;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "CURSO_ACA_INI")
    private Integer cursoAcaIni;

    @Column(name = "CURSO_ACA_FIN_DOCENCIA")
    private Integer cursoAcaFinDocencia;

    @Column(name = "CURSO_ACA_FIN")
    private Integer cursoAcaFin;

    @Column(name = "CODIGO_RUCT")
    private String codigoRuct;

    @QueryProjection
    public Planes(EstudioUji estudioId, Long titulacionId, String nombre, Integer cursoAcaIni, Integer cursoAcaFinDocencia, Integer cursoAcaFin, String codigoRuct) {
        this.estudioId = estudioId;
        this.titulacionId = titulacionId;
        this.nombre = nombre;
        this.cursoAcaIni = cursoAcaIni;
        this.cursoAcaFinDocencia = cursoAcaFinDocencia;
        this.cursoAcaFin = cursoAcaFin;
        this.codigoRuct = codigoRuct;
    }

    public Planes() {
    }

    public EstudioUji getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(EstudioUji estudioId) {
        this.estudioId = estudioId;
    }

    public Long getTitulacionId() {
        return titulacionId;
    }

    public void setTitulacionId(Long titulacionId) {
        this.titulacionId = titulacionId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCursoAcaIni() {
        return cursoAcaIni;
    }

    public void setCursoAcaIni(Integer cursoAcaIni) {
        this.cursoAcaIni = cursoAcaIni;
    }

    public Integer getCursoAcaFinDocencia() {
        return cursoAcaFinDocencia;
    }

    public void setCursoAcaFinDocencia(Integer cursoAcaFinDocencia) {
        this.cursoAcaFinDocencia = cursoAcaFinDocencia;
    }

    public Integer getCursoAcaFin() {
        return cursoAcaFin;
    }

    public void setCursoAcaFin(Integer cursoAcaFin) {
        this.cursoAcaFin = cursoAcaFin;
    }

    public String getCodigoRuct() {
        return codigoRuct;
    }

    public void setCodigoRuct(String codigoRuct) {
        this.codigoRuct = codigoRuct;
    }
}
