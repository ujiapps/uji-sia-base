package es.uji.apps.lleu.model;


import com.mysema.query.annotations.QueryProjection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "UJI_LLEU", name = "POD_TITULACIONES")
public class PodTitulaciones {
    @Id
    @Column
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "NOMVAL")
    private String nombreVal;

    @Column(name = "NOMANG")
    private String nombreAng;

    @QueryProjection
    public PodTitulaciones(Long id, String nombre, String nombreVal, String nombreAng) {
        this.id = id;
        this.nombre = nombre;
        this.nombreVal = nombreVal;
        this.nombreAng = nombreAng;
    }

    public PodTitulaciones() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreVal() {
        return nombreVal;
    }

    public void setNombreVal(String nombreVal) {
        this.nombreVal = nombreVal;
    }

    public String getNombreAng() {
        return nombreAng;
    }

    public void setNombreAng(String nombreAng) {
        this.nombreAng = nombreAng;
    }
}
