package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ASIG_SOLOREC_G")
public class AsignaturaSoloRecGrado implements Serializable
{
    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name = "TITULACION_ID")
    private Long estudioId;

    @Id
    @Column(name = "CARACTER")
    private String caracter;

    @Column(name = "CUR_ID")
    private Long curId;

    @Column(name = "ASI_NOMBRE_CA")
    private String asiNombreCA;

    @Column(name = "ASI_NOMBRE_ES")
    private String asiNombreES;

    @Column(name = "ASI_NOMBRE_EN")
    private String asiNombreEN;

    @Column(name = "CREDITOS")
    private Float creditos;

    @Column(name = "EEP")
    private String eep;
    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "SEMESTRE")
    private String semestre;

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public Long getCurId()
    {
        return curId;
    }

    public void setCurId(Long curId)
    {
        this.curId = curId;
    }

    public String getAsiNombreCA()
    {
        return asiNombreCA;
    }

    public void setAsiNombreCA(String asiNombreCA)
    {
        this.asiNombreCA = asiNombreCA;
    }

    public String getAsiNombreES()
    {
        return asiNombreES;
    }

    public void setAsiNombreES(String asiNombreES)
    {
        this.asiNombreES = asiNombreES;
    }

    public String getAsiNombreEN()
    {
        return asiNombreEN;
    }

    public void setAsiNombreEN(String asiNombreEN)
    {
        this.asiNombreEN = asiNombreEN;
    }

    public Float getCreditos()
    {
        return creditos;
    }

    public void setCreditos(Float creditos)
    {
        this.creditos = creditos;
    }

    public String getEep()
    {
        return eep;
    }

    public void setEep(String eep)
    {
        this.eep = eep;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }


}