package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_CURSOS_MASTER")
public class CursosMaster implements Serializable
{
    @Id
    @Column(name = "ESTUDIO_ID")
    Long estudioId;

    @Id
    @Column(name = "CURSO_ACA")
    Integer cursoAca;

    @Column(name = "CURSO")
    Integer curso;

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public void setCurso(Integer curso)
    {
        this.curso = curso;
    }
}
