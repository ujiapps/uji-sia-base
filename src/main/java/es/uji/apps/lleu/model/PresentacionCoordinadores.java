package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_PRESENTACION_COORD")
public class PresentacionCoordinadores implements Serializable
{
    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "DEPARTAMENTO_CA")
    private String departamentoCA;

    @Column(name = "DEPARTAMENTO_ES")
    private String departamentoES;

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getDepartamentoCA()
    {
        return departamentoCA;
    }

    public void setDepartamentoCA(String departamentoCA)
    {
        this.departamentoCA = departamentoCA;
    }

    public String getDepartamentoES()
    {
        return departamentoES;
    }

    public void setDepartamentoES(String departamentoES)
    {
        this.departamentoES = departamentoES;
    }
}
