package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ESPECIALIDADES")
public class Especialidad implements Serializable
{
    @Id
    @Column(name = "ID")
    private Long id;

    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "CURSO_INI")
    private Integer cursoIni;

    @Column(name = "CURSO_FIN")
    private Integer cursoFin;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    @Column(name = "CREDITOS_OB")
    private Float creditosOB;

    @Column(name = "CREDITOS_OP")
    private Float creditosOP;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Integer getCursoIni()
    {
        return cursoIni;
    }

    public void setCursoIni(Integer cursoIni)
    {
        this.cursoIni = cursoIni;
    }

    public Integer getCursoFin()
    {
        return cursoFin;
    }

    public void setCursoFin(Integer cursoFin)
    {
        this.cursoFin = cursoFin;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public Float getCreditosOB()
    {
        return creditosOB;
    }

    public void setCreditosOB(Float creditosOB)
    {
        this.creditosOB = creditosOB;
    }

    public Float getCreditosOP()
    {
        return creditosOP;
    }

    public void setCreditosOP(Float creditosOP)
    {
        this.creditosOP = creditosOP;
    }
}
