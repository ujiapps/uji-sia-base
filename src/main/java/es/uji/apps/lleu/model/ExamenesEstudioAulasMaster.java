package es.uji.apps.lleu.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="LLEU_EXT_EXA_EST_AULAS_MASTER")
public class ExamenesEstudioAulasMaster implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ASI_ID")
    private String asiId;

    @Id
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Id
    @Column(name = "INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ini;

    @Id
    @Column(name="FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @Id
    @Column(name="AULA")
    private String aula;

    /**
     * El tipo de asignatura: TEO, PRO, LAB, ORA, PRA, ALT
     */
    @Id
    @Column(name="TIPO")
    private String tipo;

    /**
     * Parcial es 'S' o 'N'
     */
    @Id
    @Column(name="PARCIAL")
    private String parcial;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name="CURSO_ACA", referencedColumnName = "CURSO_ACA", insertable = false, updatable = false),
            @JoinColumn(name="ESTUDIO_ID", referencedColumnName = "ESTUDIO_ID", insertable = false, updatable = false),
            @JoinColumn(name="ASI_ID", referencedColumnName = "ASI_ID", insertable = false, updatable = false),
            @JoinColumn(name="FECHA", referencedColumnName = "FECHA", insertable = false, updatable = false),
            @JoinColumn(name="INI", referencedColumnName = "INI", insertable = false, updatable = false),
            @JoinColumn(name="FIN", referencedColumnName = "FIN", insertable = false, updatable = false),
            @JoinColumn(name="TIPO", referencedColumnName = "TIPO", insertable = false,updatable = false),
            @JoinColumn(name="PARCIAL", referencedColumnName = "PARCIAL", insertable = false, updatable = false),
    })
    private ExamenesEstudioMaster examenMaster;


    public Integer getCursoAca()
        {
            return cursoAca;
        }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public String getAsiId()
    {
        return asiId;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public Date getIni()
    {
        return ini;
    }

    public Date getFin()
    {
        return fin;
    }

    public String getAula()
    {
        return aula;
    }

    public String getTipo()
    {
        return tipo;
    }

    public String getParcial()
    {
        return parcial;
    }

    public ExamenesEstudioMaster getExamenMaster() {
        return examenMaster;
    }
}
