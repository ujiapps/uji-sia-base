package es.uji.apps.lleu.model;


import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ESTUDIOS_TODOS")
public class EstudioTodos implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="NOMBRE_CA")
    private String nombreCA;
    @Column(name="NOMBRE_ES")
    private String nombreES;
    @Column(name="NOMBRE_EN")
    private String nombreEN;

    private String tipo;


    private String activa;

    @OneToMany(mappedBy = "estudioCursado")
    private Set<Reconocimiento> estudiosCursado;

    @OneToMany(mappedBy = "estudioReconocido")
    private Set<Reconocimiento> estudiosReconocido;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getActiva()
    {
        return activa;
    }

    public void setActiva(String activa)
    {
        this.activa = activa;
    }

    public Set<Reconocimiento> getEstudiosCursado()
    {
        return estudiosCursado;
    }

    public void setEstudiosCursado(Set<Reconocimiento> estudiosCursado)
    {
        this.estudiosCursado = estudiosCursado;
    }

    public Set<Reconocimiento> getEstudiosReconocido()
    {
        return estudiosReconocido;
    }

    public void setEstudiosReconocido(Set<Reconocimiento> estudiosReconocido)
    {
        this.estudiosReconocido = estudiosReconocido;
    }
}