package es.uji.apps.lleu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "LLEU_EXT_RESUMEN_CREDITOS_POP")
public class ResumenCreditosPop implements Serializable
{
    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Column(name="CREDITOS_TOTAL")
    private Double creditosTotal;

    @Column(name="CREDITOS_OB")
    private Double creditosOb;

    @Column(name="CREDITOS_OP")
    private Double creditosOp;

    @Column(name="CREDITOS_TOTAL_ESPECIFICOS")
    private Double creditosTotalEspecificos;

    @Column(name="CREDITOS_ESPECIFICOS_FB")
    private Double creditosEspecificosFb;
    @Column(name="CREDITOS_ESPECIFICOS_OB")
    private Double creditosEspecificosOb;
    @Column(name="CREDITOS_ESPECIFICOS_OP")
    private Double creditosEspecificosOp;
    @Column(name="CREDITOS_ESPECIFICOS_PE")
    private Double creditosEspecificosPe;
    @Column(name="CREDITOS_ESPECIFICOS_PF")
    private Double creditosEspecificosPf;

    @Column(name="ES_CREDITOS_ESPECIFICOS")
    private String creditosEspecificos;

    @Column(name="ESPECIALIDAD_OBLIGATORIA")
    private String especialidadOblicatoria;



    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Double getCreditosTotal()
    {
        return creditosTotal;
    }

    public void setCreditosTotal(Double creditosTotal)
    {
        this.creditosTotal = creditosTotal;
    }

    public Double getCreditosOb()
    {
        return creditosOb;
    }

    public void setCreditosOb(Double creditosOb)
    {
        this.creditosOb = creditosOb;
    }

    public Double getCreditosOp()
    {
        return creditosOp;
    }

    public void setCreditosOp(Double creditosOp)
    {
        this.creditosOp = creditosOp;
    }

    public Double getCreditosTotalEspecificos()
    {
        return creditosTotalEspecificos;
    }

    public void setCreditosTotalEspecificos(Double creditosTotalEspecificos)
    {
        this.creditosTotalEspecificos = creditosTotalEspecificos;
    }

    public Double getCreditosEspecificosFb()
    {
        return creditosEspecificosFb;
    }

    public void setCreditosEspecificosFb(Double creditosEspecificosFb)
    {
        this.creditosEspecificosFb = creditosEspecificosFb;
    }

    public Double getCreditosEspecificosOb()
    {
        return creditosEspecificosOb;
    }

    public void setCreditosEspecificosOb(Double creditosEspecificosOb)
    {
        this.creditosEspecificosOb = creditosEspecificosOb;
    }

    public Double getCreditosEspecificosOp()
    {
        return creditosEspecificosOp;
    }

    public void setCreditosEspecificosOp(Double creditosEspecificosOp)
    {
        this.creditosEspecificosOp = creditosEspecificosOp;
    }

    public Double getCreditosEspecificosPe()
    {
        return creditosEspecificosPe;
    }

    public void setCreditosEspecificosPe(Double creditosEspecificosPe)
    {
        this.creditosEspecificosPe = creditosEspecificosPe;
    }

    public Double getCreditosEspecificosPf()
    {
        return creditosEspecificosPf;
    }

    public void setCreditosEspecificosPf(Double creditosEspecificosPf)
    {
        this.creditosEspecificosPf = creditosEspecificosPf;
    }

    public String getCreditosEspecificos()
    {
        return creditosEspecificos;
    }

    public void setCreditosEspecificos(String creditosEspecificos)
    {
        this.creditosEspecificos = creditosEspecificos;
    }

    public String getEspecialidadOblicatoria() {
        return especialidadOblicatoria;
    }

    public void setEspecialidadOblicatoria(String especialidadOblicatoria) {
        this.especialidadOblicatoria = especialidadOblicatoria;
    }
}
