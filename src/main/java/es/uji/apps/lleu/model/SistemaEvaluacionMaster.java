package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_SIS_EVALUA_M")
public class SistemaEvaluacionMaster implements Serializable
{
    @Id
    @Column(name = "EVAL_ID")
    private Long evaluacionId;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Id
    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Id
    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    @Column(name = "PONDERACION")
    private Float ponderacion;

    @Column(name = "ORDEN")
    private Integer orden;

    public Long getEvaluacionId()
    {
        return evaluacionId;
    }

    public void setEvaluacionId(Long evaluacionId)
    {
        this.evaluacionId = evaluacionId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public Float getPonderacion()
    {
        return ponderacion;
    }

    public void setPonderacion(Float ponderacion)
    {
        this.ponderacion = ponderacion;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }
}
