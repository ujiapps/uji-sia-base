package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ESTUDIOS_CREDITOS")
public class EstudioCredito implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="ESTUDIO_CA", updatable = false, insertable = false)
    private String estudioCA;
    @Column(name="ESTUDIO_ES", updatable = false, insertable = false)
    private String estudioES;
    @Column(name="ESTUDIO_EN", updatable = false, insertable = false)
    private String estudioEN;

    @Id
    private Integer curso;

    @Column(name="CRD_TR")
    private Float creditosTroncales;

    @Column(name="CRD_OB")
    private Float creditosObligatorios;

    @Column(name="CRD_OP")
    private Float creditosOptativos;

    @Column(name="CRD_LE")
    private Float creditosLectivos;

    @Column(name="CRD_PFC")
    private Float creditosPFC;

    @Column(name="PROJECTE")
    private Float proyecto;

    @Column(name="TOTAL")
    private Float total;

    private Long ciclo;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getEstudioCA()
    {
        return estudioCA;
    }

    public void setEstudioCA(String estudioCA)
    {
        this.estudioCA = estudioCA;
    }

    public String getEstudioES()
    {
        return estudioES;
    }

    public void setEstudioES(String estudioES)
    {
        this.estudioES = estudioES;
    }

    public String getEstudioEN()
    {
        return estudioEN;
    }

    public void setEstudioEN(String estudioEN)
    {
        this.estudioEN = estudioEN;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public void setCurso(Integer curso)
    {
        this.curso = curso;
    }

    public Float getCreditosTroncales()
    {
        return creditosTroncales;
    }

    public void setCreditosTroncales(Float creditosTroncales)
    {
        this.creditosTroncales = creditosTroncales;
    }

    public Float getCreditosObligatorios()
    {
        return creditosObligatorios;
    }

    public void setCreditosObligatorios(Float creditosObligatorios)
    {
        this.creditosObligatorios = creditosObligatorios;
    }

    public Float getCreditosOptativos()
    {
        return creditosOptativos;
    }

    public void setCreditosOptativos(Float creditosOptativos)
    {
        this.creditosOptativos = creditosOptativos;
    }

    public Float getCreditosLectivos()
    {
        return creditosLectivos;
    }

    public void setCreditosLectivos(Float creditosLectivos)
    {
        this.creditosLectivos = creditosLectivos;
    }

    public Float getCreditosPFC()
    {
        return creditosPFC;
    }

    public void setCreditosPFC(Float creditosPFC)
    {
        this.creditosPFC = creditosPFC;
    }

    public Float getProyecto()
    {
        return proyecto;
    }

    public void setProyecto(Float proyecto)
    {
        this.proyecto = proyecto;
    }

    public Float getTotal()
    {
        return total;
    }

    public void setTotal(Float total)
    {
        this.total = total;
    }

    public Long getCiclo()
    {
        return ciclo;
    }

    public void setCiclo(Long ciclo)
    {
        this.ciclo = ciclo;
    }
}