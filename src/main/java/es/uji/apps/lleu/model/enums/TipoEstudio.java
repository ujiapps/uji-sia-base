package es.uji.apps.lleu.model.enums;

public enum TipoEstudio
{
    GRADO("G"),
    PRIMERYSEGUNDOCICLO("12C"),
    MASTER("M"),
    DOCTORADO("D");

    String value;
    TipoEstudio(String value)
    {
        this.value = value;
    }
    public String getValue()
    {
        return value;
    }
}
