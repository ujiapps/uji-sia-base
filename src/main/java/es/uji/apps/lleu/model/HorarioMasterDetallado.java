package es.uji.apps.lleu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Horario detallada de un estudio de master
 *
 *
 */
@Entity
@Table(name = "LLEU_EXT_HORARIOS_MASTER")
public class HorarioMasterDetallado implements Serializable
{

    @Id
    @Column(name="ID")
    private Long id;

    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ASIG_ID")
    private String asignaturaId;

    @Column(name="GRUPO_ID")
    private String grupo;

    @Column(name="SEMESTRE")
    private String semestre;
    @Column(name="TIPO_SUBGRUPO_ID")
    private String tipoSubgrupo;

    @Column(name="SUBGRUPO_ID")
    private Long subgrupo;

    @Column(name="CURSO")
    private Long curso;

    @Column(name="DIA_SEMANA_ID")
    private Integer diaSemana;

    @Column(name="INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ini;

    @Column(name="FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @Column(name="COMENTARIO")
    private String comentario;

    @Column(name="TIPO")
    private String tipo;

    @Column(name="CARACTER")
    private String caracter;

    @Column(name="EDI_EDIFICIO")
    private String edificio;

    @Column(name = "AULA")
    private String aula;

    @Column(name="NOMBRE_ASIG_ES")
    private String nombreAsignaturaES;

    @Column(name="NOMBRE_ASIG_CA")
    private String nombreAsignaturaCA;

    @Column(name="NOMBRE_ASIG_EN")
    private String nombreAsignaturaEN;

    public HorarioMasterDetallado() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Long estudioId) {
        this.estudioId = estudioId;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getTipoSubgrupo() {
        return tipoSubgrupo;
    }

    public void setTipoSubgrupo(String tipoSubgrupo) {
        this.tipoSubgrupo = tipoSubgrupo;
    }

    public Long getSubgrupo() {
        return subgrupo;
    }

    public void setSubgrupo(Long subgrupo) {
        this.subgrupo = subgrupo;
    }

    public Long getCurso() {
        return curso;
    }

    public void setCurso(Long curso) {
        this.curso = curso;
    }

    public Integer getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(Integer diaSemana) {
        this.diaSemana = diaSemana;
    }

    public Date getIni() {
        return ini;
    }

    public void setIni(Date ini) {
        this.ini = ini;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCaracter() {
        return caracter;
    }

    public void setCaracter(String caracter) {
        this.caracter = caracter;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    public String getNombreAsignaturaES() {
        return nombreAsignaturaES;
    }

    public void setNombreAsignaturaES(String nombreAsignaturaES) {
        this.nombreAsignaturaES = nombreAsignaturaES;
    }

    public String getNombreAsignaturaCA() {
        return nombreAsignaturaCA;
    }

    public void setNombreAsignaturaCA(String nombreAsignaturaCA) {
        this.nombreAsignaturaCA = nombreAsignaturaCA;
    }

    public String getNombreAsignaturaEN() {
        return nombreAsignaturaEN;
    }

    public void setNombreAsignaturaEN(String nombreAsignaturaEN) {
        this.nombreAsignaturaEN = nombreAsignaturaEN;
    }
}

