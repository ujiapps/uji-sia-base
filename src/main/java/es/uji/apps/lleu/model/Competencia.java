package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="LLEU_EXT_COMPETENCIAS")
public class Competencia implements Serializable
{
    @Id
    @Column(name="COMP_ID")
    private Long competenciaId;

    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoACa;

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Column(name="NOMBRE_CA")
    private String nombreCA;

    @Column(name="NOMBRE_ES")
    private String nombreES;

    @Column(name="NOMBRE_EN")
    private String nombreEN;

    @Column(name="ORDEN")
    private Integer orden;

    public Long getCompetenciaId()
    {
        return competenciaId;
    }

    public void setCompetenciaId(Long competenciaId)
    {
        this.competenciaId = competenciaId;
    }

    public Integer getCursoACa()
    {
        return cursoACa;
    }

    public void setCursoACa(Integer cursoACa)
    {
        this.cursoACa = cursoACa;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }
}
