package es.uji.apps.lleu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ESTUDIO_REC_M")
public class EstudioRecMaster
{
    @Id
    @Column(name = "ESTUDIO_ID")
    Long estudioId;
    @Column(name = "EXPERIENCIA")
    String experiencia;
    @Column(name = "SUPERIOR")
    String superior;
    @Column(name = "PROPIO")
    String propio;

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getExperiencia()
    {
        return experiencia;
    }

    public void setExperiencia(String experiencia)
    {
        this.experiencia = experiencia;
    }

    public String getSuperior()
    {
        return superior;
    }

    public void setSuperior(String superior)
    {
        this.superior = superior;
    }

    public String getPropio()
    {
        return propio;
    }

    public void setPropio(String propio)
    {
        this.propio = propio;
    }
}
