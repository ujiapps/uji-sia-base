package es.uji.apps.lleu.model;

import com.mysema.query.annotations.QueryProjection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "UJI_ESTUDIOS")
public class EstudioUji implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOMBRE_CA")
    private String nombreCa;
    @Column(name = "NOMBRE_ES")
    private String nombreEs;
    @Column(name = "NOMBRE_UK")
    private String nombreUk;
    @Column(name = "CURSO_ACA_INI")
    private Integer cursoAcaIni;
    @Column(name = "CURSO_ACA_FIN_DOCENCIA")
    private Integer cursoAcaFinDocencia;
    @Column(name = "CURSO_ACA_FIN")
    private Integer cursoAcaFin;
    @Column(name = "TIPO_ESTUDIO_ID")
    private String tipoEstudioId;
    @Column(name = "OFICIAL")
    private String oficial;
    @Column(name = "CODIGO_RUCT")
    private String codigoRuct;
    @Column(name = "DECRETO")
    private String decreto;

    @QueryProjection
    public EstudioUji(Long id, String nombreCa, String nombreEs, String nombreUk, Integer cursoAcaIni, Integer cursoAcaFinDocencia, Integer cursoAcaFin, String tipoEstudioId, String oficial, String codigoRuct, String decreto) {
        this.id = id;
        this.nombreCa = nombreCa;
        this.nombreEs = nombreEs;
        this.nombreUk = nombreUk;
        this.cursoAcaIni = cursoAcaIni;
        this.cursoAcaFinDocencia = cursoAcaFinDocencia;
        this.cursoAcaFin = cursoAcaFin;
        this.tipoEstudioId = tipoEstudioId;
        this.oficial = oficial;
        this.codigoRuct = codigoRuct;
        this.decreto = decreto;
    }

    public EstudioUji() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreCa() {
        return nombreCa;
    }

    public void setNombreCa(String nombreCa) {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs() {
        return nombreEs;
    }

    public void setNombreEs(String nombreEs) {
        this.nombreEs = nombreEs;
    }

    public String getNombreUk() {
        return nombreUk;
    }

    public void setNombreUk(String nombreUk) {
        this.nombreUk = nombreUk;
    }

    public Integer getCursoAcaIni() {
        return cursoAcaIni;
    }

    public void setCursoAcaIni(Integer cursoAcaIni) {
        this.cursoAcaIni = cursoAcaIni;
    }

    public Integer getCursoAcaFinDocencia() {
        return cursoAcaFinDocencia;
    }

    public void setCursoAcaFinDocencia(Integer cursoAcaFinDocencia) {
        this.cursoAcaFinDocencia = cursoAcaFinDocencia;
    }

    public Integer getCursoAcaFin() {
        return cursoAcaFin;
    }

    public void setCursoAcaFin(Integer cursoAcaFin) {
        this.cursoAcaFin = cursoAcaFin;
    }

    public String getTipoEstudioId() {
        return tipoEstudioId;
    }

    public void setTipoEstudioId(String tipoEstudioId) {
        this.tipoEstudioId = tipoEstudioId;
    }

    public String getOficial() {
        return oficial;
    }

    public void setOficial(String oficial) {
        this.oficial = oficial;
    }

    public String getCodigoRuct() {
        return codigoRuct;
    }

    public void setCodigoRuct(String codigoRuct) {
        this.codigoRuct = codigoRuct;
    }

    public String getDecreto() {
        return decreto;
    }

    public void setDecreto(String decreto) {
        this.decreto = decreto;
    }
}

