package es.uji.apps.lleu.model.enums;

public enum Orden
{
    // ordenacion por fecha
    CURSO("curso"), CODIGO("codigo");

  String value;

    Orden(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

