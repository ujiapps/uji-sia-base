package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ASIGNATURAS_GRADO")
public class AsignaturasGrado implements Serializable
{
    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name = "TITULACION_ID")
    private Long estudioId;

    @Id
    @Column(name = "CARACTER")
    private String caracter;

    @Column(name = "CUR_ID")
    private Long curId;

    @Column(name = "ASI_NOMBRE_CA")
    private String asiNombreCA;
    @Column(name = "ASI_NOMBRE_BUSQUEDA_CA")
    private String asiNombreBusquedaCA;
    @Column(name = "ASI_NOMBRE_ES")
    private String asiNombreES;
    @Column(name = "ASI_NOMBRE_BUSQUEDA_ES")
    private String asiNombreBusquedaES;
    @Column(name = "ASI_NOMBRE_EN")
    private String asiNombreEN;
    @Column(name = "ASI_NOMBRE_BUSQUEDA_EN")
    private String asiNombreBusquedaEN;

    @Column(name = "CREDITOS")
    private Float creditos;

    @Column(name = "EEP")
    private String eep;
    @Column(name = "TIPO")
    private String tipo;
    @Column(name = "SEMESTRE")
    private String semestre;
    @Column(name = "IDIOMA_CA")
    private String idiomaCA;
    @Column(name = "IDIOMA_ES")
    private String idiomaES;
    @Column(name = "IDIOMA_EN")
    private String idiomaEN;
    @Column(name = "HARMONIZADA")
    private String harmonizada;
    @Column(name = "CURSO_ADAPTACION")
    private String cursoAdaptacion;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name="ASI_ID", referencedColumnName = "ASI_ID", insertable = false, updatable = false),
            @JoinColumn(name="CURSO_ACA", referencedColumnName = "CURSO_ACA", insertable = false, updatable = false)
    })
    private AsignaturasInformacion asignaturasInformacion;

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public Long getCurId()
    {
        return curId;
    }

    public void setCurId(Long curId)
    {
        this.curId = curId;
    }

    public String getAsiNombreCA()
    {
        return asiNombreCA;
    }

    public void setAsiNombreCA(String asiNombreCA)
    {
        this.asiNombreCA = asiNombreCA;
    }

    public String getAsiNombreBusquedaCA()
    {
        return asiNombreBusquedaCA;
    }

    public void setAsiNombreBusquedaCA(String asiNombreBusquedaCA)
    {
        this.asiNombreBusquedaCA = asiNombreBusquedaCA;
    }

    public String getAsiNombreES()
    {
        return asiNombreES;
    }

    public void setAsiNombreES(String asiNombreES)
    {
        this.asiNombreES = asiNombreES;
    }

    public String getAsiNombreBusquedaES()
    {
        return asiNombreBusquedaES;
    }

    public void setAsiNombreBusquedaES(String asiNombreBusquedaES)
    {
        this.asiNombreBusquedaES = asiNombreBusquedaES;
    }

    public String getAsiNombreEN()
    {
        return asiNombreEN;
    }

    public void setAsiNombreEN(String asiNombreEN)
    {
        this.asiNombreEN = asiNombreEN;
    }

    public String getAsiNombreBusquedaEN()
    {
        return asiNombreBusquedaEN;
    }

    public void setAsiNombreBusquedaEN(String asiNombreBusquedaEN)
    {
        this.asiNombreBusquedaEN = asiNombreBusquedaEN;
    }

    public Float getCreditos()
    {
        return creditos;
    }

    public void setCreditos(Float creditos)
    {
        this.creditos = creditos;
    }

    public String getEep()
    {
        return eep;
    }

    public void setEep(String eep)
    {
        this.eep = eep;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public String getIdiomaCA()
    {
        return idiomaCA;
    }

    public void setIdiomaCA(String idiomaCA)
    {
        this.idiomaCA = idiomaCA;
    }

    public String getIdiomaES()
    {
        return idiomaES;
    }

    public void setIdiomaES(String idiomaES)
    {
        this.idiomaES = idiomaES;
    }

    public String getIdiomaEN()
    {
        return idiomaEN;
    }

    public void setIdiomaEN(String idiomaEN)
    {
        this.idiomaEN = idiomaEN;
    }

    public String getHarmonizada()
    {
        return harmonizada;
    }

    public void setHarmonizada(String harmonizada)
    {
        this.harmonizada = harmonizada;
    }

    public String getCursoAdaptacion()
    {
        return cursoAdaptacion;
    }

    public void setCursoAdaptacion(String cursoAdaptacion)
    {
        this.cursoAdaptacion = cursoAdaptacion;
    }

    public AsignaturasInformacion getAsignaturasInformacion()
    {
        return asignaturasInformacion;
    }

    public void setAsignaturasInformacion(AsignaturasInformacion asignaturasInformacion)
    {
        this.asignaturasInformacion = asignaturasInformacion;
    }
}