package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_ESTUDIOS_NIVEL")
public class EstudioNivel implements Serializable
{
    @Id
    @Column(name = "TIT_ID")
    private Long estudioId;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Column(name = "NIVEL")
    private Integer nivel;


    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Integer getNivel()
    {
        return nivel;
    }

    public void setNivel(Integer nivel)
    {
        this.nivel = nivel;
    }

}
