package es.uji.apps.lleu.model.enums;

import java.util.HashMap;
import java.util.Map;

public enum ModificacionCOVID2020_1
{
    ESTUDIO_209(209,"https://www.uji.es/upo/rest/contenido/596064496/raw?idioma=ca"),
    ESTUDIO_231(231,"https://www.uji.es/upo/rest/contenido/596064514/raw?idioma=ca"),
    ESTUDIO_239(239,"https://www.uji.es/upo/rest/contenido/596064525/raw?idioma=ca"),
    ESTUDIO_227(227,"https://www.uji.es/upo/rest/contenido/596064525/raw?idioma=ca"),
    ESTUDIO_228(228,"https://www.uji.es/upo/rest/contenido/596064543/raw?idioma=ca"),
    ESTUDIO_224(224,"https://www.uji.es/upo/rest/contenido/596064534/raw?idioma=ca"),
    ESTUDIO_221(221,"https://www.uji.es/upo/rest/contenido/596064622/raw?idioma=ca"),
    ESTUDIO_225(225,"https://www.uji.es/upo/rest/contenido/596064557/raw?idioma=ca"),
    ESTUDIO_222(222,"https://www.uji.es/upo/rest/contenido/596064574/raw?idioma=ca"),
    ESTUDIO_220(220,"https://www.uji.es/upo/rest/contenido/596064608/raw?idioma=ca"),
    ESTUDIO_223(223,"https://www.uji.es/upo/rest/contenido/596064633/raw?idioma=ca"),
    ESTUDIO_208(208,"https://www.uji.es/upo/rest/contenido/596064647/raw?idioma=ca"),
    ESTUDIO_203(203,"https://www.uji.es/upo/rest/contenido/596063789/raw?idioma=ca"),
    ESTUDIO_205(205,"https://www.uji.es/upo/rest/contenido/596063809/raw?idioma=ca"),
    ESTUDIO_238(238,"https://www.uji.es/upo/rest/contenido/596063798/raw?idioma=ca"),
    ESTUDIO_217(217,"https://www.uji.es/upo/rest/contenido/596063798/raw?idioma=ca"),
    ESTUDIO_237(237,"https://www.uji.es/upo/rest/contenido/596063804/raw?idioma=ca"),
    ESTUDIO_218(218,"https://www.uji.es/upo/rest/contenido/596063804/raw?idioma=ca"),
    ESTUDIO_204(204,"https://www.uji.es/upo/rest/contenido/596063816/raw?idioma=ca"),
    ESTUDIO_206(206,"https://www.uji.es/upo/rest/contenido/596063821/raw?idioma=ca"),
    ESTUDIO_207(207,"https://www.uji.es/upo/rest/contenido/598445626/raw?idioma=ca"),
    ESTUDIO_236(236,"https://www.uji.es/upo/rest/contenido/596063475/raw?idioma=ca"),
    ESTUDIO_210(210,"https://www.uji.es/upo/rest/contenido/624471117/raw?idioma=ca"),
    ESTUDIO_214(214,"https://www.uji.es/upo/rest/contenido/624471122/raw?idioma=ca"),
    ESTUDIO_213(213,"https://www.uji.es/upo/rest/contenido/596063486/raw?idioma=ca"),
    ESTUDIO_211(211,"https://www.uji.es/upo/rest/contenido/624471875/raw?idioma=ca"),
    ESTUDIO_212(212,"https://www.uji.es/upo/rest/contenido/596063500/raw?idioma=ca"),
    ESTUDIO_232(232,"https://www.uji.es/upo/rest/contenido/596063505/raw?idioma=ca"),
    ESTUDIO_201(201,"https://www.uji.es/upo/rest/contenido/596063511/raw?idioma=ca"),
    ESTUDIO_202(202,"https://www.uji.es/upo/rest/contenido/596063516/raw?idioma=ca"),
    ESTUDIO_230(230,"https://www.uji.es/upo/rest/contenido/596062347/raw?idioma=ca"),
    ESTUDIO_233(233,"https://www.uji.es/upo/rest/contenido/625506166/raw?idioma=ca"),
    ESTUDIO_235(235,"https://www.uji.es/upo/rest/contenido/596062342/raw?idioma=ca"),
    ESTUDIO_229(229,"https://www.uji.es/upo/rest/contenido/596062342/raw?idioma=ca"),
    ESTUDIO_219(219,"https://www.uji.es/upo/rest/contenido/596062331/raw?idioma=ca"),
    ESTUDIO_241(241,"https://www.uji.es/upo/rest/contenido/596064496/raw?idioma=ca"),
    ESTUDIO_242(242,"https://www.uji.es/upo/rest/contenido/596064534/raw?idioma=ca"),
    ESTUDIO_243(243,"https://www.uji.es/upo/rest/contenido/624471122/raw?idioma=ca"),
    ESTUDIO_42157(42157,"https://www.uji.es/upo/rest/contenido/596134870/raw?idioma=ca"),
    ESTUDIO_42142(42142,"https://www.uji.es/upo/rest/contenido/596133129/raw?idioma=ca"),
    ESTUDIO_42135(42135,"https://www.uji.es/upo/rest/contenido/596133136/raw?idioma=ca"),
    ESTUDIO_42150(42150,"https://www.uji.es/upo/rest/contenido/596133156/raw?idioma=ca"),
    ESTUDIO_42152(42152,"https://www.uji.es/upo/rest/contenido/596136312/raw?idioma=ca"),
    ESTUDIO_42185(42185,"https://www.uji.es/upo/rest/contenido/596136317/raw?idioma=ca"),
    ESTUDIO_42172(42172,"https://www.uji.es/upo/rest/contenido/596138497/raw?idioma=ca"),
    ESTUDIO_42175(42175,"https://www.uji.es/upo/rest/contenido/625511157/raw?idioma=ca"),
    ESTUDIO_42151(42151,"https://www.uji.es/upo/rest/contenido/596138512/raw?idioma=ca"),
    ESTUDIO_42200(42200,"https://www.uji.es/upo/rest/contenido/642542778/raw?idioma=ca");

    private Integer value;
    private String descripcion;

    ModificacionCOVID2020_1(Integer value, String descripcion)
    {
        this.value = value;
        this.descripcion = descripcion;
    }

    public Integer getValue()
    {
        return value;
    }

    public void setValue(Integer value)
    {
        this.value = value;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    private static final Map<Integer, ModificacionCOVID2020_1> MOD_BY_VALUE = new HashMap<>();

    static
    {
        for (ModificacionCOVID2020_1 e : values())
        {
            MOD_BY_VALUE.put(e.value, e);
        }
    }

    public static ModificacionCOVID2020_1 valueOfNumber(Integer value) {
        return MOD_BY_VALUE.get(value);
    }

};
