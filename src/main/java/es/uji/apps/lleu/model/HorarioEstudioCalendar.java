package es.uji.apps.lleu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Horario detallada de un estudio de grado para formato Calendar.
 *
 * @see HorarioEstudioGeneral
 */
@Entity
@Table(name = "LLEU_EXT_HORARIOS_CALENDAR")
public class HorarioEstudioCalendar implements Serializable
{
    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="GRP_ID")
    private String grupo;

    @Id
    @Column(name="SGR_TIPO")
    private String tipoSubgrupo;

    @Id
    @Column(name="SGR_ID")
    private Long subgrupo;

    @Id
    @Column(name="INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ini;

    @Id
    @Column(name="FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @Column(name="AULA")
    private String aula;

    @Column(name="NOMBRE_ASIGNATURA_ES")
    private String nombreAsignaturaES;

    @Column(name="NOMBRE_ASIGNATURA_CA")
    private String nombreAsignaturaCA;

    @Column(name="NOMBRE_ASIGNATURA_EN")
    private String nombreAsignaturaEN;

    @Column(name="COMENTARIO_DISCONT_ES")
    private String comentarioDiscontES;

    @Column(name="COMENTARIO_DISCONT_CA")
    private String comentarioDiscontCA;

    @Column(name="COMENTARIO_DISCONT_EN")
    private String comentarioDiscontEN;

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public String getTipoSubgrupo()
    {
        return tipoSubgrupo;
    }

    public void setTipoSubgrupo(String tipoSubgrupo)
    {
        this.tipoSubgrupo = tipoSubgrupo;
    }

    public Long getSubgrupo()
    {
        return subgrupo;
    }

    public void setSubgrupo(Long subgrupo)
    {
        this.subgrupo = subgrupo;
    }

    public Date getIni()
    {
        return ini;
    }

    public void setIni(Date ini)
    {
        this.ini = ini;
    }

    public Date getFin()
    {
        return fin;
    }

    public void setFin(Date fin)
    {
        this.fin = fin;
    }

    public String getAula()
    {
        return aula;
    }

    public void setAula(String aula)
    {
        this.aula = aula;
    }

    public String getNombreAsignaturaES()
    {
        return nombreAsignaturaES;
    }

    public void setNombreAsignaturaES(String nombreAsignaturaES)
    {
        this.nombreAsignaturaES = nombreAsignaturaES;
    }

    public String getNombreAsignaturaCA()
    {
        return nombreAsignaturaCA;
    }

    public void setNombreAsignaturaCA(String nombreAsignaturaCA)
    {
        this.nombreAsignaturaCA = nombreAsignaturaCA;
    }

    public String getNombreAsignaturaEN()
    {
        return nombreAsignaturaEN;
    }

    public void setNombreAsignaturaEN(String nombreAsignaturaEN)
    {
        this.nombreAsignaturaEN = nombreAsignaturaEN;
    }

    public String getComentarioDiscontES()
    {
        return comentarioDiscontES;
    }

    public void setComentarioDiscontES(String comentarioDiscontES)
    {
        this.comentarioDiscontES = comentarioDiscontES;
    }

    public String getComentarioDiscontCA()
    {
        return comentarioDiscontCA;
    }

    public void setComentarioDiscontCA(String comentarioDiscontCA)
    {
        this.comentarioDiscontCA = comentarioDiscontCA;
    }

    public String getComentarioDiscontEN()
    {
        return comentarioDiscontEN;
    }

    public void setComentarioDiscontEN(String comentarioDiscontEN)
    {
        this.comentarioDiscontEN = comentarioDiscontEN;
    }
}
