package es.uji.apps.lleu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "LLEU_EXT_CIRCUITOS")
public class Circuito implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    Integer cursoAca;
    @Id
    @Column(name="ESTUDIO_ID")
    Long estudioId;
    @Id
    @Column(name="CIRCUITO_ID")
    Long circuitoId;

    @Column(name="NOMBRE")
    String nombre;

    @Column(name="GRP_ID")
    String grupoId;

    @Column(name="INGLES")
    String ingles;

    @Column(name = "TIENE_PLAZAS")
    private String tienePlazas;

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Long getCircuitoId()
    {
        return circuitoId;
    }

    public void setCircuitoId(Long circuitoId)
    {
        this.circuitoId = circuitoId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public String getIngles()
    {
        return ingles;
    }

    public void setIngles(String ingles)
    {
        this.ingles = ingles;
    }

    public String getTienePlazas() {
        return tienePlazas;
    }

    public void setTienePlazas(String tienePlazas) {
        this.tienePlazas = tienePlazas;
    }
}
