package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mysema.query.annotations.QueryProjection;

@Entity
@Table(name = "LLEU_EXT_ASIG_INCOMPATIBLES")
public class AsignaturaIncompatible implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Column(name="NOMBRE_ES")
    private String nombreES;

    @Column(name="NOMBRE_CA")
    private String nombreCA;

    @Column(name="NOMBRE_EN")
    private String nombreEN;

    public AsignaturaIncompatible() {
        super();
    }

    @QueryProjection
    public AsignaturaIncompatible(
            Integer cursoAca,
            Long estudioId,
            String asignaturaId,
            String nombreES,
            String nombreCA,
            String nombreEN
    ) {
        this.cursoAca = cursoAca;
        this.estudioId = estudioId;
        this.asignaturaId = asignaturaId;
        this.nombreCA = nombreCA;
        this.nombreES = nombreES;
        this.nombreEN = nombreEN;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }
}
