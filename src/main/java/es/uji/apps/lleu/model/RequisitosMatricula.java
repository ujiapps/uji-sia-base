package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_REQUISITOS_MATRICULA")
public class RequisitosMatricula implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name="ASI_ID_1")
    private String asignaturaId1;

    @Column(name="CUR_ID")
    private Integer curso;

    @Column(name="TIPO")
    private String tipo;

    @Column(name="SEMESTRE")
    private String semestre;

    @Column(name="CARACTER")
    private String caracter;

    @Column(name="NOMBRE_ES")
    private String nombreES;

    @Column(name="NOMBRE_CA")
    private String nombreCA;

    @Column(name="NOMBRE_EN")
    private String nombreEN;

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public String getAsignaturaId1()
    {
        return asignaturaId1;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public String getTipo()
    {
        return tipo;
    }
}
