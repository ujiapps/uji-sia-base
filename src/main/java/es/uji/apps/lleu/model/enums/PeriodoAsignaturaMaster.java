package es.uji.apps.lleu.model.enums;

/**
 * El periodo de una asignatura de máster está en la tabla pop_oferta
 */
public enum PeriodoAsignaturaMaster
{
    SEMESTRAL("S"),
    ANUAL("A"),
    TRIMESTRAL("T"),
    CUATRIMESTRAL("Q");

    private String value;

    PeriodoAsignaturaMaster(String value) {
        this.value = value;
    }
}
