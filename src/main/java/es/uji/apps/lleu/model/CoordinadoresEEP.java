package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_COORNADORES_EEP")
public class CoordinadoresEEP implements Serializable
{
    @Id
    @Column(name = "PER_ID")
    private Long perId;

    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "URL")
    private String url;

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
}