package es.uji.apps.lleu.model.enums;

/**
 * Indica el tipo de formación
 */
public enum TipoAsignatura
{
    TE("TE"),PR("PR"),TP("TP"),LA("LA"),TU("TU"),SE("SE");

    String value;

    TipoAsignatura(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    
}
