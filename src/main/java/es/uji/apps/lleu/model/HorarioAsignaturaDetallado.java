package es.uji.apps.lleu.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Horario detallado de asignatura.
 */
@Entity
@Table(name = "LLEU_EXT_HORARIOS_ASIGNATURA")
public class HorarioAsignaturaDetallado implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoACA;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ASIGNATURA_ID")
    private String asignaturaId;

    @Column(name="GRUPO")
    private String grupo;


    @Column(name="SEMESTRE")
    private String semestre;

    @Id
    @Column(name="SGR_TIPO")
    private String subGrupoTipo;

    @Id
    @Column(name="SGR_ID")
    private Integer subGrupoId;

    @Id
    @Column(name="AULA")
    private String aula;

    @Column(name="DIA")
    private Integer dia;

    @Id
    @Column(name="INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ini;

    @Id
    @Column(name="FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @Id
    @Column(name="FHOR_INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fHorIni;

    @Id
    @Column(name="FHOR_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fHorFin;

    @Column(name="COMENTARIO_ES")
    private String comentarioES;

    @Column(name="COMENTARIO_CA")
    private String comentarioCA;

    @Column(name="COMENTARIO_EN")
    private String comentarioEN;

    @Column(name="EDIFICIO")
    private String edificio;

    @Column(name="COMENTARIO")
    private String comentario;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    public Integer getSubGrupoId()
    {
        return subGrupoId;
    }

    public void setSubGrupoId(Integer subGrupoId)
    {
        this.subGrupoId = subGrupoId;
    }

    public String getSubGrupoTipo()
    {
        return subGrupoTipo;
    }

    public void setSubGrupoTipo(String subGrupoTipo)
    {
        this.subGrupoTipo = subGrupoTipo;
    }

    public String getAula()
    {
        return aula;
    }

    public void setAula(String aula)
    {
        this.aula = aula;
    }

    public Integer getDia()
    {
        return dia;
    }

    public void setDia(Integer dia)
    {
        this.dia = dia;
    }

    public Date getIni()
    {
        return ini;
    }

    public void setIni(Date ini)
    {
        this.ini = ini;
    }

    public Date getFin()
    {
        return fin;
    }

    public void setFin(Date fin)
    {
        this.fin = fin;
    }

    public String getComentarioES()
    {
        return comentarioES;
    }

    public void setComentarioES(String comentarioES)
    {
        this.comentarioES = comentarioES;
    }

    public String getComentarioCA()
    {
        return comentarioCA;
    }

    public void setComentarioCA(String comentarioCA)
    {
        this.comentarioCA = comentarioCA;
    }

    public String getComentarioEN()
    {
        return comentarioEN;
    }

    public void setComentarioEN(String comentarioEN)
    {
        this.comentarioEN = comentarioEN;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoACA()
    {
        return cursoACA;
    }

    public void setCursoACA(Integer cursoACA)
    {
        this.cursoACA = cursoACA;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Date getfHorIni()
    {
        return fHorIni;
    }

    public void setfHorIni(Date fHorIni)
    {
        this.fHorIni = fHorIni;
    }

    public Date getfHorFin()
    {
        return fHorFin;
    }

    public void setfHorFin(Date fHorFin)
    {
        this.fHorFin = fHorFin;
    }

    public String getEdificio()
    {
        return edificio;
    }

    public void setEdificio(String edificio)
    {
        this.edificio = edificio;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public String getNombreCA() {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA) {
        this.nombreCA = nombreCA;
    }

    public String getNombreEN() {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN) {
        this.nombreEN = nombreEN;
    }
}
