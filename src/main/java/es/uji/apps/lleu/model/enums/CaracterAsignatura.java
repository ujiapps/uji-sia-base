package es.uji.apps.lleu.model.enums;

import java.util.ArrayList;
import java.util.List;

public enum CaracterAsignatura
{
    // troncales y obligatorias
    BASICA,

    // optativas y libre configuración
    OPTATIVAS,

    // proyecto y estancia en prácticas
    PRACTICAS;

    /**
     * Dado un listado de valores de la columna caracter, devuelve las instancias de CaracterAsignatura correspondientes.
     * Si hay un String que no corresponde a uno de los valores del enum, lo obviamos.
     *
     * @param caracterSQLValues
     * @return null si caracterSQLValues es null.
     */
    public static List<CaracterAsignatura> getInstancesFromSQLValues(List<String> caracterSQLValues) {

        if (caracterSQLValues == null) {
            return null;
        }

        List<CaracterAsignatura> ret = new ArrayList<>(3);
        for ( String ca : caracterSQLValues ) {
            switch (ca) {
                case "TR":
                case "OB":
                    if (!ret.contains(BASICA)) {
                        ret.add(BASICA);
                    }
                    break;
                case "OP":
                case "LC":
                    if (!ret.contains(OPTATIVAS)) {
                        ret.add(OPTATIVAS);
                    }
                    break;
                case "PF":
                case "PR":
                    if (!ret.contains(PRACTICAS)) {
                        ret.add(PRACTICAS);
                    }
                    break;
            }
        }

        return ret;
    }

    /**
     * Devuelve el valor correspondiente a la columna CARACTER de la tabla LLEU_EXT_ASIGNATURAS_GRADO para cada
     * carácter de asignatura que consideramos.
     *
     * @return
     */
    public String[] getCaracterColumnValue () {
        String []ret = null;
        if (this == BASICA){
            ret = new String[]{"TR", "OB"};
        } else if (this == OPTATIVAS) {
            ret = new String[]{"OP", "LC"};
        } else if (this == PRACTICAS) {
            // las prácticas en empresa están marcadas por la columna eep = true
            ret = new String[]{"PF", "PR"};
        }

        return ret;
    }
}
