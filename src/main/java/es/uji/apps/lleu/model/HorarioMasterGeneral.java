package es.uji.apps.lleu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Horario de estudio  master general.
 *
 * @see HorarioMasterGeneral
 */
@Entity
@Table(name="LLEU_EXT_HORARIOS_MASTER_GEN")
public class HorarioMasterGeneral implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name="CARACTER")
    private String caracter;

    @Id
    @Column(name="CUR_ID")
    private Long curso;

    @Id
    @Column(name="GRP_ID")
    private String grupo;

    @Id
    @Column(name="SGR_TIPO")
    private String tipoSubgrupo;

    @Id
    @Column(name="SGR_ID")
    private Long subgrupo;

    @Id
    @Column(name="SEMESTRE")
    private String semestre;

    @Id
    @Column(name="DIA_SEM")
    private Integer diaSemana;

    @Id
    @Column(name="INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ini;

    @Id
    @Column(name="FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @Column(name="TIPO")
    private String tipo;

    @Column(name="COMENTARIO")
    private String comentario;

    @Column(name="NOMBRE_ES")
    private String nombreES;

    @Column(name="NOMBRE_CA")
    private String nombreCA;

    @Column(name="NOMBRE_EN")
    private String nombreEN;

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public String getTipoSubgrupo()
    {
        return tipoSubgrupo;
    }

    public void setTipoSubgrupo(String tipoSubgrupo)
    {
        this.tipoSubgrupo = tipoSubgrupo;
    }

    public Long getSubgrupo()
    {
        return subgrupo;
    }

    public void setSubgrupo(Long subgrupo)
    {
        this.subgrupo = subgrupo;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public Integer getDiaSemana()
    {
        return diaSemana;
    }

    public void setDiaSemana(Integer diaSemana)
    {
        this.diaSemana = diaSemana;
    }

    public Date getIni()
    {
        return ini;
    }

    public void setIni(Date ini)
    {
        this.ini = ini;
    }

    public Date getFin()
    {
        return fin;
    }

    public void setFin(Date fin)
    {
        this.fin = fin;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }
}
