package es.uji.apps.lleu.model;


import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "LLEU_EXT_ASIGNATURAS_INFO")
public class AsignaturasInformacion implements Serializable
{
    private String id;

    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    private Float creditos;

    @Column(name = "ASI_DURACION")
    private String asignaturaDuracion;

    @Column(name = "CRD_SUPERADOS")
    private Float creditosSuperados;
    @Column(name = "CRD_TE")
    private Float creditosTeoria;
    @Column(name = "CRD_PR")
    private Float creditosPractica;
    @Column(name = "CRD_LA")
    private Float creditosLaboratorio;
    @Column(name = "CRD_SE")
    private Float creditosSeminario;
    @Column(name = "CRD_TU")
    private Float creditosTutoria;
    @Column(name = "CRD_EV")
    private Float creditosEvaluacion;
    @Column(name = "ECTS")
    private Float ects;
    @Column(name = "HORAS")
    private Float horas;
    @Column(name = "HORAS_AL1")
    private Float horasAl1;
    @Column(name = "HORAS_NO_PRESEN")
    private Float horasNoPresenciales;
    @Column(name = "INCOMPA")
    private Float incompatibilidades;

    @Column(name = "BLOQUEADO")
    private String bloqueado;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_ULT")
    private Date fechaUltima;
    @Column(name = "ASI_CONOCIMIENTOS")
    private String asignaturaConocimientos;
    @Column(name = "ASI_OBJETIVOS")
    private String asignaturaObjeticos;
    @Column(name = "ASI_CONTENIDO")
    private String asignaturaObjetivo;
    @Column(name = "ASI_METODOLOGIA")
    private String asignaturaMetodolocia;
    @Column(name = "ASI_EVALUACION")
    private String asignaturaEvaluacion;
    @Column(name = "CORREGIDA")
    private String corregida;
    @Column(name = "HORAS_AL")
    private Float horasAl;

    @OneToMany(mappedBy = "asignaturasInformacion")
    private Set<AsignaturasGrado> asignaturasGrado;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Float getCreditos()
    {
        return creditos;
    }

    public void setCreditos(Float creditos)
    {
        this.creditos = creditos;
    }

    public String getAsignaturaDuracion()
    {
        return asignaturaDuracion;
    }

    public void setAsignaturaDuracion(String asignaturaDuracion)
    {
        this.asignaturaDuracion = asignaturaDuracion;
    }

    public Float getCreditosSuperados()
    {
        return creditosSuperados;
    }

    public void setCreditosSuperados(Float creditosSuperados)
    {
        this.creditosSuperados = creditosSuperados;
    }

    public Float getCreditosTeoria()
    {
        return creditosTeoria;
    }

    public void setCreditosTeoria(Float creditosTeoria)
    {
        this.creditosTeoria = creditosTeoria;
    }

    public Float getCreditosPractica()
    {
        return creditosPractica;
    }

    public void setCreditosPractica(Float creditosPractica)
    {
        this.creditosPractica = creditosPractica;
    }

    public Float getCreditosLaboratorio()
    {
        return creditosLaboratorio;
    }

    public void setCreditosLaboratorio(Float creditosLaboratorio)
    {
        this.creditosLaboratorio = creditosLaboratorio;
    }

    public Float getCreditosSeminario()
    {
        return creditosSeminario;
    }

    public void setCreditosSeminario(Float creditosSeminario)
    {
        this.creditosSeminario = creditosSeminario;
    }

    public Float getCreditosTutoria()
    {
        return creditosTutoria;
    }

    public void setCreditosTutoria(Float creditosTutoria)
    {
        this.creditosTutoria = creditosTutoria;
    }

    public Float getCreditosEvaluacion()
    {
        return creditosEvaluacion;
    }

    public void setCreditosEvaluacion(Float creditosEvaluacion)
    {
        this.creditosEvaluacion = creditosEvaluacion;
    }

    public Float getEcts()
    {
        return ects;
    }

    public void setEcts(Float ects)
    {
        this.ects = ects;
    }

    public Float getHoras()
    {
        return horas;
    }

    public void setHoras(Float horas)
    {
        this.horas = horas;
    }

    public Float getHorasAl1()
    {
        return horasAl1;
    }

    public void setHorasAl1(Float horasAl1)
    {
        this.horasAl1 = horasAl1;
    }

    public Float getHorasNoPresenciales()
    {
        return horasNoPresenciales;
    }

    public void setHorasNoPresenciales(Float horasNoPresenciales)
    {
        this.horasNoPresenciales = horasNoPresenciales;
    }

    public Float getIncompatibilidades()
    {
        return incompatibilidades;
    }

    public void setIncompatibilidades(Float incompatibilidades)
    {
        this.incompatibilidades = incompatibilidades;
    }

    public String getBloqueado()
    {
        return bloqueado;
    }

    public void setBloqueado(String bloqueado)
    {
        this.bloqueado = bloqueado;
    }

    public Date getFechaUltima()
    {
        return fechaUltima;
    }

    public void setFechaUltima(Date fechaUltima)
    {
        this.fechaUltima = fechaUltima;
    }

    public String getAsignaturaConocimientos()
    {
        return asignaturaConocimientos;
    }

    public void setAsignaturaConocimientos(String asignaturaConocimientos)
    {
        this.asignaturaConocimientos = asignaturaConocimientos;
    }

    public String getAsignaturaObjeticos()
    {
        return asignaturaObjeticos;
    }

    public void setAsignaturaObjeticos(String asignaturaObjeticos)
    {
        this.asignaturaObjeticos = asignaturaObjeticos;
    }

    public String getAsignaturaObjetivo()
    {
        return asignaturaObjetivo;
    }

    public void setAsignaturaObjetivo(String asignaturaObjetivo)
    {
        this.asignaturaObjetivo = asignaturaObjetivo;
    }

    public String getAsignaturaMetodolocia()
    {
        return asignaturaMetodolocia;
    }

    public void setAsignaturaMetodolocia(String asignaturaMetodolocia)
    {
        this.asignaturaMetodolocia = asignaturaMetodolocia;
    }

    public String getAsignaturaEvaluacion()
    {
        return asignaturaEvaluacion;
    }

    public void setAsignaturaEvaluacion(String asignaturaEvaluacion)
    {
        this.asignaturaEvaluacion = asignaturaEvaluacion;
    }

    public String getCorregida()
    {
        return corregida;
    }

    public void setCorregida(String corregida)
    {
        this.corregida = corregida;
    }

    public Float getHorasAl()
    {
        return horasAl;
    }

    public void setHorasAl(Float horasAl)
    {
        this.horasAl = horasAl;
    }

    public Set<AsignaturasGrado> getAsignaturasGrado()
    {
        return asignaturasGrado;
    }

    public void setAsignaturasGrado(Set<AsignaturasGrado> asignaturasGrado)
    {
        this.asignaturasGrado = asignaturasGrado;
    }
}