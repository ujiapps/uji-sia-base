package es.uji.apps.lleu.model;


import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ASIG_APARTADOS")
public class AsignaturaApartado
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @Column(name="NOMBRE_CA")
    private String nombreCA;
    @Column(name="NOMBRE_ES")
    private String nombreES;
    @Column(name="NOMBRE_EN")
    private String nombreEN;

    @OneToMany(mappedBy = "apartado")
    private Set<AsignaturaTemario> temarios;

    @OneToMany(mappedBy = "apartado")
    private Set<AsignaturaMetodologia> asignaturaMetodologias;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }
}