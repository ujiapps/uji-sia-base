package es.uji.apps.lleu.model.enums;

public enum EstadoAsignatura
{
    INACTIVA("I"),
    INTRODUCIENDO("D"),
    PENDIENTE("V"),
    SLT("T"),
    FINALIZADO("F");


    private String value;

    EstadoAsignatura(String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return value;
    }

}