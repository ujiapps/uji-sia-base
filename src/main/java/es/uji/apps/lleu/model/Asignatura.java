package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ASIGNATURAS")
public class Asignatura implements Serializable
{
        @Id
        private String id;

        @Column(name = "NOMBRE")
        private String nombreCA;

        @Column(name = "NOMVAL")
        private String nombreES;

        @Column(name ="NOMANG")
        private String nombreEN;

        public String getId()
        {
                return id;
        }

        public void setId(String id)
        {
                this.id = id;
        }

        public String getNombreCA()
        {
                return nombreCA;
        }

        public void setNombreCA(String nombreCA)
        {
                this.nombreCA = nombreCA;
        }

        public String getNombreES()
        {
                return nombreES;
        }

        public void setNombreES(String nombreES)
        {
                this.nombreES = nombreES;
        }

        public String getNombreEN()
        {
                return nombreEN;
        }

        public void setNombreEN(String nombreEN)
        {
                this.nombreEN = nombreEN;
        }
}