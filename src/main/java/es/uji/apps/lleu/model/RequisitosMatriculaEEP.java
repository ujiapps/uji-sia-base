package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mysema.query.annotations.QueryProjection;

@Entity
@Table(name = "LLEU_EXT_REQUISITOS_EEP_EXT")
public class RequisitosMatriculaEEP implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name="ASIGNATURA_PREVIA")
    private String asignaturaPreviaId;

    @Id
    @Column(name="ITINERARIO_ID")
    private Long itinerarioId;

    @Column(name="NOMBRE_ASIG_ES")
    private String nombreAsigES;

    @Column(name="NOMBRE_ASIG_CA")
    private String nombreAsigCA;

    @Column(name="NOMBRE_ASIG_EN")
    private String nombreAsigEN;


    @Column(name="NOMBRE_ASIG_PREVIA_ES")
    private String nombreAsigPreviaES;

    @Column(name="NOMBRE_ASIG_PREVIA_CA")
    private String nombreAsigPreviaCA;

    @Column(name="NOMBRE_ASIG_PREVIA_EN")
    private String nombreAsigPreviaEN;


    @Column(name="ITINERARIO_ES")
    private String itinerarioES;

    @Column(name="ITINERARIO_CA")
    private String itinerarioCA;

    @Column(name="ITINERARIO_EN")
    private String itinerarioEN;

    @Column(name="CREDITOS")
    private Float creditos;

    @Column(name="CURSO_ACA_INI")
    private Integer cursoAcaIni;

    @Column(name="CURSO_ACA_FIN")
    private Integer cursoAcaFin;

    @Column(name = "CREDITOS_PORCENTAJE")
    private String creditosPorcentaje;

    @Column(name= "EXCLUIR_EEP_TFG")
    private String excluirEEPTFG;

    @Column(name = "CARACTER")
    private String caracter;

    @Column(name = "CREDITOS_OB_ITINERARIO")
    private String creditosObItinerario;

    @Column(name = "EXCLUIR_TFG")
    private String excluirTFG;

    @Column(name  = "INCLUIR_EEP")
    private String incluirEEP;

    @Column(name = "TEXTO_CA")
    private String textoCA;

    @Column(name = "TEXTO_ES")
    private String textoES;

    @Column(name = "TEXTO_EN")
    private String textoEN;

    public RequisitosMatriculaEEP() {}

    @QueryProjection
    public RequisitosMatriculaEEP (
            Integer cursoAca,
            Long estudioId,
            String asignaturaId,
            String nombreAsigES,
            String nombreAsigCA,
            String nombreAsigEN
    ) {
        this.cursoAca = cursoAca;
        this.estudioId = estudioId;
        this.asignaturaId = asignaturaId;
        this.nombreAsigCA = nombreAsigCA;
        this.nombreAsigEN = nombreAsigEN;
        this.nombreAsigES = nombreAsigES;
    }


    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombreAsigES()
    {
        return nombreAsigES;
    }

    public void setNombreAsigES(String nombreAsigES)
    {
        this.nombreAsigES = nombreAsigES;
    }

    public String getNombreAsigCA()
    {
        return nombreAsigCA;
    }

    public void setNombreAsigCA(String nombreAsigCA)
    {
        this.nombreAsigCA = nombreAsigCA;
    }

    public String getNombreAsigEN()
    {
        return nombreAsigEN;
    }

    public void setNombreAsigEN(String nombreAsigEN)
    {
        this.nombreAsigEN = nombreAsigEN;
    }

    public String getAsignaturaPreviaId()
    {
        return asignaturaPreviaId;
    }

    public void setAsignaturaPreviaId(String asignaturaPreviaId)
    {
        this.asignaturaPreviaId = asignaturaPreviaId;
    }

    public String getNombreAsigPreviaES()
    {
        return nombreAsigPreviaES;
    }

    public void setNombreAsigPreviaES(String nombreAsigPreviaES)
    {
        this.nombreAsigPreviaES = nombreAsigPreviaES;
    }

    public String getNombreAsigPreviaCA()
    {
        return nombreAsigPreviaCA;
    }

    public void setNombreAsigPreviaCA(String nombreAsigPreviaCA)
    {
        this.nombreAsigPreviaCA = nombreAsigPreviaCA;
    }

    public String getNombreAsigPreviaEN()
    {
        return nombreAsigPreviaEN;
    }

    public void setNombreAsigPreviaEN(String nombreAsigPreviaEN)
    {
        this.nombreAsigPreviaEN = nombreAsigPreviaEN;
    }

    public Long getItinerarioId()
    {
        return itinerarioId;
    }

    public void setItinerarioId(Long itinerarioId)
    {
        this.itinerarioId = itinerarioId;
    }

    public String getItinerarioES()
    {
        return itinerarioES;
    }

    public void setItinerarioES(String itinerarioES)
    {
        this.itinerarioES = itinerarioES;
    }

    public String getItinerarioCA()
    {
        return itinerarioCA;
    }

    public void setItinerarioCA(String itinerarioCA)
    {
        this.itinerarioCA = itinerarioCA;
    }

    public String getItinerarioEN()
    {
        return itinerarioEN;
    }

    public void setItinerarioEN(String itinerarioEN)
    {
        this.itinerarioEN = itinerarioEN;
    }

    public Float getCreditos()
    {
        return creditos;
    }

    public void setCreditos(Float creditos)
    {
        this.creditos = creditos;
    }

    public Integer getCursoAcaIni()
    {
        return cursoAcaIni;
    }

    public void setCursoAcaIni(Integer cursoAcaIni)
    {
        this.cursoAcaIni = cursoAcaIni;
    }

    public Integer getCursoAcaFin()
    {
        return cursoAcaFin;
    }

    public void setCursoAcaFin(Integer cursoAcaFin)
    {
        this.cursoAcaFin = cursoAcaFin;
    }

    public String getCreditosPorcentaje()
    {
        return creditosPorcentaje;
    }

    public void setCreditosPorcentaje(String creditosPorcentaje)
    {
        this.creditosPorcentaje = creditosPorcentaje;
    }

    public String getExcluirEEPTFG()
    {
        return excluirEEPTFG;
    }

    public void setExcluirEEPTFG(String excluirEEPTFG)
    {
        this.excluirEEPTFG = excluirEEPTFG;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public String getCreditosObItinerario()
    {
        return creditosObItinerario;
    }

    public void setCreditosObItinerario(String creditosObItinerario)
    {
        this.creditosObItinerario = creditosObItinerario;
    }

    public String getExcluirTFG()
    {
        return excluirTFG;
    }

    public void setExcluirTFG(String excluirTFG)
    {
        this.excluirTFG = excluirTFG;
    }

    public String getIncluirEEP()
    {
        return incluirEEP;
    }

    public void setIncluirEEP(String incluirEEP)
    {
        this.incluirEEP = incluirEEP;
    }

    public String getTextoCA()
    {
        return textoCA;
    }

    public void setTextoCA(String textoCA)
    {
        this.textoCA = textoCA;
    }

    public String getTextoES()
    {
        return textoES;
    }

    public void setTextoES(String textoES)
    {
        this.textoES = textoES;
    }

    public String getTextoEN()
    {
        return textoEN;
    }

    public void setTextoEN(String textoEN)
    {
        this.textoEN = textoEN;
    }
}
