package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.uji.apps.lleu.utils.IPersona;

@Entity
@Table(name = "LLEU_EXT_ASIG_PROFESOR")
public class AsignaturaProfesor implements Serializable, IPersona
{

    @Id
    @Column(name = "ASI_ID")
    private String asiId;
    @Id
    @Column(name = "GRP_ID")
    private String grpId;
    @Id
    @Column(name = "SGR_ID")
    private Integer sgrId;
    @Id
    @Column(name = "SGR_TIPO_REAL")
    private String sgrTipoReal;
    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Column(name = "SGR_TIPO")
    private String sgrTipo;

    @Column(name = "IDIOMA_OLD")
    private Integer idiomaOld;

    @Column(name = "IDIOMA_CA")
    private String idiomaCA;

    @Column(name = "IDIOMA_ES")
    private String idiomaES;

    @Column(name = "IDIOMA_EN")
    private String idiomaEN;
    @Id
    @Column(name = "PER_ID")
    private Long perId;
    @Column(name = "PER_NOMBRE")
    private String personaNombre;

    @Column(name = "SEMESTRE")
    private String semestre;

    @Column(name = "URL")
    private String url;

    private Integer orden;


    public String getAsiId()
    {
        return asiId;
    }

    public void setAsiId(String asiId)
    {
        this.asiId = asiId;
    }

    public String getGrpId()
    {
        return grpId;
    }

    public void setGrpId(String grpId)
    {
        this.grpId = grpId;
    }

    public Integer getSgrId()
    {
        return sgrId;
    }

    public void setSgrId(Integer sgrId)
    {
        this.sgrId = sgrId;
    }

    public String getSgrTipoReal()
    {
        return sgrTipoReal;
    }

    public void setSgrTipoReal(String sgrTipoReal)
    {
        this.sgrTipoReal = sgrTipoReal;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getSgrTipo()
    {
        return sgrTipo;
    }

    public void setSgrTipo(String sgrTipo)
    {
        this.sgrTipo = sgrTipo;
    }

    public Integer getIdiomaOld()
    {
        return idiomaOld;
    }

    public void setIdiomaOld(Integer idiomaOld)
    {
        this.idiomaOld = idiomaOld;
    }

    public String getIdiomaCA()
    {
        return idiomaCA;
    }

    public void setIdiomaCA(String idiomaCA)
    {
        this.idiomaCA = idiomaCA;
    }

    public String getIdiomaES()
    {
        return idiomaES;
    }

    public void setIdiomaES(String idiomaES)
    {
        this.idiomaES = idiomaES;
    }

    public String getIdiomaEN()
    {
        return idiomaEN;
    }

    public void setIdiomaEN(String idiomaEN)
    {
        this.idiomaEN = idiomaEN;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public String getPersonaNombre()
    {
        return personaNombre;
    }

    public void setPersonaNombre(String personaNombre)
    {
        this.personaNombre = personaNombre;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }
}
