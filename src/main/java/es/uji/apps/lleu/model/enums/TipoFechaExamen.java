package es.uji.apps.lleu.model.enums;

public enum TipoFechaExamen
{
    TEORIA("TEO"),
    PROBLEMAS("PRO"),
    LABORATORIO("LAB"),
    PRACTICAS("PRA"),
    OTROS("ALT"),
    ORAL("ORA");

    String value;

    TipoFechaExamen (String value) { this.value = value; }

    @Override
    public String toString()
    {
        return this.value;
    }
}
