package es.uji.apps.lleu.model;

import com.mysema.query.annotations.QueryProjection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "LLEU_EXT_ESTUDIOS")
public class Estudio implements Serializable
{
    @Id
    @Column(name = "ID")
    private Long id;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    // TODO: OPTIMIZACION: revisar si hace falta cursoacaini como Id
    @Column(name = "CURSO_ACA_INI")
    private Integer cursoAcaIni;

    @Column(name = "CURSO_ACA_FIN")
    private Integer cursoAcaFin;

    @Column(name = "CURSO_ACA_FIN_DOCENCIA")
    private Integer cursoAcaFinDocencia;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "UEST_ID")
    private Long uestId;

    @Column(name = "PRESENTACION")
    private String presentacion;

    @Column(name = "FAQ_URL")
    private String faqURL;

    @Column(name = "CURSOS")
    private Integer cursos;

    @Column(name = "NIVEL")
    private Integer nivel;

    @Column(name = "COMENTARIO_PORTADA")
    private Integer comentarioPortada;

    @Column(name="RESPONSABLES_URL")
    private String responsablesUrl;


    // TODO: OPTIMIZACION: Haceer un QueryProjection para no traerme presentacion en la página principal.
    @QueryProjection
    public Estudio(Long id, Integer cursoAcaFinDocencia, String nombreCA, String nombreEN, String nombreES, String tipo, Long uestId, Integer nivel, Integer comentarioPortada, String responsablesUrl) {
        this.id = id;
        this.cursoAcaFinDocencia = cursoAcaFinDocencia;
        this.nombreCA = nombreCA;
        this.nombreEN = nombreEN;
        this.nombreES = nombreES;
        this.tipo = tipo;
        this.uestId = uestId;
        this.nivel = nivel;
        this.comentarioPortada= comentarioPortada;
        this.responsablesUrl = responsablesUrl;
    }
    public Estudio() {};

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getCursoAcaIni()
    {
        return cursoAcaIni;
    }

    public void setCursoAcaIni(Integer cursoAcaIni)
    {
        this.cursoAcaIni = cursoAcaIni;
    }

    public Integer getCursoAcaFin()
    {
        return cursoAcaFin;
    }

    public void setCursoAcaFin(Integer cursoAcaFin)
    {
        this.cursoAcaFin = cursoAcaFin;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Long getUestId()
    {
        return uestId;
    }

    public void setUestId(Long uestId)
    {
        this.uestId = uestId;
    }

    public Integer getCursoAcaFinDocencia()
    {
        return cursoAcaFinDocencia;
    }

    public void setCursoAcaFinDocencia(Integer cursoAcaFinDocencia)
    {
        this.cursoAcaFinDocencia = cursoAcaFinDocencia;
    }

    // TODO: OPTIMIZACION. esto va fuera.
    public String getUrlVerifica() {
        return "popo";
    }

    public void setPresentacion(String presentacion)
    {
        this.presentacion = presentacion;
    }

    public String getPresentacion()
    {
        return presentacion;
    }

    public String getFaqURL()
    {
        return faqURL;
    }

    public void setFaqURL(String faqURL)
    {
        this.faqURL = faqURL;
    }

    public Integer getCursos()
    {
        return cursos;
    }

    public void setCursos(Integer cursos)
    {
        this.cursos = cursos;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Integer getNivel()
    {
        return nivel;
    }

    public void setNivel(Integer nivel)
    {
        this.nivel = nivel;
    }

    public Integer getComentarioPortada() {
        return comentarioPortada;
    }

    public void setComentarioPortada(Integer comentarioPortada) {
        this.comentarioPortada = comentarioPortada;
    }

    public String getResponsablesUrl() {
        return responsablesUrl;
    }

    public void setResponsablesUrl(String responsablesUrl) {
        this.responsablesUrl = responsablesUrl;
    }
}

