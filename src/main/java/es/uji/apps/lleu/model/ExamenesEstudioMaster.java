package es.uji.apps.lleu.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "LLEU_EXT_EXAMENES_MASTER")
public class ExamenesEstudioMaster implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ASI_ID")
    private String asiId;

    @Id
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    @Id
    @Column(name = "INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ini;

    @Id
    @Column(name="FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @Column(name="ASI_NOMBRE_ES")
    private String nombreAsignaturaES;

    @Column(name="ASI_NOMBRE_CA")
    private String nombreAsignaturaCA;

    @Column(name="ASI_NOMBRE_EN")
    private String nombreAsignaturaEN;

    /**
     * El tipo de asignatura: TEO, PRO, LAB, ORA, PRA, ALT
     */
    @Id
    @Column(name="TIPO")
    private String tipo;

    /**
     * Parcial es 'S' o 'N'
     */
    @Id
    @Column(name="PARCIAL")
    private String parcial;

    @Column(name="CONV_ID")
    private Integer convocatoriaId;

    @Column(name="DEFINITIVO")
    private String definitivo;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "examenMaster")
    public Set<ExamenesEstudioAulasMaster> aulas;

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public String getAsiId()
    {
        return asiId;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public Date getIni()
    {
        return ini;
    }

    public Date getFin()
    {
        return fin;
    }

    public String getTipo()
    {
        return tipo;
    }

    public String getParcial()
    {
        return parcial;
    }

    public String getNombreAsignaturaES()
    {
        return nombreAsignaturaES;
    }

    public String getNombreAsignaturaCA()
    {
        return nombreAsignaturaCA;
    }

    public String getNombreAsignaturaEN()
    {
        return nombreAsignaturaEN;
    }

    public Set<ExamenesEstudioAulasMaster> getAulas()
    {
        return aulas;
    }

    public Integer getConvocatoriaId()
    {
        return convocatoriaId;
    }

    public String getDefinitivo()
    {
        return definitivo;
    }
}
