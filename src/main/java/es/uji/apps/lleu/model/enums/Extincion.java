package es.uji.apps.lleu.model.enums;

public enum Extincion
{
    // Estudios con docencia
    CONDOCENCIA(0), SINDOCENCIA(1);

  Integer value;

    Extincion(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}

