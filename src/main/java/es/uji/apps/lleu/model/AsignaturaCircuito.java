package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ASIG_CIRCUITOS")
public class AsignaturaCircuito implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    Integer cursoAca;

    @Id
    @Column(name ="ESTUDIO_ID")
    Long estudioId;

    @Id
    @Column(name ="ASI_ID")
    String asignaturaId;

    @Id
    @Column(name ="CIRCUITO_ID")
    Long circuitoId;

    @Id
    @Column(name ="SGR_TIPO")
    String subgrupoTipo;

    @Id
    @Column(name ="SGR_ID")
    Long subgrupoId;

    @Id
    @Column(name = "GRUPO")
    String grupo;

    @Column(name ="NOMBRE_ES")
    String nombreAsignaturaES;

    @Column(name ="NOMBRE_CA")
    String nombreAsignaturaCA;

    @Column(name ="NOMBRE_EN")
    String nombreAsignaturaEN;

    @Column(name ="CARACTER")
    String caracter;

    @Column(name ="CUR_ID")
    Integer curso;

    @Column(name="TIPO")
    String tipo;

    @Column(name="SEMESTRE")
    String semestre;

    @Column(name ="ORDEN")
    Integer orden;

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Long getCircuitoId()
    {
        return circuitoId;
    }

    public void setCircuitoId(Long circuitoId)
    {
        this.circuitoId = circuitoId;
    }

    public String getSubgrupoTipo()
    {
        return subgrupoTipo;
    }

    public void setSubgrupoTipo(String subgrupoTipo)
    {
        this.subgrupoTipo = subgrupoTipo;
    }

    public Long getSubgrupoId()
    {
        return subgrupoId;
    }

    public void setSubgrupoId(Long subgrupoId)
    {
        this.subgrupoId = subgrupoId;
    }

    public String getNombreAsignaturaES()
    {
        return nombreAsignaturaES;
    }

    public void setNombreAsignaturaES(String nombreAsignaturaES)
    {
        this.nombreAsignaturaES = nombreAsignaturaES;
    }

    public String getNombreAsignaturaCA()
    {
        return nombreAsignaturaCA;
    }

    public void setNombreAsignaturaCA(String nombreAsignaturaCA)
    {
        this.nombreAsignaturaCA = nombreAsignaturaCA;
    }

    public String getNombreAsignaturaEN()
    {
        return nombreAsignaturaEN;
    }

    public void setNombreAsignaturaEN(String nombreAsignaturaEN)
    {
        this.nombreAsignaturaEN = nombreAsignaturaEN;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public void setCurso(Integer curso)
    {
        this.curso = curso;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }

    public String getTipo()
    {
        return tipo;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }
}
