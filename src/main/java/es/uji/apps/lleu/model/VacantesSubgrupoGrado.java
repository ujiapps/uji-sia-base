package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_VACANTES_SUBGRUPO_G")
public class VacantesSubgrupoGrado implements Serializable
{
    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cusoAca;

    @Id
    @Column(name = "GRUPO")
    private String grupo;

    @Id
    @Column(name = "TIPO")
    private String tipo;

    @Id
    @Column(name = "SUBGRUPO")
    private Long subGrupo;

    @Column(name = "ORDEN")
    private Integer orden;

    @Column(name = "EXTRAS")
    private String extras;

    @Column(name ="VACANTES")
    private String vacantes;

    @Column(name = "VACANTES_NUEVAS")
    private String vacantesNuevos;

    @Id
    @Column(name= "ESTUDIO_ID")
    private Long estudioId;

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public Integer getCusoAca()
    {
        return cusoAca;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public String getTipo()
    {
        return tipo;
    }

    public Long getSubGrupo()
    {
        return subGrupo;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public String getExtras()
    {
        return extras;
    }

    public String getVacantes()
    {
        return vacantes;
    }

    public String getVacantesNuevos()
    {
        return vacantesNuevos;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }
}