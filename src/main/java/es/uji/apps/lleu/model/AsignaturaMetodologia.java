package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ASIG_METODOLOGIA")
public class AsignaturaMetodologia implements Serializable
{
    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Column(name="TEXTO_CA")
    @Lob
    private String textoCA;
    @Column(name="TEXTO_ES")
    @Lob
    private String textoES;
    @Column(name="TEXTO_EN")
    @Lob
    private String textoEN;

    @Id
    private String estado;

    @Id
    @ManyToOne
    @JoinColumn(name = "APARTADO_ID", insertable = false, updatable = false)
    private AsignaturaApartado apartado;

    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoACA;

    @Column(name="CODIGO_BUSQUEDA")
    private String codigoBusqueda;

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getTextoCA()
    {
        return textoCA;
    }

    public void setTextoCA(String textoCA)
    {
        this.textoCA = textoCA;
    }

    public String getTextoES()
    {
        return textoES;
    }

    public void setTextoES(String textoES)
    {
        this.textoES = textoES;
    }

    public String getTextoEN()
    {
        return textoEN;
    }

    public void setTextoEN(String textoEN)
    {
        this.textoEN = textoEN;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public AsignaturaApartado getApartado()
    {
        return apartado;
    }

    public void setApartado(AsignaturaApartado apartado)
    {
        this.apartado = apartado;
    }

    public Integer getCursoACA()
    {
        return cursoACA;
    }

    public void setCursoACA(Integer cursoACA)
    {
        this.cursoACA = cursoACA;
    }

    public String getCodigoBusqueda()
    {
        return codigoBusqueda;
    }

    public void setCodigoBusqueda(String codigoBusqueda)
    {
        this.codigoBusqueda = codigoBusqueda;
    }
}