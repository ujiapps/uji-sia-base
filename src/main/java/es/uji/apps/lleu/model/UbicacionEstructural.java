package es.uji.apps.lleu.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_UBIC_ESTRUCTURALES")
public class UbicacionEstructural implements Serializable
{

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NOMBRE")
    private String nombreCA;

    @Column(name = "NOMBRE_CAS")
    private String nombreES;

    @Column(name = "NOMBRE_ANG")
    private String nombreEN;

    @OneToMany(mappedBy = "ubicacionEstructural")
    private Set<ProfesorMaster> profesorMasters;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public Set<ProfesorMaster> getProfesorMasters()
    {
        return profesorMasters;
    }

    public void setProfesorMasters(Set<ProfesorMaster> profesorMasters)
    {
        this.profesorMasters = profesorMasters;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nomnbreEN)
    {
        this.nombreEN = nomnbreEN;
    }

}
