package es.uji.apps.lleu.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// TODO: Tenemos varias vistas de exámenes: LLEY_EXT_AULAS_EXAMEN, LLEU_EXT_EXAMEN, LLEU_EXT_EXAMEN_EST_AULAS, LLEU_EXT_EXAMENES_ESTUDIO... eliminar las que no nos hagan falta.
@Entity
@Table(name = "LLEU_EXT_EXAMENES")
public class ExamenesGrado implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name="FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date Fecha;

    @Id
    @Column(name="CON_ID")
    private Long convocatoriaId;

    @Id
    @Column(name="EPO_ID")
    private Long epocaId;

    @Id
    @Column(name="SEMESTRE")
    private String semestre;

    @Id
    @Column(name="INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date inicio;

    @Id
    @Column(name="TIPO")
    private String tipo;

    @Column(name="PARCIAL")
    private String parcial;

    @Column(name="FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @Column(name="COMENTARIO_CA")
    private String comentarioCA;
    @Column(name="COMENTARIO_ES")
    private String comentarioES;
    @Column(name="COMENTARIO_EN")
    private String comentarioEN;

    @Column(name="NOMBRE_CA")
    private String nombreCA;

    @Column(name="NOMBRE_ES")
    private String nombreES;

    @Column(name="NOMBRE_EN")
    private String nombreEN;

    @Column(name="DEFINITIVO")
    private String definitivo;

    @OneToMany(fetch=FetchType.EAGER, mappedBy = "examen")
    private List<AulasExamenesGrado> aulas;

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getParcial()
    {
        return parcial;
    }

    public void setParcial(String parcial)
    {
        this.parcial = parcial;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Date getFecha()
    {
        return Fecha;
    }

    public void setFecha(Date fecha)
    {
        Fecha = fecha;
    }

    public Date getInicio()
    {
        return inicio;
    }

    public void setInicio(Date inicio)
    {
        this.inicio = inicio;
    }

    public Date getFin()
    {
        return fin;
    }

    public void setFin(Date fin)
    {
        this.fin = fin;
    }

    public String getComentarioCA()
    {
        return comentarioCA;
    }

    public void setComentarioCA(String comentarioCA)
    {
        this.comentarioCA = comentarioCA;
    }

    public String getComentarioES()
    {
        return comentarioES;
    }

    public void setComentarioES(String comentarioES)
    {
        this.comentarioES = comentarioES;
    }

    public String getComentarioEN()
    {
        return comentarioEN;
    }

    public void setComentarioEN(String comentarioEN)
    {
        this.comentarioEN = comentarioEN;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public Long getConvocatoriaId()
    {
        return convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public Long getEpocaId()
    {
        return epocaId;
    }

    public void setEpocaId(Long epocaId)
    {
        this.epocaId = epocaId;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public List<AulasExamenesGrado> getAulas()
    {
        return aulas;
    }

    public String getDefinitivo()
    {
        return definitivo;
    }

    public void setDefinitivo(String definitivo)
    {
        this.definitivo = definitivo;
    }

    public void setAulas(List<AulasExamenesGrado> aulas)
    {
        this.aulas = aulas;
    }
}
