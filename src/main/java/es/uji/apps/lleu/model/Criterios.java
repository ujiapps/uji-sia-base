package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="LLEU_EXT_CRITERIOS")
public class Criterios implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Column(name="TEXTO_CA")
    private String textoCA;

    @Column(name="TEXTO_ES")
    private String textoES;

    @Column(name="TEXTO_EN")
    private String textoEN;


    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getTextoCA()
    {
        return textoCA;
    }

    public void setTextoCA(String textoCA)
    {
        this.textoCA = textoCA;
    }

    public String getTextoES()
    {
        return textoES;
    }

    public void setTextoES(String textoES)
    {
        this.textoES = textoES;
    }

    public String getTextoEN()
    {
        return textoEN;
    }

    public void setTextoEN(String textoEN)
    {
        this.textoEN = textoEN;
    }
}
