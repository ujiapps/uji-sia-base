package es.uji.apps.lleu.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "LLEU_EXT_ESTUDIOS_TODO")
public class EstudioTodo implements Serializable
{
    @Id
    @Column(name="ID")
    private Long id;

    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Column(name="NOMBRE_CA")
    private String nombreCA;

    @Column(name="NOMBRE_ES")
    private String nombreES;

    @Column(name="NOMBRE_EN")
    private String nombreEN;

    @Column(name="TIPO")
    private String tipo;

    @Column(name="UEST_ID")
    private Long uestId;

    @Column(name="FAQ_URL")
    private String faqUrl;

    @Column(name="CURSOS")
    private Integer cursos;

    @Column(name = "CRD_ITINERARIO_MIN")
    private Float creditoItinerarioMin;

    @Column(name = "NIVEL")
    private Integer nivel;

    @Column(name = "COMENTARIO_PORTADA")
    private Integer comentarioPortada;

    @Column(name = "CALIDAD_URL")
    private String calidadUrl;

    @Column(name = "RECONOCIMIENTO_URL")
    private String reconocimientoUrl;

    @Column(name="RESPONSABLES_URL")
    private String responsablesUrl;


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Long getUestId()
    {
        return uestId;
    }

    public void setUestId(Long uestId)
    {
        this.uestId = uestId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getFaqUrl()
    {
        return faqUrl;
    }

    public void setFaqUrl(String faqUrl)
    {
        this.faqUrl = faqUrl;
    }

    public Integer getCursos()
    {
        return cursos;
    }

    public void setCursos(Integer cursos)
    {
        this.cursos = cursos;
    }

    public Float getCreditoItinerarioMin()
    {
        return creditoItinerarioMin;
    }

    public void setCreditoItinerarioMin(Float creditoItinerarioMin)
    {
        this.creditoItinerarioMin = creditoItinerarioMin;
    }

    public Integer getNivel()
    {
        return nivel;
    }

    public void setNivel(Integer nivel)
    {
        this.nivel = nivel;
    }

    public String getCalidadUrl()
    {
        return calidadUrl;
    }

    public void setCalidadUrl(String calidadUrl)
    {
        this.calidadUrl = calidadUrl;
    }

    public String getReconocimientoUrl()
    {
        return reconocimientoUrl;
    }

    public void setReconocimientoUrl(String reconocimientoUrl)
    {
        this.reconocimientoUrl = reconocimientoUrl;
    }

    public Integer getComentarioPortada() {
        return comentarioPortada;
    }

    public void setComentarioPortada(Integer comentarioPortada) {
        this.comentarioPortada = comentarioPortada;
    }

    public String getResponsablesUrl() {
        return responsablesUrl;
    }

    public void setResponsablesUrl(String responsablesUrl) {
        this.responsablesUrl = responsablesUrl;
    }
}
