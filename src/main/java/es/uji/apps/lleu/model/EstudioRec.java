package es.uji.apps.lleu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ESTUDIO_REC")
public class EstudioRec
{
    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "EXPERIENCIA")
    private String experiencia;

    @Column(name = "CFGS")
    private String cfgs;

    @Column(name ="CURSO_ACA_DESDE")
    private Integer cursoAcaDesde;

    @Column(name ="CRD_EXPERIENCIA")
    private Float creditosExperiencia;

    @Column(name ="CRD_CFGS")
    private Float creditosCFGS;

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getExperiencia()
    {
        return experiencia;
    }

    public void setExperiencia(String experiencia)
    {
        this.experiencia = experiencia;
    }

    public String getCfgs()
    {
        return cfgs;
    }

    public void setCfgs(String cfgs)
    {
        this.cfgs = cfgs;
    }

    public Integer getCursoAcaDesde()
    {
        return cursoAcaDesde;
    }

    public void setCursoAcaDesde(Integer cursoAcaDesde)
    {
        this.cursoAcaDesde = cursoAcaDesde;
    }

    public Float getCreditosExperiencia() {
        return creditosExperiencia;
    }

    public void setCreditosExperiencia(Float creditosExperiencia) {
        this.creditosExperiencia = creditosExperiencia;
    }

    public Float getCreditosCFGS() {
        return creditosCFGS;
    }

    public void setCreditosCFGS(Float creditosCFGS) {
        this.creditosCFGS = creditosCFGS;
    }
}
