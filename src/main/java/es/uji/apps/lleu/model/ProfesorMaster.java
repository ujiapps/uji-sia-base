package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.uji.apps.lleu.utils.IPersona;

@Entity
@Table(name = "LLEU_EXT_PROFESORES_MASTER")
public class ProfesorMaster implements Serializable, IPersona
{

    @Id
    @Column(name = "PER_ID")
    private Long perId;
    @Id
    @Column(name = "PER_NOMBRE")
    private String nombre;
    @Id
    @Column(name = "CATEGORIA_CA")
    private String categoriaCA ;
    @Id
    @Column(name = "CATEGORIA_ES")
    private String categoriaES;
    @Id
    @Column(name = "CATEGORIA_EN")
    private String categoriaEN;

    @Column(name = "UBICACION_ID")
    private Long ubicacionId;

    @Column(name = "PROF_OTRA_UNIV")
    private String otraUniversidad;

    @Column(name = "EMPRESA")
    private String empresa;

    @Column(name = "CATEGORIA")
    private String categoria;

    @Column(name = "DOCTOR")
    private String doctor;

    @Column(name = "CARGO")
    private String cargo;

    @Column(name = "URL")
    private String url;

    @Column(name = "CURRICULUM")
    private String curruculum;

    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "PROFESOR_EXT")
    private String externo;

    @Column(name = "TIPO_EXTERNO")
    private Long tipoExterno;

    private String email;

    @ManyToOne
    @JoinColumn(name = "UBICACION_ID", insertable = false, updatable = false)
    private UbicacionEstructural ubicacionEstructural;

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCategoriaCA()
    {
        return categoriaCA;
    }

    public void setCategoriaCA(String categoriaCA)
    {
        this.categoriaCA = categoriaCA;
    }

    public String getCategoriaES()
    {
        return categoriaES;
    }

    public void setCategoriaES(String categoriaES)
    {
        this.categoriaES = categoriaES;
    }

    public String getCategoriaEN()
    {
        return categoriaEN;
    }

    public void setCategoriaEN(String categoriaEN)
    {
        this.categoriaEN = categoriaEN;
    }

    public Long getUbicacionId()
    {
        return ubicacionId;
    }

    public void setUbicacionId(Long ubicacionId)
    {
        this.ubicacionId = ubicacionId;
    }

    public String getOtraUniversidad()
    {
        return otraUniversidad;
    }

    public void setOtraUniversidad(String otraUniversidad)
    {
        this.otraUniversidad = otraUniversidad;
    }

    public String getEmpresa()
    {
        return empresa;
    }

    public void setEmpresa(String empresa)
    {
        this.empresa = empresa;
    }

    public String getCategoria()
    {
        return categoria;
    }

    public void setCategoria(String categoria)
    {
        this.categoria = categoria;
    }

    public String getDoctor()
    {
        return doctor;
    }

    public void setDoctor(String doctor)
    {
        this.doctor = doctor;
    }

    public String getCargo()
    {
        return cargo;
    }

    public void setCargo(String cargo)
    {
        this.cargo = cargo;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getCurruculum()
    {
        return curruculum;
    }

    public void setCurruculum(String curruculum)
    {
        this.curruculum = curruculum;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getExterno()
    {
        return externo;
    }

    public void setExterno(String externo)
    {
        this.externo = externo;
    }

    public UbicacionEstructural getUbicacionEstructural()
    {
        return ubicacionEstructural;
    }

    public void setUbicacionEstructural(UbicacionEstructural ubicacionEstructural)
    {
        this.ubicacionEstructural = ubicacionEstructural;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public Long getTipoExterno() {
        return tipoExterno;
    }
}
