package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "LLEU_EXT_GDO_ESTADO_ASIG")
public class EstadoAsignatura implements Serializable
{
    @Id
    @Column(name = "CURSO_ACA")
    Integer cursoACa;

    @Id
    @Column(name = "ASI_ID")
    String asignaturaId;

    @Column(name = "ESTADO")
    String estado;

    public Integer getCursoACa()
    {
        return cursoACa;
    }

    public void setCursoACa(Integer cursoACa)
    {
        this.cursoACa = cursoACa;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getEstado()
    {
        return estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }
}
