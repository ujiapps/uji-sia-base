package es.uji.apps.lleu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_PER_FOTO")
public class Foto
{
    @Id
    @Column(name = "PER_ID")
    private Long perId;

    @Lob
    @Column(name = "FOTO", columnDefinition = "BLOB")
    private byte []foto;

    @Column(name = "MIME_TYPE")
    private String mimeType;

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public byte[] getFoto()
    {
        return foto;
    }

    public void setFoto(byte[] foto)
    {
        this.foto = foto;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }
}
