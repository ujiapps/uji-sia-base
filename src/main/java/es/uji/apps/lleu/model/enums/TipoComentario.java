package es.uji.apps.lleu.model.enums;

/**
 * Indica el tipo de Comentario
 */
public enum TipoComentario
{
    PRINCIPALASIG("PA"),REQMATRICULAASIG("RMA");

    String value;

    TipoComentario(String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    
}
