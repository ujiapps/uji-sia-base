package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_GRUPOS_TEORIA")
public class GrupoTeoriaGrado implements Serializable
{
    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;
    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;
    @Id
    @Column(name = "CUR_ID")
    private Integer curId;
    @Id
    @Column(name = "GRUPO")
    private String grupo;

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Integer getCurId()
    {
        return curId;
    }

    public void setCurId(Integer curId)
    {
        this.curId = curId;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }
}
