package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad para la generación de horarios personalizados.
 */
@Entity
@Table(name="LLEU_EXT_HORARIOS_GENERAR")
public class HorarioGenerar implements Serializable
{
    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name = "CARACTER")
    private String caracter;

    @Id
    @Column(name = "GRUPO")
    private String grupo;

    @Id
    @Column(name = "SUBGRUPO")
    private String subGrupo;

    @Id
    @Column(name = "SUBGRUPO_ID")
    private Integer subGrupoId;

    @Id
    @Column(name = "SEMESTRE")
    private String semestre;

    @Column(name = "CUR_ID")
    private Long curId;

    @Column(name = "ASI_NOMBRE_CA")
    private String asiNombreCA;
    @Column(name = "ASI_NOMBRE_ES")
    private String asiNombreES;
    @Column(name = "ASI_NOMBRE_EN")
    private String asiNombreEN;

    @Column(name = "TIPO")
    private String tipo;


    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public Long getCurId()
    {
        return curId;
    }

    public void setCurId(Long curId)
    {
        this.curId = curId;
    }

    public String getAsiNombreCA()
    {
        return asiNombreCA;
    }

    public void setAsiNombreCA(String asiNombreCA)
    {
        this.asiNombreCA = asiNombreCA;
    }

    public String getAsiNombreES()
    {
        return asiNombreES;
    }

    public void setAsiNombreES(String asiNombreES)
    {
        this.asiNombreES = asiNombreES;
    }

    public String getAsiNombreEN()
    {
        return asiNombreEN;
    }

    public void setAsiNombreEN(String asiNombreEN)
    {
        this.asiNombreEN = asiNombreEN;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public String getSubGrupo()
    {
        return subGrupo;
    }

    public void setSubGrupo(String subGrupo)
    {
        this.subGrupo = subGrupo;
    }

    public Integer getSubGrupoId()
    {
        return subGrupoId;
    }

    public void setSubGrupoId(Integer subGrupoId)
    {
        this.subGrupoId = subGrupoId;
    }
}
