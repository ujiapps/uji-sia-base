package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_RECONOCIMIENTOS")
public class Reconocimiento implements Serializable
{
    @Id
    @Column(name = "ASI_ID_CURSADA")
    private String asigCursada;

    @Id
    @Column(name = "ASI_ID_RECONOCIDA")
    private String asigReconocida;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name = "TIT_ID_CURSADA")
    private Integer titulacionCursada;

    @Id
    @Column(name = "TIT_ID_RECONOCIDA")
    private Integer titulacionReconocida;

    @Column(name = "NOMBRE_CURSADA")
    private String nombreCursadaES;

    @Column(name = "NOMBRECA_CURSADA")
    private String nombreCursadaCA;

    @Column(name = "NOMBREEN_CURSADA")
    private String nombreCursadaEN;

    @Column(name = "NOMBRE_RECONOCIDA")
    private String nombreReconocidaES;

    @Column(name = "NOMBRECA_RECONOCIDA")
    private String nombreReconocidaCA;

    @Column(name = "NOMBREEN_RECONOCIDA")
    private String nombreReconocidaEN;

    @Id
    @Column(name = "TCO_ID")
    private Integer tcoId;

    @Column(name = "PARCIAL")
    private String parcial;

    private String tipo;

    @ManyToOne
    @JoinColumn(name = "TIT_ID_CURSADA", insertable = false, updatable = false)
    private EstudioTodos estudioCursado;

    @ManyToOne
    @JoinColumn(name = "TIT_ID_RECONOCIDA", insertable = false, updatable = false)
    private EstudioTodos estudioReconocido;

    @Column(name = "CURSADA_EN_LLEU")
    private Short cursadaEnLleu;

    public String getAsigCursada()
    {
        return asigCursada;
    }

    public void setAsigCursada(String asigCursada)
    {
        this.asigCursada = asigCursada;
    }

    public String getNombreCursadaES()
    {
        return nombreCursadaES;
    }

    public void setNombreCursadaES(String nombreCursada)
    {
        this.nombreCursadaES = nombreCursada;
    }

    public String getNombreCursadaCA()
    {
        return nombreCursadaCA;
    }

    public void setNombreCursadaCA(String nombreCACursada)
    {
        this.nombreCursadaCA = nombreCACursada;
    }

    public String getNombreCursadaEN()
    {
        return nombreCursadaEN;
    }

    public void setNombreCursadaEN(String nombreENCursada)
    {
        this.nombreCursadaEN = nombreENCursada;
    }

    public String getAsigReconocida()
    {
        return asigReconocida;
    }

    public void setAsigReconocida(String asigReconocida)
    {
        this.asigReconocida = asigReconocida;
    }

    public String getNombreReconocidaES()
    {
        return nombreReconocidaES;
    }

    public void setNombreReconocidaES(String nombreReconocida)
    {
        this.nombreReconocidaES = nombreReconocida;
    }

    public String getNombreReconocidaCA()
    {
        return nombreReconocidaCA;
    }

    public void setNombreReconocidaCA(String nombreCAReconocida)
    {
        this.nombreReconocidaCA = nombreCAReconocida;
    }

    public String getNombreReconocidaEN()
    {
        return nombreReconocidaEN;
    }

    public void setNombreReconocidaEN(String nombreENReconocida)
    {
        this.nombreReconocidaEN = nombreENReconocida;
    }

    public Integer getTcoId()
    {
        return tcoId;
    }

    public void setTcoId(Integer tcoId)
    {
        this.tcoId = tcoId;
    }

    public String getParcial()
    {
        return parcial;
    }

    public void setParcial(String parcial)
    {
        this.parcial = parcial;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Integer getTitulacionCursada()
    {
        return titulacionCursada;
    }

    public void setTitulacionCursada(Integer titulacionCursada)
    {
        this.titulacionCursada = titulacionCursada;
    }

    public Integer getTitulacionReconocida()
    {
        return titulacionReconocida;
    }

    public void setTitulacionReconocida(Integer titulacionReconocida)
    {
        this.titulacionReconocida = titulacionReconocida;
    }

    public EstudioTodos getEstudioCursado()
    {
        return estudioCursado;
    }

    public void setEstudioCursado(EstudioTodos estudioCursado)
    {
        this.estudioCursado = estudioCursado;
    }

    public EstudioTodos getEstudioReconocido()
    {
        return estudioReconocido;
    }

    public void setEstudioReconocido(EstudioTodos estudioReconocido)
    {
        this.estudioReconocido = estudioReconocido;
    }

    public Short getCursadaEnLleu()
    {
        return cursadaEnLleu;
    }

    public void setCursadaEnLleu(Short cursadaEnLleu)
    {
        this.cursadaEnLleu = cursadaEnLleu;
    }
}