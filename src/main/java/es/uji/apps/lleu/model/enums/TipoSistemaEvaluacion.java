package es.uji.apps.lleu.model.enums;

/**
 * Utilizamos para diferencia si el sistema de evaluación es previo a 2011 o el estado no es finalizado o traducido
 * (LP) y todos los demás (LE) (ver lleu_evaluacion_g cursor listaPrueba y listaEvaluacionGuia
 */
public enum TipoSistemaEvaluacion
{
    LISTAPRUEBAS("LP"),
    LISTAEVALUACION("LE");

    private String value;

    TipoSistemaEvaluacion(String value) {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return this.value;
    }
}
