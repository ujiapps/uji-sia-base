package es.uji.apps.lleu.model.enums;


public enum  TipoDocencia
{
    GRADO("G"),MASTERES("M"),DOCTORADO("D");

    String value;

    TipoDocencia(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }
}
