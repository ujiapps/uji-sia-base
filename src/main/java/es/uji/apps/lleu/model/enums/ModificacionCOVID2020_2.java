package es.uji.apps.lleu.model.enums;

import java.util.HashMap;
import java.util.Map;

public enum ModificacionCOVID2020_2 {
    ESTUDIO_201(201, "https://www.uji.es/upo/rest/contenido/670374854/raw?idioma=ca"),
    ESTUDIO_202(202, "https://www.uji.es/upo/rest/contenido/670374860/raw?idioma=ca"),
    ESTUDIO_203(203, "https://www.uji.es/upo/rest/contenido/670375639/raw?idioma=ca"),
    ESTUDIO_204(204, "https://www.uji.es/upo/rest/contenido/670375675/raw?idioma=ca"),
    ESTUDIO_205(205, "https://www.uji.es/upo/rest/contenido/670375649/raw?idioma=ca"),
    ESTUDIO_206(206, "https://www.uji.es/upo/rest/contenido/670375681/raw?idioma=ca"),
    ESTUDIO_207(207, "https://www.uji.es/upo/rest/contenido/670375686/raw?idioma=ca"),
    ESTUDIO_208(208, "https://www.uji.es/upo/rest/contenido/670370947/raw?idioma=ca"),
    ESTUDIO_241(241, "https://www.uji.es/upo/rest/contenido/670370888/raw?idioma=ca"),
    ESTUDIO_210(210, "https://www.uji.es/upo/rest/contenido/670374760/raw?idioma=ca"),
    ESTUDIO_211(211, "https://www.uji.es/upo/rest/contenido/670374824/raw?idioma=ca"),
    ESTUDIO_212(212, "https://www.uji.es/upo/rest/contenido/670374833/raw?idioma=ca"),
    ESTUDIO_213(213, "https://www.uji.es/upo/rest/contenido/670374804/raw?idioma=ca"),
    ESTUDIO_214(214, "https://www.uji.es/upo/rest/contenido/670374768/raw?idioma=ca"),
    ESTUDIO_219(219, "https://www.uji.es/upo/rest/contenido/670372928/raw?idioma=ca"),
    ESTUDIO_220(220, "https://www.uji.es/upo/rest/contenido/670370925/raw?idioma=ca"),
    ESTUDIO_221(221, "https://www.uji.es/upo/rest/contenido/670370930/raw?idioma=ca"),
    ESTUDIO_222(222, "https://www.uji.es/upo/rest/contenido/670370919/raw?idioma=ca"),
    ESTUDIO_223(223, "https://www.uji.es/upo/rest/contenido/670370942/raw?idioma=ca"),
    ESTUDIO_224(224, "https://www.uji.es/upo/rest/contenido/670370937/raw?idioma=ca"),
    ESTUDIO_225(225, "https://www.uji.es/upo/rest/contenido/670370912/raw?idioma=ca"),
    ESTUDIO_227(227, "https://www.uji.es/upo/rest/contenido/670370902/raw?idioma=ca"),
    ESTUDIO_228(228, "https://www.uji.es/upo/rest/contenido/670370907/raw?idioma=ca"),
    ESTUDIO_229(229, "https://www.uji.es/upo/rest/contenido/670372922/raw?idioma=ca"),
    ESTUDIO_230(230, "https://www.uji.es/upo/rest/contenido/670372917/raw?idioma=ca"),
    ESTUDIO_231(231, "https://www.uji.es/upo/rest/contenido/670370896/raw?idioma=ca"),
    ESTUDIO_232(232, "https://www.uji.es/upo/rest/contenido/670374848/raw?idioma=ca"),
    ESTUDIO_233(233, "https://www.uji.es/upo/rest/contenido/670375654/raw?idioma=ca"),
    ESTUDIO_234(234, "https://www.uji.es/upo/rest/contenido/670375660/raw?idioma=ca"),
    ESTUDIO_235(235, "https://www.uji.es/upo/rest/contenido/670372922/raw?idioma=ca"),
    ESTUDIO_236(236, "https://www.uji.es/upo/rest/contenido/670374754/raw?idioma=ca"),
    ESTUDIO_237(237, "https://www.uji.es/upo/rest/contenido/670375670/raw?idioma=ca"),
    ESTUDIO_238(238, "https://www.uji.es/upo/rest/contenido/670375665/raw?idioma=ca"),
    ESTUDIO_239(239, "https://www.uji.es/upo/rest/contenido/670370902/raw?idioma=ca"),
    ESTUDIO_242(242, "https://www.uji.es/upo/rest/contenido/670370937/raw?idioma=ca"),
    ESTUDIO_243(243, "https://www.uji.es/upo/rest/contenido/670374768/raw?idioma=ca"),
    ESTUDIO_42145(42145, "https://www.uji.es/upo/rest/contenido/670381371/raw?idioma=ca"),
    ESTUDIO_42150(42150, "https://www.uji.es/upo/rest/contenido/670381499/raw?idioma=ca"),
    ESTUDIO_42152(42152, "https://www.uji.es/upo/rest/contenido/670381509/raw?idioma=ca"),
    ESTUDIO_42159(42159, "https://www.uji.es/upo/rest/contenido/670381504/raw?idioma=ca"),
    ESTUDIO_42164(42164, "https://www.uji.es/upo/rest/contenido/670381417/raw?idioma=ca"),
    ESTUDIO_42168(42168, "https://www.uji.es/upo/rest/contenido/670381272/raw?idioma=ca"),
    ESTUDIO_42177(42177, "https://www.uji.es/upo/rest/contenido/670381494/raw?idioma=ca"),
    ESTUDIO_42179(42179, "https://www.uji.es/upo/rest/contenido/670381296/raw?idioma=ca"),
    ESTUDIO_42184(42184, "https://www.uji.es/upo/rest/contenido/670381382/raw?idioma=ca"),
    ESTUDIO_42189(42189, "https://www.uji.es/upo/rest/contenido/670381514/raw?idioma=ca"),
    ESTUDIO_42564(42564, "https://www.uji.es/upo/rest/contenido/670381417/raw?idioma=ca");


    private Integer value;
    private String descripcion;

    ModificacionCOVID2020_2(Integer value, String descripcion) {
        this.value = value;
        this.descripcion = descripcion;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    private static final Map<Integer, ModificacionCOVID2020_2> MOD_BY_VALUE = new HashMap<>();

    static {
        for (ModificacionCOVID2020_2 e : values()) {
            MOD_BY_VALUE.put(e.value, e);
        }
    }

    public static ModificacionCOVID2020_2 valueOfNumber(Integer value) {
        return MOD_BY_VALUE.get(value);
    }

};
