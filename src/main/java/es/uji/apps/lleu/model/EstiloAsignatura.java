package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ESTILO_ASIGNATURAS")
public class EstiloAsignatura implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name="AMBITO")
    private String ambito;

    @Column(name="NOMBRE_CA")
    private String nombreCA;

    @Column(name="NOMBRES_ES")
    private String nombreES;

    @Column(name="NOMBRE_EN")
    private String nombreEN;

    @Column(name="CREDITOS")
    private Integer creditos;

    @Column(name="TIPO")
    private String tipo;

    @Column(name="CUR_ID")
    private Integer curso;

    @Column(name="SEMESTRE")
    private String semestre;

    @Column(name="CARACTER")
    private String caracter;

    @Column(name="HARMONIZADA")
    private String harmonizada;

    @Column(name="IDIOMA")
    private String idioma;

    public Integer getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca) {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Long estudioId) {
        this.estudioId = estudioId;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public String getNombreCA() {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA) {
        this.nombreCA = nombreCA;
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public String getNombreEN() {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN) {
        this.nombreEN = nombreEN;
    }

    public Integer getCreditos() {
        return creditos;
    }

    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getCurso() {
        return curso;
    }

    public void setCurso(Integer curso) {
        this.curso = curso;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCaracter() {
        return caracter;
    }

    public void setCaracter(String caracter) {
        this.caracter = caracter;
    }

    public String getHarmonizada() {
        return harmonizada;
    }

    public void setHarmonizada(String harmonizada) {
        this.harmonizada = harmonizada;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }
}
