package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_PRESENTACION_M")
public class PresentacionMaster implements Serializable
{
    @Id
    @Column(name ="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name ="CURSO_ACA")
    private Integer cursoAca;

    @Column(name ="COORDINACION_CA")
    private String coordinacionCA;

    @Column(name ="COORDINACION_ES")
    private String coordinacionES;

    @Column(name ="COORDINACION_EN")
    private String coordinacionEN;

    @Column(name ="UNIV_COORD_CA")
    private String univCoordCA;
    @Column(name ="UNIV_COORD_ES")
    private String univCoordES;
    @Column(name ="UNIV_COORD_EN")
    private String univCoordEN;

    @Column(name ="INTERUNIV_CON_CA")
    private String interunivConCA;
    @Column(name ="INTERUNIV_CON_ES")
    private String interunivConES;
    @Column(name ="INTERUNIV_CON_EN")
    private String interunivConEN;

    @Column(name ="DURACION_CA")
    private String duracionCA;
    @Column(name ="DURACION_ES")
    private String duracionES;
    @Column(name ="DURACION_EN")
    private String duracionEN;

    @Column(name ="FECHAS_CA")
    private String fechasCA;
    @Column(name ="FECHAS_ES")
    private String fechasES;
    @Column(name ="FECHAS_EN")
    private String fechasEN;

    @Column(name ="DIRIGIDO_A_CA")
    private String dirigidoACA;
    @Column(name ="DIRIGIDO_A_ES")
    private String dirigidoAES;
    @Column(name ="DIRIGIDO_A_EN")
    private String dirigidoAEN;

    @Column(name ="DESCRIPCION_CA")
    private String descripcionCA;
    @Column(name ="DESCRIPCION_ES")
    private String descripcionES;
    @Column(name ="DESCRIPCION_EN")
    private String descripcionEN;

    @Column(name ="ENTIDADES_CA")
    private String entidadesCA;
    @Column(name ="ENTIDADES_ES")
    private String entidadesES;
    @Column(name ="ENTIDADES_EN")
    private String entidadesEN;

    @Column(name ="OBJETIVOS_CA")
    private String objetivosCA;
    @Column(name ="OBJETIVOS_ES")
    private String objetivosES;
    @Column(name ="OBJETIVOS_EN")
    private String objetivosEN;

    @Column(name ="PROGRAMA_CA")
    private String programaCA;
    @Column(name ="PROGRAMA_ES")
    private String programaES;
    @Column(name ="PROGRAMA_EN")
    private String programaEN;

    @Column(name ="PROFESORADO_CA")
    private String profesoradoCA;
    @Column(name ="PROFESORADO_ES")
    private String profesoradoES;
    @Column(name ="PROFESORADO_EN")
    private String profesoradoEN;

    @Column(name ="CRITERIOS_CA")
    private String criteriosCA;
    @Column(name ="CRITERIOS_ES")
    private String criteriosES;
    @Column(name ="CRITERIOS_EN")
    private String criteriosEN;

    @Column(name ="OTROS_CA")
    private String otrosCA;
    @Column(name ="OTROS_ES")
    private String otrosES;
    @Column(name ="OTROS_EN")
    private String otrosEN;

    @Column(name ="PLAZAS_CA")
    private String plazasCA;
    @Column(name ="PLAZAS_ES")
    private String plazasES;
    @Column(name ="PLAZAS_EN")
    private String plazasEN;

    @Column(name ="INFO_CA")
    private String infoCA;
    @Column(name ="INFO_ES")
    private String infoES;
    @Column(name ="INFO_EN")
    private String infoEN;

    @Column(name ="WEB")
    private String web;

    @Column(name ="CREDITOS")
    private Float creditos;

    @Column(name="IMAGEN")
    private String imagen;

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getCoordinacionCA()
    {
        return coordinacionCA;
    }

    public void setCoordinacionCA(String coordinacionCA)
    {
        this.coordinacionCA = coordinacionCA;
    }

    public String getCoordinacionES()
    {
        return coordinacionES;
    }

    public void setCoordinacionES(String coordinacionES)
    {
        this.coordinacionES = coordinacionES;
    }

    public String getCoordinacionEN()
    {
        return coordinacionEN;
    }

    public void setCoordinacionEN(String coordinacionEN)
    {
        this.coordinacionEN = coordinacionEN;
    }

    public String getUnivCoordCA()
    {
        return univCoordCA;
    }

    public void setUnivCoordCA(String univCoordCA)
    {
        this.univCoordCA = univCoordCA;
    }

    public String getUnivCoordES()
    {
        return univCoordES;
    }

    public void setUnivCoordES(String univCoordES)
    {
        this.univCoordES = univCoordES;
    }

    public String getUnivCoordEN()
    {
        return univCoordEN;
    }

    public void setUnivCoordEN(String univCoordEN)
    {
        this.univCoordEN = univCoordEN;
    }

    public String getInterunivConCA()
    {
        return interunivConCA;
    }

    public void setInterunivConCA(String interunivConCA)
    {
        this.interunivConCA = interunivConCA;
    }

    public String getInterunivConES()
    {
        return interunivConES;
    }

    public void setInterunivConES(String interunivConES)
    {
        this.interunivConES = interunivConES;
    }

    public String getInterunivConEN()
    {
        return interunivConEN;
    }

    public void setInterunivConEN(String interunivConEN)
    {
        this.interunivConEN = interunivConEN;
    }

    public String getDuracionCA()
    {
        return duracionCA;
    }

    public void setDuracionCA(String duracionCA)
    {
        this.duracionCA = duracionCA;
    }

    public String getDuracionES()
    {
        return duracionES;
    }

    public void setDuracionES(String duracionES)
    {
        this.duracionES = duracionES;
    }

    public String getDuracionEN()
    {
        return duracionEN;
    }

    public void setDuracionEN(String duracionEN)
    {
        this.duracionEN = duracionEN;
    }

    public String getFechasCA()
    {
        return fechasCA;
    }

    public void setFechasCA(String fechasCA)
    {
        this.fechasCA = fechasCA;
    }

    public String getFechasES()
    {
        return fechasES;
    }

    public void setFechasES(String fechasES)
    {
        this.fechasES = fechasES;
    }

    public String getFechasEN()
    {
        return fechasEN;
    }

    public void setFechasEN(String fechasEN)
    {
        this.fechasEN = fechasEN;
    }

    public String getDirigidoACA()
    {
        return dirigidoACA;
    }

    public void setDirigidoACA(String dirigidoACA)
    {
        this.dirigidoACA = dirigidoACA;
    }

    public String getDirigidoAES()
    {
        return dirigidoAES;
    }

    public void setDirigidoAES(String dirigidoAES)
    {
        this.dirigidoAES = dirigidoAES;
    }

    public String getDirigidoAEN()
    {
        return dirigidoAEN;
    }

    public void setDirigidoAEN(String dirigidoAEN)
    {
        this.dirigidoAEN = dirigidoAEN;
    }

    public String getDescripcionCA()
    {
        return descripcionCA;
    }

    public void setDescripcionCA(String descripcionCA)
    {
        this.descripcionCA = descripcionCA;
    }

    public String getDescripcionES()
    {
        return descripcionES;
    }

    public void setDescripcionES(String descripcionES)
    {
        this.descripcionES = descripcionES;
    }

    public String getDescripcionEN()
    {
        return descripcionEN;
    }

    public void setDescripcionEN(String descripcionEN)
    {
        this.descripcionEN = descripcionEN;
    }

    public String getEntidadesCA()
    {
        return entidadesCA;
    }

    public void setEntidadesCA(String entidadesCA)
    {
        this.entidadesCA = entidadesCA;
    }

    public String getEntidadesES()
    {
        return entidadesES;
    }

    public void setEntidadesES(String entidadesES)
    {
        this.entidadesES = entidadesES;
    }

    public String getEntidadesEN()
    {
        return entidadesEN;
    }

    public void setEntidadesEN(String entidadesEN)
    {
        this.entidadesEN = entidadesEN;
    }

    public String getObjetivosCA()
    {
        return objetivosCA;
    }

    public void setObjetivosCA(String objetivosCA)
    {
        this.objetivosCA = objetivosCA;
    }

    public String getObjetivosES()
    {
        return objetivosES;
    }

    public void setObjetivosES(String objetivosES)
    {
        this.objetivosES = objetivosES;
    }

    public String getObjetivosEN()
    {
        return objetivosEN;
    }

    public void setObjetivosEN(String objetivosEN)
    {
        this.objetivosEN = objetivosEN;
    }

    public String getProgramaCA()
    {
        return programaCA;
    }

    public void setProgramaCA(String programaCA)
    {
        this.programaCA = programaCA;
    }

    public String getProgramaES()
    {
        return programaES;
    }

    public void setProgramaES(String programaES)
    {
        this.programaES = programaES;
    }

    public String getProgramaEN()
    {
        return programaEN;
    }

    public void setProgramaEN(String programaEN)
    {
        this.programaEN = programaEN;
    }

    public String getProfesoradoCA()
    {
        return profesoradoCA;
    }

    public void setProfesoradoCA(String profesoradoCA)
    {
        this.profesoradoCA = profesoradoCA;
    }

    public String getProfesoradoES()
    {
        return profesoradoES;
    }

    public void setProfesoradoES(String profesoradoES)
    {
        this.profesoradoES = profesoradoES;
    }

    public String getProfesoradoEN()
    {
        return profesoradoEN;
    }

    public void setProfesoradoEN(String profesoradoEN)
    {
        this.profesoradoEN = profesoradoEN;
    }

    public String getCriteriosCA()
    {
        return criteriosCA;
    }

    public void setCriteriosCA(String criteriosCA)
    {
        this.criteriosCA = criteriosCA;
    }

    public String getCriteriosES()
    {
        return criteriosES;
    }

    public void setCriteriosES(String criteriosES)
    {
        this.criteriosES = criteriosES;
    }

    public String getCriteriosEN()
    {
        return criteriosEN;
    }

    public void setCriteriosEN(String criteriosEN)
    {
        this.criteriosEN = criteriosEN;
    }

    public String getOtrosCA()
    {
        return otrosCA;
    }

    public void setOtrosCA(String otrosCA)
    {
        this.otrosCA = otrosCA;
    }

    public String getOtrosES()
    {
        return otrosES;
    }

    public void setOtrosES(String otrosES)
    {
        this.otrosES = otrosES;
    }

    public String getOtrosEN()
    {
        return otrosEN;
    }

    public void setOtrosEN(String otrosEN)
    {
        this.otrosEN = otrosEN;
    }

    public String getPlazasCA()
    {
        return plazasCA;
    }

    public void setPlazasCA(String plazasCA)
    {
        this.plazasCA = plazasCA;
    }

    public String getPlazasES()
    {
        return plazasES;
    }

    public void setPlazasES(String plazasES)
    {
        this.plazasES = plazasES;
    }

    public String getPlazasEN()
    {
        return plazasEN;
    }

    public void setPlazasEN(String plazasEN)
    {
        this.plazasEN = plazasEN;
    }

    public String getInfoCA()
    {
        return infoCA;
    }

    public void setInfoCA(String infoCA)
    {
        this.infoCA = infoCA;
    }

    public String getInfoES()
    {
        return infoES;
    }

    public void setInfoES(String infoES)
    {
        this.infoES = infoES;
    }

    public String getInfoEN()
    {
        return infoEN;
    }

    public void setInfoEN(String infoEN)
    {
        this.infoEN = infoEN;
    }

    public String getWeb()
    {
        return web;
    }

    public void setWeb(String web)
    {
        this.web = web;
    }

    public Float getCreditos()
    {
        return creditos;
    }

    public void setCreditos(Float creditos)
    {
        this.creditos = creditos;
    }

    public String getImagen()
    {
        return imagen;
    }

    public void setImagen(String imagen)
    {
        this.imagen = imagen;
    }
}
