package es.uji.apps.lleu.model;

import com.mysema.query.annotations.QueryProjection;

public class EstudioPortada {
    private Long estudioId;

    private String tipoEstudio;

    private String nombreCa;

    private String nombreEs;

    private String nombreEn;

    private Integer cursoAca;

    private Long planId;

    private String nombrePLanCa;

    private String nombrePlanEs;

    private String nombrePlanEn;

    private Long ubicacionId;

    private String ubicacionCa;

    private String ubicacionEs;

    private String ubicacionEn;

    public EstudioPortada(Long estudioId, String tipoEstudio, String nombreCa, String nombreEs, String nombreEn, Integer cursoAca, Long planId, String nombrePLanCa, String nombrePlanEs, String nombrePlanEn, Long ubicacionId, String ubicacionCa, String ubicacionEs, String ubicacionEn) {
        this.estudioId = estudioId;
        this.tipoEstudio = tipoEstudio;
        this.nombreCa = nombreCa;
        this.nombreEs = nombreEs;
        this.nombreEn = nombreEn;
        this.cursoAca = cursoAca;
        this.planId = planId;
        this.nombrePLanCa = nombrePLanCa;
        this.nombrePlanEs = nombrePlanEs;
        this.nombrePlanEn = nombrePlanEn;
        this.ubicacionId = ubicacionId;
        this.ubicacionCa = ubicacionCa;
        this.ubicacionEs = ubicacionEs;
        this.ubicacionEn = ubicacionEn;
    }

    public EstudioPortada() {
    }

    public Long getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Long estudioId) {
        this.estudioId = estudioId;
    }

    public String getNombreCa() {
        return nombreCa;
    }

    public void setNombreCa(String nombreCa) {
        this.nombreCa = nombreCa;
    }

    public String getNombreEs() {
        return nombreEs;
    }

    public void setNombreEs(String nombreEs) {
        this.nombreEs = nombreEs;
    }

    public String getNombreEn() {
        return nombreEn;
    }

    public void setNombreEn(String nombreEn) {
        this.nombreEn = nombreEn;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public String getNombrePLanCa() {
        return nombrePLanCa;
    }

    public void setNombrePLanCa(String nombrePLanCa) {
        this.nombrePLanCa = nombrePLanCa;
    }

    public String getNombrePlanEs() {
        return nombrePlanEs;
    }

    public void setNombrePlanEs(String nombrePlanEs) {
        this.nombrePlanEs = nombrePlanEs;
    }

    public String getNombrePlanEn() {
        return nombrePlanEn;
    }

    public void setNombrePlanEn(String nombrePlanEn) {
        this.nombrePlanEn = nombrePlanEn;
    }

    public Long getUbicacionId() {
        return ubicacionId;
    }

    public void setUbicacionId(Long ubicacionId) {
        this.ubicacionId = ubicacionId;
    }

    public String getUbicacionCa() {
        return ubicacionCa;
    }

    public void setUbicacionCa(String ubicacionCa) {
        this.ubicacionCa = ubicacionCa;
    }

    public String getUbicacionEs() {
        return ubicacionEs;
    }

    public void setUbicacionEs(String ubicacionEs) {
        this.ubicacionEs = ubicacionEs;
    }

    public String getUbicacionEn() {
        return ubicacionEn;
    }

    public void setUbicacionEn(String ubicacionEn) {
        this.ubicacionEn = ubicacionEn;
    }

    public String getTipoEstudio() {
        return tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio) {
        this.tipoEstudio = tipoEstudio;
    }

    public Integer getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca) {
        this.cursoAca = cursoAca;
    }
}


