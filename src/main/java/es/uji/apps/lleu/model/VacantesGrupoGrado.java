package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_VACANTES_GRUPO_G")
public class VacantesGrupoGrado implements Serializable
{
    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cusoAca;

    @Id
    @Column(name = "GRP_ID")
    private String grupo;

    @Column(name ="VACANTES")
    private String vacantes;

    @Column(name = "VACANTES_NUEVOS")
    private String vacantesNuevos;

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public Integer getCusoAca()
    {
        return cusoAca;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public String getVacantes()
    {
        return vacantes;
    }

    public String getVacantesNuevos()
    {
        return vacantesNuevos;
    }
}