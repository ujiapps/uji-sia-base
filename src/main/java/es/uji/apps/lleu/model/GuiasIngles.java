package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_GUIAS_INGLES")
public class GuiasIngles implements Serializable
{
    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name = "CURSO_ACA_INI")
    private Integer cursoAcaIni;

    @Id
    @Column(name = "CURSO_ACA_FIN")
    private Integer cursoAcaFin;

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoAcaIni()
    {
        return cursoAcaIni;
    }

    public void setCursoAcaIni(Integer cursoAcaIni)
    {
        this.cursoAcaIni = cursoAcaIni;
    }

    public Integer getCursoAcaFin()
    {
        return cursoAcaFin;
    }

    public void setCursoAcaFin(Integer cursoAcaFin)
    {
        this.cursoAcaFin = cursoAcaFin;
    }
}
