package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.uji.apps.lleu.utils.IPersona;

@Entity
@Table(name = "LLEU_EXT_PROFESORES_G")
public class ProfesorGrado implements Serializable, IPersona
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="UBICACION_ID")
    private Long ubicacionId;

    @Id
    @Column(name="PER_ID")
    private Long perId;

    @Column(name="PER_NOMBRE")
    private String perNombre;

    @Column(name="DEPARTAMENTO_CA")
    private String departamentoCA;

    @Column(name="DEPARTAMENTO_EN")
    private String departamentoEN;

    @Column(name="DEPARTAMENTO_ES")
    private String departamentoES;

    @Column(name="CATEGORIA_ES")
    private String categoriaES;

    @Column(name="CATEGORIA_CA")
    private String categoriaCA;

    @Column(name="URL")
    private String url;


    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Long getUbicacionId()
    {
        return ubicacionId;
    }

    public void setUbicacionId(Long ubicacionId)
    {
        this.ubicacionId = ubicacionId;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public String getPerNombre()
    {
        return perNombre;
    }

    public void setPerNombre(String perNombre)
    {
        this.perNombre = perNombre;
    }

    public String getDepartamentoCA()
    {
        return departamentoCA;
    }

    public void setDepartamentoCA(String departamentoCA)
    {
        this.departamentoCA = departamentoCA;
    }

    public String getDepartamentoEN()
    {
        return departamentoEN;
    }

    public void setDepartamentoEN(String departamentoEN)
    {
        this.departamentoEN = departamentoEN;
    }

    public String getDepartamentoES()
    {
        return departamentoES;
    }

    public void setDepartamentoES(String departamentoES)
    {
        this.departamentoES = departamentoES;
    }

    public String getCategoriaES()
    {
        return categoriaES;
    }

    public void setCategoriaES(String categoriaES)
    {
        this.categoriaES = categoriaES;
    }

    public String getCategoriaCA()
    {
        return categoriaCA;
    }

    public void setCategoriaCA(String categoriaCA)
    {
        this.categoriaCA = categoriaCA;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
}
