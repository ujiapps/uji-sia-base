package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "LLEU_EXT_SIS_EVALUA_GRADO")
public class SistemaEvaluacionGrado implements Serializable
{
    /**
     * El tipo me lo invento bastante. He puesto en una misma vista la lista de Pruebas y el listado de sistema
     * de evaluación de gdo asignaturas. En el primer caso tipo es "LP" y en el segundo caso es "LE"
     */
    @Id
    @Column(name="TIPO")
    private String tipo;

    @Id
    @Column(name="EVAL_ID")
    private Long evalId;

    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Column(name="CURSO_ACA_FIN")
    private String cursoAcaFin;

    @Id
    @Column(name="NOMBRE_CA")
    private String nombreCA;

    @Column(name="NOMBRE_ES")
    private String nombreES;

    @Column(name="NOMBRE_EN")
    private String nombreUK;

    @Column(name="PONDERACION")
    private Float ponderacion;

    @Id
    @Column(name="ORDEN")
    private Integer orden;

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Long getEvalId()
    {
        return evalId;
    }

    public void setEvalId(Long evalId)
    {
        this.evalId = evalId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getCursoAcaFin()
    {
        return cursoAcaFin;
    }

    public void setCursoAcaFin(String cursoAcaFin)
    {
        this.cursoAcaFin = cursoAcaFin;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreUK()
    {
        return nombreUK;
    }

    public void setNombreUK(String nombreUK)
    {
        this.nombreUK = nombreUK;
    }

    public Float getPonderacion()
    {
        return ponderacion;
    }

    public void setPonderacion(Float ponderacion)
    {
        this.ponderacion = ponderacion;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }
}
