package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_ASIGNATURA_COMENTARIOS")
public class AsignaturaComentarios implements Serializable
{
    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;
    @Id
    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;
    @Id
    @Column(name = "CURSO_ID")
    private Long cursoId;

    @Id
    @Column(name = "ORDEN")
    private Integer orden;

    @Id
    @Column(name = "COMENTARIO_CA")
    private String comentarioCA;
    @Column(name = "COMENTARIO_ES")
    private String comentarioES;
    @Column(name = "COMENTARIO_EN")
    private String comentarioEN;

    private String tipo;

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }

    public String getComentarioCA()
    {
        return comentarioCA;
    }

    public void setComentarioCA(String comentarioCA)
    {
        this.comentarioCA = comentarioCA;
    }

    public String getComentarioES()
    {
        return comentarioES;
    }

    public void setComentarioES(String comentarioES)
    {
        this.comentarioES = comentarioES;
    }

    public String getComentarioEN()
    {
        return comentarioEN;
    }

    public void setComentarioEN(String comentarioEN)
    {
        this.comentarioEN = comentarioEN;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }
}
