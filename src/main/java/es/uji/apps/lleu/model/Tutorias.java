package es.uji.apps.lleu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "LLEU_EXT_TUTORIAS")
public class Tutorias implements Serializable
{

    @Id
    @Column(name = "PER_ID")
    private Long perId;
    @Column(name = "PER_NOMBRE")
    private String personaNombre;

    @Id
    @Column(name = "H_INI")
    private String horaInicio;
    @Column(name = "H_FIN")
    private String horaFin;

    @Id
    @Column(name = "F_INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIni;

    @Column(name = "F_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;

    @Id
    @Column(name = "ID_DIA_SEM")
    private String idDiaSemana;
    @Column(name = "DIA_SEM")
    private String diaSemana;
    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;
    private String semestre;
    private String tutorias;

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public String getPersonaNombre()
    {
        return personaNombre;
    }

    public void setPersonaNombre(String personaNombre)
    {
        this.personaNombre = personaNombre;
    }

    public String getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(String horaFin)
    {
        this.horaFin = horaFin;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public Date getFechaIni()
    {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni)
    {
        this.fechaIni = fechaIni;
    }

    public String getIdDiaSemana()
    {
        return idDiaSemana;
    }

    public void setIdDiaSemana(String idDiaSemana)
    {
        this.idDiaSemana = idDiaSemana;
    }

    public String getDiaSemana()
    {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana)
    {
        this.diaSemana = diaSemana;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getTutorias()
    {
        return tutorias;
    }

    public void setTutorias(String tutorias)
    {
        this.tutorias = tutorias;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }
}
