package es.uji.apps.lleu.model.enums;

import java.util.HashMap;
import java.util.Map;

public enum ModificacionCOVID2019
{
    ESTUDIO_201(201, "https://ujiapps.uji.es/ade/rest/storage/LOBYZKNPMXP5YJQFXDULUMZOCESMDYDR"),
    ESTUDIO_202(202, "https://ujiapps.uji.es/ade/rest/storage/GFEYSSFDVBWS0Z1UXWNWPIRJADRVDEPU"),
    ESTUDIO_203(203, "https://ujiapps.uji.es/ade/rest/storage/Y67OXDWNJCNZIOOFEHIDWLUEGTBGOVEQ"),
    ESTUDIO_204(204, "https://ujiapps.uji.es/ade/rest/storage/HM1UZ2MEV2VQGEHZTG3R7AHPX55EXE1L"),
    ESTUDIO_205(205, "https://ujiapps.uji.es/ade/rest/storage/VCCKWNDPDHEK4DKXNNZEEQZRNTVADVST"),
    ESTUDIO_206(206, "https://ujiapps.uji.es/ade/rest/storage/16YC6WKLQAJRCI0HCOBMRAIN6UDL8VMA"),
    ESTUDIO_207(207, "https://ujiapps.uji.es/ade/rest/storage/YOE10SXYG1VHVZWF8NLJ5WSMCSS0BW2X"),
    ESTUDIO_208(208, "https://ujiapps.uji.es/ade/rest/storage/3COGJHBSL49LMOMLKYEFTEXGGMJU5GLT"),
    ESTUDIO_209(209, "https://ujiapps.uji.es/ade/rest/storage/ND43OZ6ONXWANZDTP138BJISNCXHIN0Q"),
    ESTUDIO_210(210, "https://ujiapps.uji.es/ade/rest/storage/2RWP4IBG5KV2WTCCKWSPZIQQ1RQOYBZI"),
    ESTUDIO_211(211, "https://ujiapps.uji.es/ade/rest/storage/ZQLTZBGKWGGCATAR6ZNZHJAOG7TIMLGP"),
    ESTUDIO_212(212, "https://ujiapps.uji.es/ade/rest/storage/HSCKFE5IW6DXHZ2WDWFJQ4WEP99NCET5"),
    ESTUDIO_213(213, "https://ujiapps.uji.es/ade/rest/storage/MQAYDTWIW8CKUH9LWGKDFUT5KTMVUKZS"),
    ESTUDIO_214(214, "https://ujiapps.uji.es/ade/rest/storage/0RIIBULXJJG7GRMUABFOCVQWISM10KWD"),
    ESTUDIO_217(217, "https://ujiapps.uji.es/ade/rest/storage/DI1BK23DUURJVZ8DDGF9KEKUHGJB1KSX"),
    ESTUDIO_218(218, "https://ujiapps.uji.es/ade/rest/storage/WOVF0H1A83QEBVLFE0IJR5XJPXC8RYCR"),
    ESTUDIO_219(219, "https://ujiapps.uji.es/ade/rest/storage/OWUKJB0T63JDBDQIJWEGSFJQRKTNMGLQ"),
    ESTUDIO_220(220, "https://ujiapps.uji.es/ade/rest/storage/PPF0RNZYAHOEUAHZDGPBOYW2OA5CCOYO"),
    ESTUDIO_221(221, "https://ujiapps.uji.es/ade/rest/storage/HR9XO8UGX9KB2OUEKZEICXEDJ6J6XENS"),
    ESTUDIO_222(222, "https://ujiapps.uji.es/ade/rest/storage/AYZAWEEC16YF6IVDRIJUJB5TQBTQA8UR"),
    ESTUDIO_223(223, "https://ujiapps.uji.es/ade/rest/storage/KZO6HDLZHEC4R0K3QA4GAQJ9BCYQCNZ7"),
    ESTUDIO_224(224, "https://ujiapps.uji.es/ade/rest/storage/LXARVOFWO2OKZ2QSHVLMWISYLLVP4CXV"),
    ESTUDIO_225(225, "https://ujiapps.uji.es/ade/rest/storage/7ZXJNXTMMQB3V2NKS0Y43XPSMFFLIFVV"),
    ESTUDIO_227(227, "https://ujiapps.uji.es/ade/rest/storage/JM9WEJ2QNI3NGFWXGYIYW6A2K51MB5OI"),
    ESTUDIO_228(228, "https://ujiapps.uji.es/ade/rest/storage/CM2WHSYLJ1AD1Y5TNWW0BYT2YKU92VM6"),
    ESTUDIO_229(229, "https://ujiapps.uji.es/ade/rest/storage/MYUJPMT020DL6GPFGUKOSJL1WW65C8MZ"),
    ESTUDIO_230(230, "https://ujiapps.uji.es/ade/rest/storage/LNVRWI5YASLP1M22B7QWG38CFSN0VU29"),
    ESTUDIO_231(231, "https://ujiapps.uji.es/ade/rest/storage/XU94MO27YXKF3QZRF4FL4I5H0PTGY6SQ"),
    ESTUDIO_232(232, "https://ujiapps.uji.es/ade/rest/storage/3JVZOPHC0JMLFPD6FL51BUNRJZDGWG1L"),
    ESTUDIO_233(233, "https://ujiapps.uji.es/ade/rest/storage/RWZLN9EL0K1HLZBDR08ETETOPKYCGID0"),
    ESTUDIO_234(234, "https://ujiapps.uji.es/ade/rest/storage/TEPJZYNFEHOI47MFHZ4QVKAFJWKUWGAS"),
    ESTUDIO_235(235, "https://ujiapps.uji.es/ade/rest/storage/MYUJPMT020DL6GPFGUKOSJL1WW65C8MZ"),
    ESTUDIO_236(236, "https://ujiapps.uji.es/ade/rest/storage/J4LKVXFK8UU2FICQZTTZHECZ7NEI69LG"),
    ESTUDIO_237(237, "https://ujiapps.uji.es/ade/rest/storage/WOVF0H1A83QEBVLFE0IJR5XJPXC8RYCR"),
    ESTUDIO_238(238, "https://ujiapps.uji.es/ade/rest/storage/DI1BK23DUURJVZ8DDGF9KEKUHGJB1KSX"),
    ESTUDIO_239(239, "https://ujiapps.uji.es/ade/rest/storage/JM9WEJ2QNI3NGFWXGYIYW6A2K51MB5OI"),
    ESTUDIO_42102(42102, "https://ujiapps.uji.es/ade/rest/storage/CXCN9QVFOXYVHV0MUN2LBNAP4FH73N8I"),
    ESTUDIO_42123(42123, "https://ujiapps.uji.es/ade/rest/storage/Z45MW9BWYZ9GQVNTDRTSY0ZMIGNVJKHV"),
    ESTUDIO_42139(42139, "https://ujiapps.uji.es/ade/rest/storage/OBVZQWR6FCAH0VYXGV3TIP04POI3C9NA"),
    ESTUDIO_42142(42142, "https://ujiapps.uji.es/ade/rest/storage/B7CXABJW51V4JFNHIUYEBBSRHTHHWN1X"),
    ESTUDIO_42146(42146, "https://ujiapps.uji.es/ade/rest/storage/5ZAENKH8QAMBLKNNPLTWT5OC91544PBY"),
    ESTUDIO_42150(42150, "https://ujiapps.uji.es/ade/rest/storage/PVNNQ9OYMBKC17FK8KMFVHTOWCODOW41"),
    ESTUDIO_42151(42151, "https://ujiapps.uji.es/ade/rest/storage/HQQ4IJCKVARIDUWIMQK6OWPKSG942KJJ"),
    ESTUDIO_42152(42152, "https://ujiapps.uji.es/ade/rest/storage/3CN09G4NJVJWZWKZ7EIV2JDKUKMJV4YG"),
    ESTUDIO_42154(42154, "https://ujiapps.uji.es/ade/rest/storage/9HXY53DMNNBD2EOTKUSFVJ1IERKMDPKZ"),
    ESTUDIO_42155(42155, "https://ujiapps.uji.es/ade/rest/storage/T7GJKSPOX3N7F8GMAZLYZ6VKRB1PZ5LD"),
    ESTUDIO_42157(42157, "https://ujiapps.uji.es/ade/rest/storage/HJ6RHW5ARJ0FHKYAXZGKSCQ2KIALOA33"),
    ESTUDIO_42159(42159, "https://ujiapps.uji.es/ade/rest/storage/A5HCQI7ZQVUJASNNJKXMTXIZFYIUSVKI"),
    ESTUDIO_42163(42163, "https://ujiapps.uji.es/ade/rest/storage/Q1SY4YYUAERVLVOBZLAH2FKY73GUIKOD"),
    ESTUDIO_42164(42164, "https://ujiapps.uji.es/ade/rest/storage/LQ9NYDHZXCHV9C7B1AXFDP2D13CXJCNN"),
    ESTUDIO_42166(42166, "https://ujiapps.uji.es/ade/rest/storage/F5IXH0SOS1DZ0OSMETGSEBRYVEW4PGAK"),
    ESTUDIO_42168(42168, "https://ujiapps.uji.es/ade/rest/storage/FT0ARJQRZQTWZCEWIOLVQS3LHJ8VNKAI"),
    ESTUDIO_42169(42169, "https://ujiapps.uji.es/ade/rest/storage/BRPNZY5M0SHC1YDYBEEMMCGFTODUGLQB"),
    ESTUDIO_42171(42171, "https://ujiapps.uji.es/ade/rest/storage/LQXC7OER35FMCU13B6IFVIUPSDQRCM8O"),
    ESTUDIO_42173(42173, "https://ujiapps.uji.es/ade/rest/storage/HEDWSYV4Z5PLO00GMBG3SLHZOG7HQMH8"),
    ESTUDIO_42175(42175, "https://ujiapps.uji.es/ade/rest/storage/0LUEKJY70DSCRLZE181J6LDM87OLHURG"),
    ESTUDIO_42179(42179, "https://ujiapps.uji.es/ade/rest/storage/VTNKBPDENHKAEOWROR7COQBYRSMOLTEC"),
    ESTUDIO_42180(42180, "https://ujiapps.uji.es/ade/rest/storage/KWPTTFYGMDGIXW8HMNL62USO9EKESNCN"),
    ESTUDIO_42181(42181, "https://ujiapps.uji.es/ade/rest/storage/MVMQZAQCKABADZ8GRJDF92FLYFE15R4S"),
    ESTUDIO_42184(42184, "https://ujiapps.uji.es/ade/rest/storage/6IHMHXBBVVBEHQ8WJSC1DN7AA7FJVJZ1"),
    ESTUDIO_42185(42185, "https://ujiapps.uji.es/ade/rest/storage/ED5RKH1IMFZHYEURX6QOKKZZXND1QIC7"),
    ESTUDIO_42186(42186, "https://ujiapps.uji.es/ade/rest/storage/6KJOMSQFCOJH48RVDDGBGV1A1FT6FKIH"),
    ESTUDIO_42188(42188, "https://ujiapps.uji.es/ade/rest/storage/8DQD2XUWOZ9XLILA3P76Y22JYIFMIRNV"),
    ESTUDIO_42189(42189, "https://ujiapps.uji.es/ade/rest/storage/GDXMD4GYGDIBZVHRD519OHFTYSXPAGNM"),
    ESTUDIO_42192(42192, "https://ujiapps.uji.es/ade/rest/storage/DKXJ9JQYU6ZQZJK4QMOHC8NUCDNFYQGG"),
    ESTUDIO_42194(42194, "https://ujiapps.uji.es/ade/rest/storage/DNH1N62DL2SMBJCMLQK8RLI410WO4IEU"),
    ESTUDIO_42195(42195, "https://ujiapps.uji.es/ade/rest/storage/LB1NIKEY5TB7EPFC7CB0VGD0PRQBATZW"),
    ESTUDIO_42554(42554, "https://ujiapps.uji.es/ade/rest/storage/XJM4TA55TNBPGTUJC5UNVTIQTDCCWXF7"),
    ESTUDIO_42564(42564, "https://ujiapps.uji.es/ade/rest/storage/LQ9NYDHZXCHV9C7B1AXFDP2D13CXJCNN"),
    ESTUDIO_42591(42591, "https://ujiapps.uji.es/ade/rest/storage/TTHNAXQW40ES9KQLGBR1PHY04ZWEKYE0");
    private Integer value;
    private Integer cursoAca;
    private String descripcion;

    ModificacionCOVID2019(Integer value, String descripcion)
    {
        this.value = value;
        this.descripcion = descripcion;
    }

    public Integer getValue()
    {
        return value;
    }

    public void setValue(Integer value)
    {
        this.value = value;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    private static final Map<Integer, ModificacionCOVID2019> MOD_BY_VALUE = new HashMap<>();

    static
    {
        for (ModificacionCOVID2019 e : values())
        {
            MOD_BY_VALUE.put(e.value, e);
        }
    }

    public static ModificacionCOVID2019 valueOfNumber(Integer value) {
        return MOD_BY_VALUE.get(value);
    }

};
