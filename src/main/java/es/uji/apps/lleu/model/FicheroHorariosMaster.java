package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mysema.query.annotations.QueryProjection;

@Entity
@Table(name="LLEU_EXT_FILE_HORARIOS_MASTER")
public class FicheroHorariosMaster implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="URL")
    private String url;

    @Column(name="DESCRIPCION_CA")
    private String descripcionCA;

    @Column(name="DESCRIPCION_ES")
    private String descripcionES;

    @Column(name="DESCRIPCION_EN")
    private String descripcionEN;

    @Column(name="CONTENT_TYPE")
    private String contentType;

    @Column(name="FICHERO_NOMBRE")
    private String ficheroNombre;

    @Column(name="FICHERO")
    private byte[] fichero;


    public FicheroHorariosMaster() {
        super();
    }

    /**
     * Utilizado cuando lo único que queremos obtener es un listado de los ficheros de horarios disponibles sin el
     * contenido de los propios ficheros.
     *
     * @param cursoAca
     * @param estudioId
     * @param id
     * @param descripcionCA
     * @param descripcionEN
     * @param descripcionES
     */
    @QueryProjection
    public FicheroHorariosMaster(Integer cursoAca, Long estudioId, Long id, String descripcionCA, String descripcionEN, String descripcionES) {
        this.cursoAca = cursoAca;
        this.estudioId = estudioId;
        this.id = id;
        this.descripcionCA = descripcionCA;
        this.descripcionEN = descripcionEN;
        this.descripcionES = descripcionES;

        this.url = null;
        this.contentType = null;
        this.fichero = null;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getDescripcionCA()
    {
        return descripcionCA;
    }

    public void setDescripcionCA(String descripcionCA)
    {
        this.descripcionCA = descripcionCA;
    }

    public String getDescripcionES()
    {
        return descripcionES;
    }

    public void setDescripcionES(String descripcionES)
    {
        this.descripcionES = descripcionES;
    }

    public String getDescripcionEN()
    {
        return descripcionEN;
    }

    public void setDescripcionEN(String descripcionEN)
    {
        this.descripcionEN = descripcionEN;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public byte[] getFichero()
    {
        return fichero;
    }

    public void setFichero(byte[] fichero)
    {
        this.fichero = fichero;
    }

    public String getFicheroNombre()
    {
        return ficheroNombre;
    }

    public void setFicheroNombre(String ficheroNombre)
    {
        this.ficheroNombre = ficheroNombre;
    }
}
