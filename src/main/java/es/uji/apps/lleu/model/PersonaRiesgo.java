package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "GRI_WWW", name = "WWW_PERSONAS_EN_RIESGO")
public class PersonaRiesgo implements Serializable {

    @Id
    @Column(name = "PER_ID")
    private Long perId;

    public Long getPerId() {
        return perId;
    }

    public void setPerId(Long perId) {
        this.perId = perId;
    }
}