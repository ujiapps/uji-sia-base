package es.uji.apps.lleu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="LLEU_EXT_AULAS_EXAMEN_MASTER")
public class AulasExamenesMaster implements Serializable
{

    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name="FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Id
    @Column(name="CON_ID")
    private Long convocatoriaId;

    @Id
    @Column(name="EPO_ID")
    private Long epocaId;

    @Id
    @Column(name="SEMESTRE")
    private String semestre;

    @Id
    @Column(name="INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ini;

    @Id
    @Column(name="TIPO")
    private String tipo;

    @Id
    @Column(name="aula")
    private String aula;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "CURSO_ACA", referencedColumnName = "CURSO_ACA", insertable = false, updatable = false),
            @JoinColumn(name = "ASI_ID", referencedColumnName = "ASI_ID", insertable = false, updatable = false),
            @JoinColumn(name = "FECHA", referencedColumnName = "FECHA", insertable = false, updatable = false),
            @JoinColumn(name = "CON_ID", referencedColumnName = "CON_ID", insertable = false, updatable = false),
            @JoinColumn(name = "EPO_ID", referencedColumnName = "EPO_ID", insertable = false, updatable = false),
            @JoinColumn(name = "SEMESTRE", referencedColumnName = "SEMESTRE", insertable = false, updatable = false),
            @JoinColumn(name = "INI", referencedColumnName = "INI", insertable = false, updatable = false),
            @JoinColumn(name = "TIPO", referencedColumnName = "TIPO", insertable = false, updatable = false)
            })
    private ExamenesMaster examen;


    public String getAula()
    {
        return aula;
    }

    public void setAula(String aula)
    {
        this.aula = aula;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Long getConvocatoriaId()
    {
        return convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public Long getEpocaId()
    {
        return epocaId;
    }

    public void setEpocaId(Long epocaId)
    {
        this.epocaId = epocaId;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getIni()
    {
        return ini;
    }

    public void setIni(Date ini)
    {
        this.ini = ini;
    }

    public ExamenesMaster getExamen()
    {
        return examen;
    }

    public void setExamen(ExamenesMaster examen)
    {
        this.examen = examen;
    }
}