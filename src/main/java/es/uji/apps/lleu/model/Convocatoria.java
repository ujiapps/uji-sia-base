package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_CONVOCATORIAS")
public class Convocatoria implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="NOMBRE_CA")
    private String nombreCA;
    @Column(name="NOMBRE_ES")
    private String nombreES;
    @Column(name="NOMBRE_EN")
    private String nombreEN;
    private String tipo;
    private Integer orden;
    private String extra;
    @Column(name="NOMBRE_CERT_CA")
    private String nombreCertCA;
    @Column(name="NOMBRE_CERT_ES")
    private String nombreCertES;
    @Column(name="NOMBRE_CERT_EN")
    private String nombreCertEN;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }

    public String getExtra()
    {
        return extra;
    }

    public void setExtra(String extra)
    {
        this.extra = extra;
    }

    public String getNombreCertCA()
    {
        return nombreCertCA;
    }

    public void setNombreCertCA(String nombreCertCA)
    {
        this.nombreCertCA = nombreCertCA;
    }

    public String getNombreCertES()
    {
        return nombreCertES;
    }

    public void setNombreCertES(String nombreCertES)
    {
        this.nombreCertES = nombreCertES;
    }

    public String getNombreCertEN()
    {
        return nombreCertEN;
    }

    public void setNombreCertEN(String nombreCertEN)
    {
        this.nombreCertEN = nombreCertEN;
    }

}