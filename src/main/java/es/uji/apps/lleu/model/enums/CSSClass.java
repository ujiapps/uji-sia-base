package es.uji.apps.lleu.model.enums;

/**
 * Para aplicar las clases CSS correctas en la plantilla.
 */
public enum CSSClass {

    ESTCE("estce"),
    FCJE("fcje"),
    FCHS("fchs"),
    FCS("fcs"),
    UNKNOWN("unknown");

    private String value;

    CSSClass(String value) {
        this.value = value.toLowerCase();
    }

    /**
     * Devuelve un string con el nombre de la clase en función del nombre en castellano del centro.
     *
     * @param centro el nombre del centro en castellano
     * @return String clase CSS con el nombre del centro
     */
    public static String getCSSByCentro(String centro)
    {
        if (centro == null) {
            return CSSClass.UNKNOWN.value;
        } else if (centro.toLowerCase().contains("jurídicas")) {
            return CSSClass.FCJE.value;
        } else if (centro.toLowerCase().contains("tecnología")) {
            return CSSClass.ESTCE.value;
        } else if (centro.toLowerCase().contains("salud")) {
            return CSSClass.FCS.value;
        } else if (centro.toLowerCase().contains("humanas")) {
            return CSSClass.FCHS.value;
        } else {
            return CSSClass.UNKNOWN.value;
        }
    }

    public static String getCSSByCentro(Long uestId)
    {
        if (uestId == null) {
            return CSSClass.UNKNOWN.value;
        } else if (uestId==3) {
            return CSSClass.FCJE.value;
        } else if (uestId==4) {
            return CSSClass.ESTCE.value;
        } else if (uestId==2922) {
            return CSSClass.FCS.value;
        } else if (uestId==2) {
            return CSSClass.FCHS.value;
        } else {
            return CSSClass.UNKNOWN.value;
        }
    }
};
