package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.uji.apps.lleu.utils.IPersona;

@Entity
@Table(name = "LLEU_EXT_ASIG_PROF_MASTER")
public class AsignaturaProfesorMaster implements Serializable, IPersona
{

    @Id
    @Column(name = "ASIGNATURA_ID")
    private String asiId;
    @Id
    @Column(name = "GRP_ID")
    private String grpId;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Column(name = "IDIOMA_CA")
    private String idiomaCA;

    @Column(name = "IDIOMA_ES")
    private String idiomaES;

    @Column(name = "IDIOMA_EN")
    private String idiomaEN;
    @Id
    @Column(name = "PER_ID")
    private Long perId;
    @Column(name = "NOMBRE")
    private String personaNombre;

    @Column(name = "URL")
    private String url;

    @Column(name = "MAIL")
    private String mail;

    @Column(name="NO_TIENE_TUTORIAS_EXT")
    private Boolean noTieneTutoriasProfesorExterno;


    public String getAsiId()
    {
        return asiId;
    }

    public void setAsiId(String asiId)
    {
        this.asiId = asiId;
    }

    public String getGrpId()
    {
        return grpId;
    }

    public void setGrpId(String grpId)
    {
        this.grpId = grpId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getIdiomaCA()
    {
        return idiomaCA;
    }

    public void setIdiomaCA(String idiomaCA)
    {
        this.idiomaCA = idiomaCA;
    }

    public String getIdiomaES()
    {
        return idiomaES;
    }

    public void setIdiomaES(String idiomaES)
    {
        this.idiomaES = idiomaES;
    }

    public String getIdiomaEN()
    {
        return idiomaEN;
    }

    public void setIdiomaEN(String idiomaEN)
    {
        this.idiomaEN = idiomaEN;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public String getPersonaNombre()
    {
        return personaNombre;
    }

    public void setPersonaNombre(String personaNombre)
    {
        this.personaNombre = personaNombre;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getMail()
    {
        return mail;
    }

    public void setMail(String mail)
    {
        this.mail = mail;
    }

    public Boolean getNoTieneTutoriasProfesorExterno() {
        return noTieneTutoriasProfesorExterno;
    }

    public Boolean isNoTieneTutoriasProfesorExterno() {
        return noTieneTutoriasProfesorExterno;
    }

    public void setNoTieneTutoriasProfesorExterno(Boolean noTieneTutoriasProfesorExterno) {
        this.noTieneTutoriasProfesorExterno = noTieneTutoriasProfesorExterno;
    }
}
