package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ITINERARIOS")
public class Itinerario implements Serializable
{
    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;
    @Column(name="NOMBRE_ASI_CA")
    private String nombreAsiCA;
    @Column(name="NOMBRE_ASI_ES")
    private String nombreAsiES;
    @Column(name="NOMBRE_ASI_EN")
    private String nombreAsiEN;

    private Float creditos;
    private String caracter;

    @Id
    @Column(name="CURSO_ACA_INI")
    private Integer cursoAcaIni;

    @Column(name="CURSO_ACA_FIN")
    private Integer cursoAcaFin;

    @Id
    @Column(name="ITINERARIO_ID")
    private Long itinerarioId;
    @Column(name="TIT_ID")
    private Long titulacionId;
    @Column(name= "NOMBRE_CA")
    private String nombreCA;
    @Column(name= "NOMBRE_ES")
    private String nombreES;
    @Column(name= "NOMBRE_EN")
    private String nombreEN;
    @Column(name= "CRD_OBLIGATORIOS")
    private Long creditosOB;
    @Column(name= "CRD_OPTATIVOS")
    private Long creditosOP;

    private String mencion;

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombreAsiCA()
    {
        return nombreAsiCA;
    }

    public void setNombreAsiCA(String nombreAsiCA)
    {
        this.nombreAsiCA = nombreAsiCA;
    }

    public String getNombreAsiES()
    {
        return nombreAsiES;
    }

    public void setNombreAsiES(String nombreAsiES)
    {
        this.nombreAsiES = nombreAsiES;
    }

    public String getNombreAsiEN()
    {
        return nombreAsiEN;
    }

    public void setNombreAsiEN(String nombreAsiEN)
    {
        this.nombreAsiEN = nombreAsiEN;
    }

    public Float getCreditos()
    {
        return creditos;
    }

    public void setCreditos(Float creditos)
    {
        this.creditos = creditos;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public Integer getCursoAcaIni()
    {
        return cursoAcaIni;
    }

    public void setCursoAcaIni(Integer cursoAcaIni)
    {
        this.cursoAcaIni = cursoAcaIni;
    }

    public Integer getCursoAcaFin()
    {
        return cursoAcaFin;
    }

    public void setCursoAcaFin(Integer cursoAcaFin)
    {
        this.cursoAcaFin = cursoAcaFin;
    }

    public Long getItinerarioId()
    {
        return itinerarioId;
    }

    public void setItinerarioId(Long itinerarioId)
    {
        this.itinerarioId = itinerarioId;
    }

    public Long getTitulacionId()
    {
        return titulacionId;
    }

    public void setTitulacionId(Long titulacionId)
    {
        this.titulacionId = titulacionId;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public Long getCreditosOB()
    {
        return creditosOB;
    }

    public void setCreditosOB(Long creditosOB)
    {
        this.creditosOB = creditosOB;
    }

    public Long getCreditosOP()
    {
        return creditosOP;
    }

    public void setCreditosOP(Long creditosOP)
    {
        this.creditosOP = creditosOP;
    }

    public String getMencion()
    {
        return mencion;
    }

    public void setMencion(String mencion)
    {
        this.mencion = mencion;
    }
}