package es.uji.apps.lleu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by juan on 6/04/17.
 */
@Entity
@Table(name = "LLEU_EXT_ESTUDIOS_URLS")
public class EstudioUrl
{
    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "ESTUDIO_URL")
    private String url;

    @Column(name = "URL_VERIFICA")
    private String urlVerifica;

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUrlVerifica()
    {
        return urlVerifica;
    }

    public void setUrlVerifica(String urlVerifica)
    {
        this.urlVerifica = urlVerifica;
    }
}
