package es.uji.apps.lleu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Horario detallada de un estudio de grado.
 *
 * @see HorarioEstudioGeneral
 */
@Entity
@Table(name = "LLEU_EXT_HORARIOS_GRADO")
public class HorarioEstudioDetallado implements Serializable
{
    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="CUR_ID")
    private Long curso;

    @Id
    @Column(name="GRP_ID")
    private String grupo;

    @Id
    @Column(name="SGR_TIPO")
    private String tipoSubgrupo;

    @Id
    @Column(name="SGR_ID")
    private Long subgrupo;

    @Id
    @Column(name="CARACTER")
    private String caracter;

    @Id
    @Column(name="SEMESTRE")
    private String semestre;

    @Id
    @Column(name="DIA_SEM")
    private Integer diaSemana;

    @Id
    @Column(name="INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ini;

    @Id
    @Column(name="FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @Column(name="TIPO")
    private String tipo;

    @Column(name="COMENTARIO")
    private String comentario;

    @Column(name="COMENTARIO_DISCONT")
    private String comentarioDiscont;

    @Column(name="COMENTARIO_DISCONT_ES")
    private String comentarioDiscontES;

    @Column(name="COMENTARIO_DISCONT_CA")
    private String comentarioDiscontCA;

    @Column(name="COMENTARIO_DISCONT_EN")
    private String comentarioDiscontEN;

    @Column(name="AULA")
    private String aula;
    @Column(name="EDIFICIO")
    private String edificio;

    @Column(name = "GRP_ID_TEORIA")
    private String grupoTeoria;

    @ManyToOne
    @JoinColumn(name = "ASI_ID", insertable = false, updatable = false)
    private Asignatura asignatura;

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getGrupo()
    {
        return grupo;
    }

    public void setGrupo(String grupo)
    {
        this.grupo = grupo;
    }

    public String getTipoSubgrupo()
    {
        return tipoSubgrupo;
    }

    public void setTipoSubgrupo(String tipoSubgrupo)
    {
        this.tipoSubgrupo = tipoSubgrupo;
    }

    public Long getSubgrupo()
    {
        return subgrupo;
    }

    public void setSubgrupo(Long subgrupo)
    {
        this.subgrupo = subgrupo;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public Integer getDiaSemana()
    {
        return diaSemana;
    }

    public void setDiaSemana(Integer diaSemana)
    {
        this.diaSemana = diaSemana;
    }

    public Date getIni()
    {
        return ini;
    }

    public void setIni(Date ini)
    {
        this.ini = ini;
    }

    public Date getFin()
    {
        return fin;
    }

    public void setFin(Date fin)
    {
        this.fin = fin;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public String getComentarioDiscont()
    {
        return comentarioDiscont;
    }

    public void setComentarioDiscont(String comentarioDiscont)
    {
        this.comentarioDiscont = comentarioDiscont;
    }

    public String getAula()
    {
        return aula;
    }

    public void setAula(String aula)
    {
        this.aula = aula;
    }

    public String getEdificio()
    {
        return edificio;
    }

    public void setEdificio(String edificio)
    {
        this.edificio = edificio;
    }

    public String getGrupoTeoria()
    {
        return grupoTeoria;
    }

    public void setGrupoTeoria(String grupoTeoria)
    {
        this.grupoTeoria = grupoTeoria;
    }

    public Asignatura getAsignatura()
    {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura)
    {
        this.asignatura = asignatura;
    }

    public String getComentarioDiscontES()
    {
        return comentarioDiscontES;
    }

    public void setComentarioDiscontES(String comentarioDiscontES)
    {
        this.comentarioDiscontES = comentarioDiscontES;
    }

    public String getComentarioDiscontCA()
    {
        return comentarioDiscontCA;
    }

    public void setComentarioDiscontCA(String comentarioDiscontCA)
    {
        this.comentarioDiscontCA = comentarioDiscontCA;
    }

    public String getComentarioDiscontEN()
    {
        return comentarioDiscontEN;
    }

    public void setComentarioDiscontEN(String comentarioDiscontEN)
    {
        this.comentarioDiscontEN = comentarioDiscontEN;
    }
}
