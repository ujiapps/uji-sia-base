package es.uji.apps.lleu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Horario detallado de asignatura de master.
 */
@Entity
@Table(name = "LLEU_EXT_HOR_ASI_MASTER_DET")
public class HorarioAsigMasterDetallado implements Serializable
{
    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoACA;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name="ASIGNATURA_ID")
    private String asignaturaId;

    @Column(name="GRUPO_ID")
    private String grupo;


    @Column(name="SEMESTRE")
    private String semestre;

    @Id
    @Column(name="TIPO_SUBGRUPO_ID")
    private String subGrupoTipo;

    @Id
    @Column(name="SUBGRUPO_ID")
    private Integer subGrupoId;

    @Id
    @Column(name="AULA")
    private String aula;

    @Column(name="DIA_SEMANA_ID")
    private Integer dia;

    @Id
    @Column(name="INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ini;

    @Id
    @Column(name="FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;

    @Id
    @Column(name="FHOR_INI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fHorIni;

    @Id
    @Column(name="FHOR_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fHorFin;

    @Column(name="COMENTARIO")
    private String comentario;

    @Column(name="EDIFICIO")
    private String edificio;

    @Column(name="TIPO")
    private String tipo;


    @Column(name="CARACTER")
    private String caracter;


    @Column(name="CURSO")
    private Long curso;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    public Integer getCursoACA() {
        return cursoACA;
    }

    public void setCursoACA(Integer cursoACA) {
        this.cursoACA = cursoACA;
    }

    public Long getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Long estudioId) {
        this.estudioId = estudioId;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getSubGrupoTipo() {
        return subGrupoTipo;
    }

    public void setSubGrupoTipo(String subGrupoTipo) {
        this.subGrupoTipo = subGrupoTipo;
    }

    public Integer getSubGrupoId() {
        return subGrupoId;
    }

    public void setSubGrupoId(Integer subGrupoId) {
        this.subGrupoId = subGrupoId;
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    public Integer getDia() {
        return dia;
    }

    public void setDia(Integer dia) {
        this.dia = dia;
    }

    public Date getIni() {
        return ini;
    }

    public void setIni(Date ini) {
        this.ini = ini;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public Date getfHorIni() {
        return fHorIni;
    }

    public void setfHorIni(Date fHorIni) {
        this.fHorIni = fHorIni;
    }

    public Date getfHorFin() {
        return fHorFin;
    }

    public void setfHorFin(Date fHorFin) {
        this.fHorFin = fHorFin;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCaracter() {
        return caracter;
    }

    public void setCaracter(String caracter) {
        this.caracter = caracter;
    }

    public Long getCurso() {
        return curso;
    }

    public void setCurso(Long curso) {
        this.curso = curso;
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public String getNombreCA() {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA) {
        this.nombreCA = nombreCA;
    }

    public String getNombreEN() {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN) {
        this.nombreEN = nombreEN;
    }
}
