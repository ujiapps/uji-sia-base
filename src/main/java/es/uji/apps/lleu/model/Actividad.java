package es.uji.apps.lleu.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="LLEU_EXT_ACTIVIDADES")
public class Actividad implements Serializable
{
    @Id
    @Column(name = "ACT_ID")
    private Long actividadId;

    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    @Column(name = "H_PRE")
    private Float horasPresenciales;

    @Column(name = "H_NO_PRE")
    private Float horasNoPresenciales;

    private Integer orden;

    public Long getActividadId()
    {
        return actividadId;
    }

    public void setActividadId(Long actividadId)
    {
        this.actividadId = actividadId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public Float getHorasPresenciales()
    {
        return horasPresenciales;
    }

    public void setHorasPresenciales(Float horasPresenciales)
    {
        this.horasPresenciales = horasPresenciales;
    }

    public Float getHorasNoPresenciales()
    {
        return horasNoPresenciales;
    }

    public void setHorasNoPresenciales(Float horasNoPresenciales)
    {
        this.horasNoPresenciales = horasNoPresenciales;
    }

    public Integer getOrden()
    {
        return orden;
    }

    public void setOrden(Integer orden)
    {
        this.orden = orden;
    }
}
