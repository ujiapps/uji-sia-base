package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_RESUMEN_CREDITOS_G")
public class ResumenCreditosGrado implements Serializable
{
    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name = "CURSO")
    private Integer curso;

    @Column(name = "CRD_TR")
    private Double crdTr;

    @Column(name = "CRD_OB")
    private Double crdOb;

    @Column(name = "CRD_OP")
    private Double crdOp;

    @Column(name = "CRD_LE")
    private Double crdLe;

    @Column(name = "CRD_PFG")
    private Double crdPfg;

    @Column(name = "CICLO")
    private Integer ciclo;

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public void setCurso(Integer curso)
    {
        this.curso = curso;
    }

    public Double getCrdTr()
    {
        return crdTr;
    }

    public void setCrdTr(Double crdTr)
    {
        this.crdTr = crdTr;
    }

    public Double getCrdOb()
    {
        return crdOb;
    }

    public void setCrdOb(Double crdOb)
    {
        this.crdOb = crdOb;
    }

    public Double getCrdOp()
    {
        return crdOp;
    }

    public void setCrdOp(Double crdOp)
    {
        this.crdOp = crdOp;
    }

    public Double getCrdLe()
    {
        return crdLe;
    }

    public void setCrdLe(Double crdLe)
    {
        this.crdLe = crdLe;
    }

    public Double getCrdPfg()
    {
        return crdPfg;
    }

    public void setCrdPfg(Double crdPfg)
    {
        this.crdPfg = crdPfg;
    }

    public Integer getCiclo()
    {
        return ciclo;
    }

    public void setCiclo(Integer ciclo)
    {
        this.ciclo = ciclo;
    }
}
