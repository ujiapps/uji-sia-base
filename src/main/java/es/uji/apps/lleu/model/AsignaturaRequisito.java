package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name="LLEU_EXT_ASIG_REQUISITOS")
public class AsignaturaRequisito implements Serializable
{

    @Id
    @Column(name="ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name="CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name="ASI_ID_INCOMPATIBLE")
    private String asignaturaIdIncompatible;

    @Id
    @Column(name="ESTUDIO_ID")
    private Long estudioId;

    @Column(name="SEMESTRE")
    private String semestre;

    @Column(name="CUR_ID")
    private Integer curso;

    private String caracter;

    private Integer creditos;

    @Column(name="ASI_NOMBRE_CA")
    private String asiNombreCA;

    @Column(name="ASI_NOMBRE_ES")
    private String asiNombreES;

    @Column(name="ASI_NOMBRE_EN")
    private String asiNombreEN;


    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public String getAsignaturaIdIncompatible()
    {
        return asignaturaIdIncompatible;
    }

    public void setAsignaturaIdIncompatible(String asignaturaIdIncompatible)
    {
        this.asignaturaIdIncompatible = asignaturaIdIncompatible;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public Integer getCurso()
    {
        return curso;
    }

    public void setCurso(Integer curso)
    {
        this.curso = curso;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public Integer getCreditos()
    {
        return creditos;
    }

    public void setCreditos(Integer creditos)
    {
        this.creditos = creditos;
    }

    public String getAsiNombreCA()
    {
        return asiNombreCA;
    }

    public void setAsiNombreCA(String asiNombreCA)
    {
        this.asiNombreCA = asiNombreCA;
    }

    public String getAsiNombreES()
    {
        return asiNombreES;
    }

    public void setAsiNombreES(String asiNombreES)
    {
        this.asiNombreES = asiNombreES;
    }

    public String getAsiNombreEN()
    {
        return asiNombreEN;
    }

    public void setAsiNombreEN(String asiNombreEN)
    {
        this.asiNombreEN = asiNombreEN;
    }
}
