package es.uji.apps.lleu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "LLEU_EXT_ESTUDIOS_PLANES_V2")
public class EstudiosPlanesVW implements Serializable {
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "TIPO_ESTUDIO")
    private String tipoEstudio;

    @Column(name = "NOMBRE_ESTUDIO_CA")
    private String nombreEstudioCa;

    @Column(name = "NOMBRE_ESTUDIO_ES")
    private String nombreEstudioEs;

    @Column(name = "NOMBRE_ESTUDIO_EN")
    private String nombreEstudioEn;

    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Column(name = "PLAN_ID")
    private Long planId;

    @Column(name = "NOMBRE_PLAN_CA")
    private String nombrePlanCa;

    @Column(name = "NOMBRE_PLAN_ES")
    private String nombrePlanEs;

    @Column(name = "NOMBRE_PLAN_EN")
    private String nombrePlanEn;

    @Column(name = "UBICACION_ID")
    private Long ubicacionId;

    @Column(name = "UBICACION_CA")
    private String ubicacionCa;

    @Column(name = "UBICACION_ES")
    private String ubicacionEs;

    @Column(name = "UBICACION_EN")
    private String ubicacionEn;

    public EstudiosPlanesVW(Long id, Long estudioId, String tipoEstudio, String nombreEstudioCa, String nombreEstudioEs, String nombreEstudioEn, Integer cursoAca, Long planId, String nombrePlanCa, String nombrePlanEs, String nombrePlanEn, Long ubicacionId, String ubicacionCa, String ubicacionEs, String ubicacionEn) {
        this.id = id;
        this.estudioId = estudioId;
        this.tipoEstudio = tipoEstudio;
        this.nombreEstudioCa = nombreEstudioCa;
        this.nombreEstudioEs = nombreEstudioEs;
        this.nombreEstudioEn = nombreEstudioEn;
        this.cursoAca = cursoAca;
        this.planId = planId;
        this.nombrePlanCa = nombrePlanCa;
        this.nombrePlanEs = nombrePlanEs;
        this.nombrePlanEn = nombrePlanEn;
        this.ubicacionId = ubicacionId;
        this.ubicacionCa = ubicacionCa;
        this.ubicacionEs = ubicacionEs;
        this.ubicacionEn = ubicacionEn;
    }

    public EstudiosPlanesVW() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Long estudioId) {
        this.estudioId = estudioId;
    }

    public String getTipoEstudio() {
        return tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio) {
        this.tipoEstudio = tipoEstudio;
    }

    public String getNombreEstudioCa() {
        return nombreEstudioCa;
    }

    public void setNombreEstudioCa(String nombreEstudioCa) {
        this.nombreEstudioCa = nombreEstudioCa;
    }

    public String getNombreEstudioEs() {
        return nombreEstudioEs;
    }

    public void setNombreEstudioEs(String nombreEstudioEs) {
        this.nombreEstudioEs = nombreEstudioEs;
    }

    public String getNombreEstudioEn() {
        return nombreEstudioEn;
    }

    public void setNombreEstudioEn(String nombreEstudioEn) {
        this.nombreEstudioEn = nombreEstudioEn;
    }


    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Integer getCursoAca() {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca) {
        this.cursoAca = cursoAca;
    }

    public String getNombrePlanCa() {
        return nombrePlanCa;
    }

    public void setNombrePlanCa(String nombrePlanCa) {
        this.nombrePlanCa = nombrePlanCa;
    }

    public String getNombrePlanEs() {
        return nombrePlanEs;
    }

    public void setNombrePlanEs(String nombrePlanEs) {
        this.nombrePlanEs = nombrePlanEs;
    }

    public String getNombrePlanEn() {
        return nombrePlanEn;
    }

    public void setNombrePlanEn(String nombrePlanEn) {
        this.nombrePlanEn = nombrePlanEn;
    }

    public Long getUbicacionId() {
        return ubicacionId;
    }

    public void setUbicacionId(Long ubicacionId) {
        this.ubicacionId = ubicacionId;
    }

    public String getUbicacionCa() {
        return ubicacionCa;
    }

    public void setUbicacionCa(String ubicacionCa) {
        this.ubicacionCa = ubicacionCa;
    }

    public String getUbicacionEs() {
        return ubicacionEs;
    }

    public void setUbicacionEs(String ubicacionEs) {
        this.ubicacionEs = ubicacionEs;
    }

    public String getUbicacionEn() {
        return ubicacionEn;
    }

    public void setUbicacionEn(String ubicacionEn) {
        this.ubicacionEn = ubicacionEn;
    }
}
