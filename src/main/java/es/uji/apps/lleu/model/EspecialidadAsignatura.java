package es.uji.apps.lleu.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LLEU_EXT_ESPECIALIDADES_ASIG")
public class EspecialidadAsignatura implements Serializable
{
    @Id
    @Column(name = "CURSO_ACA")
    private Integer cursoAca;

    @Id
    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Id
    @Column(name = "PIE_ID")
    private Long pieId;

    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    @Column(name = "CREDITOS")
    private Float creditos;

    @Column(name = "CARACTER")
    private String caracter;

    public Integer getCursoAca()
    {
        return cursoAca;
    }

    public void setCursoAca(Integer cursoAca)
    {
        this.cursoAca = cursoAca;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Long getPieId()
    {
        return pieId;
    }

    public void setPieId(Long pieId)
    {
        this.pieId = pieId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public Float getCreditos()
    {
        return creditos;
    }

    public void setCreditos(Float creditos)
    {
        this.creditos = creditos;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }
}
