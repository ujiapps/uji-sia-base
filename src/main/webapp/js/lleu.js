/**
 * Scripts comunes a todas las páginas.
 */
$(document).ready( function() {
    // El componente AcadSelector se encarga de gestionar el cambio de curso académico.
    this.acadSelector = new AcadSelector('#acadselector');
});
