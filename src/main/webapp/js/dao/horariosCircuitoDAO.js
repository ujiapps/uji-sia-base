/**
 * Devuelve los horarios de un circuito.
 *
 * @constructor
 */
HorariosCircuitoDAO = function() {
    this._baseURL = LLEU_CONFIG.baseUrl + LLEU_CONFIG.cursoAca + '/estudio/' + LLEU_CONFIG.estudioId + '/horarios/circuito';
};
HorariosCircuitoDAO.prototype.get = function(circuitoId, semestre, from) {
    var url = this._baseURL + '/' + circuitoId;

    var data = {
        semestre: semestre,
    };
    if (from) {
        data.from = from;
    }

    return RestProvider.get(url, data);
};
