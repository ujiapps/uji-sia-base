
HorariosEstudioDAO = function() {
    this._baseURL = LLEU_CONFIG.baseUrl + LLEU_CONFIG.cursoAca + '/estudio/' + LLEU_CONFIG.estudioId + '/horarios/';
};

/**
 * Devuelve el horario de estudio detallado (semana a semana)
 *
 * @param {Array} grupos grupos por los que queremos filtrar.
 * @param {Array} semestres semestres por los que queremos filtrar.
 * @param {Date} from fecha inicial
 * @returns {*|Promise}
 */
HorariosEstudioDAO.prototype.get = function(semestres, grupos, cursos, caracteres, from) {
    var data = {};
    if (semestres) {
        data.semestres = semestres.join(",");
    }
    if (grupos) {
        data.grupos = grupos.join(",");
    }
    if (cursos) {
        data.cursos = cursos.join(",");
    }
    if (caracteres) {
        data.caracteres = caracteres.join(",");
    }

    data.from = from.getTime();

    return RestProvider.get(this._baseURL + 'json', data);
};

/**
 * Obtiene el horario de estudio general.
 *
 * @param semestres
 * @param grupos
 * @param cursos
 * @param caracteres
 * @param from
 * @returns {*|{promise, reqid}}
 */
HorariosEstudioDAO.prototype.getGeneral = function(semestres, grupos, cursos, caracteres, from) {
    var data = {};
    if (semestres) {
        data.semestres = semestres.join(",");
    }
    if (grupos) {
        data.grupos = grupos.join(",");
    }
    if (cursos) {
        data.cursos = cursos.join(",");
    }
    if (caracteres) {
        data.caracteres = caracteres.join(",");
    }

    data.from = from.getTime();


    return RestProvider.get(this._baseURL + 'general', data);
};


/**
 * Devuelve el último día lectivo dadas las condiciones de filtrado indicadas.
 *
 * @param semestres
 * @param grupos
 * @param cursos
 * @param caracteres
 * @returns {*|{promise, reqid}}
 */
HorariosEstudioDAO.prototype.getUltimoDiaLectivo = function(semestres, grupos, cursos, caracteres) {
    var data = {};
    if (semestres) {
        data.semestres = semestres.join(",");
    }
    if (grupos) {
        data.grupos = grupos.join(",");
    }
    if (cursos) {
        data.cursos = cursos.join(",");
    }
    if (caracteres) {
        data.caracteres = caracteres.join(",");
    }
    var ret = RestProvider.get(this._baseURL + 'ultimolectivo', data);
    ret.promise = ret.promise.then(function(r) {
        if (r.data.data == 0) {
            r.data.data = (new Date()).getTime();
        }
        return r;
    });
    return ret;
};



