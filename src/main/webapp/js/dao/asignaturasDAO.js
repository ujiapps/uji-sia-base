AsignaturasDAO = function() {
    this._baseURL = LLEU_CONFIG.baseUrl + LLEU_CONFIG.cursoAca + '/estudio/' + LLEU_CONFIG.estudioId + '/asignaturas';
};

AsignaturasDAO.prototype.get = function(semestres, grupos, cursos,  caracteres, nombre, orden) {
    var data = {};
    if (cursos) {
        data.cursos = cursos.join(",");
    }
    if (semestres) {
        data.semestres = semestres.join(",");
    }
    if (grupos) {
        data.grupos = grupos.join(",");
    }
    if (caracteres) {
        data.caracteres = caracteres.join(",");
    }
    if (nombre) {
        data.nombre = nombre;
    }
    if (orden) {
        data.orden = orden;
    }
    return RestProvider.get(this._baseURL, data);
};