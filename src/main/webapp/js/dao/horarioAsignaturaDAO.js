
HorarioAsignaturaDAO = function(asignaturaId) {
    this._baseURL = LLEU_CONFIG.baseUrl + LLEU_CONFIG.cursoAca + '/estudio/' + LLEU_CONFIG.estudioId + '/asignatura/' + asignaturaId + '/horarios';
};

/**
 * Pide el horario detallado desde un fecha dada.
 *
 * @param {Array} grupos grupos por los que queremos filtrar.
 * @param {Integer} from el timestamp desde el cual queremos obtener los datos de eventos.
 * @returns {*|Promise}
 */
HorarioAsignaturaDAO.prototype.getDetallado = function(grupos, from) {
    var data = {};
    if (grupos) {
        data.grupos = grupos.join(",");
    }
    data.from = from.getTime();
    return RestProvider.get(this._baseURL, data);
};

HorarioAsignaturaDAO.prototype.getGeneral = function(grupos) {
    var data = {};
    if (grupos) {
        data.grupos = grupos.join(",");
    }
    return RestProvider.get(this._baseURL, data);
};


HorarioAsignaturaDAO.prototype.getUltimoLectivo = function(grupos) {
    var data = {};
    if (grupos) {
        data.grupos = grupos.join(",");
    }
    var ret = RestProvider.get(this._baseURL + '/ultimolectivo', data);
    ret.promise = ret.promise.then(function(r) {
        // si la respuesta es cero, entonces indicamos que la fecha máxima es hoy.
        if (r.data.data == 0) {
            r.data.data = (new Date()).getTime();
        }
        return r;
    });
    return ret;
};
