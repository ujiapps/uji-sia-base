ExamenesEstudioDAO = function() {
    this._baseURL = LLEU_CONFIG.baseUrl + LLEU_CONFIG.cursoAca + '/estudio/' + LLEU_CONFIG.estudioId + '/examenes/json';
};

ExamenesEstudioDAO.prototype.get = function(convocatoria) {
    var data = {
        convocatoria: convocatoria
    };
    return RestProvider.get(this._baseURL, data);
};
