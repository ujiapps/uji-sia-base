VacantesSubGruposDAO = function() {
    this._baseURL = LLEU_CONFIG.baseUrl + LLEU_CONFIG.cursoAca + '/estudio/' + LLEU_CONFIG.estudioId + '/asignaturas/vacantes/';
};

VacantesSubGruposDAO.prototype.get = function(asignatura) {
    var data = {};
    data.asignatura = asignatura;
    return RestProvider.get(this._baseURL, data);
};