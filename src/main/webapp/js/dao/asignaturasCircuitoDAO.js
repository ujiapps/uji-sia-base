AsignaturasCircuitoDAO = function() {
    this._baseURL = LLEU_CONFIG.baseUrl + LLEU_CONFIG.cursoAca + '/estudio/' + LLEU_CONFIG.estudioId + '/circuito';
};

AsignaturasCircuitoDAO.prototype.get = function(circuitoId, semestre) {
    var url = this._baseURL + '/' + parseInt(circuitoId) + '/asignaturas';
    return RestProvider.get(url, {semestre: semestre});
};

