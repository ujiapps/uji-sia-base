/**
 * Se encarga de llamar al API REST del LLEU. Singleton
 *
 * Requiere jquery >= 1.5
 */
RestProvider = {
    /**
     * timeout de la petición en milisegundos
     * @private
     */
    _timeout: 8000,

    /**
     * Peticione en curso. Lo utilizamos para cancelar posibles peticiones anteriores
     * @private
     */
    _currentRequests: {},

    /**
     * Asociamos un id a cada petición para saber cuál sacar de la lista de peticiones
     * @private
     */
    _lastReqId: 0,

    /**
     * Data una URL y parámetros, hace una petición AJAX con los parámetros indicados en data.
     *
     * @param url la url que vamos a atacar
     * @param data un objeto cuyos campos son los parámetros del GET y valores los valores del GET :-)
     * @returns {promise: Promise, reqid: int}
     */
    get: function (url, data) {
        var me = this;

        var jq = $.ajax({
            method: 'GET',
            url: url,
            data: data,
            timeout: me._timeout
        });

        jq.lleuid = me._lastReqId;
        me._lastReqId += 1;
        me._currentRequests[jq.lleuid] = jq;

        var p = new Promise(function(resolve, reject) {

            jq.done(function(data, textStatus, jqXHR) {

                resolve({
                    data: data,
                    reqid: jqXHR.lleuid
                });

            }).fail(function(a,b,c) {
                reject(b);
            });
        });
        return {promise: p, reqid: jq.lleuid};
    },

    /**
     *
     * @param url
     * @param data
     * @param {boolean} postJSON indica si debemos codificar como JSON los datos.
     * @returns {{promise: (*|o), reqid: (number|*)}}
     */
    post: function(url, data, postJSON) {
        var me = this;

        var conf = {
            method: 'POST',
            url: url,
            timeout: me._timeout
        };
        if (postJSON) {
            conf.contentType = 'application/json; charset=utf-8';
            conf.data = JSON.stringify(data);
        } else {
            conf.data = data;
        }

        var jq = $.ajax(conf);

        jq.lleuid = me._lastReqId;
        me._lastReqId += 1;
        me._currentRequests[jq.lleuid] = jq;

        var p = new Promise(function(resolve, reject) {

            me._currentRequests[jq.lleuid] = jq;
            jq.done(function(data, textStatus, jqXHR) {
                resolve({
                    data: data,
                    reqid: jqXHR.lleuid
                });

            }).fail(function(a,b,c) {
                reject(b);
            });
        });
        return {promise: p, reqid: jq.lleuid};
    },

    postJSON: function(url, data) {
        return this.post(url, data, true);
    },

    /**
     * Indica el timeout a establecer en las siguientes peticiones
     *
     * @param int timeout el timeout en milisegundos
     */
    setTimeout: function (timeout) {
        this._timeout = timeout;
    },

    /**
     * Cancela las peticiones ajax en curso.
     */
    cancelAllRequests: function () {
        for (var i = 0, to = this._currentRequests.length; i < to; i++) {
            this._currentRequests[i].abort();
        }
        this._currentRequests = {};
    },

    /**
     * Aborta una petición en concreto
     * @param reqId
     */
    cancelRequest: function(reqId) {
        if (reqId in this._currentRequests) {
            this._currentRequests[reqId].abort();
            delete this._currentRequests[reqId];
        }
    }
};
