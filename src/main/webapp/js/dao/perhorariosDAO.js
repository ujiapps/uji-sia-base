/**
 * DAO para personalización de horarios
 */

PerHorariosDAO = function() {
    this._baseURL = LLEU_CONFIG.baseUrl + LLEU_CONFIG.cursoAca + '/estudio/' + LLEU_CONFIG.estudioId + '/horarios/generar';
};

/**
 * Obtiene los datos de un evento para un usuario en concreto.
 *
 * @param {array} asig_grupos array de AsignaturaGrupos
 */
PerHorariosDAO.prototype.post = function(asig_grupos) {
    var data = asig_grupos.map(function(e) {
        return {
            asignatura: e.asignatura,
            grupo: e.grupo,
            subgrupos: e.subgrupos
        };
    });
    return RestProvider.postJSON(this._baseURL, data);
};