$(document).ready(function () {
    if (!$('body').hasClass('page-generar-horarios')) {
        return;
    }
    new PersonalizacionHorariosPage();
});

PersonalizacionHorariosPage = function () {

    LoadingIndicator.working(i18n.get_string('cargando'));

    var me = this;

    this._horarios = [null, null];
    this._btnGenerar = $('input[name=generar]');
    this._btnGenerar.click(this.onBtnGenerarClick.bind(this));

    $('input[name=check]').click(this._onCheckClick.bind(this));
    $('input[name=curso]').click(this.onBtnCursoClick.bind(this));
    $('.selector-grupos').change(this.onSelectGrupo.bind(this))

    this._paleta = new Paleta();
    this._asigColors = {};

    this._leyenda = new LeyendaEx('#leyenda-subgrupos');
    this._leyenda.click(this._onLeyendaClick.bind(this));

    this._horarioPrint1 = new HorarioPrint('#eventosimprimir1');
    this._horarioPrint2 = new HorarioPrint('#eventosimprimir2');

    this._badges = {};
    $('.filtro-curso input[name=curso]').each(function(i, btnCurso) {
        var curso = $(btnCurso).data('curso');
        me._badges[curso] = new Badge(btnCurso, 0);
    });

    // Contiene los manejadores de las cajas de asignatura finales.
    this._cajaAsignaturas = {};

    LoadingIndicator.hide();
};

PersonalizacionHorariosPage.prototype._onLeyendaClick = function (disabled) {
    var toDisable = disabled.map(function (e) {
        return "__" + e;
    });
    for (var i in this._horarios) {
        if (this._horarios[i] != null) {
            this._horarios[i].disableEventsByClass(toDisable);
        }
    }
};

PersonalizacionHorariosPage.prototype.onSelectGrupo = function (e) {
    let valueSeleccionado = $(e.currentTarget).val();
    let asignaturaId = $(e.currentTarget).attr('id').split('_')[1];
    let grupo = '#'+asignaturaId +'-'+valueSeleccionado;
    $(e.currentTarget).closest('.seleccion_grupos').find('.grupo').addClass('hidden');
    $(grupo).toggleClass('hidden')
}

PersonalizacionHorariosPage.prototype._onCheckClick = function (e) {
    $(e.currentTarget).closest('div[data-asignatura]').toggleClass('selected');
    $(e.currentTarget).closest('div[data-asignatura]').find('.seleccion_grupos').toggleClass('hidden');
    $(e.currentTarget).closest('div[data-asignatura]').find('.seleccion_grupos').find('.grupo').first().toggleClass('hidden');
};

PersonalizacionHorariosPage.prototype.onBtnGenerarClick = function () {

    LoadingIndicator.working(i18n.get_string('cargando'));

    var asignaturas = [];

    var els = $('ul.accordion input[type=checkbox]:checked').closest('div');

    for (var i = 0; i < els.length; i++) {
        var ag = new AsignaturaGrupos();
        ag.setAsignatura($(els[i]).data('asignatura'));
        let grupoSeleccionado = $('#grupos_' + ag.asignatura).val();
        ag.setGrupo(grupoSeleccionado);
        var subgrupos = $(els[i]).find('div[data-grupo="'+grupoSeleccionado+'"]').find('.subgrupos');
        for (var j = 0; j < subgrupos.length; j++) {
            var s = $(subgrupos[j]).find('option:selected');
            ag.setSubgrupo(
                s.data('subgrupo'),
                $(subgrupos[j]).val()
            );
        }
        asignaturas.push(ag);
    }

    // AJAX
    var me = this;
    if (asignaturas.length > 0) {
        var dao = new PerHorariosDAO();
        dao.post(asignaturas).promise.then(function (r) {
            var data = r.data;
            me._onEventsDataAvailable.bind(me)(data);

            // Mostramos el listado de asignaturas inferior.
            if (asignaturas.length == 0) {
                $('.listado-asignaturas h3').addClass('hidden');
            } else {
                $('.listado-asignaturas h3').removeClass('hidden');
            }

            $('.listado-asignaturas > div').not('hidden').addClass('hidden');

            asignaturas.forEach(function(ag) {

                var element = $('.listado-asignaturas > div[data-asignatura=' + ag.asignatura + ']');
                if (!(ag.asignatura in me._cajaAsignaturas)) {
                   me._cajaAsignaturas[ag.asignatura] = new CajaAsignatura(element);
                }

                var cajaAsignatura = me._cajaAsignaturas[ag.asignatura];
                element.removeClass('hidden');

                cajaAsignatura.addInfo(ag.grupo, i18n.get_string('grupo'));
                for (subgrupoTipo in ag.subgrupos) {
                    var subgrupoId = ag.subgrupos[subgrupoTipo];
                    cajaAsignatura.addInfo(subgrupoId, i18n.get_string(subgrupoTipo.toLowerCase()));
                }
            });

        }).catch(function(e) {
            LoadingIndicator.error(e);
        });

    } else {
        LoadingIndicator.hide();
    }
    // TODO: hay que controlar el error de esta llamada ajax
};

PersonalizacionHorariosPage.prototype.onBtnCursoClick = function (event) {

    var oldActiveButton = $('.filtro-curso input[type=button].active');
    var oldCourse = oldActiveButton.data('curso');

    // Dejamos activo sólo un botón.
    $('.filtro-curso input[type=button]').removeClass('active');
    $(event.target).toggleClass('active');

    // Oculto el badge del curso activo.
    var cursoActivo = $(event.target).data('curso');
    this._badges[cursoActivo].hide();

    // Mostramos sólo las asignaturas del curso que hemos seleccionado, sin perder el estado de checked.
    $('.selector-asignaturas .selector-asignatura').not('hidden').addClass('hidden');
    var asignaturas = $('div[data-curso=' + $(event.target).data('curso') + ']');
    asignaturas.toggleClass('hidden');

    // Si no hay un acordeón seleccionado, seleccionamos uno.
    var accs = $('.selector-asignaturas .accordion-navigation.active');
    if (accs.length == 0) {
        $('.selector-asignaturas .accordion-navigation:first-child').addClass('active');
    }

    // Actualizamos el badge a mostrar si los cursos que hemos seleccionado son distintos.
    if (oldCourse != null && cursoActivo != oldCourse) {
        this._updateBadge(oldActiveButton);
    }
};
/**
 * Actualiza los badges del filtro de selección de cursos.
 *
 * @private
 */
PersonalizacionHorariosPage.prototype._updateBadge = function(button) {
    var curso = button.data('curso');
    var badgeText = $('.selector-asignatura[data-curso=' + curso + '].selected').length;
    if (badgeText > 0) {
        this._badges[curso].setText(badgeText);
        this._badges[curso].show();
    }
};

PersonalizacionHorariosPage.prototype._onEventsDataAvailable = function (data) {

    data = data.data;

    // recogemos los tipos de grupo que tenemos en los eventos para crear correctamente la leyenda de filtrado por
    // subgrupos.

    var titulos = {
        "TE": i18n.get_string('teoria'),
        "LA": i18n.get_string('laboratorio'),
        "PR": i18n.get_string('problemas'),
        "TU": i18n.get_string('tutoria'),
        "SE": i18n.get_string('seminario'),
        "ERA": i18n.get_string('evaluacion'),
        "AV": i18n.get_string('evaluacion')
    };
    var sortWeight = {
        TE: 1,
        LA: 2,
        PR: 3,
        TU: 4,
        SE: 5,
        ERA: 6,
        AV: 7
    };

    this._leyenda.removeAll();

    var subgrupos = [];
    var auxSub = {};
    for (var i = 0; i < data.length; i++) {
        if (data[i].tipoSubgrupo in auxSub) {
            continue;
        }
        subgrupos.push(data[i].tipoSubgrupo);
    }
    subgrupos.sort(function (a, b) {
        return sortWeight[a] - sortWeight[b];
    });
    for (var i = 0; i < subgrupos.length; i++) {
        this._leyenda.addItem(
            titulos[subgrupos[i]],
            subgrupos[i],
            '',
            'white'
        );
    }

    // ahora el horario.

    var eventos = [[], []];
    var from = [23, 23];
    var to = [0, 0];


    $('#leyenda-subgrupos').removeClass('hidden');

    for (var i = 0; i < data.length; i++) {

        var color;
        if (data[i].asignaturaId in this._asigColors) {
            color = this._asigColors[data[i].asignaturaId];
        } else {
            color = this._paleta.getUnusedColorByName(data[i].asignaturaId);
        }

        this._paleta.getUnusedColorByName(data[i].asignaturaId);

        var grupoSubgrupo =
            i18n.get_string('grupo') + ' ' + data[i].grupo + "<br />" +
            titulos[data[i].tipoSubgrupo] + " " + data[i].subgrupo;

        var comentario;
        if(LLEU_CONFIG.lang=='es'){
            comentario = [data[i].comentarioES, data[i].comentarioDiscont].filter(Boolean).map(function(p) {
                return "<div><p>".concat(p).concat("</p></div>")}).join("");
        }else if(LLEU_CONFIG.lang=='en'){
            comentario = [data[i].comentarioEN, data[i].comentarioDiscont].filter(Boolean).map(function(p) {
                return "<div><p>".concat(p).concat("</p></div>")}).join("");
        } else {
            comentario = [data[i].comentarioCA, data[i].comentarioDiscont].filter(Boolean).map(function(p) {
                return "<div><p>".concat(p).concat("</p></div>")}).join("");
        }

        // Añadimos el evento al horario
        var e = new Evento(
            data[i].ini,    // from
            data[i].fin,    // to
            data[i].diaSemana,    // día de la semana
            data[i].tipoSubgrupo,  // tipo
            grupoSubgrupo,  // título
            data[i].asignaturaId,   // código
            comentario, // observaciones
            null,  // localizacion
            null,  // url
            color,  // color
            '__' + data[i].tipoSubgrupo   // clase css. la utilizaré para filtrar después por tipo de subgrupo
        );

        // TODO: revisar qué pasa con las asignaturas anuales
        var s;
        if (data[i].semestre == 'semestre.1') {
            s = 0;
        } else {
            s = 1;
        }
        eventos[s].push(e);
        if (e.from.getHours() < from[s]) {
            from[s] = e.from.getHours();
        }
        if (e.to.getHours() >= to[s]) {
            to[s] = e.to.getHours() + 1;
        }

    }


    var me = this;

    this.refreshHorarios(
        eventos[0],
        from[0],
        to[0],
        eventos[1],
        from[1],
        to[1]
    ).then(function () {

        var toDisable = me._leyenda.getItemsDesactivados().map(function(d) {
            return '__' + d;
        });

        if (me._horarios[0]) {
            me._horarios[0].disableEventsByClass(toDisable);
        }
        if (me._horarios[1]) {
            me._horarios[1].disableEventsByClass(toDisable);
        }

        LoadingIndicator.hide();
    });


};
/**
 * Devuelve una promesa para actualizar los datos de los horarios
 *
 * @param eventos1 eventos del primer semestre
 * @param eventos2 eventos del segundo semestre
 * @returns {*|o}
 */
PersonalizacionHorariosPage.prototype.refreshHorarios = function (eventos1, from1, to1, eventos2, from2, to2) {
    var me = this;

    return new Promise(function (resolve, reject) {

        var data = [eventos1, eventos2];
        var sizes = [[from1, to1], [from2, to2]];

        for (var i = 0; i < me._horarios.length; i++) {
            if (me._horarios[i] != null) {
                me._horarios[i].remove();
            }
            if (data[i].length > 0) {
                me._horarios[i] = new Horario(
                    '#horario-personalizacion-' + (i + 1),
                    sizes[i][0],
                    sizes[i][1],
                    Horario.TIPO.SEMANAL,
                    new Date()
                );
                me._horarios[i].addEvents(data[i]);
                $('#titulo-horario-s' + (i + 1)).removeClass('hidden');
            } else {
                me._horarios[i] = null;
            }

            // añado los eventos al horario imprimible correspondiente
            me['_horarioPrint' + (i+1)].addEvents(data[i]);
        }

        resolve();
    });
};