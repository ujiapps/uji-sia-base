$(document).ready(function () {
    if (!$('body').hasClass('page-circuitos')) {
        return;
    }
    var page = new CircuitosPage();
});

CircuitosPage = function () {

    var me = this;

    $(document).foundationTabs();
    $.fn.foundationTabs({
        callback: this._onTabChange.bind(this)
    });

    if($('#filtro-circuito').length == 0){
        return;
    }
    this._loadingIndicators = 0;

    // manejador para el filtro de circuitos.
    this._filterCircuitos = new Filtro("#filtro-circuito", false, true);
    this._filterCircuitos.change(this._onFilterChange.bind(this));

    // qué debemos mostrar: el listado de asignaturas o el horario.
    this._scene = CircuitosPage.SHOW.HORARIO;

    this._tabs = $('#tabs-vistas');

    this._lastActiveTab = null;

    // indican el circuito actual seleccionado. Lo usamos para evitar peticiones ajax innecesarias.
    this._currentCircuitoLista = null;
    this._currentCircuitoHorario = null;

    this._forceHorariosReload = false;

    // tenemos dos horarios, uno para el primer semestre y otro para el segundo semestre.
    this._horarios = [null, null];

    this._paleta = new Paleta();
    this._asigColors = {};

    // la leyenda de asignaturas.
    this._leyenda = new LeyendaEx('#leyenda-horario');
    this._leyenda.click(this.onLeyendaClick.bind(this));

    // ids de las últimas peticiones ajax para cada subapartado.
    this._lastAsignaturasReq = null;

    // Configuramos el horario del semestre 1.
    this._horarios[0] = new Horario('#horario-circuito-s1', 8, 22, Horario.TIPO.SEMANAL, Horario.getPrimerDiaSemestre1('#horario-circuito-s1'), true, true);
    this._horarios[0].setLoadGeneralCb(this._getEventoInstance.bind(this), this._loadGeneralCb.bind(this));
    this._horarios[0].setLoadDetalladaCb(this._getEventoInstance.bind(this), this._loadDetalladaCb.bind(this));
    this._horarios[0].setWeekNav('#weeknav-s1');
    this._horarios[0].setOnLoadFinishedCb(this._onHorarioLoadFinished.bind(this));

    // Configuramos el horario del semestre 2.
    this._horarios[1] = new Horario('#horario-circuito-s2', 8, 22, Horario.TIPO.SEMANAL, Horario.getPrimerDiaSemestre2('#horario-circuito-s2'), true, true);
    this._horarios[1].setLoadGeneralCb(this._getEventoInstance.bind(this), this._loadGeneralCb.bind(this));
    this._horarios[1].setLoadDetalladaCb(this._getEventoInstance.bind(this), this._loadDetalladaCb.bind(this));
    this._horarios[1].setWeekNav('#weeknav-s2');
    this._horarios[1].setOnLoadFinishedCb(this._onHorarioLoadFinished.bind(this));

    this._vistaHorario1 = new VistaHorario('#estilo1');
    this._vistaHorario1.change(function (tipo) {
        var tipoHorario = (tipo === VistaHorario.TIPO.DETALLADA) ? Horario.TIPO.CALENDARIO : Horario.TIPO.SEMANAL;

        me._forceHorariosReload = true;
        me._horarios[0].setTipo(tipoHorario);
        me._horarios[0].load().then(function () {
            me._forceHorariosReload = false;
        });
    });

    this._vistaHorario2 = new VistaHorario('#estilo2');
    this._vistaHorario2.change(function (tipo) {
        var tipoHorario = (tipo === VistaHorario.TIPO.DETALLADA) ? Horario.TIPO.CALENDARIO : Horario.TIPO.SEMANAL;

        me._forceHorariosReload = true;

        me._horarios[1].setTipo(tipoHorario);
        me._horarios[1].load().then(function () {
            me._forceHorariosReload = false;
        });
    });

    // Cargamos los datos.
    if (this._isCircuitoActivoAbierto()) {
        me.setVistaCircuitoAbierto();
    } else {
        me.setVistaCircuitoCerrado();
    }
    var activeTab = this._tabs.find('.tabs-content .active');
    if (activeTab.attr('id') === 'horario-generalTab') {
        this.loadAllHorarios();
    } else {
        var circuitoActivo = this._filterCircuitos.getFilterData();
        circuitoActivo = circuitoActivo.circuito[0];
        this._loadAsignaturas(circuitoActivo);
    }

};

CircuitosPage.SHOW = {
    LISTAASIGNATURAS: 1,
    HORARIO: 2,
    HORARIO_DETALLADO: 3
};

/**
 * Evento click de leyenda.
 *
 * @param e
 */
CircuitosPage.prototype.onLeyendaClick = function (code) {
    for (var i = 0; i < 2; i++) {
        if (this._horarios[i] !== null) {
            this._horarios[i].disableItemsByCode(code);
        }
    }
    return true;
};
CircuitosPage.prototype.showLoadingMask = function (msg) {
    if (typeof (msg) == 'undefined') {
        msg = i18n.get_string('cargando');
    }
    if (this._loadingIndicators == 0) {
        LoadingIndicator.working(msg);
    }
    this._loadingIndicators++;
    console.log("MOSTRAR: " + this._loadingIndicators);
};
CircuitosPage.prototype.showError = function (msg) {
    LoadingIndicator.error(msg);
};
CircuitosPage.prototype.hideLoadingMask = function () {
    this._loadingIndicators--;
    if (this._loadingIndicators == 0) {
        LoadingIndicator.hide();
    }
};

/**
 * Devuelve el circuitoId del circuito activo en ese momento.
 * @returns {*}
 * @private
 */
CircuitosPage.prototype._getCircuitoActivo = function () {
    var active = this._filterCircuitos.getFilterData();
    return active.circuito[0];
};


/**
 * Se dispara en el click de cualquier filtro.
 *
 * @param active
 * @returns {boolean}
 */
CircuitosPage.prototype._onFilterChange = function (active) {
    var me = this;
    if (me._isCircuitoActivoAbierto()) {
        me.setVistaCircuitoAbierto();
    } else {
        me.setVistaCircuitoCerrado();
    }
    if (this._scene === CircuitosPage.SHOW.LISTAASIGNATURAS) {

        if ((this._currentCircuitoLista === null) || (active.circuito[0] !== this._currentCircuitoLista)) {

            this.showLoadingMask();
            this._lastActiveTab = null;
            this._loadAsignaturas(active.circuito[0]).then(function (data) {
                me.hideLoadingMask.bind(me)();
            }).catch(function (e) {
                if (e === 'abort') {
                    me.hideLoadingMask.bind(me)();
                    return;
                }
                me.showError.bind(me)("ERROR: " + e);
            });

        }
    } else {
        this._lastActiveTab = null;
        this.loadAllHorarios().then(function () {

        })
    }


    return true;
};

/**
 * Actualiza el listado de asignaturas con el circuito activo en este momento.
 *
 * @returns Promise
 * @private
 */
CircuitosPage.prototype._loadAsignaturas = function (active) {
    var me = this;
    var dao = new AsignaturasCircuitoDAO();

    if (this._lastAsignaturasReq !== null) {
        RestProvider.cancelRequest(this._lastAsignaturasReq);
    }
    var req = dao.get(active);
    this._lastAsignaturasReq = req.reqid;

    return req.promise.then(function (r) {

        if (r.reqid !== me._lastAsignaturasReq) {
            return;
        }

        me._lastAsignaturasReq = null;

        var data = r.data;

        $('#asignaturas-circuito').empty();
        $('#asignaturas-circuito').append(data);

        me._currentCircuitoLista = active;
    });
};

/**
 * Cambia la escena mostrada: bien el listado de asignaturas o bien el horario.
 *
 * @param newScene
 * @private
 */
CircuitosPage.prototype._changeScene = function (newScene) {
    this._scene = newScene;
};

/**
 * Cancela cualquier petición AJAX pendiente.
 *
 * @private
 */
CircuitosPage.prototype._cancelAllPendingRequests = function () {
    if (this._lastAsignaturasReq !== null) {
        RestProvider.cancelRequest(this._lastAsignaturasReq);
        this._lastAsignaturasReq = null;
    }
    this._horarios[0].cancelPendingRequest();
    this._horarios[1].cancelPendingRequest();
};

/**
 * Dado un evento rest devuelve una instancia de la clase Evento a insertar en el horario correspondiente. No siempre
 * pintamos los eventos de la misma manera, por eso necesitamos una función que nos proporcione las instancias.
 *
 * @param eventoRest
 * @returns {Evento}
 * @private
 */
CircuitosPage.prototype._getEventoInstance = function (eventoRest) {

    var titulos = {
        "TE": i18n.get_string('teoria'),
        "LA": i18n.get_string('laboratorio'),
        "PR": i18n.get_string('problemas'),
        "TU": i18n.get_string('tutoria'),
        "SE": i18n.get_string('seminario'),
        "ERA": i18n.get_string('evaluacion'),
        "AV": i18n.get_string('evaluacion')
    };

    var color = this._leyenda.getItemColor(eventoRest.asignaturaId);
    var asigUrl =
        LLEU_CONFIG.baseUrl +
        LLEU_CONFIG.cursoAca + '/estudio/' +
        LLEU_CONFIG.estudioId + '/asignatura/' +
        eventoRest.asignaturaId;

    var comentario = [eventoRest.comentario, eventoRest.comentarioDiscont].filter(Boolean).map(function(p) {
        return "<div><p>".concat(p).concat("</p></div>")}).join("");
    return new Evento(
        eventoRest.ini,
        eventoRest.fin,
        eventoRest.diaSemana,
        eventoRest.tipoSubgrupo,
        "<div>".concat(i18n.get_string('grupo'))
            .concat(' ' + eventoRest.grupo)
            .concat('</div><div>')
            .concat(titulos[eventoRest.tipoSubgrupo] + " " + eventoRest.subgrupo)
            .concat("</div>"),
        eventoRest.asignaturaId,
        comentario,
        eventoRest.aula,
        asigUrl,
        color,
        null
    );

};

/**
 * Evento click de los horarios.
 *
 * @param e
 */
CircuitosPage.prototype._onTabChange = function () {

    var me = this;
    var active = this._tabs.find('li.active');
    active = active[1].id;

    if (active === this._lastActiveTab) {
        return;
    }

    this._lastActiveTab = active;
    this._cancelAllPendingRequests();

    var circuitoActivo = this._filterCircuitos.getFilterData();
    circuitoActivo = circuitoActivo.circuito[0];

    if (active === 'asignaturasTab') {

        this._changeScene(CircuitosPage.SHOW.LISTAASIGNATURAS);

        // recargamos, si es necesario, los datos del listado de asignaturas

        if (this._currentCircuitoLista === null || circuitoActivo !== this._currentCircuitoLista) {
            me.showLoadingMask.bind(me)();

            this._loadAsignaturas(circuitoActivo).then(function () {
                me.hideLoadingMask.bind(me)();
            }).catch(function (e) {
                if (e === 'abort') {
                    me.hideLoadingMask.bind(me)();
                    return;
                }
                me.showError.bind(me)("ERROR: " + e);
            });
        }
    } else if (active === 'horario-generalTab') {

        this._changeScene(CircuitosPage.SHOW.HORARIO);
        if (this._forceHorariosReload || this._currentCircuitoHorario === null || circuitoActivo !== this._currentCircuitoHorario) {
            this.loadAllHorarios().then(function () {
                me._currentCircuitoHorario = circuitoActivo;
                me._forceHorariosReload = false;
            });
        }
    }
};

/**
 * Actualiza los elementos de la leyenda.
 *
 * @param eventos
 * @private
 */
CircuitosPage.prototype._updateLeyenda = function (eventos) {

    var me = this;

    // Actualizamos los datos de la leyenda.
    var asignaturas = [];
    var auxAsignaturas = {};

    eventos.forEach(function (e) {
        if (e.asignaturaId in auxAsignaturas) {
            return;
        }
        asignaturas.push({
            codigo: e.asignaturaId,
            titulo: e.asignaturaId + " - " + e.nombreAsignatura
        });
        auxAsignaturas[e.asignaturaId] = true;
    });
    auxAsignaturas = null;
    asignaturas.sort(function (a, b) {
        return a.codigo.toUpperCase().localeCompare(b.codigo.toUpperCase());
    });
    asignaturas.forEach(function (a) {
        me._leyenda.addItem(a.titulo, a.codigo)
    });

};

/**
 * Callback de carga de datos para el horario.
 *
 * @param horario
 * @private
 */
CircuitosPage.prototype._loadGeneralCb = function (horario) {

    this.showLoadingMask();

    var me = this;

    var semestre = (horario.getId() === 'horario-circuito-s1') ? 1 : 2;
    var circuitoId = this._getCircuitoActivo();
    var dao = new HorariosCircuitoDAO();
    var req = dao.get(circuitoId, semestre);
    me._leyenda.removeAll();
    req.promise = req.promise.then(function (r) {

        if (!r.data.success) {
            throw new Error("El servidor no pudo devolver los datos");
        }
        var eventos = r.data.data;
        me._updateLeyenda(eventos);

        me.hideLoadingMask.bind(me)();

        return r;
    }).catch(function (error) {
        if (error === 'abort') {
            me.hideLoadingMask.bind(me)();
            return error;
        }
        me.showError("ERROR: " + error);
        return error;
    });
    return req;
};
CircuitosPage.prototype._loadDetalladaCb = function (horario) {

    this.showLoadingMask();

    var me = this;
    var semestre = (horario.getId() === 'horario-circuito-s1') ? 1 : 2;
    var circuitoId = this._getCircuitoActivo();
    var dao = new HorariosCircuitoDAO();
    var req = dao.get(circuitoId, semestre, horario.getCurrentWeek().getTime());

    req.promise = req.promise.then(function (data) {
        me.hideLoadingMask.bind(me)();
        return data;
    }).catch(function (error) {
        if (error === 'abort') {
            me.hideLoadingMask.bind(me)();
            return error;
        }
        me.showError("ERROR: " + e);
        return error;

    });

    return req;
};

/**
 * Realiza la carga de todos los horarios.
 *
 * @returns {Promise}
 */
CircuitosPage.prototype.loadAllHorarios = function () {

    var me = this;
    var promises = [];

    promises.push(this._horarios[0].load());
    promises.push(this._horarios[1].load());

    return Promise.all(promises).then(function () {
        me._leyenda.show();
    });

};

/**
 * Invocada cuando el horario ha terminado de pintar los eventos en la parrilla.
 *
 * @param {Horario} horario
 * @private
 */
CircuitosPage.prototype._onHorarioLoadFinished = function (horario) {

    // deshabilitamos los eventos que estuvieran deshabilitados en la leyenda.
    var codes = this._leyenda.getItemsDesactivados();
    horario.disableItemsByCode(codes);

    // actualizamos le valor del circuito activo (cargado) para evitar recargas de datos innecesarias.
    var circuitoActivo = this._filterCircuitos.getFilterData();
    this._currentCircuitoHorario = circuitoActivo.circuito[0];

};

/**
 * Funcion que comprueba si el circuito esta abierto es decir sitiene plazas disponibles. Si no estamos en periodo de matricula siempre estan abiertos.
 * @returns {boolean}
 * @private
 */
CircuitosPage.prototype._isCircuitoActivoAbierto = function () {
    var me = this;
    var circuitoActivo = me._filterCircuitos._el.find(".active");
    if (circuitoActivo.data("plazasdisponibles") === 'S' || !LLEU_CONFIG.periodoCircuitos) {
        return true;
    }
    return false;
};

/**
 * Funcion que muestra el error de circuito cerrado.
 */
CircuitosPage.prototype.setVistaCircuitoCerrado = function () {
    var me = this;
    // me._tabs.addClass('hidden');
    // me._leyenda._el.addClass('hidden');
    $("#circuito-cerrado").removeClass('hidden');
};

/**
 * Funcion que muestra la vista normal de la pagina.
 */
CircuitosPage.prototype.setVistaCircuitoAbierto = function () {
    var me = this;
    // me._tabs.removeClass('hidden');
    // me._leyenda._el.removeClass('hidden');
    $("#circuito-cerrado").addClass('hidden');

};