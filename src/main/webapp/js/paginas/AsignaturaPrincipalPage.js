$(document).ready(function() {
    if (!$('body').hasClass('page-asignaturaprincipal')) {
        return;
    }
    var page = new AsignaturaPrincipalPage();
});

AsignaturaPrincipalPage = function() {

    this._el = $('#contenedorhorario');
    if (!this._el) {
        return;
    }

    var me = this;

    this._lastReqId = null;

    this._semestre1 = Horario.getPrimerDiaSemestre1('#contenedorhorario');
    this._semestre2 = Horario.getPrimerDiaSemestre2('#contenedorhorario');

    var fecha;
    if ((Horario.getSemestreAsignatura('#contenedorhorario') === 'a'
        || Horario.getSemestreAsignatura('#contenedorhorario') === '1'
        || Horario.getSemestreAsignatura('#contenedorhorario') === 1) && this._semestre1 !== null) {
        fecha = this._semestre1;
    } else if ((Horario.getSemestreAsignatura('#contenedorhorario') === 'a'
        || Horario.getSemestreAsignatura('#contenedorhorario') === '2'
        || Horario.getSemestreAsignatura('#contenedorhorario') === 2) && this._semestre2 !== null) {
        fecha = this._semestre2;
    } else {
        // doy un margen de 20 días.
        fecha = new Date();
        fecha.setUTCMilliseconds(fecha.getUTCMilliseconds() + 20*24*3600 * 1000);

        if (this._semestre2 && fecha >= this._semestre2) {
            fecha = this._semestre2;
        } else if(this._semestre1 !== null){
            fecha = this._semestre1;
        } else {
            fecha.setUTCMilliseconds(fecha.getUTCMilliseconds() - 20*24*3600 * 1000);
        }
    }

    this._horario = new Horario('#contenedorhorario', 8, 23, Horario.TIPO.SEMANAL, fecha, true);
    this._horario.setWeekNav('#weeknav');
    this._horario.setLoadGeneralCb(this._horarioEventoCb.bind(this), this._horarioLoadGeneralCb.bind(this));
    this._horario.setLoadDetalladaCb(this._horarioEventoCb.bind(this), this._horarioLoadDetalladaCb.bind(this));
    this._horario.setVistaHorario('#vista-horario');
    this._horario.setOnLoadFinishedCb(this._horarioLoadFinished.bind(this));

    this._filtro = new Filtro('#filtro-horario', false, true);
    this._filtro.change(this.onFilterChange.bind(this));

    this._leyendaHorario = new LeyendaEx('#leyenda-subgrupos');
    this._leyendaHorario.click(this.onLeyendaClick.bind(this));

    this._horarioPrint1 = new HorarioPrint('#horarioprint1');

    // Cargamos los datos.
    LoadingIndicator.working(i18n.get_string('cargando'));
    this._getUltimoLectivo().then(function(data) {
        me._horario.setMaxDate(new Date(data.ultimo));
        return me._horario.load().then(function (value) {
            if (value.length == 0) {
                if(data.extinto && data.extinto==1) {
                    $('.sineventos').text(i18n.get_string('asignatura.sineventosExtincion'));
                }
                $('.sineventos').toggleClass('hidden');
            }else{
                $('.coneventos').toggleClass('oculto');
            }
        });
    });
};

AsignaturaPrincipalPage.prototype.onLeyendaClick = function(items) {
    this._horario.disableEventsByClass.bind(this._horario)(items);
};
AsignaturaPrincipalPage.prototype.onFilterChange = function() {
    this._horario.load();
    return true;
};
AsignaturaPrincipalPage.prototype._getUltimoLectivo = function(grupo) {
    var dao = new HorarioAsignaturaDAO(this._el.data('asignatura'));
    return dao.getUltimoLectivo(grupo).promise.then(function(data) {
        return data.data.data;
    });
};
AsignaturaPrincipalPage.prototype._horarioLoadGeneralCb = function(horario) {
    LoadingIndicator.working(i18n.get_string('cargando'));
    var fd = this._filtro.getFilterData();
    var dao = new HorarioAsignaturaDAO(this._el.data('asignatura'));
    return dao.getGeneral(fd['grupo']);
};
AsignaturaPrincipalPage.prototype._horarioLoadDetalladaCb = function(horario) {
    LoadingIndicator.working(i18n.get_string('cargando'));
    var fd = this._filtro.getFilterData();
    var dao = new HorarioAsignaturaDAO(this._el.data('asignatura'));
    return dao.getDetallado(fd['grupo'], horario.getCurrentWeek());
};
AsignaturaPrincipalPage.prototype._horarioEventoCb = function(eventoRest) {
    var css;
    var titulo;

    switch (eventoRest.tipoSubgrupo) {
        case Evento.TIPO.TEORIA:
            titulo = i18n.get_string('teoria') + ' ' + eventoRest.subgrupo;
            css = "teoria";
            break;
        case Evento.TIPO.PROBLEMAS:
            titulo = i18n.get_string('problemas') + ' ' + eventoRest.subgrupo;
            css = "problemas";
            break;
        case Evento.TIPO.TUTORIA:
            titulo = i18n.get_string('tutoria') + ' ' + eventoRest.subgrupo;
            css = "tutoria";
            break;
        case Evento.TIPO.LABORATORIO:
            titulo = i18n.get_string('laboratorio') + ' ' + eventoRest.subgrupo;
            css = "laboratorio";
            break;
        case Evento.TIPO.SEMINARIO:
            titulo = i18n.get_string('seminario') + ' ' + eventoRest.subgrupo;
            css = "seminario";
            break;
        case Evento.TIPO.AV:
        case Evento.TIPO.ERA:
            titulo = i18n.get_string('era') + ' ' + eventoRest.subgrupo;
            css = "era";
            break;
        default:
            titulo = i18n.get_string('unknown') + ' ' + eventoRest.subgrupo;
            break;
    }

    var comentario = [eventoRest.comentario, eventoRest.comentarioDiscont].filter(Boolean).map(function(p) {
        return "<div><p>".concat(p).concat("</p></div>")}).join("");
    var semestre = null;
    if (eventoRest.tipo === 'A') {
        semestre = eventoRest.semestre;
    }

    return new Evento(
        eventoRest.ini,           // from
        eventoRest.fin,           // to
        eventoRest.diaSemana,     // day
        eventoRest.tipoSubgrupo,  // tipo
        titulo,                   // título
        (eventoRest.aula === null) ? i18n.get_string('aulaNoDisponible') : eventoRest.aula,
        comentario,
        null,
        null,
        null,
        css,
        eventoRest.edificio,
        semestre
    );
};

AsignaturaPrincipalPage.prototype._horarioLoadFinished = function(horario, data, eventos) {
    var desactivados = this._leyendaHorario.getItemsDesactivados();
    this._horario.disableEventsByClass(desactivados);
    this._horarioPrint1.addEvents(eventos);

    LoadingIndicator.hide();
};