$(document).ready(function() {
    if (!$('body').hasClass('page-asignaturas')) {
        return;
    }
    var _psp = new AsignaturasPage();
});

/**
 * Controlador de la página de presentación del estudio. Básicamente se trata de gestionar la carga de asignaturas
 * utilizando el filtro.
 *
 * @constructor
 */
AsignaturasPage = function() {

    this._filtro = new Filtro('#filtro-asignaturas');
    this._filtro.change(this.onFilterChange.bind(this));

    if (LLEU_CONFIG.periodoAsignaturas) {
        $('.mostrar-detalle').click(this.onMostrarDetalleClick.bind(this));
    }

    this._lastReqId = null;
};

AsignaturasPage.prototype.onMostrarDetalleClick= function (e) {

    var me = this;
    var mostrardetalle = e.currentTarget;
    var asignaturaId = $(mostrardetalle).data('asignatura');
    if($(mostrardetalle).hasClass('loaded')){
        return;
    }
    LoadingIndicator.working(i18n.get_string("cargando"));
    var dao = new VacantesSubGruposDAO();
    var p = dao.get(
        asignaturaId
    );
    p.promise.then(function (data) {
        if($(mostrardetalle).hasClass('loaded')){
            return;
        }
        var tblVacantes = $(mostrardetalle).closest('li').find('.info').append(data.data);
        $(mostrardetalle).addClass('loaded');
        me.expandSubgrupoClick(tblVacantes);
        LoadingIndicator.hide();
    }).catch(function(err) {
        if (err == 'abort') {
            return;
        }
    });

};

AsignaturasPage.prototype.expandSubgrupoClick = function (tblVacantes) {
    tblVacantes.find('tr.vacantesGrupo td.expand').click(function() {

    if ($(this).hasClass('icon-right-dir')) {
        $(this).removeClass('icon-right-dir').addClass('icon-down-dir');
    } else {
        $(this).removeClass('icon-down-dir').addClass('icon-right-dir');
    }

    var parent = $(this).parent();
    var grupo = parent.data('grupo');
    parent.nextAll('tr.vacantesSubgrupo[data-grupo=' + grupo + ']').toggleClass('hidden');
    }
    );
};

/**
 * Ante un cambio en el filtro, lanzamos una petición ajax que recoje las asignaturas del estudio en el que nos
 * encontramos y las pinta en pantalla.
 *
 * @returns {boolean}
 */
AsignaturasPage.prototype.onFilterChange = function() {

    if (this._lastReqId != null) {
        RestProvider.cancelRequest(this._lastReqId);
    }

    LoadingIndicator.working(i18n.get_string('cargando'));

    var me = this;
    var fd = this._filtro.getFilterData();
    var dao = new AsignaturasDAO();
    var p = dao.get(
        fd['semestre'],
        fd['grupo'],
        fd['curso'],
        fd['caracter'],
        fd['nombre']
    );

    this._lastReqId = p.reqid;

    p.promise.then(function(data) {
        if (data.reqid != me._lastReqId) {
            return;
        }

        me._lastReqId = null;
        $(".asignaturas").replaceWith(data.data);
        if (LLEU_CONFIG.periodoAsignaturas) {
            $('.mostrar-detalle').click(me.onMostrarDetalleClick.bind(me));
        }
        LoadingIndicator.hide();
    }).catch(function(err) {
        if (err == 'abort') {
            return;
        }
        me._lastReqId = null;
        LoadingIndicator.error();
    });

    return true;
};
