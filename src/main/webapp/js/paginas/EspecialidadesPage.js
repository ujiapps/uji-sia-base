$(document).ready(function() {
    if (! $('body').hasClass('page-especialidades')) {
        return;
    }

    rep = new EspecialidadesPage();
});

EspecialidadesPage = function() {
    $(document).foundationCustomForms();

    this._select = $('#dropdown-especialidades');
    this._select.change(this.onEspecialidadSelected.bind(this));

    this._selected = this._select.find('option:selected').attr('value');
    this._selected = $('#' + this._selected);
};

EspecialidadesPage.prototype.onEspecialidadSelected = function(e) {
    var asignaturaId = $(e.currentTarget).find('option:selected').attr('value');
    this._selected.toggleClass('hidden');
    this._selected = $('#' + asignaturaId);
    this._selected.toggleClass('hidden');
};
