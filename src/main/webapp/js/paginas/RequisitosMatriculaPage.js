$(document).ready(function() {
    if (! $('body').hasClass('page-requisitos-matricula')) {
        return;
    }
    var rmp = new RequisitosMatriculaPage();
});

RequisitosMatriculaPage = function() {
    $(document).foundationCustomForms();

    this._select = $('#dropdown-asignatura');
    this._select.change(this.onAsignaturaSelected.bind(this));

    this._selected = this._select.find('option:selected').attr('value');
    this._selected = $('[data-asignaturaId=' + this._selected +']');
};

RequisitosMatriculaPage.prototype.onAsignaturaSelected = function(e) {
    var asignaturaId = $(e.currentTarget).find('option:selected').attr('value');

    this._selected.toggleClass('hidden');
    this._selected = $('[data-asignaturaId=' + asignaturaId +']');
    this._selected.toggleClass('hidden');
};