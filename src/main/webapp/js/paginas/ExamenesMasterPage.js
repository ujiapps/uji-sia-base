$(document).ready(function () {
    if (!$('body').hasClass('page-examenesmaster')) {
        return;
    }
    $(document).foundationButtons();
    var page = new ExamenesMasterPage();
});

/**
 * La página principal de una asignatura. Lo que muestra es el horario, los exámenes y más información. Este controlador
 * se encarga de gestionar el horario y el filtro del horario.
 *
 * @constructor
 */
ExamenesMasterPage = function () {
    this._examenes = $('#contenedorexamen');
    this._calendarios = [];

    if($('#filtro-convocatoria').length == 0){
        return;
    }
    this._filtro = new Filtro('#filtro-convocatoria', false, true);
    this._filtro.change(this.onFiltroClick.bind(this));

    LoadingIndicator.working(i18n.get_string('cargando'));
    this.loadData(this._filtro.getFilterData()).then(function() {
        LoadingIndicator.hide();
    }).catch(function(err) {
        LoadingIndicator.error(i18n.get_string('error.cargando.examenes'));
    })
};

/**
 * Manejador del onlick del Filtro de épocas
 * @param actives
 */
ExamenesMasterPage.prototype.onFiltroClick = function (actives) {
    LoadingIndicator.working(i18n.get_string('cargando'));

    RestProvider.cancelAllRequests();
    this.loadData(actives).then(function() {
        LoadingIndicator.hide();
    }).catch(function(err) {
        LoadingIndicator.error(i18n.get_string('error.cargando.examenes'));
    });

    return true;
};

ExamenesMasterPage.prototype.loadData = function (convocatoria) {

    var me = this;
    var dao = new ExamenesEstudioDAO();
    if(!convocatoria.convocatoria){
        return Promise.reject();
    }
    var conv = convocatoria.convocatoria[0];

    return dao.get(conv).promise.then(function (r) {

        var data = r.data;

        if (data.succes == false) {
            throw "ERROR AJAX";
        }
        var events = data.data;
        me.addCalendars(events);
        for (var i = 0, to = events.length; i < to; i++) {
            var parsedFecha = events[i].fecha.split("/");
            var tipo;
            switch (events[i].tipo) {
                case CalendarEvent.TIPO.TEORIA:
                    tipo = i18n.get_string('teoria');
                    break;
                case CalendarEvent.TIPO.PROBLEMAS:
                    tipo = i18n.get_string('problemas');
                    break;
                case CalendarEvent.TIPO.LABORATORIO:
                    tipo = i18n.get_string('laboratorio') ;
                    break;
                case CalendarEvent.TIPO.PRACTICAS:
                    tipo = i18n.get_string('practica');
                    break;
                default:
                    tipo = i18n.get_string('unknown');
                    break;
            }
            var asigUrl =
                LLEU_CONFIG.baseUrl +
                LLEU_CONFIG.cursoAca + '/estudio/' +
                LLEU_CONFIG.estudioId + '/asignatura/' +
                events[i].codigoAsignatura + '/examenes';

            var e = new CalendarEvent(
                new Date(Date.UTC(parsedFecha[2], parseInt(parsedFecha[1], 10) - 1, parsedFecha[0], 0, 0, 0, 0)),
                events[i].nombreAsignatura,
                tipo,
                events[i].codigoAsignatura,
                events[i].ini,
                events[i].fin,
                events[i].aulas,
                asigUrl,
                events[i].definitivo
            );
            me._calendarios[me.getMonth(events[i].fecha)].addEvent(e);
        }
        for(m in me._calendarios){
            me._calendarios[m].recortarCalendario();
        }

    });
};

ExamenesMasterPage.prototype.addCalendars = function (events) {
    var me = this;

    $('.contenedorexamen').remove();

    var n = {};
    for (var i = 0, to = events.length; i < to; i++) {
        var mes = me.getMonth(events[i].fecha);
        if (!n[mes]) {
            n[mes] = true;
            $(me._examenes).append('<div id="contenedorexamenes_'+mes+'" class="contenedorexamen"></div>');
            me._calendarios[mes] = new Calendario('#contenedorexamenes_'+mes, mes, me.getYear(events[i].fecha));
        }
    }
};

ExamenesMasterPage.prototype.getMonth = function (fecha) {
    var parsedFecha = fecha.split("/");
    return parseInt(parsedFecha[1], 10) - 1;
};
ExamenesMasterPage.prototype.getYear = function (fecha) {
    var parsedFecha = fecha.split("/");
    return parseInt(parsedFecha[2], 10);
};