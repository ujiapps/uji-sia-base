$(document).ready(function() {
    if (!$('body').hasClass('page-publicacion')) {
        return;
    }
    new PublicacionPage();
});
PublicacionPage = function() {
    $.fn.foundationTabs('init', {
        callback: this._onTipoEstudioChanged.bind(this)
    });
    this._tabs = $('div.estudios');

    var active = Props.get(Props.ITEM.tipoestudio);
    switch (active) {
        case "masteresTab":
        case "gradosTab":
            this._tabs.find('.active').toggleClass('active');
            this._tabs.find('.'+active).toggleClass('active');
            this._tabs.find('#' + active).toggleClass('active');
            break;
    }

    $('.btnMuestraListaEstudiosExtincionGrado').on('click', function () {
        PublicacionPage.prototype._onClickBotonEstudiosExtincionGrado();
    });

    $('.btnOcultaListaEstudiosExtincionGrado').on('click', function () {
        PublicacionPage.prototype._onClickBotonEstudiosExtincionGrado();
    });

    $('.btnMuestraListaEstudiosExtincionMaster').on('click', function () {
        PublicacionPage.prototype._onClickBotonEstudiosExtincionMaster();
    });

    $('.btnOcultaListaEstudiosExtincionMaster').on('click', function () {
        PublicacionPage.prototype._onClickBotonEstudiosExtincionMaster();
    });

};

PublicacionPage.prototype._onTipoEstudioChanged = function() {
    var tab = this._tabs.find('li.active').attr('id');
    Props.set(Props.ITEM.tipoestudio, tab);
};

PublicacionPage.prototype._onClickBotonEstudiosExtincionGrado = function (){
    $('.listadoEstudiosExtincionGrado').toggleClass('hidden');
    $('.btnMuestraListaEstudiosExtincionGrado').toggleClass('hidden');
    $('.btnOcultaListaEstudiosExtincionGrado').toggleClass('hidden');
}

PublicacionPage.prototype._onClickBotonEstudiosExtincionMaster = function (){
    $('.listadoEstudiosExtincionMaster').toggleClass('hidden');
    $('.btnMuestraListaEstudiosExtincionMaster').toggleClass('hidden');
    $('.btnOcultaListaEstudiosExtincionMaster').toggleClass('hidden');
}
