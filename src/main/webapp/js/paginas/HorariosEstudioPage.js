$(document).ready(function () {
    if (!$('body').hasClass('page-horariosestudio')) {
        return;
    }
    new HorariosEstudioPage();
});

HorariosEstudioPage = function () {

    this._filter = new Filtro('#filtro-horario', true);
    this._filter.change(this._onFilterChange.bind(this))

    this._semestre1 = Horario.getPrimerDiaSemestre1('#horario-estudio');
    this._semestre2 = Horario.getPrimerDiaSemestre2('#horario-estudio');

    var fecha = new Date();

    var fd = this._filter.getFilterData();
    if (fd['semestre'].length === 0 || fd['semestre'].length === 2) {
        if (fecha < this._semestre1 && fecha < this._semestre2) {
            fecha = this._semestre1;
        } else {
            fecha = this._semestre2;
        }
    } else if (fd['semestre'].indexOf("1") !== -1) {
        fecha = this._semestre1;
    } else {
        fecha = this._semestre2;

    }

    this._horario = new Horario('#horario-estudio', 8, 23, Horario.TIPO.SEMANAL, fecha, true);
    this._horario.setWeekNav('#weeknav');
    this._horario.setLoadGeneralCb(this._horarioEventoCb.bind(this), this._horarioLoadGeneralCb.bind(this));
    this._horario.setLoadDetalladaCb(this._horarioEventoCb.bind(this), this._horarioLoadDetalladaCb.bind(this));
    this._horario.setVistaHorario('#vista-horario');
    this._horario.setOnLoadFinishedCb(this._horarioLoadFinished.bind(this));

    this._leyenda = new LeyendaEx('#leyenda-horario');
    this._leyenda.click(this.onLeyendaClick.bind(this));

    this._weeknavalert = null;

    this._horarioPrint1 = new HorarioPrint('#horarioprint1');

    // peticiones ajax de último día lectivo. Para poder cancelar peticiones en curso.
    this._reqIdUltimoLectivo = null;

    // peticiones ajax de datos. Para poder cancelar peticiones ajax en curso.
    this._reqIdDatos = null;

    // obtenemos el último día lectivo y cargamos el horario.
    var me = this;
    this._getUltimoDiaLectivo().then(function (response) {
        if (response.data.sucess === true) {
            var datos = response.data.data;
            var maxDate = Date.lleuGetFirstDateInWeek(new Date(datos.ultimo));
            me._horario.setMaxDate(maxDate);
            if(datos.curso) {
                me._filter.activaCurso(datos.curso);
                for (c = 1; c < datos.curso; c++) {
                    me._filter.eliminaCurso(c);
                }
            }
            if(datos.semestre) {
                me._filter.activaSemestre(datos.semestre);
                for (s = 1; s < datos.semestre; s++) {
                    me._filter.eliminaSemestre(s);
                }
            }
        }
        // me._horario.load();

        me._horario.load().then(function (value) {
            if (value.length == 0) {
                if(response.data.data.extinto && response.data.data.extinto==1) {
                    $('.sineventos').text(i18n.get_string('estudio.sineventosExtincion'));
                }
                $('.sineventos').toggleClass('hidden');
            }else{
                $('.coneventos').toggleClass('oculto');
            }
        });

    });
};

/**
 * @type {{TE: *, LA: *, PR: *, TU: *, SE: *, ERA: *, AV: *}}
 * @private
 */
HorariosEstudioPage._titulosEvento = {
    "TE": i18n.get_string('teoria'),
    "LA": i18n.get_string('laboratorio'),
    "PR": i18n.get_string('problemas'),
    "TU": i18n.get_string('tutoria'),
    "SE": i18n.get_string('seminario'),
    "ERA": i18n.get_string('evaluacion'),
    "AV": i18n.get_string('evaluacion')
};

/**
 * Manejador de evento del click de un ítem de leyenda.
 * @param disabledItems
 */
HorariosEstudioPage.prototype.onLeyendaClick = function (disabledItems) {
    this._horario.disableItemsByCode(disabledItems);
};

/**
 * Se dispara cuando hay algún cambio en el estado de los filtros.
 *
 * @returns {boolean}
 * @private
 */
HorariosEstudioPage.prototype._onFilterChange = function () {
    var me = this;
    this._horario.load();
    return true;
};

HorariosEstudioPage.prototype._getUltimoDiaLectivo = function () {
    LoadingIndicator.working(i18n.get_string('cargando'));
    var dao = new HorariosEstudioDAO();
    return dao.getUltimoDiaLectivo().promise;
};

/**
 * Actualiza los datos de la leyenda.
 * @param {Array} data un array con datos de los eventos que vamos a utilizar
 * @private
 */
HorariosEstudioPage.prototype._updateLeyenda = function (data) {
    var me = this;

    me._leyenda.removeAll();

    data.sort(function (a, b) {
        return a.asignaturaId.localeCompare(b.asignaturaId);
    }).forEach(function (e) {
        me._leyenda.addItem(
            e.asignaturaId + ' - ' + e.nombreAsignatura,
            e.asignaturaId
        );
    });
};

/**
 * Instancia un objeto de clase Evento para el horario.
 *
 * @param eventoRest
 * @returns {Evento}
 * @private
 */
HorariosEstudioPage.prototype._horarioEventoCb = function (eventoRest) {

    var itemColor = this._leyenda.getItemColor(eventoRest.asignaturaId);

    var asigUrl =
        LLEU_CONFIG.baseUrl +
        LLEU_CONFIG.cursoAca + '/estudio/' +
        LLEU_CONFIG.estudioId + '/asignatura/' +
        eventoRest.asignaturaId;

    var comentario = [eventoRest.comentario, eventoRest.comentarioDiscont].filter(Boolean).map(function(p) {
        return "<div><p>".concat(p).concat("</p></div>")}).join("");

    // Añadimos el evento al horario
    return new Evento(
        eventoRest.ini,
        eventoRest.fin,
        eventoRest.diaSemana,
        eventoRest.tipoSubgrupo,
        HorariosEstudioPage._titulosEvento[eventoRest.tipoSubgrupo] + " " + eventoRest.subgrupo,
        eventoRest.asignaturaId,
        comentario,
        eventoRest.aula,
        asigUrl,
        itemColor,
        null,
        eventoRest.edificio
    );

};

/**
 * Callback de carga de datos para la vista general del horario.
 *
 * @param horario
 * @returns {*|Promise}
 * @private
 */
HorariosEstudioPage.prototype._horarioLoadGeneralCb = function (horario) {

    LoadingIndicator.working(i18n.get_string('cargando'));

    var dao = new HorariosEstudioDAO();
    var fd = this._filter.getFilterData();
    return dao.getGeneral(
        fd['semestre'],
        fd['grupo'],
        fd['curso'],
        fd['caracter'],
        horario.getCurrentWeek()
    );
};

/**
 * Callback de carga de datos para la vista detallada del horario.
 *
 * @param horario
 * @returns {*|Promise}
 * @private
 */
HorariosEstudioPage.prototype._horarioLoadDetalladaCb = function (horario) {

    LoadingIndicator.working(i18n.get_string('cargando'));
    var dao = new HorariosEstudioDAO();
    var fd = this._filter.getFilterData();
    return dao.get(
        fd['semestre'],
        fd['grupo'],
        fd['curso'],
        fd['caracter'],
        horario.getCurrentWeek()
    );
};

HorariosEstudioPage.prototype._horarioLoadFinished = function (horario, eventos) {
    this._updateLeyenda(eventos);
    this._horario.disableItemsByCode(this._leyenda.getItemsDesactivados());
    LoadingIndicator.hide();
};