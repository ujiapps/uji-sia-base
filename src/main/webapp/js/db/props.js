/**
 * Se encarga de almacenar las "preferencias" de la aplicación.
 * @constructor
 */
Props = {};

if (typeof(Storage) == 'undefined') {
    Props._storage = null;
} else {
    Props._storage = sessionStorage;
}

Props.ITEM = {
    'tipoestudio': { name: 'tipoestudio', default: 'grados' }
};

Props.get = function (item) {
    if ( Props._storage == null ) {
        return Props.ITEM[item.name].default;
    }
    return Props._storage.getItem('lleu-' + item.name);
};
Props.set = function (item, value) {
    if (Props._storage == null) {
        return;
    }
    Props._storage.setItem('lleu-' + item.name, value);
};
