FlatPickr = function(containerEl, date, lang) {
    DatePicker.call(this, containerEl, date, lang);
};
FlatPickr.prototype = Object.create(DatePicker.prototype);
FlatPickr.prototype.constructor = FlatPickr;

FlatPickr.prototype._init = function() {
    DatePicker.prototype._init.call(this);

    var locale;
    switch (this._lang) {
        case 'es':
            locale = 'es';
            break;
        case 'en':
            locale = null;
            break;
        default:
            locale = 'cat';
    }

    var id = this._el.attr('id');
    this._datePicker = flatpickr('#' + id, {
        wrap: true,
        clickOpens: false,

        locale: locale,
        weekNumbers: false,
        enableTime: false,
        defaultDate: this._date,

        onOpen: this.onOpen.bind(this),
        onClose: this.onClose.bind(this),
        onChange: this.onChange.bind(this),
        // es importante que tratemos las fechas como UTC para evitar problemas en equipos mal configurados.
        utc: true
    });
};

FlatPickr.prototype.onOpen = function(a, date, picker) {
    picker.setDate(this._date, false);
    DatePicker.prototype.onOpen.call(this);
};


/**
 * Espera una fecha en formato YYYY-MM-DD y devuelve un objeto de tipo Date con la fecha que se pide en la zona horaria
 * UTC.
 *
 * @param {string} strDate la fecha a parsear en formato YYYY-MM-DD
 * @private
 */
FlatPickr.prototype._parseDate = function(strDate) {
    var parsed = strDate.split("-").map(function(n) { return parseInt(n, 10); });
    return new Date(
        Date.UTC(
            parsed[0],
            parsed[1] - 1,
            parsed[2]
        )
    );
};
FlatPickr.prototype.onChange = function(a, datePicked, picker) {
    if (!datePicked) {
        return;
    }
    var date = this._parseDate(datePicked);
    DatePicker.prototype.onChange.call(this, date);
};

FlatPickr.prototype.setDate = function(date) {
    DatePicker.prototype.setDate.call(this, date);
    this._datePicker.setDate(date, false);
};
