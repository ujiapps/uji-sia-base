JQueryPicker = function(containerEl, date, lang) {
    DatePicker.call(this, containerEl, date, lang);

    // no hay manera de decirle a datepicker de jquery que trabaje en modo UTC como sí se puede hacer con flatpickr.
    // lo que hacemos es "transformar" esa fecha a la zona horaria que toca. No tenemos que operar con ella...
    this._mydate = this._fromUTC2Timezone(this._date);

};
JQueryPicker.prototype = Object.create(DatePicker.prototype);
JQueryPicker.prototype.constructor = JQueryPicker;

JQueryPicker.prototype._init = function() {
    DatePicker.prototype._init.call(this);

    this._datePicker = this._el.find('.ie9inputpicker');

    this._datePicker.datepicker({
        changeYear: false,
        onSelect: this.onChange.bind(this),
        onClose: this.onClose.bind(this),
        firstDay: 1
    });

    /**
     * Cargo el fichero de locale correspondiente al idioma desde aquí.
     * @type {JQueryPicker}
     */
    var me = this;

    var url = "//static.uji.es/js/jquery-ui/jquery-ui-1.9.2/ui/i18n/jquery.ui.datepicker";
    var locale;
    switch ( this._lang ) {
        case "es":
            url += "-es.js";
            locale = this._lang;
            break;

        case "en":
            url += "-en-GB.js";
            locale = this._lang;
            break;
        default:
            url += "-ca.js";
            locale = "ca";
    }
    $.ajax({
        url: url,
        dataType: 'script',
        cache: true,
        success: function() {
            me._datePicker.datepicker($.datepicker.regional[locale]);
        }
    });

};

JQueryPicker.prototype.show = function() {
    this._datePicker.datepicker('setDate', this._mydate);
    this._datePicker.datepicker('show');
    DatePicker.prototype.onOpen.call(this);
};

JQueryPicker.prototype.onChange = function() {
    var d = this._datePicker.datepicker('getDate');
    d = this._fromTimezone2UTC(d);
    DatePicker.prototype.onChange.call(this, d);
};

JQueryPicker.prototype.setDate = function(date) {
    DatePicker.prototype.setDate.call(this, date);
    this._mydate = this._fromUTC2Timezone(this._date);
    this._datePicker.datepicker('setDate', this._mydate);
};

JQueryPicker.prototype._fromUTC2Timezone = function(date) {
    return new Date(
        date.getUTCFullYear(),
        date.getUTCMonth(),
        date.getUTCDate(),
        date.getUTCHours(),
        date.getUTCMinutes(),
        date.getUTCSeconds(),
        date.getUTCMilliseconds()
    );
};
JQueryPicker.prototype._fromTimezone2UTC = function(date) {
    var ret = new Date();
    ret.setTime(
        Date.UTC(
            date.getFullYear(),
            date.getMonth(),
            date.getDate(),
            date.getHours(),
            date.getMinutes(),
            date.getSeconds(),
            date.getMilliseconds()
        )
    );

    return ret;
};
