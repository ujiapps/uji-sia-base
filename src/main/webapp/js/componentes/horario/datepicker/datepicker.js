/**
 * Envoltorio para los DatePicker que utlizamos en el LLEU (FlatPickr y JQueryPicker)
 *
 * @param date
 * @param lang
 * @constructor
 */
DatePicker = function(containerEl, date, lang) {

    this._el = containerEl;

    // La fecha que tiene que representar. Siempre la obtendremos en la zona horaria UTC.
    this._date = date;

    // El idioma en que queremos que salga el asunto.
    this._lang = lang;

    // Instancia del datepicker que vamos a utilizar internamente: FlatPick o JQUeryPickr.
    this._datePicker = null;

    // Callbacks a invocar cuando abrimos el picker, hay un cambio en la fecha o se cierra el picker.
    this._openCallbacks = [];
    this._changeCallbacks = [];
    this._closeCallbacks = [];

    this._init();
};

DatePicker.PICKER = {
    FLATPICKER: 'flatpickr',
    JQUERYPICKR: 'jquerypickr'
};

DatePicker.getInstance = function(containerEl, pickerName, date, lang) {
    var ret = null;
    switch (pickerName) {
        case DatePicker.PICKER.FLATPICKER:
            ret = new FlatPickr(containerEl, date, lang);
            break;

        case DatePicker.PICKER.JQUERYPICKR:
            ret = new JQueryPicker(containerEl, date, lang);
            break;
    }

    return ret;
};

DatePicker.prototype._init = function() {
};
DatePicker.prototype.open = function(cb) {
    this._openCallbacks.push(cb);
};
DatePicker.prototype.change = function(cb) {
    this._changeCallbacks.push(cb);
};
DatePicker.prototype.close = function(cb) {
    this._closeCallbacks.push(cb);
};
DatePicker.prototype.onOpen = function(cb) {
    for (var i = 0; i < this._openCallbacks.length; i++) {
        this._openCallbacks[i]();
    }
};
DatePicker.prototype.onChange = function(date) {
    var d = Date.lleuGetFirstDateInWeek(date);
    for (var i = 0; i < this._changeCallbacks.length; i++ ) {
        this._changeCallbacks[i](d);
    }
};
DatePicker.prototype.onClose = function() {
    for (var i = 0; i < this._closeCallbacks.length; i++ ) {
        this._closeCallbacks[i]();
    }
};

DatePicker.prototype.setDate = function(date) {
    this._date = date;
};
