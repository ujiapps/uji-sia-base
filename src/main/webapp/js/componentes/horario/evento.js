/**
 * Representa un evento para pintar en el componente Horario. El evento no puede durar más de un día.
 *
 * @param {string} from indica la hora de inicio del evento. Por ejemplo, "15:00".
 * @param {string} to indica la hora de fin del evento. Por ejemplo, "17:30".
 * @param {number} day indica el día de la semana en el que está el evento. Utilizar Evento.DAY.
 * @param {Evento.TIPO} tipo indica el tipo del evento (DEPRECATED).
 * @param {string} titulo indica el título que pintaremos para el evento.
 * @param {string} codigo indica un código para el evento.
 * @param {string} observaciones indica algún comentario que pueda estar asociado al evento.
 * @param {string} localizacion indica la localización del evento (el aula, etc.)
 * @param {string} url una url que utilizaremos para enlazar el evento. Normalmente la URL de una asignatura.
 * @param {string} color indica un color asociado al evento (para que lo use Horario)
 * @param {string} cssClass indica una clase que queremao (indica una clase que podemos añadir al evento)
 * @constructor
 */
Evento = function(from, to, day, tipo, titulo, codigo, observaciones, localizacion, url, color, cssClass, edificio, semestre) {
    this.from = this._normalizeTime(from);
    this.to = this._normalizeTime(to);
    this.day = day;
    // TODO: Creo que podemos eliminar esto del tipo
    this.tipo = tipo;
    this.titulo = titulo;
    this.codigo = codigo;
    this.observaciones = observaciones;
    this.localizacion = localizacion;

    this.url = url;
    this.edificio = edificio;
    this.color = color;
    this.cssClass = cssClass;

    // indica si el evento está deshabilitado --> resultado de deshabilitarlo en una leyenda.
    this.disabled = false;

    this.semestre = semestre;
};
Evento.DAY = {
    LUNES : 1,
    MARTES : 2,
    MIERCOLES : 3,
    JUEVES : 4,
    VIERNES : 5,
    SABADO : 6,
    DOMINGO : 7
};
Evento.TIPO = {
    TEORIA: "TE",
    LABORATORIO: "LA",
    PROBLEMAS: "PR",
    TUTORIA: "TU",
    SEMINARIO: "SE",
    ERA: "ERA",
    AV: "AV"
};
Evento.prototype._normalizeTime = function(time) {
    var match = /([0-9]{1,2})(:([0-9]{1,2}))?(:([0-9]{1,2}))?$/g.exec(time.trim());
    var hour = match[1];
    var minutes;
    if (match[3]) {
        minutes = match[3];
    } else {
        minutes = "00";
    }
    return this._redondeaACuartos(new Date(1980,1,8,hour,minutes,0,0));
};
Evento.prototype.toString = function() {
    return this.titulo + " / " + this.codigo + " / " + this.day + " / " + this.from.lleuGetFormattedTime() + " / " + this.to.lleuGetFormattedTime() + " / " + this.color;
};
Evento.prototype._redondeaACuartos = function(date) {
    var ret = new Date(date.getTime());
    ret.setMinutes(0);
    var minutes = date.getMinutes();
    var offset = (Math.round(minutes/15) * 15) * 60 * 1000;
    return new Date(ret.getTime() + offset);
};
