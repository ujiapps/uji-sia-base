ColeccionEventos = function() {
    /**
     * Lista ordenada de eventos for fecha de inicio
     * @type {Array}
     * @private
     */
    this.eventos = [];
    this.from = null;
    this.to = null;
    // en una colección de eventos tenemos eventos del mismo día.
    this.day = null;
};
ColeccionEventos.prototype.canBeAdded = function(evento) {
    return this.day == evento.day;
};
ColeccionEventos.prototype.add = function(evento) {

    if (!this.day) {
        this.day = evento.day;
    } else if (this.day != evento.day) {
        throw "No puedo añadir un evento a la colección de otro día de la semana";
    }

    if (this.eventos.length == 0) {
        if ( evento instanceof ColeccionEventos) {
            this.eventos = evento.eventos;
        } else {
            this.eventos.push(evento);
        }
    } else {
        var pos = 0;
        do {
            if (this.eventos[pos].from > evento.from) {
                break;
            }
            pos = pos +1;
        } while (pos < this.eventos.length);

        var toadd;
        if (evento instanceof ColeccionEventos) {
            toadd = evento.eventos;
        } else {
            toadd = [evento];
        }
        if (pos == 0) {
            this.eventos = toadd.concat(this.eventos);
        } else {
            this.eventos = this.eventos.slice(0, pos).concat(toadd, this.eventos.slice(pos));
        }
    }
    if (!evento.disabled) {
        if (!this.from || this.from > evento.from) {
            this.from = evento.from;
        }
        if (!this.to || this.to < evento.to) {
            this.to = evento.to;
        }
    }
};
