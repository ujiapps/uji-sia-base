/**
 * Controlador para el navegador por semanas de un horario.
 *
 * @param contenedor
 * @param week fecha de inicio de la semana
 * @param maxDate indica la fecha máxima de navegación
 * @param lang el idioma en el que queremos sacar el date picker
 * @constructor
 */
WeekNav = function (contenedor, week, lang) {
    this._el = $(contenedor);
    if (!this._el) {
        return;
    }

    this._lang = lang;

    this._enabled = true;

    this._prevCbs = [];
    this._nextCbs = [];
    this._todayCbs = [];
    this._weekCbs = [];
    this._maxWeekCbs = [];
    this._minWeekCbs = [];
    this._semestre1Cbs = [];
    this._semestre2Cbs = [];

    this._fromDate = null;
    this.setWeek(week);

    this._maxDate = null;
    this._minDate = new Date(this._fromDate.getTime());

    this._el.find('.nextweek').click(this.onNextWeek.bind(this));
    this._el.find('.prevweek').click(this.onPrevWeek.bind(this));

    this._el.find('.gotoToday').click(this._onTodayClick.bind(this));

    // Podemos tener el menú "Ir a" para mostrar saltos a primer semestre, segundo o para ir al día de hoy. Eso
    // lo manejamos de manera diferente.
    if (this._el.hasClass('showira')) {
        this._el.find('.btn_ira a').click(this._onBtnIraClick.bind(this));
    } else {
        this._el.find('.today').click(this._onTodayClick.bind(this));
    }

    // navegación entre semanas con las teclas.
    var me = this;
    $(document).keydown(function(e) {
        if (me._el.hasClass('hidden')) {
            return;
        }

        switch (e.keyCode) {
            case WeekNav.LEFT_ARROW:
                me._el.find('.prevweek').click();
                break;
            case WeekNav.RIGHT_ARROW:
                me._el.find('.nextweek').click();
                break;
        }
    });

    this._datePickerShown = false;
    this._datePicker = null;

    var pickerName;

    // utilizo jquery datepicker en internet explorer. si no flatpickr.
    var msie = window.navigator.userAgent.indexOf("MSIE");
    if (msie > 0) {
        pickerName = DatePicker.PICKER.JQUERYPICKR;
        this._el.find('.week').click(function() {
            me._datePicker.show();
        });

    } else {
        pickerName = DatePicker.PICKER.FLATPICKER;
    }

    this._datePicker = DatePicker.getInstance(
        this._el,
        pickerName,
        this._fromDate,
        this._lang
    );
    this._datePicker.open(function() {
        me._datePickerShown = true;
    });
    this._datePicker.close(function() {
        me._datePickerShown = false;
    });
    this._datePicker.change(function(d) {
        // si no cambiamos de semana, no hace falta que continuemos.
        if ( d.getUTCFullYear() == me._fromDate.getUTCFullYear() &&
            d.getUTCMonth() == me._fromDate.getUTCMonth() &&
            d.getUTCDate() == me._fromDate.getUTCDate()
        ) {
            return;
        }

        me.setWeek(d);

        // llamamos a las callbacks registradas con la función week
        for (var i = 0; i < me._weekCbs.length; i++) {
            me._weekCbs[i]();
        }
    });
};

WeekNav.LEFT_ARROW = 37;
WeekNav.RIGHT_ARROW = 39;

WeekNav.prototype.getId = function() {
    return this._el.attr('id');
};

/**
 * Oculta el navegador por semanas.
 */
WeekNav.prototype.hide = function() {
    if (!this._el.hasClass('hidden')) {
        this._el.addClass('hidden');
    }

};

WeekNav.prototype.show = function() {
    if (this._el.hasClass('hidden')) {
        this._el.removeClass('hidden');
    }
};


WeekNav.prototype.setMaxDate = function(maxDate) {
    if (this._maxDate && maxDate.getTime() === this._maxDate.getTime()) {
        return;
    }
    this._maxDate = maxDate;
    if (this._fromDate.getTime() > this._maxDate.getTime()) {
        this.setWeek(Date.lleuGetFirstDateInWeek(this._maxDate));
        this.onMaxWeek(this._fromDate, this._maxDate);
    }
};
WeekNav.prototype.getMaxDate = function() {
    return this._maxDate;
};
WeekNav.prototype.getMinDate = function() {
    return this._minDate;
};

/**
 * Registra un manejador en caso de que se pulse el botón de semana siguiente.
 *
 * @param cb
 */
WeekNav.prototype.nextWeek = function (cb) {
    this._nextCbs.push(cb);
};

/**
 * Registra un manejador en caso de que se pulse el botón de semana previa
 *
 * @param cb
 */

WeekNav.prototype.prevWeek = function (cb) {
    this._prevCbs.push(cb);
};

/**
 * Devuelve una promesa que se resuelve cuando hacemos click en el botón "Hoy"
 *
 * @returns {Promise}
 */
WeekNav.prototype.today = function(cb) {
    this._todayCbs.push(cb);
};

/**
 * Registra una callback para cuando pulsamos el botón week.
 *
 * @param cb
 */
WeekNav.prototype.week = function(cb) {
    this._weekCbs.push(cb);
};
/**
 * Registra una callback para ser notificada cuando se pulsa el botón de ir a semestre 1.
 *
 * @param {Function} cb
 */
WeekNav.prototype.semestre1 = function(cb) {
    this._semestre1Cbs.push(cb);
};
/**
 * Registra una callback para ser notificada cuando se pulsa el botón de ir a semestre 2.
 *
 * @param {Function} cb
 */
WeekNav.prototype.semestre2 = function(cb) {
    this._semestre2Cbs.push(cb);
};

/**
 * Registra callbacks a llamar si se ha intentado sobrepasar la fecha máxima de navegación.
 */
WeekNav.prototype.maxWeekSuperseed = function(cb) {
    this._maxWeekCbs.push(cb);
};

/**
 * Registra callbacks a llamar si se ha intentado sobrepasar la fecha máxima de navegación.
 */
WeekNav.prototype.minWeekSuperseed = function(cb) {
    this._minWeekCbs.push(cb);
};


/**
 *
 * @param e
 * @private
 */
WeekNav.prototype._onTodayClick = function(e) {
    for (var i = 0; i < this._nextCbs.length; i++) {
        this._todayCbs[i]();
    }
    e.preventDefault();
};

/**
 * Navegamos hacia la siguiente semana
 */
WeekNav.prototype.onNextWeek = function () {
    for (var i = 0; i < this._nextCbs.length; i++) {
        this._nextCbs[i]();
    }
};

/**
 * Navegamos hacia la semana previa
 */
WeekNav.prototype.onPrevWeek = function () {
    for (var i = 0; i < this._prevCbs.length; i++) {
        this._prevCbs[i]();
    }
};

/**
 * Se dispara si intentamos establecer una fecha mayor a la fecha máxima.
 *
 * @param {Date} date la fecha que hemos intentado establecer.
 * @param {Date} maxDate la fecha máximo que se puede establecer.
 */
WeekNav.prototype.onMaxWeek = function (date, maxDate) {
    for (var i = 0; i < this._maxWeekCbs.length; i++) {
        this._maxWeekCbs[i](date, maxDate);
    }
};

WeekNav.prototype.onMinWeek = function (date) {
    for (var i = 0; i < this._minWeekCbs.length; i++) {
        this._minWeekCbs[i](date, this._fromDate);
    }
};

/**
 * Pinta la semana.
 *
 * @param {Date} weekDay primer día de la semana que queremos pintar.
 */
WeekNav.prototype.setWeek = function (weekDay) {

    // Si se supera la fecha máxima, avisamos y establecemos la fecha máxima como fecha actual.
    if (this._maxDate && weekDay > this._maxDate) {
        this.onMaxWeek(weekDay, this._maxDate);
        weekDay = Date.lleuGetFirstDateInWeek(this._maxDate);
    }
    if (weekDay < this._minDate) {
        this.onMinWeek(weekDay, this._minDate);
        weekDay = this._minDate;
    }

    var toDate = new Date();

    // sólo me interesa tener el día, por eso sumo 4 días. Si sumara 5 (que es lo primero que viene a la cabeza)
    // me daría el sábado a las 12 de la noche y, realmente, las semanas dentro de la parrilla son de lunes a viernes.
    toDate.setTime(weekDay.getTime() + 4 * 24 * 3600 * 1000);

    this._fromDate = weekDay;

    var weekStr =
        weekDay.getUTCDate() + ' ' +
        i18n.get_month(weekDay.getUTCMonth()).toUpperCase() + ' - ' +
        toDate.getUTCDate() + ' ' +
        i18n.get_month(toDate.getUTCMonth()).toUpperCase();

    this._el.find('.weekstr').html(weekStr);

    // actualizamos el picker si está mostrado
    if (this._datePicker) {
        this._datePicker.setDate(this._fromDate);
    }
};

/**
 * Devuelve el día de inicio de la semana que se considera
 *
 * @returns {Date}
 */
WeekNav.prototype.getWeek = function () {
    return this._fromDate;
};

/**
 * Click al menú de Ira (si se muestra).
 *
 * @param e
 * @private
 */
WeekNav.prototype._onBtnIraClick = function(e) {
    var target = $(e.currentTarget);
    // if (target.hasClass('gotoToday')) {
    //     this._onTodayClick(e);
    // } else
        if (target.hasClass('gotoS1')) {
        this._onSemestre1Click(e);
    } else if (target.hasClass('gotoS2')) {
        this._onSemestre2Click(e);
    }
    e.stopPropagation();

    this._el.find('.btn_ira ul.show-dropdown').removeClass('show-dropdown');
};
WeekNav.prototype._onSemestre1Click = function(e) {
    for (var i = 0, to = this._semestre1Cbs.length; i < to; i++) {
        this._semestre1Cbs[i]();
    }
    e.preventDefault();
};
WeekNav.prototype._onSemestre2Click = function(e) {
    for (var i = 0, to = this._semestre1Cbs.length; i < to; i++) {
        this._semestre2Cbs[i]();
    }
    e.preventDefault();
};
