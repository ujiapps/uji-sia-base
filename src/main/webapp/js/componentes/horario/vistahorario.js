VistaHorario = function(containerId, tipo) {
    this._el = $(containerId);
    if (! this._el) {
        return;
    }

    if (tipo === VistaHorario.TIPO.DETALLADA) {
        if (!this._el.find('.active').hasClass('vista-detallada')) {
            this._el.find('.active').removeClass('active');
            this._el.find('.vista-detallada').addClass('active');
        }
    } else if ( tipo === VistaHorario.TIPO.GENERAL) {
        if (!this._el.find('.active').hasClass('vista-general')) {
            this._el.find('.active').removeClass('active');
            this._el.find('.vista-general').addClass('active');
        }
    }

    this._el.find('input').on('click', this._onClick.bind(this));
    this._changeCallbacks = [];

};

VistaHorario.TIPO = {
    GENERAL: 'vista-general',
    DETALLADA: 'vista-detallada'
};

/**
 * Registra una callback para el evento change.
 * @param cb
 */
VistaHorario.prototype.change = function(cb) {
    this._changeCallbacks.push(cb);
};


VistaHorario.prototype._onClick = function(e) {

    this._el.find('.active').removeClass('active');
    $(e.target).addClass('active');

    var activa = $(e.target);
    if (activa.hasClass('vista-detallada')) {
        activa = 'vista-detallada';
    } else {
        activa = 'vista-general';
    }
    for (var i = 0; i < this._changeCallbacks.length; i++) {

        this._changeCallbacks[i](activa);
    }
};
