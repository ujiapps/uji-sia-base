/**
 * Construye un horario en el contenedor indicado. Si encuentra un contenedor acabado en -listado incluira 2 vistas una de horario y otra un listado de eventos
 *
 * @param container id del div contenedor del horario
 * @param fromHour la hora de inicio (entero)
 * @param toHour la hora de fin (entero)
 * @param tipo el tipo de horario: SEMANAL, CALENDARIO
 * @param fromDate la fecha de inicio para un Horario de tipo CALENDARIO
 * @param mostrarEdificio mostrar plano de edificio en modal al hacer click
 * @param {Boolean} doShrink acortar el horario si es necesario.
 * @constructor
 */
Horario = function (container, fromHour, toHour, tipo, fromDate, mostrarEdificio, doShrink) {

    this._el = $(container);
    if (!this._el) {
        return;
    }
    this._modoListado = false;
    this._forceHorariosReload = true;

    this._listadoContainerEl = $(container + '-listado');
    if (this._listadoContainerEl.length) {
        $.fn.foundationTabs({
            callback: this._onTabChange.bind(this)
        });
        this._tabs = $('#tabs-vistas');
        this._setModoVista();
    }

    // si no tiene ID, añadimos un ID aleatorio (viene bien si tenemos más de un horario en la misma página)
    if (!this._el.attr('id')) {
        this._el.attr('id', "horario-" + Math.floor(Math.random() * 100) + 1);
    }

    this._id = this._el.attr('id');

    /**
     * Objeto de eventos donde cada objeto tien los siguientes campos:
     *     * ui: elemento de la interfaz de usuario
     *     * evento: instancia de la clase Evento asociada
     */
    this._eventos = {};

    this._eventosSabado = [];

    // Eventos deshabilitados.
    this._disabledEvents = [];

    // Utilizamos este array para controlar los zindex de los eventos
    this._eventsZindex = {};

    // Hora de inicio del calendario.
    this._fromHour = fromHour;

    // Hora final del calendario.
    this._toHour = toHour;

    this._availableId = 1;

    // Indica si mostrar el edificio en los eventos.
    this._mostrarEdificio = mostrarEdificio;

    // Control de vista horarios.
    this._vistaHorarioControl = null;

    // El tipo de Horario (ver Horario.TIPO).
    this._tipo = tipo;

    // Indica la fecha inicial en que el horario (de tipo CALENDARIO) puede empezar a representar datos.

    if (!fromDate) {
        fromDate = new Date();
    }

    this._fromDate = new Date();
    this._fromDate.setTime(fromDate.getTime());

    if (this._fromDate.getUTCDay() !== 1) {
        var offset = this._fromDate.getUTCDay() - 1;
        this._fromDate.setTime(this._fromDate.getTime() - offset * 24 * 3600 * 1000);
    }

    this._fromDate.setUTCHours(0);
    this._fromDate.setUTCMinutes(0);
    this._fromDate.setUTCSeconds(0);
    this._fromDate.setUTCMilliseconds(0);

    // La fecha máxima que puede navegar el horario en caso de que gestionemos un horario de tipo CALENDARIO.
    if (this._el.data('ultimolectivo')) {
        this._maxDate = Horario.getUltimoLectivo(container);
    }
    if (!this._maxDate || this._maxDate <= this._fromDate) {
        this._maxDate = new Date(this._fromDate.getTime() + 365 * 24 * 3600 * 1000);
    }

    // Podemos asociar un weeknav al Horario para evitar tener que gestionar la lógica desde un controlador de página.
    this._weeknav = null;

    // Callback a llamar para realizar la carga de eventos de la vista detallada (calendario) del horario.
    this._loadDetalladaCb = null;

    // Callback a llamar para realizar la carga de eventos de la vista general (semanal) del horario.
    this._loadGeneralCb = null;

    // Último id de petición AJAX que está en marcha. Lo utilizamos para cancelar peticiones en curso.
    this._lastReqId = null;

    // Invocada cuando el horario ha terminado de pintar los eventos. Esto es interesante cuando es el horario el que
    // provoca una carga de datos, por ejemplo, cuando gestiona el mismo las llamadas al componente WeekNav.
    this._loadFinishedCb = null;

    // Aviso de semana sobrepasada.
    this._weekAlertBox = null;

    // Acortar el horario.
    this._doShrink = doShrink;

    // Indica si es la primera carga o no.
    this._firstLoad = true;

    this._generateAll();
};

/**
 * Tipos de horario. El semanal indica únicamente días de la semana. El CALENDARIO tiene en cuenta, además, el día del
 * año y mes a la hora de tratar los eventos y de representar los datos en la cabecera del horario.
 *
 * @type {{SEMANAL: number, CALENDARIO: number, LISTADO: number}}
 */
Horario.TIPO = {
    SEMANAL: 1,
    CALENDARIO: 2,
    LISTADO: 3
};

Horario.GROUNDZINDEX = 9999;
Horario.TOPZINDEX = 10000;

Horario.MONTHNAMES = [
    i18n.get_string('enero'),
    i18n.get_string('febrero'),
    i18n.get_string('marzo'),
    i18n.get_string('abril'),
    i18n.get_string('mayo'),
    i18n.get_string('junio'),
    i18n.get_string('julio'),
    i18n.get_string('agosto'),
    i18n.get_string('septiembre'),
    i18n.get_string('octubre'),
    i18n.get_string('noviembre'),
    i18n.get_string('diciembre')
];
Horario.DAYNAMES = [
    i18n.get_string('lunes'),
    i18n.get_string('martes'),
    i18n.get_string('miercoles'),
    i18n.get_string('jueves'),
    i18n.get_string('viernes'),
    i18n.get_string('sabado'),
    i18n.get_string('domingo')
];

Horario.TIPOSUBGRUPOS = {
    TE: {name: 'teoria', traduccion: i18n.get_string('teoria')},
    LA: {name: 'laboratorio', traduccion: i18n.get_string('laboratorio')},
    PR: {name: 'problemas', traduccion: i18n.get_string('problemas')},
    TU: {name: 'tutoria', traduccion: i18n.get_string('tutoria')},
    SE: {name: 'seminario', traduccion: i18n.get_string('seminario')},
    ERA: {name: 'era', traduccion: i18n.get_string('evaluacion')},
    AV: {name: 'era', traduccion: i18n.get_string('evaluacion')}
};


Horario.MAPS = {'T': 'ESTCE', 'H': 'FCHS', 'HD': 'AFCHS', 'J': 'FCJE', 'M': 'FCS'};

Horario.prototype._setModoVista = function () {
    var active = this._tabs.find('li.active');
    active = active[1].id;
    this._modoListado = (active === 'listadoTab')
};


Horario.prototype.getId = function () {
    return this._el.attr('id');
};

Horario.prototype.getMaxDate = function () {
    return this._maxDate;
};
Horario.prototype.setMaxDate = function (maxDate) {
    this._maxDate = maxDate;
    if (this._weeknav) {
        if (this._weeknav.getWeek() > maxDate) {
            this._weeknav.setWeek(maxDate);
        }
        this._weeknav.setMaxDate(maxDate);
    }
    this._maxDate = maxDate;
};

Horario.prototype.remove = function () {
    var me = this;
    me._el.find('.evento,.coleccioneventos').remove();
    me._el.children().remove();
};
/**
 * Añade un array de eventos
 */
Horario.prototype.addEvents = function (eventos) {
    for (var i = 0; i < eventos.length; i++) {
        this.addEvento(eventos[i]);
    }
};

/**
 * En un resize, es posible que se produzcan overflows de eventos y colecciones. Hay que gestionar el asunto.
 * @param e
 */
Horario.prototype.onResize = function (e) {
    var els = this._el.find('.evento');
    for (var i = 0; i < els.length; i++) {
        this.handleEventOverflow($(els[i]));
    }
    els = this._el.find('.coleccioneventos');
    for (var i = 0; i < els.length; i++) {
        this.handleEventOverflow($(els[i]));
    }
};
/**
 * Devuelve un id único para gestionar un evento
 */
Horario.prototype._generateId = function () {
    var ret = "event-" + this._id + '-' + this._availableId;
    this._availableId += 1;
    return ret;
};
/**
 * Inserta la interfaz del horario en el contenedor
 *
 * @private
 * @depends i18n
 */
Horario.prototype._generateTable = function () {

    var c;
    if (this._tipo === Horario.TIPO.CALENDARIO) {
        c = "horario calendario";
    } else {
        c = "horario semanal";
    }
    this._el.append('<table class="' + c + ' tbody" role="presentation"></table>');

    var table = this._el.find('table');
    var thead = '';
    if (this._tipo === Horario.TIPO.CALENDARIO) {

        var dayTemplate = '<div class="weekday" data-day="{dayn}">' +
            '<div class="daystr"></div>' +
            '</div>';

        thead = '<tr class="thead">' +
            '<td></td>';

        var f = new Date();
        f.setTime(this._fromDate);
        for (var i = 0; i < 5; i++) {
            var aux = dayTemplate;

            aux = aux.replace(/\{dayn}/, i.toString());
            thead += '<td>' + aux + '</td>';

            f.setTime(f.getTime() + 3600 * 24 * 1000);
        }

        thead += '</tr>';
    } else {
        thead = '<tr class="thead">' +
            '<td></td>' +
            '<td>' + i18n.get_string('lunes') + '</td>' +
            '<td>' + i18n.get_string('martes') + '</td>' +
            '<td>' + i18n.get_string('miercoles') + '</td>' +
            '<td>' + i18n.get_string('jueves') + '</td>' +
            '<td>' + i18n.get_string('viernes') + '</td>' +
            '</tr>';
    }

    table.append(thead);
    table.append('<tr class="tbody-row hidden"></tr>');
    for (var i = this._fromHour; i < this._toHour; i++) {
        for (var j = 0; j < 2; j++) {
            var hora, tr, css = "", data = '';

            if (i === (this._toHour - 1) && j === 1) {
                css += "last";
            }
            if (j % 2 === 0) {
                hora = 12 * parseInt(i / 12, 10) + i % 12 + ':00';
                data = hora;
            } else {
                hora = "";
                data = 12 * parseInt(i / 12, 10) + i % 12 + ':30';
                css += " bold";
            }

            this.appendRow(css, data);
        }
    }
};


Horario.prototype.appendRow = function (css, hour) {

    var tbody = this._el.find('table');

    if (css.length > 0) {
        tr = '<tr class="' + css.trim() + ' tbody-row" data-hour="' + hour + '">';
    } else {
        tr = '<tr data-hour="' + hour + '" class="tbody-row">';
    }
    tr += '<td>' + hour + '</td>' +
        '<td data-day="1"></td>' +
        '<td data-day="2"></td>' +
        '<td data-day="3"></td>' +
        '<td data-day="4"></td>' +
        '<td data-day="5"></td>' +
        '</tr>';

    tbody.append(tr);
};
Horario.prototype.prependRow = function (css, hour) {
    var tbody = this._el.find('table');

    if (css.length > 0) {
        tr = '<tr class="' + css.trim() + ' tbody-row" data-hour="' + hour + '">';
    } else {
        tr = '<tr data-hour="' + hour + '" class="tbody-row">';
    }
    tr += '<td>' + hour + '</td>' +
        '<td data-day="1"></td>' +
        '<td data-day="2"></td>' +
        '<td data-day="3"></td>' +
        '<td data-day="4"></td>' +
        '<td data-day="5"></td>' +
        '</tr>';

    tbody.prepend(tr);
};


Horario.prototype._getHourHTML = function (hour, css) {
    var tr;

    if (css.length > 0) {
        tr = '<tr class="' + css.trim() + ' tbody-row" data-hour="' + hour + '">';
    } else {
        tr = '<tr data-hour="' + hour + '" class="tbody-row">';
    }
    tr += '<td>' + hora + '</td>' +
        '<td data-day="1"></td>' +
        '<td data-day="2"></td>' +
        '<td data-day="3"></td>' +
        '<td data-day="4"></td>' +
        '<td data-day="5"></td>' +
        '</tr>';
    return tr;
};

Horario.prototype._dameModal = function (edificio) {
    var centro = edificio in Horario.MAPS ? Horario.MAPS[edificio] : null;
    if (!centro)
        return '';

    return '<div class="modal"><img class="modal-content" src="/sia/pix/' + centro + '-' + LLEU_CONFIG.lang + '.jpg"/ alt="Plano ' + centro + '"></div>';
};

/**
 * Dado un evento, contruye un div para representar dentro de la tabla
 */
Horario.prototype._getEventoUI = function (evento) {

    var css = "evento";
    var style = "";

    if (evento.color !== null) {
        style = 'style="background-color: ' + evento.color + ';"';
    }
    if (evento.cssClass !== null) {
        css += " " + evento.cssClass;
    }
    var div = '<div class="' + css + '" ' + style + ' data-codigo="' + evento.codigo + '">';

    if (evento.url) {
        div += '<p class="evasignatura">' +
            '<a href="' + evento.url + '" class="actionlink">' + evento.codigo + '</a>' +
            '</p>';
    } else {
        evento.codigo.split(',').forEach(e => {
                div +=  '<p class="evasignatura">'+
                            (this._mostrarEdificio && evento) ? this._dameLocalizacionEspacios(e) : e +
                        '</p><br>';
            }
        );

    }

    div += '<div class="evtitulo">';
    if (evento.observaciones) {
        div += '<span class="tip"><span class="icon-info icon-info-circled"><span class="observaciones">' + evento.observaciones + '</span></span></span>';
        div += '<p>' + evento.titulo + '</p></div>';
    } else {
        div += '<p>' + evento.titulo + '</p></div>';
    }

    if (this._tipo === Horario.TIPO.SEMANAL && evento.semestre) {
        div += '<div class="evsemestre">';
        div += '<p>' + i18n.get_string(evento.semestre) + '</p>';
        div += '</div>';
    }

    if (evento.localizacion) {
        if (evento.localizacion) {
            evento.localizacion.split(',').forEach(e => {
                    div +=  '<p class="evlocaliza">'+
                                (this._mostrarEdificio && evento) ? this._dameLocalizacionEspacios(e) : e +
                            '</p>';
                }
            );
        }
    }
    div += '<p class="evhorario"><i class="icon-clock" />' + evento.from.lleuGetFormattedTime() + ' - ' + evento.to.lleuGetFormattedTime() + '</p>';

    div += '</div>';

    return div;
};

Horario.prototype._dameLocalizacionEspacios = function (evento) {
    const localizacionUrl = "https://www.uji.es/u/" + evento;
    if (evento !== "Aula no disponible")
        return `<i class="icon-location" /><a href=` + localizacionUrl + ` target="_blank">` + evento + `</a>`;
    else return evento;
}

Horario.prototype._dameEdificio = function (evento) {
    let eventoArea = evento.substr(0, 1);
    let eventoEdificio = evento.substr(1, 2);
    return ((eventoArea == 'H') ? ((eventoEdificio == 'D') ? 'HD' : 'H') : eventoArea)
}

Horario.prototype._getColeccionEventosUI = function (coleccion) {
    var div = '<div class="coleccioneventos">';
    coleccion.eventos.sort(function (a, b) {
        if (a.titulo > b.titulo) {
            return 1;
        }
        if (a.titulo < b.titulo) {
            return -1;
        }
        return 0;
    });
    for (var i = 0; i < coleccion.eventos.length; i++) {
        var evento = coleccion.eventos[i];

        var css = "coleventotipo";
        var style = "";
        var coleventoClass = "";

        if (evento.color !== null) {
            style = 'style="background-color: ' + evento.color + ';"';
        }
        if (evento.cssClass !== null) {
            coleventoClass = ' ' + evento.cssClass;
        }

        div += '<div class="colevento' + coleventoClass + '" data-codigo="' + evento.codigo + '">';
        div += '<div class="' + css + '" ' + style + '></div>';
        div += '<div class="coleventoinfo">';

        // título
        if (evento.url) {
            div += '<p class="evasignatura">' +
                '<a href="' + evento.url + '" target="_blank" class="actionlink">' + evento.codigo + '</a>' +
                '</p>';
        } else {
            evento.codigo.split(',').forEach(e => {
                    div +=  '<p class="evasignatura">'+
                                (this._mostrarEdificio && evento) ? this._dameLocalizacionEspacios(e) : e +
                            '</p><br>';
                }
            );
        }

        // subtítulo
        div += '<div class="evtitulo">';
        if (evento.observaciones) {
            div += '<span class="tip"><span class="icon-info icon-info-circled"><span class="observaciones">' + evento.observaciones + '</span></span></span>';
            div += '<p>' + evento.titulo + '</p></div>';
        } else {
            div += '<p>' + evento.titulo + '</p></div>';
        }

        // semestre
        if (evento.semestre) {
            div += '<div class="evsemestre">';
            div += '<p>' + i18n.get_string(evento.semestre) + '</p>';
            div += '</div>';
        }

        // localización
        if (evento.localizacion) {
            evento.localizacion.split(',').forEach(e => {
                div +=  '<p class="evlocaliza"><i class="icon-location" />' +
                            (this._mostrarEdificio && evento) ? this._dameLocalizacionEspacios(e) : e +
                        '</p>';
                }
            );
        }

        div += '<p class="evhorario"><i class="icon-clock" />' + evento.from.lleuGetFormattedTime() + ' - ' + evento.to.lleuGetFormattedTime() + '</p>';

        div += '</div>';
        div += '</div>';
    }

    div += '</div>';
    return div;

};

Horario.prototype._ocultarHorariosSabado = function () {
    let eventosDiaSabado = document.getElementById("horarios-sabado");
    if (eventosDiaSabado != null)
        eventosDiaSabado.classList.add("hidden");
};

Horario.prototype._mostrarHorariosSabado = function () {
    let eventosDiaSabado = document.getElementById("horarios-sabado");
    if (eventosDiaSabado != null){
        eventosDiaSabado.classList.remove("hidden");
    }
}

Horario.prototype._loadListadoEventosSabado = function () {
    const me = this;
    let ul = document.createElement("ul");
    let eventosDiaSabado = document.getElementById("horarios-sabado")
    != null
        ? document.getElementById("horarios-sabado")
        : document.getElementById("contenedorhorario");
    if (eventosDiaSabado.querySelector("ul"))
        eventosDiaSabado.removeChild(eventosDiaSabado.querySelector("ul"));
    me._eventosSabado.forEach(function (evento) {
            let li = document.createElement("li");
            li.innerHTML = me._templateAsignaturaListado(evento);
            ul.appendChild(li);
        }
    )
    return ul;
}

Horario.prototype._listadoEventosSabado = function () {
    var me = this;
    let eventosDiaSabado = document.getElementById("horarios-sabado")
    != null
        ? document.getElementById("horarios-sabado")
        : document.getElementById("contenedorhorario");
    if (me._eventosSabado.length === 0) {
        me._ocultarHorariosSabado();
        return;
    }

    me._mostrarHorariosSabado();
    eventosDiaSabado.append(me._loadListadoEventosSabado());

};

/**
 * Pinta un evento en el calendario.
 *
 * @param evento el evento que queremos añadir.
 * @depends Evento
 */
Horario.prototype.addEvento = function (evento) {
    var me = this;

// Añado filas que puedan hacerme falta para darle sitio al evento.
    if (evento.from.getHours() < this._fromHour) {
        var h = this._fromHour - 1;
        while (h >= evento.from.getHours()) {
            this.prependRow("", h + ":30");
            this.prependRow("", h + ":00");
            h--;
        }
        this._fromHour = evento.from.getHours();
    }
    if (evento.to.getHours() > this._toHour) {
        var h = this._toHour + 1;
        while (h <= evento.to.getHours()) {
            this.appendRow("", h + ":00");
            this.appendRow("", h + ":30");
            h++;
        }
        this._toHour = evento.to.getHours();
    }

// ¿Colisiona con algún evento ya insertado? Si es así creamos una colección de eventos
    var colision = this.hayColision(evento);
    while (colision) {

        colision.e.ui.remove();

// Añadimos el evento a una colección.
        var coleccion;
        if (colision.e.evento instanceof ColeccionEventos) {
            coleccion = colision.e.evento;
            coleccion.add(evento);
        } else {
            coleccion = new ColeccionEventos();
            coleccion.add(colision.e.evento);
            coleccion.add(evento);
        }

        evento = coleccion;

// Elimino el evento que colisiona de la lista de eventos (se añadirá posteriormente).
        delete this._eventos[colision.pos];

        colision = this.hayColision(evento);
    }

// Determinamos la posición que debe ocupar el div del evento.
    var minutes = evento.from.getMinutes();
    if (minutes !== 0 || minutes !== 30) {
        minutes = "0" + 30 * parseInt(minutes / 30, 10);
        minutes = minutes.substr(minutes.length - 2);
    }
    var dataHour = evento.from.getHours() + ":" + minutes;

    var tdFrom = this._el.find('.tbody tr[data-hour="' + dataHour + '"] td[data-day="' + evento.day + '"]');
    var from = {
        hour: dataHour,
        day: evento.day
    };

// Añado el div a la casilla correspondiente.
    var div;
    if (evento instanceof Evento) {
        div = $(this._getEventoUI(evento));
    } else if (evento instanceof ColeccionEventos) {
        div = $(this._getColeccionEventosUI(evento));
    }
    div.appendTo(tdFrom);

// Inicializamos atributos.
    var divId = this._generateId();
    div.attr('id', divId);

// Guardamos las casillas de inicio y de fin para posicionar después correctamente en redimensionados de ventana.
    div.data('fromHour', from.hour);
    div.data('fromEventoMinutes', evento.from.getMinutes());
    div.data('fromDay', evento.day);
    minutes = evento.to.getMinutes();
    if (minutes !== 0 || minutes !== 30) {
        minutes = "0" + 30 * parseInt(minutes / 30, 10);
        minutes = minutes.substr(minutes.length - 2);
    }
    dataHour = evento.to.getHours() + ":" + minutes;
    div.data('toHour', dataHour);
    div.data('toEventoMinutes', evento.to.getMinutes());
    div.data('toDay', evento.day);

// Me guardo el z-index.
    this._eventsZindex[divId] = [div.css('z-index')];

// A partir de aquí es posicionado del evento / colección
    this._setPositionCollapsed(div);

// Gestionamos los overflow de los eventos.
    this.handleEventOverflow(div);

// Para expandir un evento haciendo click.
    if (div.hasClass('overflowed')) {
        div.click(function (e) {
            me.onOverflowedClick(div);
        });
    }

// Gestionamos el hover de la información de un evento.
    div.find('.icon-info').hover(
        function () {
            $(this).find('.observaciones').css('display', 'block');

            var el = $(this).closest('.coleccioneventos');
            if (el.length === 0) {
                el = $(this).closest('.evento');
                if (el.length === 0) {
                    return;
                }
            }

            var zIndex = parseInt(el.css('z-index'));

            me._eventsZindex[el.attr('id')].push(zIndex);
            el.css('z-index', zIndex + 100);
            el.css('overflow', 'visible');
        },
        function () {
            $(this).find('.observaciones').css('display', 'none');
            var el = $(this).closest('.coleccioneventos');
            if (el.length === 0) {
                el = $(this).closest('.evento');
                if (el.length === 0) {
                    return;
                }
            }
            el.css('z-index', me._eventsZindex[el.attr('id')].pop());
            el.css('overflow', 'hidden');
        }
    );

// Gestionamos el modal.
    div.find('.modal').prev().click(function (event) {

        var el = $(this).closest('.coleccioneventos');
        if (el.length === 0) {
            el = $(this).closest('.evento');
            if (el.length === 0) {
                return;
            }
        }
        var zIndex = parseInt(el.css('z-index'));

        me._eventsZindex[el.attr('id')].push(zIndex);
        el.css('z-index', zIndex + 100);

        var modal = $(this).next('div.modal');
        $(modal).css('display', 'block');
        event.stopPropagation();
    });
    div.find('.modal').click(function (event) {
        var el = $(this).closest('.coleccioneventos');
        if (el.length === 0) {
            el = $(this).closest('.evento');
            if (el.length === 0) {
                return;
            }
        }

        el.css('z-index', me._eventsZindex[el.attr('id')].pop());
        $(this).css('display', 'none');
        event.stopPropagation();
    });
    this._eventos[divId] = {evento: evento, ui: div}
};
Horario.prototype._setPositionCollapsed = function (div) {
    var fromDay = div.data('fromDay');
    var fromHour = div.data('fromHour');
    var eventoFromMinutes = div.data('fromEventoMinutes');
    var toDay = div.data('toDay');
    var toHour = div.data('toHour');
    var eventoToMinutes = div.data('toEventoMinutes');

    var tdTo = this._el.find('.tbody tr[data-hour="' + toHour + '"] td[data-day="' + toDay + '"]');
    var top = (this._tdHeight / 30) * (eventoFromMinutes % 30);
    if (div.css('position') !== 'absolute') {
        div.css('position', 'absolute');
    }
    div.css('top', top + 'px');

    var height = tdTo.offset().top - div.offset().top + (eventoToMinutes % 30) * this._tdHeight / 30;
    div.css('height', height);
};
Horario.prototype._getCollapsedHeight = function (div) {
    var fromDay = div.data('fromDay');
    var fromHour = div.data('fromHour');
    var toDay = div.data('toDay');
    var toHour = div.data('toHour');
    var eventoToMinutes = div.data('toEventoMinutes');

    var tdTo = this._el.find('.tbody tr[data-hour="' + toHour + '"] td[data-day="' + toDay + '"]');
    var tdHeight = tdTo.height();
    var height = tdTo.offset().top - div.offset().top + (eventoToMinutes % 30) * tdHeight / 30;

    return height;
};

/**
 * Elimina todos los eventos del horario
 */
Horario.prototype.removeAllEvents = function () {
    this._el.find('.evento').remove();
    this._el.find('.coleccioneventos').remove();
    this._eventos = {};
    this._disabledEvents = [];
};

/**
 * Devuelve el evento o colección de la lista de eventos que colisiona.
 *
 * @param {pos: posicion, e: elemento de eventos}
 */
Horario.prototype.hayColision = function (evento) {
    var ret = null;

    for (var id in this._eventos) {
        var e = this._eventos[id];

        if (!e.disabled && e.evento.day == evento.day) {

            var colision = (evento.from < e.evento.from && evento.to > e.evento.from) ||
                (evento.from > e.evento.from && evento.from < e.evento.to) ||
                (evento.from.getTime() == e.evento.from.getTime());

            if (colision) {
                ret = {
                    pos: id,
                    e: e
                };
                break;
            }
        }
    }
    return ret;
};

/**
 * Se encarga de pintar adecuadamente los eventos en caso de que su contenido se salga de la caja:
 *
 *    * Añade una flecha de expandir
 *    * Establece el evento para gestionar el click de la caja
 *    * Oculta el contenido que se sale de la caja
 *
 * @param div
 */

Horario.prototype.handleEventOverflow = function (div) {

    if (div.get(0).scrollHeight > div.get(0).clientHeight) {

// añadimos la flechita de expandir si no la hubiere
        var down = div.find('.expand');
        if (down.length === 0) {
            down = $('<div class="expand">&#9660;</div>');
            down.appendTo(div);
            var me = this;
            down.click(function (e) {
                e.stopPropagation();
                me.onOverflowedClick(div)
            });
        }

// indicamos que está overflowed :-)
        div.addClass('overflowed');

// Ocultamos los colevento que se salen de la parte visible del div de colección de eventos.
        var coleventos = div.find('div.colevento').not('.disabled');
        var downArrowPos = down.offset().top;

        for (var i = 0, to = coleventos.length; i < to; i++) {
            var ch = $(coleventos[i]).outerHeight();
            var colBottomPos = $(coleventos[i]).offset().top + ch;

            if (colBottomPos > downArrowPos) {
// si es el primer evento el que hace overflow, mostraremos sólo el título
                if (i == 0) {
// usamos la clase .tohide para saber qué elementos debemos ocultar/mostrar
                    $(coleventos[i]).find('.evhorario').addClass('tohide');
                    $(coleventos[i]).find('.evtitulo').addClass('tohide');
                    $(coleventos[i]).find('.evsemestre').addClass('tohide');
                    $(coleventos[i]).find('.evlocaliza').addClass('tohide');
                    $(coleventos[i]).find('.evtitulo').css('visibility', 'hidden');
                    $(coleventos[i]).find('.evhorario').css('visibility', 'hidden');
                    $(coleventos[i]).find('.evlocaliza').css('visibility', 'hidden');
                    $(coleventos[i]).find('.evsemestre').css('visibility', 'hidden');
                } else {
                    $(coleventos[i]).addClass('tohide');
                    $(coleventos[i]).css('visibility', 'hidden');
                }
            } else {
                $(coleventos[i]).find('.tohide').toggleClass('tohide');
            }
        }
    }
};

Horario.prototype.onOverflowedClick = function (div) {

    var down = div.find('.expand');

    if (div.hasClass('expanded')) {

        div.animate({
            height: this._getCollapsedHeight(div)
        }, 'fast');

        var d = div.find('.tohide')
        d.css('visibility', 'hidden');
        d.css('z-index', Horario.GROUNDZINDEX);
        down.html('&#9660');
    } else {

        div.animate({
            height: div.get(0).scrollHeight + 10
        }, 'fast');

        var d = div.find('.tohide');
        d.css('visibility', 'visible');
        div.css('z-index', Horario.TOPZINDEX);

        down.html('&#9650');

        Horario.TOPZINDEX += 1;

    }

    div.toggleClass('expanded');
};

/**
 * Dada una fecha pintamos la parrilla en la semana a la que pertenece esa fecha. Este método sólo tiene sentido
 * si el tipo de Horario es CALENDARIO. Para representar la fecha utilizaremos los métodos UTC de la fecha, así es que
 * los días, etc. que queremos representar deben estar en la zona horaria UTC.
 *
 * Por ejemplo, si queremos establecer la semana de el día actual haríamos lo siguiente:
 *
 * var d = new Date(Date.UTC(now.getFullYear, now.getMonth(), now.getDate(), now.getHour(), now.getMinutes(), now.getSeconds(), now.getMilliseconds()));
 * Horario.setWeek(d);
 *
 * @param date
 */
Horario.prototype.setWeek = function (date, removeEvents) {
    if (this._tipo !== Horario.TIPO.CALENDARIO) {
        return;
    }
    if (!this._fromDate) {
        this._fromDate = new Date();
    }

    this._fromDate.setTime(date.getTime());

// Ajustamos la fecha de inicio
    if (this._fromDate.getUTCDay() !== 1) {
        var offset = this._fromDate.getUTCDay() - 1;
        this._fromDate.setTime(this._fromDate.getTime() - offset * 24 * 3600 * 1000);
    } else {
        this._fromDate.setTime(date.getTime());
    }

    this._fromDate.setUTCHours(0);
    this._fromDate.setUTCMinutes(0);
    this._fromDate.setUTCSeconds(0);
    this._fromDate.setUTCMilliseconds(0);

// Eliminamos los eventos que tenemos cargados
    if (removeEvents) {
        this.removeAllEvents();
    }

// Actualizamos los datos de la cabecera de la parrilla
    this._repaintHeader();
};

/**
 * Devuelve la fecha de inicio de la semana actual. Recordad que el dato correcto lo proporcionan los métodos UTC del
 * objeto Date. La hora y los minutos están establecidos a 0.
 *
 * Por ejemplo, para obtener el día deberemos hacer:
 *
 * var fromDate = horario.getCurrentWeek();
 * var day = fromDate.getUTCDay();
 *
 * Si queremos obtener un entresijo de datos
 *
 * @returns {*}
 */
Horario.prototype.getCurrentWeek = function () {
    return this._fromDate;
};

/**
 * Pinta la semana siguiente a la actual
 */
Horario.prototype.nextWeek = function () {
    if (this._tipo !== Horario.TIPO.CALENDARIO) {
        return;
    }
    this._fromDate.setTime(this._fromDate.getTime() + 7 * 24 * 3600 * 1000);
    this._repaintHeader();
};

/**
 * Pinta la semana anterior a la actual
 */
Horario.prototype.prevWeek = function () {
    if (this._tipo !== Horario.TIPO.CALENDARIO) {
        return;
    }
    this._fromDate.setTime(this._fromDate.getTime() - 7 * 24 * 3600 * 1000);
    this._repaintHeader();
};

/**
 * En un calendario de tipo CALENDARIO se encarga de pintar las cabeceras en la semana que corresponda.
 *
 * @private
 */
Horario.prototype._repaintHeader = function () {

    if (this._tipo !== Horario.TIPO.CALENDARIO) {
        return;
    }

    var me = this;
    var date = new Date();

    me._el.find('.thead .weekday').each(function (i, e) {
        e = $(e);
        date.setTime(me._fromDate.getTime() + e.data('day') * 24 * 3600000);
        e.find('.daystr').html(i18n.get_week_day(date.getUTCDay()) + ' ' + date.getUTCDate() + '/' + (date.getUTCMonth() + 1));
    });
};


/**
 * Dado un array de clases css deshabilita los eventos que tengan esa clases.
 *
 * @param classes
 */
Horario.prototype.disableEventsByClass = function (classes) {
    var me = this;
    me._forceHorariosReload = true;
    if (this._modoListado) {
        if (classes.length > 0) {
            const elementos = $(me._listadoContainerEl).find(".elemento-asignatura");
            elementos.each(function () {
                const e = $(this);
                let deshabilita = false;
                classes.forEach(function (code) {
                    if (e.data("tipo-asignatura") == code) {
                        deshabilita = true;
                    }
                });
                if (deshabilita) {
                    e.hide();
                } else {
                    e.show();
                }
            })
        } else {
            const elementos = $(me._listadoContainerEl).find(".elemento-asignatura");
            elementos.show();
        }
    }

// habilito los eventos que no deba deshabilitar
    var i = 0;
    while (i < this._disabledEvents.length) {
        var e = this._disabledEvents[i];
        if (classes.indexOf(e.cssClass) === -1) {
            e.disabled = false;
            this._disabledEvents = this._disabledEvents.slice(0, i).concat(this._disabledEvents.slice(i + 1));
            this.addEvento(e);
        } else {
            i++;
        }
    }

// deshabilito los eventos
    classes.forEach(function (c) {

// eventos
        var eventos = this._el.find('.evento.' + c);
        eventos.each(function (e) {
            var eventId = $(this).attr('id');
            me._eventos[eventId].evento.disabled = true;
            me._eventos[eventId].ui.remove();
            me._disabledEvents.push(me._eventos[eventId].evento);
            delete me._eventos[eventId];
        });

// colecciones
        var colEventos = this._el.find('.colevento.' + c);
        var colecciones = colEventos.closest('.coleccioneventos');
        colecciones.each(function () {

            var eventId = $(this).attr('id');

            var aReinsertar = [];
            me._eventos[eventId].evento.eventos.forEach(function (e) {
                if (classes.indexOf(e.cssClass) != -1 && !e.disabled) {
                    me._disabledEvents.push(e);
                    e.disabled = true;
                } else if (!e.disabled) {
                    aReinsertar.push(e);
                }
            }, me);

            me._eventos[eventId].ui.remove();
            delete me._eventos[eventId];

            me.addEvents(aReinsertar);

        });
    }, this);

};

/**
 * Dado un array de códigos de items actualiza el horario para habiltar/deshabilitar los correspondientes.
 *
 * @param {array} codes array de códigos de ítems que queremos deshabilitar
 */
Horario.prototype.disableItemsByCode = function (codes) {

    var me = this;
    me._forceHorariosReload = true;
    if (this._modoListado) {
        if (codes.length > 0) {
            const elementos = $(me._listadoContainerEl).find(".elemento-asignatura");
            elementos.each(function () {
                const e = $(this);
                let deshabilita = false;
                codes.forEach(function (code) {
                    if (e.data("codigo-asignatura") == code) {
                        deshabilita = true;
                    }
                });
                if (deshabilita) {
                    e.hide();
                } else {
                    e.show();
                }
            })
        } else {
            const elementos = $(me._listadoContainerEl).find(".elemento-asignatura");
            elementos.show();
        }
    }

// habilito los eventos que no deba deshabilitar
    var i = 0;
    while (i < this._disabledEvents.length) {
        var e = this._disabledEvents[i];
        if (codes.indexOf(e.codigo) == -1) {
            e.disabled = false;
            this._disabledEvents = this._disabledEvents.slice(0, i).concat(this._disabledEvents.slice(i + 1));
            this.addEvento(e);
        } else {
            i++;
        }
    }

// deshabilito los eventos
    codes.forEach(function (c) {

// eventos
        var eventos = this._el.find('.evento[data-codigo=' + c + ']');
        eventos.each(function (e) {
            var eventId = $(this).attr('id');
            me._eventos[eventId].evento.disabled = true;
            me._eventos[eventId].ui.remove();
            me._disabledEvents.push(me._eventos[eventId].evento);
            delete me._eventos[eventId];
        });

// colecciones
        var colEventos = this._el.find('.colevento[data-codigo=' + c + ']');
        var colecciones = colEventos.closest('.coleccioneventos');

        var aInsertar = [];

        colecciones.each(function () {

            var aReinsertar = [];
            var eventId = $(this).attr('id');
            me._eventos[eventId].evento.eventos.forEach(function (e) {
                if (codes.indexOf(e.codigo) != -1 && !e.disabled) {
                    me._disabledEvents.push(e);
                    e.disabled = true;
                } else if (!e.disabled) {
                    aReinsertar.push(e);
                }
            }, me);

            me._eventos[eventId].ui.remove();
            delete me._eventos[eventId];

            aInsertar = aInsertar.concat(aReinsertar);
        });

        me.addEvents(aInsertar);
    }, this);
};

/**
 * Devuelve el semestre de la asignatura. Puede ser: 1,2,a
 * @returns {*}
 */
Horario.getSemestreAsignatura = function (containerId) {
    return $(containerId).data('semestre-asignatura');
};

/**
 * Devuelve el primer día del primer semestre
 */

Horario.getPrimerDiaSemestre1 = function (containerId) {
    var ts = $(containerId).data('primerdiasemestre1');
    if (!ts) {
        return null;
    }
    var ret = new Date();
    ret.setTime(ts);
    return ret;
};

/**
 * Devuelve el primer día del segundo semestre
 */
Horario.getPrimerDiaSemestre2 = function (containerId) {
    var ts = $(containerId).data('primerdiasemestre2');
    if (!ts) {
        return null;
    }
    var ret = new Date();
    ret.setTime(ts);
    return ret;
};

/**
 * Devuelve el día máximo en que tenemos eventos.
 *
 * @param containerId
 * @returns {*}
 */
Horario.getUltimoLectivo = function (containerId) {
    var ts = $(containerId).data('ultimolectivo');
    if (!ts) {
        return null;
    }
    return new Date(ts);
};


/**
 * Genera la tabla y añade manejador de evento de redimensionado.
 *
 * @private
 */
Horario.prototype._generateAll = function () {

    this._generateTable();
    this._repaintHeader();

    this._tdHeight = this._el.find('.tbody-row:nth-child(2) td').height();

    var me = this;
    $(document).keyup(function (e) {
        if (e.keyCode === 27) {
            var modal = '';
            $('.modal').each(function () {
                if ($(this).css('display') == 'block') {
                    modal = this;
                }
            });
            var el = $(modal).closest('.coleccioneventos');
            if (el.length == 0) {
                el = $(modal).closest('.evento');
                if (el.length == 0) {
                    return;
                }
            }
            el.css('z-index', me._eventsZindex[el.attr('id')].pop());
            $('.modal').css('display', 'none');
            e.preventDefault();
        }
    });
    $(window).resize(this.onResize.bind(this));
};

/**
 * Establece el tipo de Horario que queremos.
 *
 * @param tipo Horario.TIPO
 */
Horario.prototype.setTipo = function (tipo) {
    if (tipo === this._tipo) {
        return;
    }

    this.removeAllEvents();
    this._el.find('*').remove();

    this._tipo = tipo;

    this._generateAll();

    if (this._weeknav) {
        if (tipo === Horario.TIPO.SEMANAL) {
            this._weeknav.hide();
        } else {
            this._weeknav.show();
        }
    }
};

/**
 * Establece el control de cambio de vista.
 *
 * @param {String} containerSelector
 */
Horario.prototype.setVistaHorario = function (containerSelector) {
    var tipoVista;
    if (this._tipo === Horario.TIPO.CALENDARIO) {
        tipoVista = VistaHorario.TIPO.DETALLADA;
    } else {
        tipoVista = VistaHorario.TIPO.GENERAL;
    }
    this._vistaHorarioControl = new VistaHorario(containerSelector, tipoVista);
    this._vistaHorarioControl.change(this._onChangeVista.bind(this));

};

/**
 * Callback para manejar un cambio de tipo de horario.
 *
 * @param tipo
 * @private
 */
Horario.prototype._onChangeVista = function (tipo) {
    if (tipo === VistaHorario.TIPO.DETALLADA) {
        this.setTipo(Horario.TIPO.CALENDARIO);
    } else {
        this.setTipo(Horario.TIPO.SEMANAL);
    }
    this.load();
};

Horario.prototype.setLoadDetalladaCb = function (eventoDetalladaCb, loadDetalladaCb) {
    this._loadDetalladaCb = loadDetalladaCb;
    this._eventoDetalladaCb = eventoDetalladaCb;
};
/**
 * Establece la callback de carga de eventos general.
 *
 * @param loadGeneralCb callback de carga de datos.
 * @param eventoGeneralCb callback de obtención de instancias de evento
 */
Horario.prototype.setLoadGeneralCb = function (eventoGeneralCb, loadGeneralCb) {
    this._loadGeneralCb = loadGeneralCb;
    this._eventoGeneralCb = eventoGeneralCb;
};

/**
 * Establece la callback para determinar la fecha máxima y mínima entre las que se puede navegar en un horario de tipo
 * calendario.
 *
 * @param maxDateCb devuelve una promesa que al resolverse da el valor de la fecha máxima a la que es posible navegar.
 * @param minDateDb devuelve una promesa que al resolverse da el valor de la fecha mínima a la que es posible navegar.
 */
Horario.prototype.setDateLimitsCb = function (maxDateCb, minDateCb) {
    this._maxDateCb = maxDateCb;
    this._minDateCb = minDateCb;
};

/**
 * Funcion que pinta el listado de eventos
 * @param listadoEventos
 * @private
 */
Horario.prototype._pintaListadoHorario = function (listadoEventos) {
    var me = this;
    me._listadoContainerEl.empty();
    if (me._tipo == Horario.TIPO.CALENDARIO) {
        me._listadoHorarioDetallado(listadoEventos);
    } else {
        me._listadoHorarioGeneral(listadoEventos);
    }
};

Horario.prototype._listadoHorarioGeneral = function (listadoEventos) {
    var me = this;
    if (listadoEventos.length === 0) return;
    let eventosDia = document.createElement("div");
    eventosDia.classList.add('eventos-dia');

    var ul = document.createElement("ul");
    listadoEventos.forEach(function (evento) {
            var li = document.createElement("li");
            li.innerHTML = me._templateAsignaturaListado(evento);
            ul.appendChild(li);
        }
    )
    eventosDia.append(ul);
    me._listadoContainerEl.append(eventosDia);
};

Horario.prototype._listadoHorarioDetallado = function (listadoEventos) {
    var me = this;

    if (listadoEventos.length === 0) return;

    let eventosDia = document.createElement("div");
    eventosDia.classList.add('eventos-dia');
    var ul = document.createElement("ul");
    listadoEventos.forEach(function (evento) {
            var li = document.createElement("li");
            li.innerHTML = me._templateAsignaturaListado(evento);
            ul.appendChild(li);
        }
    )
    eventosDia.append(ul);
    me._listadoContainerEl.append(eventosDia);
};

Horario.prototype._templateAsignaturaListado = function (evento) {
    let diaString = Horario.DAYNAMES[evento.diaSemana - 1];

    if (this._tipo == Horario.TIPO.CALENDARIO) {
        diaString += ' - ' + evento.ini.split(' ')[0];
    }
    const horaInicio = evento.ini.split(" ")[1].slice(0, -3);
    const horaFin = evento.fin.split(" ")[1].slice(0, -3);
    const comentario = [evento.comentario, evento.comentarioDiscont].filter(Boolean).map(function (p) {
        return "<p>".concat(p).concat("</p>")
    }).join("");

    let template = `<div class="elemento-asignatura" data-codigo-asignatura='${evento.asignaturaId}' data-tipo-asignatura='${Horario.TIPOSUBGRUPOS[evento.tipoSubgrupo].name}'">
<div class="titulo">${evento.asignaturaId} - ${evento.nombreAsignatura}</div>
<div class="contenido">
<div><span class="label-tipo-grupo">${i18n.get_string('tipoDeGrupos')}:</span> ${Horario.TIPOSUBGRUPOS[evento.tipoSubgrupo].traduccion} ${evento.subgrupo}</div>
<div>${diaString} ${horaInicio} - ${horaFin}</div>
</div>
<div class="observaciones">${comentario}</div>
</div>`;

    return template;
}

/**
 * Carga los eventos para el horario en función del tipo del mismo y los añade al mismo.
 *
 * Para obtener una instancia de evento es necesario pasar una callback en la que dado un evento REST devuelve una
 * instancia de la clase Evento.
 *
 * @returns {Promise}
 */
Horario.prototype.load = function () {

    var me = this;

    var loadCallback = (this._tipo === Horario.TIPO.SEMANAL) ? this._loadGeneralCb : this._loadDetalladaCb;
    var getEventoCallback = (this._tipo === Horario.TIPO.SEMANAL) ? this._eventoGeneralCb : this._eventoDetalladaCb;

    var eq = loadCallback(this, 'listado');

    if (this._lastReqId !== null) {
        RestProvider.cancelRequest(this._lastReqId);
        this._lastReqId = null;
    }
    this._lastReqId = eq.reqid;
    this._forceHorariosReload = true;

    return eq.promise.then(function (response) {
        var eventos = [];
        if (response.reqid !== me._lastReqId) {
            throw "abort";
        }

        me._lastReqId = null;

        if (!response.data.success) {
            throw new Error("El servidor devolvió error en la carga de los eventos de horario");
        }


        var from = 23;
        var to = 0;
        var data = response.data.data;
        if (me._modoListado) {
            me._pintaListadoHorario(data);
        } else {
            if (data.length === 0) {
                this._fromHour = 8;
                this._toHour = 14;
            } else {
                me._eventosSabado = [];
                data.forEach(function (e) {
                    if (e.diaSemana === 6 || e.diaSemana === "6") {
                        me._eventosSabado.push(e);
                    } else {
                        var evento = getEventoCallback(e);
                        eventos.push(evento);
                        if (evento.from.getHours() < from) {
                            from = evento.from.getHours();
                        }
                        if (evento.to.getHours() > to) {
                            to = evento.to.getHours();
                        }
                    }
                });

                if ((me._firstLoad || me._doShrink || me._tipo === Horario.TIPO.SEMANAL) && from > me._fromHour) {
                    me._fromHour = from;
                } else if (from < me._fromHour) {
                    me._fromHour = from;
                }
                if ((me._firstLoad || me._doShrink || me._tipo === Horario.TIPO.SEMANAL) && to < me._toHour) {
                    me._toHour = to + 1;
                } else if (to >= me._toHour) {
                    me._toHour = to + 1;
                }
            }

            me.removeAllEvents();

            me._el.children().remove();
            me._eventos = {};
            me._disabledEvents = [];
            me._generateAll();

            if (me._firstLoad) {
                me._firstLoad = false;
            }

            eventos.forEach(function (e) {
                me.addEvento(e);
            });
        }

        if (me._loadFinishedCb) {
            me._loadFinishedCb(me, data, eventos);
            me._listadoEventosSabado();
        }

        return data;
    });
};

/**
 * Cancela cualquier petición en curso del horario.
 */
Horario.prototype.cancelPendingRequest = function () {
    if (this._lastReqId === null) {
        return;
    }
    RestProvider.cancelRequest(this._lastReqId);
    this._lastReqId = null;
};

/**
 * Recorta "por arriba" y "por abajo" el horario
 */
Horario.prototype.shrink = function () {
    var hour;
    for (hour = 0; hour < this._fromHour; hour++) {
        this._el.find('tr[data-hour^="' + hour + ':"]').remove();
    }
    for (hour = 23; hour > this._toHour; hour--) {
        this._el.find('tr[data-hour^="' + hour + ':"]').remove();
    }
};

/* NAVEGADOR POR SEMANAS **********************************************************************************************/

/**
 * Establece un manejador de navegador por semanas para el horario.
 *
 * @param {String} contenendorId
 */
Horario.prototype.setWeekNav = function (contenedorId) {
    this._weeknav = new WeekNav(contenedorId, this.getCurrentWeek(), LLEU_CONFIG.lang);
    this._weeknav.setMaxDate(this._maxDate);
    this._weeknav.nextWeek(this._onWeekNavNextWeek.bind(this));
    this._weeknav.prevWeek(this._onWeekNavPrevWeek.bind(this));
    this._weeknav.week(this._onWeekNavWeek.bind(this));
    this._weeknav.today(this._onWeekNavToday.bind(this));
    this._weeknav.semestre1(this._onWeekNavSemestre1.bind(this));
    this._weeknav.semestre2(this._onWeekNavSemestre2.bind(this));

    if (this._tipo === Horario.TIPO.SEMANAL) {
        this._weeknav.hide();
    }
};
/**
 * Se llama en los eventos de cambio de semana del weeknav.
 *
 * @param date
 * @returns {boolean} true si cambió correctamente de semana. false si no cambió correctamente de semana.
 * @private
 */
Horario.prototype._onSetWeekNavWeek = function (newWeek) {
    if (!this._weeknav.getMaxDate() || !this._weeknav.getMinDate()) {
        return false;
    }

    if (this._weekAlertBox !== null) {
        this._weekAlertBox.hide();
        this._weekAlertBox = null;
    }
    if (newWeek.getTime() > this._weeknav.getMaxDate().getTime()) {
        this._weekAlertBox = new AlertBox('#' + this._weeknav.getId());
        this._weekAlertBox.show(i18n.get_string('weeknav.maxsemana'));
        return false
    }
    if (newWeek.getTime() < this._weeknav.getMinDate().getTime()) {
        this._weekAlertBox = new AlertBox('#' + this._weeknav.getId());
        this._weekAlertBox.show(i18n.get_string('weeknav.maxsemana'));
        return false
    }
    this.setWeek(newWeek);
    this._weeknav.setWeek(newWeek);
    return true;
};
Horario.prototype._onWeekNavNextWeek = function () {
    var doLoad = this._onSetWeekNavWeek(Date.lleuGetNextWeek(this.getCurrentWeek()));
    if (doLoad) {
        this.load();
    }
};
Horario.prototype._onWeekNavPrevWeek = function () {
    var doLoad = this._onSetWeekNavWeek(Date.lleuGetPrevWeek(this.getCurrentWeek()));
    if (doLoad) {
        this.load();
    }
};
Horario.prototype._onWeekNavWeek = function () {
    var doLoad = this._onSetWeekNavWeek(this._weeknav.getWeek());
    if (doLoad) {
        this.load();
    }
};
Horario.prototype._onWeekNavToday = function () {
    var doLoad = this._onSetWeekNavWeek(new Date());
    if (doLoad) {
        this.load();
    }
};
Horario.prototype._onWeekNavSemestre1 = function () {
    var doLoad = this._onSetWeekNavWeek(Horario.getPrimerDiaSemestre1('#' + this._id));
    if (doLoad) {
        this.load();
    }
};
Horario.prototype._onWeekNavSemestre2 = function () {
    var doLoad = this._onSetWeekNavWeek(Horario.getPrimerDiaSemestre2('#' + this._id));
    if (doLoad) {
        this.load();
    }
};

/**
 * Registra una función callback que será invocada cuando el horario haya terminado de pintar los eventos en la parrilla
 * tras una carga.
 *
 * @param onLoadFinishedCb
 */
Horario.prototype.setOnLoadFinishedCb = function (onLoadFinishedCb) {
    this._loadFinishedCb = onLoadFinishedCb;
};

Horario.prototype._onTabChange = function () {
    var me = this;
    var active = this._tabs.find('li.active');
    active = active[1].id;
    if (active === this._lastActiveTab) {
        return;
    }
    this._lastActiveTab = active;

    if (active === 'listadoTab') {
        me._modoListado = true;
        if (me._forceHorariosReload) {
            this.load().then(function () {
                me._currentVistaHorario = active;
                me._forceHorariosReload = false;
            });
        }
    } else if (active === 'horariosTab') {
        me._modoListado = false;
        if (me._forceHorariosReload) {
            me.removeAllEvents()
            this.load().then(function () {
                me._currentVistaHorario = active;
                me._forceHorariosReload = false;
            });
        }
    }
}
