/**
 * Devuelve horas y minutos en formato HH:mm
 *
 * @returns {string}
 */
Date.prototype.lleuGetFormattedTime = function() {
    return ("0" + this.getHours()).slice(-2) + ":" + ("0" + this.getMinutes()).slice(-2);
};

Date.lleuGetFirstDateInWeek = function(date) {
    var ret;
    // si no es lunes, vamos a buscar el lunes
    if (date.getUTCDate() != 1) {
        ret = new Date();

        var currentDay = date.getUTCDay() == 0 ? 7 : date.getUTCDay();
        var offset     = (currentDay - 1) * 24 * 3600 * 1000;

        ret.setTime(date.getTime() - offset);
    } else {
        ret = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()));
    }
    return ret;
};
Date.lleuGetNextWeek = function(date) {
    var t = date.getTime() + 7 * 24 * 3600 * 1000;
    return Date.lleuGetFirstDateInWeek(new Date(t));
};
Date.lleuGetPrevWeek = function(date) {
    var t = date.getTime() - 7 * 24 * 3600 * 1000;
    return Date.lleuGetFirstDateInWeek(new Date(t));
};