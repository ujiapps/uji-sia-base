/**
 * Es una vista de horarios como lista de eventos. Lo utilizamos para generar las vistas imprimibles
 * de los horarios.
 *
 * @param containerid
 * @constructor
 */
HorarioPrint = function(containerid) {
    this._el = $(containerid);
    if (this._el.length == 0) {
        throw "El id del contenedor no es válido: " + containerid;
    }
};

/**
 * Dado un listado de eventos agrupados por fecha, añade los eventos a la lista.
 *
 * @param eventsByDate
 */
HorarioPrint.prototype.addEvents = function(events) {

    // versión imprimible del horario.
    var eventsByDate = {};
    for ( var i = 0; i < events.length; i++) {
        if ( !(events[i].day in eventsByDate) ) {
            eventsByDate[events[i].day] = [];
        }
        eventsByDate[events[i].day].push(events[i]);
    }

    // borramos los hijos.
    this._el.children().remove();

    // empezamos a añadir los eventos.
    for (var d = 0; d < 7; d++) {
        if (!(d in eventsByDate)) {
            continue;
        }
        // ordenamos los eventos por hora.
        var events = eventsByDate[d].sort(function (a, b) {
            return a.from.getUTCHours() - b.from.getUTCHours();
        });
        // pìntamos los eventos

        var lEventos = '<ul class="listaEventos">' +
            '<li class="diaExamen">' + i18n.get_week_day(d) + '</li>';

        var item = '';
        for (var i = 0; i < events.length; i++) {
            item += '<li class="codigoEvento">' + events[i].codigo + '</li>' +
                '<li class="tituloEvento">' + events[i].titulo + '</li>' +
                '<li class="horaEvento">' + events[i].from.lleuGetFormattedTime() + ' - ' + events[i].to.lleuGetFormattedTime() + '</li>';
            if (events[i].observaciones) {
                item += '<li class="observacionesEvento">' + events[i].observaciones + '</li>';
            }
        }

        lEventos += item + '</ul>';

        this._el.append(lEventos);
    }
};



