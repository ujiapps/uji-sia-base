/**
 * Un evento para el widget Calendario
 *
 * @param date
 * @constructor
 */
CalendarEvent = function(fecha, asignatura, tipo,codigoAsignatura, ini, fin, aulas,url,definitivo) {
    this.fecha = fecha;
    this.asignatura = asignatura;
    this.tipo=tipo;
    this.codigoAsignatura = codigoAsignatura;
    this.ini = ini;
    this.fin = fin;
    this.aulas= aulas;
    this.url=url;
    this.definitivo= definitivo;
};


CalendarEvent.TIPO = {
    TEORIA: "TEO",
    LABORATORIO: "LAB",
    PROBLEMAS: "PRO",
    PRACTICAS: "PRA",
    ALT: "ALT",
    ORA: "ORA"
};