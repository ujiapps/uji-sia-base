Calendario = function(container, month, year) {

    this._el = $(container);
    if (this._el.length == 0) {
        return;
    }

    // El año que queremos representar
    this._year = year;

    // El mes que queremos representar.
    this._month = month;

    // El primer día del mes que queremos representar.
    this._monthFirst = null;

    // El último día del mes que queremos representar.
    this._monthLast = null;

    // El primer día que representaremos en el calendario (lunes).
    this._first = null;

    // El último día que representaremos en el calendario (sábado).
    this._last = null;

    // Número de semanas que tendrá el calendario.
    this._weeks = null;

    this.init(month, year);
};
Calendario.prototype.getFirst = function() {
    return this._first;
};
Calendario.prototype.getLast = function() {
    return this._last;
};

/**
 * Elimina los eventos que existan, inicializa de nuevo la interfaz con el nuevo mes
 * @param month
 * @param year
 */
Calendario.prototype.changeDate = function (month, year) {

    this._el.find('table.cal').remove();

    this._month = month;

    this._monthFirst = null;
    this._monthLast = null;
    this._first = null;
    this._last = null;
    this._weeks = null;

    this.init(month, year);
};

/**
 * Inicializa un wbe
 * @param month
 * @param year
 */
Calendario.prototype.init = function (month, year) {

    this._monthFirst = new Date(Date.UTC(year, month, 1, 0, 0, 0));

    // Calculamos el primer día que debe representar el calendario (el lunes anterior al día 1 si éste no es lunes).
    this._first = new Date(this._monthFirst.getTime());
    var weekDay = this._first.getUTCDay();
    if (weekDay != 1) {
        var offset = (weekDay-1) * 24 * 3600 * 1000;
        this._first.setTime(this._first.getTime() - offset);
    }

    /**
     * Calculamos le último día del mes. Para eso sumo 30 días al día inicial (seguro que pasa al siguiente mes) y
     * luego voy restando 1 día hasta que el mes se iguala al inicial
     */

    this._last = new Date(this._monthFirst.getTime() + 30*24*3600*1000);
    while (this._last.getUTCMonth() != this._monthFirst.getUTCMonth()) {
        this._last.setTime(this._last.getTime() - 24*3600*1000);
    }
    this._monthLast = new Date(this._last.getTime());

    // si el último día de mes es domingo, entonces el último día que pintamos en el calendario es el día anterior
    // que es sábado.

    if (this._last.getUTCDay() == 0) {
        this._last.setTime(this._last.getTime() - 24*3600*1000);
    } else if (this._last.getUTCDay() != 6) {
        var offset = (6 - this._last.getUTCDay()) * 24 * 3600 * 1000;
        this._last.setTime(this._last.getTime() + offset)

    }

    // Precalculamos cuántas semanas va a tener el calendario.

    var days = (this._last.getTime() - this._first.getTime()) / ( 24*3600*1000);
    this._weeks = Math.ceil(days / 7);

    this._render();
};

/**
 * Pinta la tabla inicial
 * @private
 */
Calendario.prototype._render = function() {

    var html = '<table class="cal">' +
        '<caption>' + i18n.get_month(this._month) + ' ' + this._year + '</caption>' +
        '<thead>' +
        '<tr>' +
        '<th>' + i18n.get_string('lunes') + '</th>' +
        '<th>' + i18n.get_string('martes') + '</th>' +
        '<th>' + i18n.get_string('miercoles') + '</th>' +
        '<th>' + i18n.get_string('jueves') + '</th>' +
        '<th>' + i18n.get_string('viernes') + '</th>' +
        '<th>' + i18n.get_string('sabado') + '</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>';

    var weekTemplate = '<tr>' +
        '<td data-monthday="{monthdaydata-1}"><div class="cal-monthday{aditionalcss-1}">{monthday-1}</div><div class="cal-content"></div></td>' +
        '<td data-monthday="{monthdaydata-2}"><div class="cal-monthday{aditionalcss-2}">{monthday-2}</div><div class="cal-content"></div></td>' +
        '<td data-monthday="{monthdaydata-3}"><div class="cal-monthday{aditionalcss-3}">{monthday-3}</div><div class="cal-content"></div></td>' +
        '<td data-monthday="{monthdaydata-4}"><div class="cal-monthday{aditionalcss-4}">{monthday-4}</div><div class="cal-content"></div></td>' +
        '<td data-monthday="{monthdaydata-5}"><div class="cal-monthday{aditionalcss-5}">{monthday-5}</div><div class="cal-content"></div></td>' +
        '<td data-monthday="{monthdaydata-6}"><div class="cal-monthday{aditionalcss-6}">{monthday-6}</div><div class="cal-content"></div></td>' +
        '</tr>';

    var d = new Date(this._first.getTime());
    for (var semana = 0; semana < this._weeks; semana++) {
        var weekHtml = weekTemplate;
        for (var day = 1; day <= 6; day++) {

            var regexMonth = new RegExp("{monthday-" + day + "}", "g");
            var regexMonthData = new RegExp("{monthdaydata-" + day + "}", "g");
            var regexCss = new RegExp("{aditionalcss-" + day + "}", "g");

            if (d.getUTCMonth() != this._monthFirst.getUTCMonth()) {
                weekHtml = weekHtml.replace(regexCss, " cal-othermonthday");
            } else {
                weekHtml = weekHtml.replace(regexCss, "");
            }

            var monthday = d.getUTCFullYear().toString() + d.getUTCMonth().toString() + d.getUTCDate().toString() ;
            weekHtml = weekHtml.replace(regexMonthData, monthday);
            weekHtml = weekHtml.replace(regexMonth, d.getUTCDate());
            d.setTime(d.getTime() + 24 * 3600 * 1000);
        }
        // hay que saltarse el domingo
        d.setTime(d.getTime() + 24 * 3600 * 1000);

        html += weekHtml;
    }

    html += '</tbody>' +
            '</table>';

    $(html).appendTo(this._el);
};

/**
 * Devuelve el html del evento de calendario que pasamos por argumento.
 *
 * @param e CalendarEvent
 * @private
 */
Calendario.prototype._getEventoUI = function(e) {
    var eventTemplate = '';
    let localizacionUrl = '';
    if (e.url) {
        eventTemplate = '<div class="cal-event">' +
            '<div class="cal-event-asignatura"><a href="{url}" class="actionlink">{asignatura}</a></div>' +
            '<div class="cal-event-codigoasignatura"><a href="{url-cod}" class="actionlink">{codigo}</a></div>' +
            '<div class="cal-event-tipo">{tipo}</div>' +
            '<div class="cal-event-inifin">{inifin}</div>' +
            '<div class="cal-event-aula"><i class="icon-location" />';
    } else {
        eventTemplate = '<div class="cal-event">' +
            '<div class="cal-event-asignatura">{asignatura}</div>' +
            '<div class="cal-event-codigoasignatura">{codigo}</div>' +
            '<div class="cal-event-tipo">{tipo}</div>' +
            '<div class="cal-event-inifin">{inifin}</div>' +
            '<div class="cal-event-aula"><i class="icon-location" />';
    }
    let aulasSplit = e.aulas.split(', ');
    aulasSplit.forEach((e, i) => {
        localizacionUrl = "https://www.uji.es/u/" + e;
        eventTemplate += '<a href=' + localizacionUrl + ' target="_blank">' + e + '</a>';
        if (i !== aulasSplit.length -1) eventTemplate += ', ';
    });
    eventTemplate += '</div>';

    if(e.definitivo=="N") {
        eventTemplate += '<div class="cal-event-provisional alert-box alert">'+ i18n.get_string('provisional') +'</div>';
    }

    return eventTemplate.replace("{asignatura}", e.asignatura)
        .replace("{url}",e.url)
        .replace("{url-cod}",e.url)
        .replace("{codigo}", e.codigoAsignatura)
        .replace("{tipo}", e.tipo)
        .replace("{inifin}", (e.definitivo=="N")?"":e.ini + " - " + e.fin)
        .replace("{aulas}", e.aulas);
};

/**
 * Añade un evento al calendario.
 * @param e CalendarEvent
 */
Calendario.prototype.addEvent = function(e) {

    // no puedo añadir eventos cuya fecha esté fuera del calendario que se considera.
    if ((e.fecha < this._first) || (e.fecha > this._last)) {
        return;
    }

    this._el.find(
        'td[data-monthday=' + e.fecha.getUTCFullYear().toString() + e.fecha.getUTCMonth().toString() + e.fecha.getUTCDate().toString() +'] .cal-content'
    ).append(this._getEventoUI(e));
};

Calendario.prototype.recortarCalendario= function() {
    var trs = this._el.find('tbody tr');
    for(var i=0; i< trs.length; i++){
        if($(trs[i]).find('.cal-event').length> 0){
            break;
        }
        $(trs[i]).addClass('hidden');
    }
    for(var i=trs.length-1; i>=0 ; i--){
        if($(trs[i]).find('.cal-event').length> 0){
            break;
        }
        $(trs[i]).addClass('hidden');
    }
};
