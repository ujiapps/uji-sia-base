/**
 * Para gestionar el control de selección de curso académico
 *
 * @constructor
 */
AcadSelector = function(id) {
    this._basePath = '/sia/rest/publicacion/';
    this._selectorId = id;
    this._acadSelector = $(this._selectorId);
    if (! this._acadSelector) {
        return;
    }
    this._acadSelector.change(this.onChange.bind(this));
};
/**
 * Dada una url del lleu cambia el año que estamos consultado.
 *
 * // TODO: Asumo que el lleu estará en /sia/rest/publicacion
 *
 * @param url la url que queremos cambiar
 * @param newYear el nuevo año
 * @private
 */

AcadSelector.prototype._changeYearInUrl = function(url, newYear) {
    var newUrl = null;

    var r = /^(([^\/]+\/\/[^\/]+\/sia\/rest\/publicacion\/)([0-9]{4}))(.*)/;
    var aux = r.exec(url);
    if (aux) {
        var currentYear = aux[3];
        if (currentYear) {
            newUrl = url.replace(r, "$2" + newYear);
        }
    }
    return newUrl;
};

/**
 * Manejador del evento change() para el selector de curso académico.
 *
 * @param e
 */
AcadSelector.prototype.onChange = function(e) {
    var newYear = $(this._selectorId + ' option:selected').val();
    var newUrl = this._changeYearInUrl(location.href, newYear);
    if (newUrl) {
        location.assign(newUrl)
    }
};

