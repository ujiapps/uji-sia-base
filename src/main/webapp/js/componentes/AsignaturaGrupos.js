/**
 * Utilizamos esta clase para recoger la selección de grupos en el subsistema de personalización de horarios.
 *
 * @constructor
 */
AsignaturaGrupos = function() {
    this.asignatura = null;
    this.grupo = null;

    this.subgrupos = {};
};

/**
 * Establece el código de la asignatura.
 *
 * @param {string} asignatura el código de la asignatura.
 */
AsignaturaGrupos.prototype.setAsignatura = function(asignatura) {
    this.asignatura = asignatura;
};

/**
 * Establece el grupo que queremos para la asignatura
 *
 * @param {string} grupo grupo que queremos añadir
 */
AsignaturaGrupos.prototype.setGrupo = function(grupo) {
    this.grupo = grupo;
};

/**
 * Añade un subgrupo
 *
 * @param {string} tipo el tipo de subgrupo.
 * @param {int} id el id de subgrupo
 */
AsignaturaGrupos.prototype.setSubgrupo = function (tipo, id) {
    this.subgrupos[tipo] = id;
};
