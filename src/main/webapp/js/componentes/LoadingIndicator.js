/**
 * Singleton. Se encarga de mostrar/ocultar indicadores de progreso de carga / error.
 *
 */
LoadingIndicator = {

    _el: $('#loading-indicator'),

    _workingCount: 0,

    /**
     * Elimina el mensaje actual
     *
     * @param callback
     * @private
     */
    _removeCurrent: function(callback) {
        var me = this;
        if (me._el.children('.alert-box').length == 0) {
            callback && callback();
            return;
        }
        me._el.children('.alert-box').stop(true, true);
        me._el.children('.alert-box').fadeOut('fast', function() {
            me._el.children('.alert-box').remove();
            callback && callback();
        })
    },
    /**
     * Añade un indicador de carga
     *
     * @param msg
     */
    working: function(msg) {
        var me = this;
        this._workingCount++;
        if ( this._workingCount > 1 ){
            return;
        }
        me._removeCurrent(function() {
            me._el.append('<div class="alert-box secondary" style="width: 10em;"><div class="icon-spin6 animate-spin" style="margin-right: 0.5em;"/>' + msg + '</div>');
        });
    },
    /**
     * Añade un indicador de que se produjo un error
     * @param msg
     */
    error: function(msg) {
        var me = this;
        this._workingCount = 0;
        me._removeCurrent(function () {
            me._el.append('<div class="alert-box alert">' + msg + '</div>');
        });
    },
    /**
     * Oculta el indicador mostrado
     */
    hide: function() {
        this._workingCount = 0;
        this._removeCurrent();
    }
};

