/**
 * Aqui implementamos los manjadores de eventos para los Filtros y ordenacion de la página.
 *
 *  @param string container el id del contenedor del filtro
 *  @param boolean activeDefaultValues indica que active por defecto el primer curso de formación básica en el primer semestre
 *  @param boolean singleSelection indica que queremos seleccionar sólo una opción cada vez
 * @constructor
 */
Filtro = function(container, activeDefaultValues, singleSelection) {
    var me = this;
    me._cbs = [];
    me._el = $(container);
    if (me._el.length == 0) {
        console.error("No se indicó contendor para la página");
        return;
    }
    me._el.find("form.filtro-buscar").submit(function(e) {
        e.preventDefault();
        me.onFilterExButtonClick.bind(me)(e);
    });
    me._el.find("input[type=button]").click(me.onFilterExButtonClick.bind(me));
    me._el.find("img.clear-img").click(me.onFilterExClearButtonClick.bind(me));
    me._el.find('.filtro-buscar input[type=text]').focus();

    /**
     * La primera vez que accedermos activamos formación básica, curso 1 y semestre 1 si así nos lo piden.
     * @type {*|Array}
     */
    if (activeDefaultValues) {
        var active = me.getActiveFilterValues();
        let optativasActive = document.querySelector("input[data-value='optativas']").classList.contains('active')
        let practicasActive = document.querySelector("input[data-value='practicas']").classList.contains('active')
        if (active.length === 0) {
            me._el.find("ul.filtro-caracter input[data-value=fbasica]").toggleClass('active', (!optativasActive && !practicasActive));
            me._el.find("ul.filtro-curso input[data-value=1]").toggleClass('active', true);
            me._el.find("ul.filtro-semestre input[data-value=1]").toggleClass('active', true);
        }
    }

    this._singleSelection = singleSelection;

    /**
     * Una pila que almacena los cambios que ha habido en los botones para dejarlos en el estado original tras un
     * error de carga rest
     */
    this._undoStack = [];

    /**
     * Timer que utilizamos para hacer llamadas ajax. Cuando pulsamos el botón de algún filtro, añadimos un timer
     * para ejecutar la llamada AJAX transcurrido medio segundo. Ver constante AJAX_CALL_DELAY_MS. Lo mismo aplicamos
     * cuando se modifica el contenido del campo de búsqueda.
     */
    this._reqTimer = null;

    /**
     * Milisegundos que deben transcurrir entre la pulsación del último botón de filtro y la llamada AJAX
     * correspondiente.
     */
    this._AJAX_CALL_DELAY_MS = 100;

};

/**
 * Registra una función callback a llamar ante un cambio de estado del filtro.
 */
Filtro.prototype.change = function(cb) {
    this._cbs.push(cb);
};

/**
 * Dada una clase CSS para un filtro, devuelve los valores (atributo data-value) de los botones activos.
 *
 * @param filterClass el string de la clase general del filtro: filtro-grupo, filtro-semestre, etc.
 * @returns {Array} valores de los botones activos
 */
Filtro.prototype.getActiveFilterValues = function(filterClass) {
    var ret = [];
    var selector = "." + filterClass + " .button.active";
    this._el.find(selector).each(function (index,el) {
        ret.push($(el).attr('data-value'));
    });
    return ret;
};

/**
 * Aplica los cambios del undostack
 *
 * @private
 */
Filtro.prototype._applyUndoStack = function() {
    var i;
    while (i = this._undoStack.pop()) {
        if (i.active && ! i.element.hasClass('active')) {
            i.element.toggleClass('active', true);
        } else if (!i.active && i.element.hasClass('active')) {
            i.element.toggleClass('active', false);
        }
    }
};

Filtro.prototype._cancelReqTimer = function() {
    if (this._reqTimer != null) {
        clearTimeout(this._reqTimer);
        this._reqTimer = null;
    }
};

Filtro.prototype._scheduleAjaxRequest = function() {
    this._reqTimer = setTimeout(
        this.onAjaxTimerTimeout.bind(this),
        this._AJAX_CALL_DELAY_MS
    );
};

Filtro.prototype._pushFilterState = function(element) {
    var o = {
        element: element
    };
    if (element.attr('type') == 'input') {
        o.active = false;
        o.value = element.val();
    } else {
        o.active = element.hasClass('active');
    }
    this._undoStack.push(o);
};

/**
 * Manejador del evento click de cualquier botón de los filtros.
 *
 * @param e
 */
Filtro.prototype.onFilterExButtonClick = function(e) {

    // aborto timer
    this._cancelReqTimer();

    // aborto cualquier petición rest en curso
    RestProvider.cancelAllRequests();

    // almacenamos el estado del elemento para deshacer el cambio de clase activo en caso de error
    this._pushFilterState($(e.currentTarget));

    if (this._singleSelection) {
        $(e.currentTarget).closest('ul').find('input[type=button].active').removeClass('active');
    }

    $(e.currentTarget).toggleClass('active');

    // programamos la petición ajax
    this._scheduleAjaxRequest();
};

/**
 * Limpia el campo de búsqueda por nombre
 */
Filtro.prototype.onFilterExClearButtonClick = function(e) {
    e.preventDefault();
    this._el.find(".filtro-buscar input[type=text]").val("");
    this._el.find(".filtro-buscar input[type=text]").focus();
    this.onFilterExButtonClick(e);
};

/**
 * Se ejecuta a los _AJAX_CALL_DELAY_MS milisegundos del último botón de filtros pulsado.
 */
Filtro.prototype.onAjaxTimerTimeout = function() {

    var me = this;

    //LoadingIndicator.working(i18n.get_string('cargando'));

    try {
        var filterData = this.getFilterData();

        // Aplicamos la pila de acciones a ejecutar
        for (var i = 0; i < this._cbs.length; i++) {
            if (!this._cbs[i](filterData)) {
                throw "";
            }
        }
    } catch (e) {
        me._applyUndoStack();
        //LoadingIndicator.error(i18n.get_string('cargando.error') + ": " + e);
        throw e;
    }
};

Filtro.prototype.getFilterData = function() {
    var ret = {};

    var filtros = this._el.find('[class*="filtro-"]');
    for (var i = 0; i< filtros.length; i++) {
        var matches = /(^|\s)(filtro-([^\s]+))(\s|$)/.exec($(filtros[i]).attr('class'));

        var filterClass = matches[2];
        var itemName = matches[3];

        if (itemName == 'buscar') {
            ret['nombre'] = this._el.find('.filtro-buscar input[type=text]').val();
        } else {
            ret[itemName] = this.getActiveFilterValues(filterClass);
        }
    }

    return ret;
};

Filtro.prototype.activaCurso = function(curso) {
    this._el.find("ul.filtro-curso input[data-value="+curso+"]").toggleClass('active', true);
};

Filtro.prototype.eliminaCurso = function(curso) {
    this._el.find("ul.filtro-curso input[data-value=" + curso + "]").remove();;
};

Filtro.prototype.activaSemestre= function(curso) {
    this._el.find("ul.filtro-semestre input[data-value=" + curso + "]").toggleClass('active', true);
};

Filtro.prototype.eliminaSemestre = function(curso) {
    this._el.find("ul.filtro-semestre input[data-value=" + curso + "]").remove();;
};

