
AlertBox = function(contenedor) {
    this._container = $(contenedor);
    if (!this._container) {
        return;
    }
    this._el = null;
    this._timeout = null;
};
AlertBox._template = '<div class="alert-box lleualertbox"><p>{{text}}</p></div>';

AlertBox._render = function(data) {
    return AlertBox._template.replace(/\{\{text\}\}/, data.text);
};
AlertBox.prototype.show = function(text, timeout) {
    if (this._el) {
        return;
    }
    var html = AlertBox._render({text: text});
    this._el = $(html);
    this._container.append(this._el);
    this._el.fadeIn(400);

    if (!timeout) {
        timeout = 5000;
    }
//    this._timeout = setTimeout(this.hide.bind(this), timeout);
};
AlertBox.prototype.hide = function() {
    if (this._el) {
        clearTimeout(this._timeout);
        this._timeout = null;
        this._el.remove();
        this._el = null;
    }
};
