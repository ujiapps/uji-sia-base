/**
 * Un pequeño controlador para actualizar directamente las cajas de asignatura
 *
 * @param {jQuery} el el elemento que tiene la caja.
 */
CajaAsignatura = function(el) {
    this._el = el;
    this._info = this._el.find('.caja-asignatura-info');
    this._cantidad = this._info.find('.cantidad');
    this._leyenda = this._info.find('.leyenda');

    this._infoIds = {};
    this._freeInfoId = 1;
};
/**
 * Devuelve una leyenda.
 *
 * @param {String} leyenda
 * @private
 */
CajaAsignatura.prototype._getInfoByLeyenda = function(leyenda) {
    return this._leyenda.find('td').filter(function() {
        return $(this).text() === leyenda;
    });
};
/**
 * Actualiza el dato de una leyenda dada.
 *
 * @param dato
 * @param leyenda
 * @private
 */
CajaAsignatura.prototype._updateInfo = function(dato, leyenda) {
    var id = this._infoIds[leyenda];
    this._cantidad.find('td[data-leyendaid=' + id + ']').text(dato);
    this._leyenda.find('td[data-leyendaid=' + id + ']').text(leyenda);
};
/**
 * Añade información en el apartado de información de la caja de asignatura. El apartado de información se encuentra
 * a la derecha de la caja de asignatura. Si el apartado ya fue añadido a la caja, actualiza los datos asociados a
 * la leyenda.
 *
 * @param dato
 * @param leyenda
 */
CajaAsignatura.prototype.addInfo = function(dato, leyenda) {
    if (leyenda in this._infoIds) {
        this._updateInfo(dato, leyenda);
    } else {
        $('<td data-leyendaid="' + this._freeInfoId + '">' + dato + '</td>').appendTo(this._cantidad);
        $('<td data-leyendaid="' + this._freeInfoId + '">' + leyenda + '</td>').appendTo(this._leyenda);

        this._infoIds[leyenda] = this._freeInfoId++;
    }
};
