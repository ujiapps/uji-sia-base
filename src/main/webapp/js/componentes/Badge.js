/**
 * No tenemos Badges en foundation 3, así es que vamos implementándolo.
 *
 * @param jqEl Objeto jquery contenedor del badge.
 * @param msg
 * @constructor
 */
Badge = function(jqEl, msg) {
    var data = this._generateTemplate(msg);
    this._el = $(data);
    this._el.insertAfter(jqEl);
};

Badge._template = '' +
    '<div class="sia_badge hidden">' +
    '<p>{{texto}}</p>' +
    '</div>';

Badge.prototype._generateTemplate = function(msg) {
    return Badge._template.replace(/\{\{texto\}\}/, msg);
};

Badge.prototype.show = function() {
    if (this._el.hasClass('hidden')) {
        this._el.removeClass('hidden');
    }
};

Badge.prototype.hide = function() {
    if (!this._el.hasClass('hidden')) {
        this._el.addClass('hidden');
    }
};

Badge.prototype.getText = function() {
    return this._el.find('p').text();
};

Badge.prototype.setText = function(msg) {
    this._el.find('p').text(msg);
};
