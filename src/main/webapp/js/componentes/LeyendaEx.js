/**
 * El contenedor es un <ul>. Esta clase debe acabar subtituyendo a Leyenda
 *
 * @param container
 * @constructor
 */
LeyendaEx = function(container) {

    this._el = $(container);
    if (this._el.length == 0) {
        return;
    }

    this._items = {};
    this._itemsDisabled = [];

    // callbacks que llamaremos en un evento click
    this._cbs = [];

    this._paleta = new Paleta();
    this._init();
};

LeyendaEx.prototype.show = function() {
    if (!this._el.hasClass('hidden')) {
        return;
    }
    this._el.removeClass('hidden');
};

/**
 * Se encarga de inicializar los datos de una leyenda. Biene bien cuando ponemos una leyenda fija en la plantilla.
 *
 * @private
 */
LeyendaEx.prototype._init = function() {
    var els = this._el.find('li.item-leyenda');
    for (var i = 0; i < els.length ; i++ ) {
        this._items[$(els[i]).data('codigo')] = true;
    }
    els.find('a').click(this._onclick.bind(this));
};

/**
 * Plantilla para añadir items dinámicamente.
 *
 * @type {string}
 * @private
 */
LeyendaEx._itemTemplate = '<li class="item-leyenda {class}" data-codigo="{codigo}" style="padding-bottom: 0.5em;">' +
                            '<a href="#">' +
                              '<div style="display: table-cell; vertical-align: top;">' +
                                '<div class="color-item"  style="background-color: {color};">' +
                                    '<input type="checkbox" {checked} aria-label="{titulo}"/>' +
                                '</div>' +
                              '</div>' +
                              '<div style="display: table-cell; padding-left: 0.5em;">' +
                                  '<p class="item">{titulo}</p>' +
                              '</div>' +
                            '</a>' +
                          '</li>';

/**
 * Añade una nueva asignatura
 *
 * @param {titulo} titulo un título para el item (será visible)
 * @param {string} codigo un código para el ítem (no es visible)
 * @param {string} cssClass la clase css que añadiremos al item.
 */
LeyendaEx.prototype.addItem = function(titulo, codigo, cssClass, color) {
    if (codigo in this._items) {
        return;
    }
    this._items[codigo] = true;

    var html = LeyendaEx._itemTemplate.replace(/\{titulo\}/g, titulo);
    html = html.replace(/\{codigo\}/g, codigo);
    if (typeof color === "undefined") {
        html = html.replace(/\{color\}/, this.getItemColor(codigo));
    } else {
        html = html.replace(/\{color\}/, color);
    }

    var css = "";
    if (typeof cssClass != "undefined") {
        css = cssClass;
    }
    if (this._itemsDisabled.indexOf(codigo) !== -1) {
        css += " disabled";
        html = html.replace(/\{checked\}/, '');
    }else{
        html = html.replace(/\{checked\}/, 'checked');
    }
    html = html.replace(/{class}/g, css);


    var codigos = Object.keys(this._items);
    if (codigos) {
        codigos.sort(function(a,b) {
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        });
    }

    var codeToSearch = null;
    for (var i = 0; i < codigos.length; i++) {
        if (codigo > codigos[i]) {
            codeToSearch = codigos[i];
        }
    }

    var el;
    if (codeToSearch == null) {
        el = $(html).prependTo(this._el);
    } else {
        el = $(html).insertAfter(this._el.find('[data-codigo="' + codeToSearch + '"]'));
    }

    el.click(this._onclick.bind(this));
};

/**
 * Elimina todos los elementos de la leyenda.
 */

LeyendaEx.prototype.removeAll = function() {
    this._items = {};
    this._el.find('li').remove();
};

/**
 * Devuelve un color de fondo aleatorio para los items.
 */
LeyendaEx.prototype.getItemColor = function(codigo) {
    return this._paleta.getUnusedColorByName(codigo);
};

LeyendaEx.prototype._onclick = function(e) {
    $(e.currentTarget).closest('li.item-leyenda').toggleClass('disabled');
    if (!$(e.target).is('input')) {
        e.preventDefault();
        var checkBox = $(e.currentTarget).find('input[type=checkbox]');
        checkBox.prop('checked', !checkBox.prop('checked'));
    }

    this._itemsDisabled = this.getItemsDesactivados();
    for (var i = 0; i < this._cbs.length; i++) {
        this._cbs[i](this._itemsDisabled);
    }
};

/**
 * Devuelve un array de strings con los códigos de items que están desactivados
 *
 * @returns {Array}
 * @private
 */
LeyendaEx.prototype.getItemsDesactivados = function() {
    var ret = [];
    var els = this._el.find('.item-leyenda.disabled');
    for (var i = 0 ; i < els.length ;i++) {
        ret.push($(els[i]).data('codigo'));
    }
    return ret;
};

/**
 * Registra una función para ser llamada cuando se hace click sobre un item de la leyenda. La función es llamada con
 * un array de los códigos de los items desactivados.
 *
 * @param cb
 */
LeyendaEx.prototype.click = function(cb) {
    this._cbs.push(cb);
};
