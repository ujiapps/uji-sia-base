/**
 * Gestiona una paleta de colores. Lo utilizamos para asignar colores a los eventos del horario.
 *
 * @constructor
 */
Paleta = function() {
    this._used = {};
    this._codeToColor = {};
    this._firstFree = 0;
};

// http://tools.medialab.sciences-po.fr/iwanthue/
Paleta._colors = [
    "#afc287",
    "#bebaf0",
    "#d1eca8",
    "#e8b7e6",
    "#90d1a4",
    "#eca1bd",
    "#62cdd7",
    "#efab91",
    "#69cced",
    "#ede4a9",
    "#8abdf2",
    "#d8ba8d",
    "#a3c6e7",
    "#dfb89b",
    "#96e9df",
    "#e9b0b5",
    "#c0eec5",
    "#c3a8d2",
    "#d5d2ab",
    "#8fc4b0"
];

/**
 * Devuelve un color dado un string. Lo que hacemos es hacer una función de mapeo entre el código y el array de colores
 * para devolver siempre el mismo color dado un código.
 *
 * Además, si el color ya ha sido usado, buscamos el primero libre. Si ya no hay, le asignamos el que le corresponde.
 *
 * Con este algoritmo sabemos que, dependiendo del orden de inserción de los eventos, el color asignado a cualquiera de
 * ellos puede variar.
 *
 * @param {string} code Es un string que utilizaremos para asignar siempre el mismo color. Puede ser, por ejemplo, una
 *        asignatura.
 */
Paleta.prototype.getUnusedColorByName = function(code) {

    if ( code in this._codeToColor) {
        return this._codeToColor[code]
    }
    var index = this._mapCodeToColor(code);
    if ( index in this._used ) {
        if ( this._firstFree != -1 ) {
            index = this._firstFree;
            this._used[index] = true;
            this._firstFree += 1;
        }
    } else {
        this._used[index] = true;
    }

    // Comprobamos si tenemos que actualizar el puntero al primer color libre.
    while ( (this._firstFree in this._used) && this._firstFree < Paleta._colors.length ) {
        this._firstFree += 1;
    }
    if ( this._firstFree == Paleta._colors.length) {
        this._firstFree = -1;
    }

    this._codeToColor[code] = Paleta._colors[index];
    return this._codeToColor[code];
};

/**
 * Dado un string, devuelve un entero con el color que le corresponde dentro del array de la paleta.
 * @param code
 */
Paleta.prototype._mapCodeToColor = function(code) {
    var index = 0;
    for ( var i = 0; i < code.length; i++) {
        index = (index + code.charCodeAt(i)) % code.length;
    }
    return index;
};
